<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">        
        <?php include('part/nav.php') ?>
        <!-- Masthead-->
        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
        <header class="masthead" style="background-image: url('assets/images/Cara Franchise-01-min.png'); height: 100vh;">
            <div class="container pt-3 mw-90">
                <div class="row w-75 mx-auto">
                    <div class="col-6 col-lg-6" style="padding: 10% 9% 0 0;">
                    <a href="<?= base_url(); ?>syarat"><img src="assets/images/Franchise r-01.png" class="d-block w-100" alt="..." loading="lazy"></a>
                    <br>
                    <a href="<?= base_url(); ?>biaya"><img src="assets/images/Franchise r2-01-01.png" class="d-block w-100" alt="..." loading="lazy"></a>
                    <br>
                    <a href="<?= base_url(); ?>bergabung"><img src="assets/images/Franchise r3-01-01-01.png" class="d-block w-100" alt="..." loading="lazy"></a>
                    </div>
                    <div class="col-6 col-lg-6" style="padding: 12% 0 0 4%;">
                        <h2 class="text-uppercase pb-1" style="color: #ffcc00;">Fasilitas dan Syarat Frenchise</h2>
                        <p class="font-rajdhani" style="color: #fff; ">Jadilah Franchise Lup Lup Buble Drink dan miliki bisnis paling menguntungkan dengan sistem auto pilot! Apakah anda tertarik membangun bisnis franchise di indonesia? Jika iya, silahkan melihat persyaratannya.</p>
                    </div>
                </div>
            </div>
        </header>
        <!-- About Section-->
        <section class="page-section portfolio" id="about" style="background-color: #f2ebe4;background-size: cover;">
        <div class="container pt-5 mw-90">
            <div class="row w-75 mx-auto">
                <div class="col-6 col-lg-6" style="border-right: solid 1px #333;">
                    <h2 class="text-uppercase pb-1" style="color: #333; margin-left: 25%;">Syarat Menjadi Frenchise</h2>
                    <img src="assets/images/Syarat Syarat-01.png" class="d-block w-100" alt="..." loading="lazy">
                </div>
                <div class="col-6 col-lg-6" style="padding: 5% 0 0 10%;">
                    <ul style="padding: 8% 0px 0px 10%;font-size: 20px;font-family: rajdhani, sans-serif !important;font-weight: 600;">
                      <li><i class="icofont-check-circled"></i>Perencanaan lokasi usaha dilakukan oleh tim survey dari frenchisor.</li>
                      <li><i class="icofont-check-circled"></i>Menyukai jenis produk Drink Bubble.</li>
                      <li><i class="icofont-check-circled"></i>Bersedia bekerja keras dan berperan aktif dalam mengoperasikan bisnis Lup Lup Bubble Drink.</li>
                      <li><i class="icofont-check-circled"></i>Berkomitmen untuk mematuhi standarisasi bisnis Lup Lup Bubble Drink.</li>
                      <li><i class="icofont-check-circled"></i>Berkomitmen untuk mengembangkan brand/merk Lup Lup Bubble Drink.</li>
                      <li><i class="icofont-check-circled"></i>Pernah mengunjungi gerai atau menjadi pelanggan Lup Lup Bubble Drink.</li>
                    </ul>
                </div>
            </div>
            <hr style="height:2px;border-width:0;color:gray;background-color:gray">
            <div class="row w-75 mx-auto">
                <div class="col-6 col-lg-6" style="border-right: solid 1px #333;">
                    <h2 class="text-uppercase pb-1" style="color: #333; margin-left: 25%;">Fasilitas yang didapat</h2>
                    <img src="assets/images/Outlate-01.png" class="d-block w-100" alt="..." loading="lazy">
                </div>
                <div class="col-6 col-lg-6" style="padding: 5% 0 0 10%;">
                    <ul style="padding: 8% 0px 0px 10%;font-size: 20px;font-family: rajdhani, sans-serif !important;font-weight: 600;">
                      <li><i class="icofont-check-circled"></i>Pencarian lokasi usaha yang dilakukan oleh tim survey dari frenchisor.</li>
                      <li><i class="icofont-check-circled"></i>Rekrutmen dan pelatihan karyawan.</li>
                      <li><i class="icofont-check-circled"></i>Satu unit outlet.</li>
                      <li><i class="icofont-check-circled"></i>Paket perlengkapan outlet.</li>
                      <li><i class="icofont-check-circled"></i>Buku SOP.</li>
                      <li><i class="icofont-check-circled"></i>Software keuangan.</li>
                      <li><i class="icofont-check-circled"></i>Masa kerja 5 tahun.</li>
                      <li><i class="icofont-check-circled"></i>Sistem oprasional yang sudah terbukti.</li>
                    </ul>
                </div>
            </div>
            <a href="<?= base_url(); ?>franchise"><span class="btn btn-primary btn-xl btn btn-primary btn-xl mt-2">Kembali</span></a>
        </div>
    </section>
        <?php include('part/footer.php') ?>
    </body>
</html>
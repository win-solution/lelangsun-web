<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        
        <section class="page-section">

          <div class="container mw-75">

            <div class="row">
              
              <div class="col-12 col-lg-3 my-auto">
                <h3>PT. BALAI LELANG SUN</h3>
                <ul class="list-unstyled">
                  <li>
                    <span><b>Auction Office </b></span>
                    : Jl. HOS Cokroaminoto No.59 Ruko <br>Ciledug Mas D3 Ciledug Tanggerang
                  </li>
                </ul>
              </div>

              <div class="col-12 col-lg-2 px-0 my-auto">
                <ul class="ml-4 mb-0 fa-ul">
                  <li><span class="fa-li"><i class="fas fa-phone-alt"></i></span>Phone : (021) 22271959</li>
                  <li><span class="fa-li"><i class="fab fa-youtube"></i></span>Youtube : BalaiLelangSun</li>
                  <li><span class="fa-li"><i class="fab fa-facebook-square"></i></span>Facebook : BalaiLelangSun</li>
                  <li><span class="fa-li"><i class="fab fa-instagram-square"></i></span>Instagram : BalaiLelangSun</li>
                </ul>
              </div>

              <div class="col-12 col-lg-5">
                <div class="col-lg-12 mw-90 mx-auto border">
                    
                  <div class="row">
                    
                    <div class="col-lg-12">
                          
                      <h4>Pembayaran</h4>

                    </div>

                    <div class="col-lg-12">
                        
                      <h4>1. Debit BCA</h4>

                    </div>

                    <div class="col-lg-12">
                          
                      <h4>2. Transfer : No. Rek 5025 06 2288 <br> a.n PT. Balai Lelang SUN</h4>

                    </div>

                  </div>

                </div>
              </div>

              <div class="col-12 col-lg-2 text-center">
                <img src="assets/images/logo.png" alt="...">
              </div>

            </div>

            <hr>

            <div class="row">
              
              <div class="col my-auto">
                <h3 class="text-uppercase">Bukti Serah terima npl</h3>
              </div>

              <div class="col">

                <div class="row">

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Nomor Urut </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span>20 <hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Tanggal & Jam </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span>25/03/2021    18:50:55<hr class="m-0 mb-1"></span>
                  </div>

                </div>

              </div>

            </div>

            <div class="border">
              <div>
                <h5 class="m-1">Hasil Lelang</h5>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 mw-75">
                <div class="row">

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Nama Pekerja </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Nama Pekerja<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>NIK </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> NIK<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>NPWP </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> NPWP<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Alamat </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Alamat<hr class="m-0 mb-1"></span>
                    <span> Lengkap<hr class="m-0 mb-1"></span>
                    <span> Peserta<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Telpon / HP </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Telepon / HP<hr class="m-0 mb-1"></span>
                  </div>

                </div>                
              </div>
            </div>

            <div class="row">
              
              <div class="col-lg-6">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                      
                      <div class="border">
                        <div>
                          <span class="m-1">NPL Diterima</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Bank </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Bank <hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>No. Rekening Asal </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>No. Rekening Asal<hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Atas Nama </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Atas Nama<hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-12 text-right">
                            <span>Paraf/TTD </span>
                          </div>

                        </div>

                      </div>

                  </div>

                  <div class="col">

                    <div class="row mw-90 mx-auto">

                      <div class="col-lg-6 pr-0">
                        <div class="row">
                          <div class="col-lg-9">
                            <span>Diterima Tanggal & Jam </span>
                          </div>
                          <div class="col-lg-3">
                            <span>:</span>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-6 pl-0">
                        <span>25/03/2021    18:50:55<hr class="m-0 mb-1"></span>
                      </div>

                    </div>

                  </div>

                </div>

                <div class="col-lg-12">

                  <div class="row">

                    <div class="col-lg-6 pr-0">
                        
                        <div class="border">
                          <div class="text-center p-3">
                            <span class="m-1">Peserta Lelang</span>
                          </div>
                        </div>

                        <div class="border px-3">
                          
                          <div class="row">

                            <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                              <span>Peserta Lelang</span>
                            </div>

                          </div>

                        </div>

                        <div class="border">
                          <div class=" text-center">
                            <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                          </div>
                        </div>

                        <span class="small">*Mohon ditulis dengan nama jelas</span>

                    </div>

                    <div class="col-lg-6 pl-0">
                        
                        <div class="border">
                          <div class="text-center p-3">
                            <span class="m-1">Auction Office</span>
                          </div>
                        </div>

                        <div class="border px-3">
                          
                          <div class="row">

                            <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                              <span>Auction Office</span>
                            </div>

                          </div>

                        </div>

                        <div class="border">
                          <div class=" text-center">
                            <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                          </div>
                        </div>

                    </div>

                  </div>

                </div>

              </div>

              <div class="col-lg-6">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                      
                      <div class="border">
                        <div>
                          <span class="m-1">NPL Dikembalikan</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Bank </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Bank <hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>No. Rekening Asal </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>No. Rekening Asal<hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Atas Nama </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Atas Nama<hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-12 text-right">
                            <span>Paraf/TTD </span>
                          </div>

                        </div>

                      </div>

                  </div>

                  <div class="col">

                    <div class="row mw-90 mx-auto">

                      <div class="col-lg-6 pr-0">
                        <div class="row">
                          <div class="col-lg-9">
                            <span>Diterima Tanggal & Jam </span>
                          </div>
                          <div class="col-lg-3">
                            <span>:</span>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-6 pl-0">
                        <span>25/03/2021    18:50:55<hr class="m-0 mb-1"></span>
                      </div>

                    </div>

                  </div>

                </div>

                <div class="col-lg-12">

                  <div class="row">

                    <div class="col-lg-6 pr-0">
                        
                        <div class="border">
                          <div class="text-center p-3">
                            <span class="m-1">Peserta Lelang</span>
                          </div>
                        </div>

                        <div class="border px-3">
                          
                          <div class="row">

                            <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                              <span>Peserta Lelang</span>
                            </div>

                          </div>

                        </div>

                        <div class="border">
                          <div class=" text-center">
                            <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                          </div>
                        </div>

                        <span class="small">*Mohon ditulis dengan nama jelas</span>

                    </div>

                    <div class="col-lg-6 pl-0">
                        
                        <div class="border">
                          <div class="text-center p-3">
                            <span class="m-1">Auction Office</span>
                          </div>
                        </div>

                        <div class="border px-3">
                          
                          <div class="row">

                            <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                              <span>Auction Office</span>
                            </div>

                          </div>

                        </div>

                        <div class="border">
                          <div class=" text-center">
                            <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                          </div>
                        </div>

                    </div>

                  </div>

                </div>

              </div>

        </section>
        
    </body>
</html>
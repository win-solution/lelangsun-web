<!DOCTYPE html>
<html lang="en">

    <?php include('part/head.php') ?>
    
    <body id="page-top">
        <?php include('part/nav.php') ?>

        <!-- Masthead-->
        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
        <header class="masthead" style="background-image: url('assets1/images/Franchise 1-01-min.png'); height: 100vh; margin-top: 50px;">
            <div class="container mw-90">
                <div class="row mw-100">
                    <div class="col-lg-6 offset-lg-6" style="padding-top: 150px;">
                        <div class="page-heading">
                            <h1>Tentang Kami</h1>                            
                            <span class="subheading text-white">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ex, provident laboriosam id porro repellendus quo accusamus consequatur odit distinctio eos, perferendis assumenda quas quisquam fugit cumque sed quam nostrum tenetur.</span>                            
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- ======= Lup Lup ======= -->
            <section id="apaitu" style="background-image: url('asset1/Tentang Kami 7.png'); position: relative; padding: 0 0 0 0; background-size: cover">
                <div class="container pt-5 mw-90">
                    <div class="row w-75 mx-auto">
                        <div class="col-6 col-lg-6">
                            <h2 class="text-uppercase pb-1" style="color: #ffcc00;">Kenapa harus Lup Lup bubble drink.??</h2>
                            <p class="font-rajdhani"><strong>Lup Lup Bubble Drink</strong> merupakan franchise minuman bubble dibawah naungan <strong>PT Dwijaya Global Food</strong> yang berdiri sejak 2011. Lup Lup Bubble Drink menawarkan peluang usaha minuman bubble melalui sistem franchise waralaba atau kemitraan.</p>
                            <p>Hingga saat ini, sudah lebih dari <strong>1.200 Outlet telah dibuka</strong> yang tersebar diseluruh Indonesia. Lup Lup Bubble Drink menjadi franchise minuman dengan varian rasa paling banyak diantara yang lain.</p>
                            <p>Setiap tahunnya Lup Lup Bubble Drink selalu membuat varian rasa terbaru kekinian. Kini Lup Lup Bubble Drink memiliki lebih dari <strong>52 Varian Rasa Minuman</strong></p>
                            <p>Selain itu Lup Lup Bubble Drink memudahkan mitra dengan menyediakan <strong>Paket Mini Set</strong> untuk setiap varian rasa dimana terdiri dari Powder Sachet, Gula Sachet, Dan Gelas Lup Lup Bubble Drink.</p>
                        </div>
                        <div class="col-6 col-lg-6">
                        <img src="asset/Foto Menu 6-01.png" class="d-block w-100" alt="..." loading="lazy">
                        </div>
                    </div>
                </div>
            </section>

        <?php include('part/footer.php') ?>
    </body>
</html>

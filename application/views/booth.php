<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">        
        <?php include('part/nav.php') ?>
        <!-- Masthead-->
        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
        <header class="masthead" style="background-image: url('assets/images/Tentang Kami 8 ( )-01.png'); height: 100vh; margin-top: 50px;">
            <div class="container mw-90">
                <div class="row mw-100">
                    <div class="col-lg-6 offset-lg-6" style="padding-top: 150px;">
                        <div class="page-heading">
                            <h1 class="text-uppercase" style="color: #ffcc00;">Booth</h1>                            
                            <span class="subheading text-white alert-heading fontrajdhani" style="color: #ffcc00;">Hingga saat ini, sudah lebih dari <b>1.200 outlate telah dibuka</b> yang tersebar di seluruh Indonesia. Lup Lup Buddble Dring menjadi franchise minuman dengan varian rasa paling banyak diantara yang lain.</span>                            
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- About Section-->
        <section class="page-section portfolio" id="about" style="background-image: url('assets/images/Kontak 1-01-min.png'); background-size: cover;">
            <div class="container pt-5 mw-90">
                <div class="row w-100 mx-auto">
                    <div class="col-12">
                        <h1 class="text-center pb-1 text-uppercase pb-1">Tipe Booth</h1>
                        <br>
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Outlate-01.png" alt="" srcset="" width="200px" loading="lazy">
                                <h2>Booth Ruby</h2>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Outlate-01.png" alt="" srcset="" width="200px" loading="lazy">
                                <h2>Booth Custome</h2>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Outlate-01.png" alt="" srcset="" width="200px" loading="lazy">
                                <h2>Booth Diamond</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center mt-5">       
                        <h1 class="text-uppercase">Lokasi Booth Kami</h1>                 
                        <img src="assets/images/Peta-01.png" alt="" srcset="" class="w-75" loading="lazy">
                    </div>
                </div>
            </div>
        </section>
        <!-- <section class="page-section bg-primary text-white mb-0" id="portfolio">
            <div class="container">
                <h2 class="page-section-heading text-center text-uppercase text-white">About</h2>
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <div class="row">
                    <div class="col-lg-4 ml-auto"><p class="lead">Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional SASS stylesheets for easy customization.</p></div>
                    <div class="col-lg-4 mr-auto"><p class="lead">You can create your own custom avatar for the masthead, change the icon in the dividers, and add your email address to the contact form to make it fully functional!</p></div>
                </div>
                <div class="text-center mt-4">
                    <a class="btn btn-xl btn-outline-light" href="https://startbootstrap.com/theme/freelancer/">
                        <i class="fas fa-download mr-2"></i>
                        Free Download!
                    </a>
                </div>
            </div>
        </section> -->
        <!-- <section class="page-section" id="contact">
            <div class="container">
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Me</h2>
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <form id="contactForm" name="sentMessage" novalidate="novalidate">
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Name</label>
                                    <input class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Email Address</label>
                                    <input class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Phone Number</label>
                                    <input class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Message</label>
                                    <textarea class="form-control" id="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <br />
                            <div id="success"></div>
                            <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Send</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section> -->
        <?php include('part/footer.php') ?>
    </body>
</html>
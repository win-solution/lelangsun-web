<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>

    <script type="text/javascript">
        
        document.addEventListener("DOMContentLoaded", function(){
          document.querySelectorAll('.sidebar .nav-link').forEach(function(element){
            
            element.addEventListener('click', function (e) {

              let nextEl = element.nextElementSibling;
              let parentEl  = element.parentElement;    

                if(nextEl) {
                    e.preventDefault(); 
                    let mycollapse = new bootstrap.Collapse(nextEl);
                    
                    if(nextEl.classList.contains('show')){
                      mycollapse.hide();
                    } else {
                        mycollapse.show();
                        // find other submenus with class=show
                        var opened_submenu = parentEl.parentElement.querySelector('.submenu.show');
                        // if it exists, then close all of them
                        if(opened_submenu){
                          new bootstrap.Collapse(opened_submenu);
                        }
                    }
                }
            }); // addEventListener
          }) // forEach
        }); 
        // DOMContentLoaded  end

    </script>

    <style type="text/css">

        input[type=text] {
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;
            background-image: url('assets/images/magnifying-glass.png');
            background-position: 10px 10px; 
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }
        
        /* If the screen size is 601px wide or more, set the font-size of <div> to 80px */
        @media screen and (min-width: 601px) {
          .lot {
            background-image: url('assets/images/y/web-04.png');
            height: 112vh;
            background-position: left;
            background-size: cover;
            background-repeat: no-repeat;
          }
        }
        /* If the screen size is 600px wide or less, set the font-size of <div> to 30px */
        @media screen and (max-width: 600px) {
          .lot {
            background-image: url('assets/images/y/mobile-04.png');
            height: 30vh;
            background-position: left;
            background-size: cover;
            background-repeat: no-repeat;
          }
        }

        .img-cover {
            height: 25vh;
            object-fit: cover;
        }

        @media screen and (max-width: 600px) {
            .img-cover {
                height: 30vh;
            }
        }

        
    </style>
    
    <body id="page-top" style="background-image: url('assets/images/dark2/dark2-04.png'); background-position: bottom;">
      <?php include('part/nav.php') ?>

      <header class="masthead" style="background-image: url('assets/images/y/web-05.png'); background-position: bottom;">
          <div class="container mw-100 mh-100 lot">
              <div class="row justify-content-center h-100 mw-90 mx-auto hidden-mobile">
                  <div class="col-12 col-lg-6" style="margin: 23vh 0 0 0;">
                      <div>
                            <h1 class="text-uppercase text-uppercase fontbold" style="font-size: 5rem; color: #ff0000">History</h1>
                            <span class="subheading fontlight" style="font-size: 2rem;">Ayo ikuti event lelang secara langsung</span>
                        </div>
                  </div>
                    <div class="col-12 col-lg-6 d-flex justify-content-center text-center hidden-mobile" style="margin: 12vh 0 0 0;">
                        <!-- <div>
                            <img src="assets/images/web-05.png" class="card-img-top" alt="...">
                        </div> -->
                    </div>
              </div>
          </div>
      </header>


        <section class="page-section portfolio" id="about" style="">
            <div class="container mh-100 mw-90">
              <div class="row">
                <div class="col-12 offset-sm-2 col-md-3 mx-auto pb-4">
                    <div class="card-body" style="background-color: #31869b;">
                        <h4 class="text-uppercase text-white p-5 text-center fontbold" style="color: #333;">Filter</h4>
                          <div class="pb-4">
                            <div class="nav flex-column nav-pills fontlight" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active mb-3 dropdown-toggle" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Merek</a>
                                <li class="nav-item dropdown mb-3 active">
                                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tahun</a>
                                  <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                  </div>
                                </li>
                                <li class="nav-item dropdown mb-3 active">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Kata</a>
                                    <div class="dropdown-menu">
                                      <a class="dropdown-item" href="#">Action</a>
                                      <a class="dropdown-item" href="#">Another action</a>
                                      <a class="dropdown-item" href="#">Something else here</a>
                                      <div class="dropdown-divider"></div>
                                      <a class="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown mb-3 active">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Harga</a>
                                    <div class="dropdown-menu">
                                      <a class="dropdown-item" href="#">Action</a>
                                      <a class="dropdown-item" href="#">Another action</a>
                                      <a class="dropdown-item" href="#">Something else here</a>
                                      <div class="dropdown-divider"></div>
                                      <a class="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </li>
                            </div>
                          </div>
                    </div>
                </div>
                <div class="col-12 col-md-9">
                    <form class="form-inline mt-2 mb-4">
                      <input class="form-control form-control-lg col mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                      <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
                    </form>

                    <div class="row mb-2">

                      <div class="col-lg-4 col-md-4 col-12 mb-4 px-3">
                        <a href="<?= base_url(); ?>/detaillot" style="color: inherit; text-decoration: none;">
                         <div class="card h-100 text-left">

                           <div class="img">
                               <img src="assets/images/revisi/suzuki-ertiga-i_4121.jpg" class="card-img-top img-cover" alt="...">
                           </div>

                           <div class="card-body">
                            <div class="promo-box"></div>
                            <div class="grade-box">
                                <p class="mb-0 mt-1">Grade</p>
                                <p class="text-grade mb-0">D</p>
                            </div>
                            <p class="card-text">6 mei 2021</p>
                             <h5 class="card-tittle mb-0">Vios 1.5 G M/T</h5>
                             <span><p class="card-text">A 1076 TN</p></span>
                             <span><p class="card-text mt-3">Terjual :</p></span>
                             <h1 class="card-tittle mb-3">Rp 36.000.000</h1>
                             <h5><i class="fas fa-tachometer-alt mr-3" style="color: #ff4500"></i>NPL 23</h5>
                             <h5><i class="fas fa-calendar mr-3" style="color: #ff4500"></i>22 November 2021 / 13.00 WIB</h5>
                           </div>
                         </div>   
                        </a>
                      </div>
                      
                      <div class="col-lg-4 col-md-4 col-12 mb-4 px-3">
                        <a href="<?= base_url(); ?>/detaillot" style="color: inherit; text-decoration: none;">
                         <div class="card h-100 text-left">

                           <img src="assets/images/revisi/download.jfif" class="card-img-top img-cover" alt="...">

                           <div class="card-body">
                            <div class="promo-box"></div>
                            <div class="grade-box">
                                <p class="mb-0 mt-1">Grade</p>
                                <p class="text-grade mb-0">D</p>
                            </div>
                            <p class="card-text">6 mei 2021</p>
                            <h5 class="card-tittle mb-0">APV 1.5 DLX M/T 2012</h5>
                            <span><p class="card-text">B 1613 BKN</p></span>
                            <span><p class="card-text mt-3">Terjual :</p></span>
                            <h1 class="card-tittle mb-3">Rp 60.000.000</h1>
                            <h5><i class="fas fa-tachometer-alt mr-3" style="color: #ff4500"></i>NPL 23</h5>
                             <h5><i class="fas fa-calendar mr-3" style="color: #ff4500"></i>22 November 2021 / 13.00 WIB</h5>
                           </div>
                         </div>   
                        </a>
                      </div>
                      
                      <div class="col-lg-4 col-md-4 col-12 mb-4 px-3">
                        <a href="<?= base_url(); ?>/detaillot" style="color: inherit; text-decoration: none;">
                         <div class="card h-100 text-left">

                           <img src="assets/images/revisi/etios-valco_1224.jpg" class="card-img-top img-cover" alt="...">

                           <div class="card-body">
                            <div class="promo-box"></div>
                            <div class="grade-box">
                                <p class="mb-0 mt-1">Grade</p>
                                <p class="text-grade mb-0">D</p>
                            </div>
                            <p class="card-text">6 mei 2021</p>
                             <h5 class="card-tittle mb-0">Ertiga 1.4 GX M/T 2012</h5>
                             <span><p class="card-text">DT 48 IB</p></span>
                             <span><p class="card-text mt-3">Terjual :</p></span>
                             <h1 class="card-tittle mb-3">Rp 72.000.000</h1>
                             <h5><i class="fas fa-tachometer-alt mr-3" style="color: #ff4500"></i>NPL 23</h5>
                             <h5><i class="fas fa-calendar mr-3" style="color: #ff4500"></i>22 November 2021 / 13.00 WIB</h5>
                           </div>
                         </div>   
                        </a>
                      </div>
                      
                      <div class="col-lg-4 col-md-4 col-12 mb-4 px-3">
                        <a href="<?= base_url(); ?>/detaillot" style="color: inherit; text-decoration: none;">
                         <div class="card h-100 text-left">

                           <img src="assets/images/revisi/nissan_PNG69.png" class="card-img-top img-cover" alt="...">

                           <div class="card-body">
                            <div class="promo-box"></div>
                            <div class="grade-box">
                                <p class="mb-0 mt-1">Grade</p>
                                <p class="text-grade mb-0">D</p>
                            </div>
                            <p class="card-text">6 mei 2021</p>
                             <h5 class="card-tittle mb-0">JUKE 2011</h5>
                             <span><p class="card-text">B 1449 KVL</p></span>
                             <span><p class="card-text mt-3">Terjual :</p></span>
                             <h1 class="card-tittle mb-3">Rp 93.000.000</h1>
                             <h5><i class="fas fa-tachometer-alt mr-3" style="color: #ff4500"></i>NPL 23</h5>
                             <h5><i class="fas fa-calendar mr-3" style="color: #ff4500"></i>22 November 2021 / 13.00 WIB</h5>
                           </div>
                         </div>   
                        </a>
                      </div>
                      
                      <div class="col-lg-4 col-md-4 col-12 mb-4 px-3">
                        <a href="<?= base_url(); ?>/detaillot" style="color: inherit; text-decoration: none;">
                         <div class="card h-100 text-left">

                           <img src="assets/images/revisi/pmcciosmrqlv9gmlrgqw.jpg" class="card-img-top img-cover" alt="...">

                           <div class="card-body">
                            <div class="promo-box"></div>
                            <div class="grade-box">
                                <p class="mb-0 mt-1">Grade</p>
                                <p class="text-grade mb-0">D</p>
                            </div>
                            <p class="card-text">6 mei 2021</p>
                             <h5 class="card-tittle mb-0">Vios 1.5 G M/T</h5>
                             <span><p class="card-text">A 1076 TN</p></span>
                             <span><p class="card-text mt-3">Terjual :</p></span>
                             <h1 class="card-tittle mb-3">Rp 36.000.000</h1>
                             <h5><i class="fas fa-tachometer-alt mr-3" style="color: #ff4500"></i>NPL 23</h5>
                             <h5><i class="fas fa-calendar mr-3" style="color: #ff4500"></i>22 November 2021 / 13.00 WIB</h5>
                           </div>
                         </div>   
                        </a>
                      </div>
                      
                      <div class="col-lg-4 col-md-4 col-12 mb-4 px-3">
                        <a href="<?= base_url(); ?>/detaillot" style="color: inherit; text-decoration: none;">
                         <div class="card h-100text-left">

                           <img src="assets/images/revisi/suzuki-ertiga-i_4121.jpg" class="card-img-top img-cover" alt="...">

                           <div class="card-body">
                            <div class="promo-box"></div>
                            <div class="grade-box">
                                <p class="mb-0 mt-1">Grade</p>
                                <p class="text-grade mb-0">D</p>
                            </div>  
                            <p class="card-text">6 mei 2021</p>
                             <h5 class="card-tittle mb-0">APV 1.5 DLX M/T 2012</h5>
                             <span><p class="card-text">B 1613 BKN</p></span>
                             <span><p class="card-text mt-3">Terjual :</p></span>
                             <h1 class="card-tittle mb-3">Rp 60.000.000</h1>
                             <h5><i class="fas fa-tachometer-alt mr-3" style="color: #ff4500"></i>NPL 23</h5>
                             <h5><i class="fas fa-calendar mr-3" style="color: #ff4500"></i>22 November 2021 / 13.00 WIB</h5>
                           </div>
                         </div>   
                        </a>
                      </div>
                    </div>
                </div>
              </div>
            </div>
        </section>

        <?php include('part/footer.php') ?>

    </body>
</html>
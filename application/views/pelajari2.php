<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        <?php include('part/nav.php') ?>
        <!-- Masthead-->
        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
        <header class="masthead" style="background-image: url('assets/images/Tentang Kami 8 ( )-01.png'); height: 110vh;">
            <div class="container mw-75">
                <div class="row" style="padding-top: 20vh;">
                    <div class="col-12 col-md-6" >
                        <img src="assets/images/Foto Menu 3-01-min.png" class="d-block w-75 m-auto" alt="..." loading="lazy">
                    </div>
                    <div class="col-12 col-lg-4 offset-md-0 offset-sm-2 my-auto" >
                        <h1 class="text-uppercase " style="color: #ffcc00;">keunggulan lup lup bubble drink</h1>
                    </div>
                </div>
            </div>
        </header>
        <!-- ======= Lup Lup ======= -->
    <section style="background-image: url('assets/images/Tentang Kami 7 ( )-01.png'); position: relative; padding: 0 0 0 0; background-size: cover">
        <div class="container pt-5">
                <div class="row w-100 mx-auto">
                    <div class="col-12">
                        <h4 class="text-center pb-1 text-uppercase pb-1 ">Kenapa Harus Lup Lup <br> Bubble Drink</h4>
                        <br>
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Keunggulan 1.png" alt="" srcset="" width="150px" loading="lazy">
                                <p >Brand yang sudah terpercaya sejak 2011</p>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Keunggulan 2.png" alt="" srcset="" width="150px" loading="lazy">
                                <p >Investasi yang terjangkau</p>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Keunggulan 3.png" alt="" srcset="" width="134px" loading="lazy">
                                <p >100% keuntungan milik mitra</p>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Keunggulan 4.png" alt="" srcset="" width="150px" loading="lazy">
                                <p >Memproduksi bahan bakunya sendiri yang bersertifikat halal MUI</p>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Keunggulan 5.png" alt="" srcset="" width="160px" loading="lazy">
                                <p >Balik modal relatif cepat 3 - 4 bulan</p>
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Keunggulan 6-01.png" alt="" srcset="" width="160px" loading="lazy">
                                <p >Produksi higienis</p>
                            </div>
                            <div class="col-lg-4 text-center">
                            </div>
                            <div class="col-lg-4 text-center">
                                <img src="assets/images/Keunggulan 7-01.png" alt="" srcset="" width="160px" loading="lazy">
                                <p >Menejemen stock</p>
                            </div>
                            <div class="col-lg-4 text-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
        <?php include('part/footer.php') ?>
    </body>
</html>
<?php // var_dump(@$barang_lelang_detail) 
?>

<!-- Bootstrap Switch -->
<script src="<?= base_url() . 'assets/'; ?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_barang_lelang" id="id_barang_lelang" value="<?= @$barang_lelang_detail[0]->id_barang_lelang ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label>Kategori Barang Lelang <span style="color:red;">*</span></label>
          <select name="id_kategori" id="id_kategori" class="form-control select2" data-placeholder="Pilih Kategori Lelang" style="width: 100%;" required>
            <?php foreach ($kategori_detail as $key => $val) {
              $selected = "";
              if (strtolower($proses) == "ubah") {
                $selected = (($val->id_kategori == @$barang_lelang_detail[0]->id_kategori)) ? "selected" : "";
              }
              echo '<option value="' . $val->id_kategori . '" ' . $selected . '>' . strtoupper($val->kategori) . '</option>';
            } ?>
          </select>
        </div>

        <div class="form-group">
          <label for="nama_barang_lelang">Nama Barang <span style="color:red;">*</span></label>
          <input class="form-control text-uppercase" id="nama_barang_lelang" placeholder="Nama Barang" type="text" name="nama_barang_lelang" value="<?= @$barang_lelang_detail[0]->nama_barang_lelang ?>" required>
        </div>

        <div class="form-group">
          <label for="brand">Brand <span style="color:red;">*</span></label>
          <input class="form-control text-uppercase" id="brand" placeholder="Brand" type="text" name="brand" value="<?= @$barang_lelang_detail[0]->brand ?>" required>
        </div>

        <div class="form-group">
          <label for="warna">Warna <span style="color:red;">*</span></label>
          <input class="form-control text-uppercase" id="warna" placeholder="Warna" type="text" name="warna" value="<?= @$barang_lelang_detail[0]->warna ?>" required>
        </div>

        <div class="form-group">
          <label for="warna">Lokasi Barang <span style="color:red;">*</span></label>
          <input class="form-control text-uppercase" id="lokasi_barang" placeholder="Lokasi Barang" type="text" name="lokasi_barang" value="<?= @$barang_lelang_detail[0]->lokasi_barang ?>" required>
        </div>

        <?php $arrKategori = array(1, 2); // var_dump(@$barang_lelang_detail[0]->id_kategori,in_array(@$barang_lelang_detail[0]->id_kategori,$arrKategori)) 
        ?>
        <div id="groupInpKendaraan" class="<?= (@$barang_lelang_detail[0]->id_kategori) ? ((in_array(@$barang_lelang_detail[0]->id_kategori, $arrKategori)) ? "" : "d-none") : "" ?>">
          <hr style="border: 2px solid black;">
          <legend>VEHICLE SPECIFICATIONS:</legend>
          <div class="row">
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label for="nama_event">Nomer Rangka </label>
                <input class="form-control text-uppercase" id="no_rangka" placeholder="Nomer Rangka" type="text" name="no_rangka" value="<?= @$barang_lelang_detail[0]->no_rangka ?>">
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label for="nama_event">Nomer Mesin </label>
                <input class="form-control text-uppercase" id="no_mesin" placeholder="Nomer Mesin" type="text" name="no_mesin" value="<?= @$barang_lelang_detail[0]->no_mesin ?>">
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="tipe_model">Tipe Mobil </label>
            <input class="form-control text-uppercase" id="tipe_model" placeholder="Tipe Mobil" type="text" name="tipe_model" value="<?= @$barang_lelang_detail[0]->tipe_model ?>">
          </div>

          <div class="row">
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label for="nama_event">Transmisi Mobil </label>
                <select name="transmisi" id="transmisi" class="form-control select2 text-uppercase" data-placeholder="Pilih Transmisi Kendaraan" style="width: 100%;" required>
                  <option value="AT" <?= (@$barang_lelang_detail[0]->transmisi == "AT") ? "selected" : ""; ?>>Automatic Transmission</option>
                  <option value="MT" <?= (@$barang_lelang_detail[0]->transmisi == "MT") ? "selected" : ""; ?>>Manual Transmission</option>
                </select>
              </div>
            </div>
            <div class="col-md-6 col-12">
              <div class="form-group">
                <label for="bahan_bakar">Bahan Bakar </label>
                <select name="bahan_bakar" id="bahan_bakar" class="form-control select2 text-uppercase" data-placeholder="Pilih Transmisi Kendaraan" style="width: 100%;" required>
                  <option value="bensin" <?= (@$barang_lelang_detail[0]->bahan_bakar == "bensin") ? "selected" : ""; ?>>Bensin</option>
                  <option value="solar" <?= (@$barang_lelang_detail[0]->bahan_bakar == "solar") ? "selected" : ""; ?>>Solar</option>
                </select>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="nama_event">Odometer </label>
            <input class="form-control text-uppercase" id="odometer" placeholder="Odometer" type="number" min="0" maxlength="8" name="odometer" value="<?= @$barang_lelang_detail[0]->odometer ?>">
          </div>
          <hr style="border: 2px solid black;">

          <hr style="border: 2px solid black;">
          <legend>VEHICLE DOCUMENTS:</legend>
          <div class="row">
            <div class="col-md-12 col-12">
              <div class="form-group">
                <label for="grade">Grade Utama </label>
                <select name="grade" id="grade" class="form-control select2 text-uppercase" data-placeholder="Pilih Grade Akumulasi" style="width: 100%;" required>
                  <option value="A" <?= (strtoupper(@$barang_lelang_detail[0]->grade) == "A") ? "selected" : ""; ?>>A</option>
                  <option value="B" <?= (strtoupper(@$barang_lelang_detail[0]->grade) == "B") ? "selected" : ""; ?>>B</option>
                  <option value="C" <?= (strtoupper(@$barang_lelang_detail[0]->grade) == "C") ? "selected" : ""; ?>>C</option>
                  <option value="D" <?= (strtoupper(@$barang_lelang_detail[0]->grade) == "D") ? "selected" : ""; ?>>D</option>
                  <option value="E" <?= (strtoupper(@$barang_lelang_detail[0]->grade) == "E") ? "selected" : ""; ?>>E</option>
                  <option value="F" <?= (strtoupper(@$barang_lelang_detail[0]->grade) == "F") ? "selected" : ""; ?>>F</option>
                </select>
              </div>
            </div>
            <div class="col-md-4 col-12">
              <div class="form-group">
                <label for="grade_mesin">Grade Mesin </label>
                <select name="grade_mesin" id="grade_mesin" class="form-control select2 text-uppercase" data-placeholder="Pilih Grade Mesin" style="width: 100%;" required>
                  <option value="A" <?= (strtoupper(@$barang_lelang_detail[0]->grade_mesin) == "A") ? "selected" : ""; ?>>A</option>
                  <option value="B" <?= (strtoupper(@$barang_lelang_detail[0]->grade_mesin) == "B") ? "selected" : ""; ?>>B</option>
                  <option value="C" <?= (strtoupper(@$barang_lelang_detail[0]->grade_mesin) == "C") ? "selected" : ""; ?>>C</option>
                  <option value="D" <?= (strtoupper(@$barang_lelang_detail[0]->grade_mesin) == "D") ? "selected" : ""; ?>>D</option>
                  <option value="E" <?= (strtoupper(@$barang_lelang_detail[0]->grade_mesin) == "E") ? "selected" : ""; ?>>E</option>
                  <option value="F" <?= (strtoupper(@$barang_lelang_detail[0]->grade_mesin) == "F") ? "selected" : ""; ?>>F</option>
                </select>
              </div>
            </div>
            <div class="col-md-4 col-12">
              <div class="form-group">
                <label for="grade_interior">Grade Interior </label>
                <select name="grade_interior" id="grade_interior" class="form-control select2 text-uppercase" data-placeholder="Pilih Grade Interior" style="width: 100%;" required>
                  <option value="A" <?= (strtoupper(@$barang_lelang_detail[0]->grade_interior) == "A") ? "selected" : ""; ?>>A</option>
                  <option value="B" <?= (strtoupper(@$barang_lelang_detail[0]->grade_interior) == "B") ? "selected" : ""; ?>>B</option>
                  <option value="C" <?= (strtoupper(@$barang_lelang_detail[0]->grade_interior) == "C") ? "selected" : ""; ?>>C</option>
                  <option value="D" <?= (strtoupper(@$barang_lelang_detail[0]->grade_interior) == "D") ? "selected" : ""; ?>>D</option>
                  <option value="E" <?= (strtoupper(@$barang_lelang_detail[0]->grade_interior) == "E") ? "selected" : ""; ?>>E</option>
                  <option value="F" <?= (strtoupper(@$barang_lelang_detail[0]->grade_interior) == "F") ? "selected" : ""; ?>>F</option>
                </select>
              </div>
            </div>
            <div class="col-md-4 col-12">
              <div class="form-group">
                <label for="grade_exterior">Grade Exterior </label>
                <select name="grade_exterior" id="grade_exterior" class="form-control select2 text-uppercase" data-placeholder="Pilih Grade Exterior" style="width: 100%;" required>
                  <option value="A" <?= (strtoupper(@$barang_lelang_detail[0]->grade_exterior) == "A") ? "selected" : ""; ?>>A</option>
                  <option value="B" <?= (strtoupper(@$barang_lelang_detail[0]->grade_exterior) == "B") ? "selected" : ""; ?>>B</option>
                  <option value="C" <?= (strtoupper(@$barang_lelang_detail[0]->grade_exterior) == "C") ? "selected" : ""; ?>>C</option>
                  <option value="D" <?= (strtoupper(@$barang_lelang_detail[0]->grade_exterior) == "D") ? "selected" : ""; ?>>D</option>
                  <option value="E" <?= (strtoupper(@$barang_lelang_detail[0]->grade_exterior) == "E") ? "selected" : ""; ?>>E</option>
                  <option value="F" <?= (strtoupper(@$barang_lelang_detail[0]->grade_exterior) == "F") ? "selected" : ""; ?>>F</option>
                </select>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="no_polisi">No Polisi </label>
            <input class="form-control text-uppercase" id="no_polisi" placeholder="No Polisi" type="text" name="no_polisi" value="<?= @$barang_lelang_detail[0]->no_polisi ?>">
          </div>

          <?php if (strtolower($proses) == "ubah") { ?>
            <!-- <div class="form-group">
              <label>Foto STNK</label><br>
              <img id="prevSTNK" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/stnk/origin/') . @$barang_lelang_detail[0]->stnk ?>" alt="" srcset="">
            </div> -->
          <?php } else { ?>
            <!-- <div class="form-group">
              <label>Foto STNK</label><br>
              <img class="img-thumbnail" style="max-width: 15vw;" id="prevSTNK" src="" alt="" srcset="">
            </div> -->
          <?php } ?>

          <!-- <div class="form-group">
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="stnk" name="stnk" multiple accept="image/jpg, image/jpeg, image/png" onchange="getNameFile(this,'labelSTNK'); readURL(this,'#prevSTNK','#stnk')" readonly>
              <label id="labelSTNK" class="custom-file-label" for="stnk">Pilih Foto STNK</label>
            </div>
          </div> -->

          <div class="form-group">
            <label for="stnk">STNK</label><br>
            <input type="checkbox" id="stnk" name="stnk" value="ADA" <?= (@$barang_lelang_detail[0]->stnk == 'ADA') ? 'checked' : ''; ?> data-bootstrap-switch="" data-off-color="danger" data-on-color="success" data-on-text="ADA" data-off-text="TIDAK ADA">
          </div>

          <div class="form-group">
            <label for="valid_stnk">STNK Berlaku </label>
            <div class="input-group datepicker" id="datepicker1" data-target-input="nearest">
              <input type="text" id="valid_stnk" name="valid_stnk" class="form-control datetimepicker-input" data-target="#datepicker1" value="<?= @$barang_lelang_detail[0]->valid_stnk ?>" readonly />
              <div class="input-group-append" data-target="#datepicker1" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="tahun">Tahun Produksi </label>
            <div class="input-group yearpicker" id="yearpicker" data-target-input="nearest">
              <input type="text" id="tahun" name="tahun" class="form-control datetimepicker-input" data-target="#yearpicker" value="<?= @$barang_lelang_detail[0]->tahun ?>" readonly />
              <div class="input-group-append" data-target="#yearpicker" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-12">
              <?php if (strtolower($proses) == "ubah") { ?>
                <!-- <div class="form-group">
                  <label>Foto BPKB</label><br>
                  <img id="prevBPKB" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/bpkb/origin/') . @$barang_lelang_detail[0]->bpkb ?>" alt="" srcset="">
                </div> -->
              <?php } else { ?>
                <!-- <div class="form-group">
                  <label>Foto BPKB</label><br>
                  <img class="img-thumbnail" style="max-width: 15vw;" id="prevBPKB" src="" alt="" srcset="">
                </div> -->
              <?php } ?>

              <!-- <div class="form-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="bpkb" name="bpkb" multiple accept="image/jpg, image/jpeg, image/png" onchange="getNameFile(this,'labelBPKB'); readURL(this,'#prevBPKB','#bpkb')">
                  <label id="labelBPKB" class="custom-file-label" for="bpkb">Pilih Foto BPKB</label>
                </div>
              </div> -->

              <div class="form-group">
                <label for="bpkb">BPKB</label><br>
                <input type="checkbox" id="bpkb" name="bpkb" value="ADA" <?= (@$barang_lelang_detail[0]->bpkb == 'ADA') ? 'checked' : ''; ?> data-bootstrap-switch="" data-off-color="danger" data-on-color="success" data-on-text="ADA" data-off-text="TIDAK ADA">
              </div>
            </div>

            <div class="col-md-6 col-12">
              <?php if (strtolower($proses) == "ubah") { ?>
                <!-- <div class="form-group">
                  <label>Foto Faktur</label><br>
                  <img id="prevFaktur" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/faktur/origin/') . @$barang_lelang_detail[0]->faktur ?>" alt="" srcset="">
                </div> -->
              <?php } else { ?>
                <!-- <div class="form-group">
                  <label>Foto Faktur</label><br>
                  <img class="img-thumbnail" style="max-width: 15vw;" id="prevFaktur" src="" alt="" srcset="">
                </div> -->
              <?php } ?>

              <!-- <div class="form-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="faktur" name="faktur" multiple accept="image/jpg, image/jpeg, image/png" onchange="getNameFile(this,'labelFaktur'); readURL(this,'#prevFaktur','#faktur')">
                  <label id="labelFaktur" class="custom-file-label" for="faktur">Pilih Foto Faktur</label>
                </div>
              </div> -->

              <div class="form-group">
                <label for="faktur">Faktur</label><br>
                <input type="checkbox" id="faktur" name="faktur" value="ADA" <?= (@$barang_lelang_detail[0]->faktur == 'ADA') ? 'checked' : ''; ?> data-bootstrap-switch="" data-off-color="danger" data-on-color="success" data-on-text="ADA" data-off-text="TIDAK ADA">
              </div>
            </div>

            <div class="col-md-6 col-12">
              <?php if (strtolower($proses) == "ubah") { ?>
                <!-- <div class="form-group">
                  <label>Foto KTP Pemilik</label><br>
                  <img id="prevKTP" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/ktp_pemilik/origin/') . @$barang_lelang_detail[0]->ktp_pemilik ?>" alt="" srcset="">
                </div> -->
              <?php } else { ?>
                <!-- <div class="form-group">
                  <label>Foto KTP Pemilik</label><br>
                  <img class="img-thumbnail" style="max-width: 15vw;" id="prevKTP" src="" alt="" srcset="">
                </div> -->
              <?php } ?>

              <!-- <div class="form-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="ktp_pemilik" name="ktp_pemilik" multiple accept="image/jpg, image/jpeg, image/png" onchange="getNameFile(this,'labelKTP'); readURL(this,'#prevKTP','#ktp_pemilik')">
                  <label id="labelKTP" class="custom-file-label" for="ktp_pemilik">Pilih Foto KTP Pemilik</label>
                </div>
              </div> -->

              <div class="form-group">
                <label for="ktp_pemilik">KTP Pemilik</label><br>
                <input type="checkbox" id="ktp_pemilik" name="ktp_pemilik" value="ADA" <?= (@$barang_lelang_detail[0]->ktp_pemilik == 'ADA') ? 'checked' : ''; ?> data-bootstrap-switch="" data-off-color="danger" data-on-color="success" data-on-text="ADA" data-off-text="TIDAK ADA">
              </div>
            </div>

            <div class="col-md-6 col-12">
              <?php if (strtolower($proses) == "ubah") { ?>
                <!-- <div class="form-group">
                  <label>Foto Kwitansi</label><br>
                  <img id="prevKwitansi" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/kwitansi/origin/') . @$barang_lelang_detail[0]->kwitansi ?>" alt="" srcset="">
                </div> -->
              <?php } else { ?>
                <!-- <div class="form-group">
                  <label>Foto Kwitansi</label><br>
                  <img class="img-thumbnail" style="max-width: 15vw;" id="prevKwitansi" src="" alt="" srcset="">
                </div> -->
              <?php } ?>

              <!-- <div class="form-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="kwitansi" name="kwitansi" multiple accept="image/jpg, image/jpeg, image/png" onchange="getNameFile(this,'labelKwitansi'); readURL(this,'#prevKwitansi','#kwitansi')">
                  <label id="labelKwitansi" class="custom-file-label" for="kwitansi">Pilih Foto Kwitansi</label>
                </div>
              </div> -->

              <div class="form-group">
                <label for="kwitansi">Kwitansi</label><br>
                <input type="checkbox" id="kwitansi" name="kwitansi" value="ADA" <?= (@$barang_lelang_detail[0]->kwitansi == 'ADA') ? 'checked' : ''; ?> data-bootstrap-switch="" data-off-color="danger" data-on-color="success" data-on-text="ADA" data-off-text="TIDAK ADA">
              </div>
            </div>

            <div class="col-md-6 col-12">
              <div class="form-group">
                <label for="sph">SPH</label><br>
                <input type="checkbox" id="sph" name="sph" value="ADA" <?= (@$barang_lelang_detail[0]->sph == 'ADA') ? 'checked' : ''; ?> data-bootstrap-switch="" data-off-color="danger" data-on-color="success" data-on-text="ADA" data-off-text="TIDAK ADA">
              </div>
            </div>

            <div class="col-md-6 col-12">
              <div class="form-group">
                <label for="kir">KIR</label><br>
                <input type="checkbox" id="kir" name="kir" value="ADA" <?= (@$barang_lelang_detail[0]->kir == 'ADA') ? 'checked' : ''; ?> data-bootstrap-switch="" data-off-color="danger" data-on-color="success" data-on-text="ADA" data-off-text="TIDAK ADA">
              </div>
            </div>
          </div>

          <!-- <div class="form-group">
            <label for="kapasitas_kendaraan">Kapasitas Kendaraan </label>
            <input class="form-control text-uppercase" id="kapasitas_kendaraan" placeholder="Kapasitas Kendaraan" type="number" min="0" max="8" name="kapasitas_kendaraan" value="<?= @$barang_lelang_detail[0]->kapasitas_kendaraan ?>">
          </div> -->
          <hr style="border: 2px solid black;">
        </div>

        <div class="form-group">
          <label>Pilih Gambar Barang Lelang</label>
          <div class="input-images"></div>
        </div>

        <div class="form-group">
          <label for="deskripsi" class="col-sm-2 control-label">Deskripsi</label>
          <textarea name="deskripsi" id="deskripsi" cols="30" rows="5" class="form-control summernote" required><?= @$barang_lelang_detail[0]->deskripsi ?></textarea>
        </div>

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function() {
    $('.summernote').summernote({
      height: 150,
      disableResizeEditor: true
    });
    $('.select2').select2();
    $('.datetimepicker').datetimepicker({
      debug: true,
      format: 'YYYY-MM-DD HH:mm',
      setDate: new Date(),
      buttons: {
        showClose: true
      },
      ignoreReadonly: true,
      <?php if (strtolower($proses) == "ubah") { ?>
      <?php } ?>
    });
    $('.datepicker').datetimepicker({
      debug: true,
      format: 'YYYY-MM-DD',
      setDate: new Date(),
      buttons: {
        showClose: true
      },
      ignoreReadonly: true,
      <?php if (strtolower($proses) == "ubah") { ?>
      <?php } ?>
    });
    $('.yearpicker').datetimepicker({
      debug: true,
      format: 'YYYY',
      setDate: new Date(),
      buttons: {
        showClose: true
      },
      ignoreReadonly: true,
      <?php if (strtolower($proses) == "ubah") { ?>
      <?php } ?>
    });
    $("input[data-bootstrap-switch]").each(function() {
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })
    let preloaded = <?= (is_null(@$foto_barang_lelang)) ? '[]' : json_encode(@$foto_barang_lelang) ?>;
    $('.input-images').imageUploader({
      preloaded: preloaded,
      imagesInputName: 'gambar',
      preloadedInputName: 'old-gambar',
      extensions: ['.jpg', '.jpeg', '.png', 'image/jpeg', 'image/png'],
      // mime: ['image/jpeg', 'image/png'],
      maxSize: 2000000,
      maxFiles: 10,
    });
    // akses();
    // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $("#myform").validate({
      rules: {
        id_kategori: {
          required: true,
        },
        nama_barang_lelang: {
          required: true,
        },
        brand: {
          required: true,
        },
        warna: {
          required: true,
        },
        lokasi_barang: {
          required: true,
        },
        deskripsi: {
          required: true,
        },
      }
    });
  });

  $('#id_kategori').on('change', function() {
    let idKategori = $('#id_kategori').val();
    if ((idKategori == 1) || (idKategori == 2)) {
      $("#groupInpKendaraan").removeClass('d-none');
    } else {
      $("#groupInpKendaraan").addClass('d-none');
    }

  });

  $("form").submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  // $(".gambar").change(function() {
  //   // console.log("test");
  //   readURL(this,'#prevGambar','#labelGambar');
  // });
  function getNameFile(el, target) {
    console.log(el);
    // console.log(document.getElementById(target));
    if (el.files.length) {
      document.getElementById(target).innerHTML = "";
      document.getElementById(target).innerHTML = el.files[0].name;
    } else {
      document.getElementById(target).innerHTML = "";
    }
  };
</script>
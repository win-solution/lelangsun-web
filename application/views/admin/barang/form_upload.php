<?php  // var_dump($event_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="file_import" name="file_import" accept="application/vnd.ms-excel" onchange="getNameFile(this,'labelGambar');">
            <label id="labelGambar" class="custom-file-label" for="file_import">Pilih File Import</label>
          </div>
        </div>

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="Import Data" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        // setTimeout(() => {     
        // }, 1 * 1000);
        // akses();
        // getopendata();

        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $( "#myform" ).validate({
        rules: {
            file_import: {
                required:true,
                extension: "xls",
                maxsize: 2200000,
            }
        }
        });
    });

    $('#submit_btn').on('click',function () {
        // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
        // // $('#submit_btn').attr('disabled',true); //set button disable
        // $('#submit_btn').attr('style','display:none;'); // hide button
        // $('#cancle_btn').attr('style','display:none;'); // hide button
    });

    $( "form" ).submit(function(e) {
        // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
        // $('#submit_btn').attr('disabled',true); //set button disable
        // $('#submit_btn').attr('style','display:none;'); // hide button
        // $('#cancle_btn').attr('style','display:none;'); // hide button
        // if ($('#hak_akses').val() == 1) {
        //   return;
        // } else {
        //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
        //     return;
        //   } else {
        //     alert('Hak akses harus dipilih!!!');
        //     e.preventDefault(e);
        //   }
        // }
    });
    function getNameFile(el,target) { 
      // console.log(el);
      // console.log(document.getElementById(target));
      document.getElementById(target).innerHTML = "";
      document.getElementById(target).innerHTML = el.files[0].name;
    };
</script>

<?php // var_dump($propinsi) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_outlet" id="id_outlet" value="<?= @$outlet_detail[0]->id_outlet ?>">
        <input type="hidden" name="id_mitra" id="id_mitra" value="<?= @$outlet_detail[0]->id_mitra ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="nomor_outlet">Nomer Outlet<span style="color:red;">*</span></label>
          <input class="form-control" id="nomor_outlet" placeholder="Nomer Outlet" type="text" name="nomor_outlet" value="<?= @$outlet_detail[0]->nomor_outlet ?>" pattern="[0-9]+"  required>
        </div>

        <div class="form-group">
          <label for="nama_outlet">Nama Pemilik<span style="color:red;">*</span></label>
          <input class="form-control" id="nama_outlet" placeholder="Nama Outlet" type="text" name="nama_outlet" value="<?= @$outlet_detail[0]->nama_outlet ?>" required>
        </div>

        <div class="form-group">
          <label for="id_tipe_kemitraan">Kategori Outlet <span style="color:red;">*</span></label>
          <select name="id_tipe_kemitraan" id="id_tipe_kemitraan" class="select2" data-placeholder="Pilih Tipe Kemitraan" style="width: 100%;" required>
            <?php foreach ($tipe_kemitraan as $key => $val) {
              $selected = "";
              if (strtolower($proses) == "ubah") {
                $selected = ($val->id_tipe_kemitraan == @$outlet_detail[0]->id_tipe_kemitraan)?"selected":"";
              }
              echo '<option value="'.$val->id_tipe_kemitraan.'" '.$selected.'>'.strtoupper($val->nama_kemitraan).'</option>';
            } ?>
          </select>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="kode_prop">Provinsi <span style="color:red;">*</span></label>
              <select name="kode_prop" id="kode_prop" class="select2" data-placeholder="Pilih Provinsi" style="width: 100%;" required>
                <?php foreach ($propinsi as $key => $val) { $selected = ($val['id'] == @$outlet_detail[0]->kode_prop)?'selected':''; ?>
                  <option value="<?= $val['id']?>" <?= $selected ?>><?= $val['text']?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <!-- <div class="col-lg-6">
            <div class="form-group">
              <label for="kode_kab">Kabupaten / Kota </label>
              <select name="kode_kab" id="kode_kab" class="select2" data-placeholder="Pilih Kabupaten / Kota" style="width: 100%;" required>
              </select>
            </div>
          </div> -->
          <!-- <div class="col-lg-6">
            <div class="form-group">
              <label for="kode_kec">Kecamatan </label>
              <select name="kode_kec" id="kode_kec" data-placeholder="Pilih Kecamatan" style="width: 100%;" required>
              </select>
            </div>
          </div> -->
          <!-- <div class="col-lg-6">
            <div class="form-group">
              <label for="kode_kel">Kelurahan </label>
              <select name="kode_kel" id="kode_kel" data-placeholder="Pilih Kelurahan" style="width: 100%;" required>
              </select>
            </div>
          </div> -->
        </div>

        <div class="form-group">
            <label for="join_at">Tanggal Gabung <span style="color:red;">*</span></label>
            <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                <input type="text" id="join_at" name="join_at" class="form-control datetimepicker-input" data-target="#datetimepicker1" value="<?= @$outlet_detail[0]->join_at ?>" readonly required/>
                <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>

        <div class="form-group">
          <label for="link_maps">Link Peta <span style="color:red;">*</span></label>
          <input class="form-control" id="link_maps" placeholder="Nama Outlet" type="text" name="link_maps" value="<?= @$outlet_detail[0]->link_maps ?>" required>
        </div>

        <div class="form-group">
          <label for="alamat_mitra">Alamat <span style="color:red;">*</span></label>
          <textarea name="alamat_mitra" id="alamat_mitra" cols="30" rows="5" class="form-control"  required><?= @$outlet_detail[0]->alamat_mitra ?></textarea>
        </div>

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
    $('.summernote').summernote(
      {height:150,disableResizeEditor:true}
    );
    $('#datetimepicker1').datetimepicker({
        debug: true,
        format: 'YYYY-MM-DD',
        setDate: new Date(),
        buttons: {showClose:true},
        ignoreReadonly: true,        
        <?php if (strtolower($proses) == "ubah") { ?>
        <?php } ?>
      });
      $('.select2').select2();
      // getProp();
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        nama_outlet: { required: true, },
        id_tipe_kemitraan: { required: true, },
        kode_prop: { required: true, },
        join_at: { required: true, },
        link_maps: { required: true, },
        alamat_mitra: { required: true, },
      },
      messages: {
        'gambar': {
          maxsize: "File size must not exceed 2 MB.",
        },
      }
    });
  });
  function getProp() {     
    $("#kode_prop").html("");  
    $('#kode_prop').select2({
      ajax: {
        url: "<?= base_url('admin/rest_api/getProp'); ?>",
        type: "post",
        dataType: 'json',
        delay: 250,
        processResults: function (response) {
          return {
              results: response
          };
        },
        cache: true
      }
    });
  }
  function getKab() {    
    $("#kode_kab").html("");  
    $('#kode_kab').select2({
      ajax: {
        url: "<?= base_url('admin/rest_api/getKab'); ?>",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: {
          kode_prop: document.getElementById("kode_prop").value
        },
        processResults: function (response) {
          return {
              results: response
          };
        },
        cache: true
      }
    });
  }
  $('#kode_prop').change(function (e) {
    // console.log(kode_prop)
    // getKab();
  });
  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#gambar").change(function() {
        // console.log("test");
        readURL(this,'#prevGambar','#labelGambar');
    });
</script>

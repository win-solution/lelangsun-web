<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url('admin'); ?>" class="brand-link">
        <img src="<?= base_url().'assets/'; ?>images/logo.png" alt="AdminLTE Logo" class="brand-image img-thumbnail elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">CMS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="<?= base_url().'assets/'; ?>dist/img/avatar_icon.png" class="img-circle elevation-2" alt="User Image" style="background: #fff;">
        </div>
        <div class="info">
            <a href="#" class="d-block"><?= $_SESSION['nama'] ?></a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header">Menu</li>
            <?php 
                foreach ($_SESSION['menu'] as $key => $val) { 
                    if ($val['header']) { ?>
                        <li class="nav-header"><?= $val['nama_menu'];?></li>                        
                    <?php } else { ?>
                        <li class="nav-item ">
                            <a href="<?= base_url($val['link']) ?>" class="nav-link <?= ($page == $val['page'])?"active":""; ?>">
                                <i class="<?= $val['icon'];?>"></i>
                                <p><?= $val['nama_menu'];?></p>
                            </a>
                        </li>
                    <?php } ?>            
            <?php } ?>
            <!-- <li class="nav-header">MANAGEMENT</li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/management') ?>" class="nav-link <?= ($page == "management")?"active":""; ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>EO</p>
                </a>
            </li> -->
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
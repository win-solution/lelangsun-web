<?php  // var_dump($kategori_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger">
                    <strong><?= $this->session->flashdata('error') ?></strong>
                </div>
                <?php } ?>
                <input type="hidden" name="id_pemenang_lelang" id="id_pemenang_lelang" value="<?= @$pemenang_detail[0]->id_pemenang_lelang ?>">
                <input type="hidden" name="id_peserta" id="id_peserta" value="<?= @$_SESSION['id_peserta'] ?>">
                <input type="hidden" name="submit" id="submit" value="submit">

                <div class="form-group">
                    <label for="no_rek">No Rekening <span style="color:red;">*</span></label>
                    <input class="form-control" id="no_rek" placeholder="No Rekening Peserta" type="text" name="no_rek" value="<?= @$pemenang_detail[0]->no_rek ?>" readonly>
                </div>

                <div class="form-group">
                    <label for="no_rek">Nama Pemilik Rekening <span style="color:red;">*</span></label>
                    <input class="form-control" id="nama_pemilik" placeholder="Nama Pemilik Rekening" type="text" name="nama_pemilik" value="<?= @$pemenang_detail[0]->nama_pemilik ?>" readonly>
                </div>
                
                <div class="form-group">
                    <label for="nominal">Nominal <span style="color:red;">*</span></label>
                    <input class="form-control" id="nominal" placeholder="Nominal Transfer" type="text" name="nominal" value="<?= number_format(@$pemenang_detail[0]->nominal,0,',','.') ?>" readonly>
                </div>
                
                <div class="form-group">
                    <label for="nominal">Nominal Yang Harus Dibayar <span style="color:red;">*</span></label>
                    <input class="form-control" id="nominal" placeholder="Nominal Transfer" type="text" name="nominal" value="<?= number_format(@$pemenang_detail[0]->harga_bidding,0,',','.') ?>" readonly>
                </div>
                
                <div class="form-group">
                    <label for="tanggal_transfer">Waktu Transfer <span style="color:red;">*</span></label>
                    <input class="form-control" id="tanggal_transfer" placeholder="Waktu Transfer Pembelian NPL" type="text" name="tanggal_transfer" value="<?= @$pemenang_detail[0]->tanggal_transfer ?>" readonly>
                </div>
                
                <div class="form-group">
                    <label>Bukti Transfer</label><br>
                    <img id="prevGambar" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/pelunasan_lelang/thumb/').@$pemenang_detail[0]->bukti ?>" alt="" srcset="">
                </div>

                <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input type="checkbox" class="custom-control-input" id="status_pembayaran"  name="status_pembayaran" value="1">
                        <label class="custom-control-label" for="status_pembayaran">Verifikasi Pembayaran</label>
                    </div>
                </div>
        
                <div class="form-group text-right">
                    <span id="text_submit"></span>
                    <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
                    <input type="submit" id="submit_btn" name="submit" value="Verifikasi NPL" class="btn btn-success" onclick="save();">          
                    <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $('.summernote').summernote(
            {height:150,disableResizeEditor:true}
        );
        // akses();
        // getopendata();

        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $( "#myform" ).validate({
            rules: {
                kategori: { required: true, },       
                // <?php if (strtolower($proses) == "tambah") { ?>
                //   gambar: {
                //     required:true,
                //     extension: "jpg,jpeg,png",
                //     maxsize: 20000,
                //   }
                // <?php } else { ?>
                //   gambar: {
                //     extension: "jpg,jpeg,png",
                //     maxsize: 20000,
                //   }
                // <?php } ?>
            }
        });
    });

    $('#submit_btn').on('click',function () {
        // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
        // // $('#submit_btn').attr('disabled',true); //set button disable
        // $('#submit_btn').attr('style','display:none;'); // hide button
        // $('#cancle_btn').attr('style','display:none;'); // hide button
    });

    $( "form" ).submit(function(e) {
        // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
        // $('#submit_btn').attr('disabled',true); //set button disable
        // $('#submit_btn').attr('style','display:none;'); // hide button
        // $('#cancle_btn').attr('style','display:none;'); // hide button
        // if ($('#hak_akses').val() == 1) {
        //   return;
        // } else {
        //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
        //     return;
        //   } else {
        //     alert('Hak akses harus dipilih!!!');
        //     e.preventDefault(e);
        //   }
        // }
    });
    $("#gambar").change(function() {
        // console.log("test");
        readURL(this,'#prevGambar','#labelGambar');
    });
</script>

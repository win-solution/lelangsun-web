<?php  // var_dump($kategori_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger">
                    <strong><?= $this->session->flashdata('error') ?></strong>
                </div>
                <?php } ?>
                <input type="hidden" name="id_pemenang_lelang" id="id_pemenang_lelang" value="<?= @$pemenang_detail[0]->id_pemenang_lelang ?>">
                <input type="hidden" name="id_peserta" id="id_peserta" value="<?= @$_SESSION['id_peserta'] ?>">
                <input type="hidden" name="tipe_pelunasan" id="tipe_pelunasan" value="2">
                <input type="hidden" name="submit" id="submit" value="submit">                    
                
                <div class="form-group">
                    <label for="nominal">Nominal Yang Harus Dibayar <span style="color:red;">*</span></label>
                    <input class="form-control" id="nominal" placeholder="Nominal Transfer" type="text" name="nominal" value="<?= number_format(@$pemenang_detail[0]->harga_bidding,0,',','.') ?>" readonly>
                </div>
                <hr>

                <div class="form-group">
                    <label for="no_rek">No NPL <span style="color:red;">*</span></label>
                    <input class="form-control" id="no_npl" placeholder="No NPL" type="text" name="no_npl" onkeypress="return onlyNumberKey(event)" required>
                </div>

                <div class="form-group">
                    <label for="no_rek">No Rekening <span style="color:red;">*</span></label>
                    <input class="form-control" id="no_rek" placeholder="No Rekening Peserta" type="text" name="no_rek" required>
                </div>

                <div class="form-group">
                    <label for="no_rek">Nama Pemilik Rekening <span style="color:red;">*</span></label>
                    <input class="form-control" id="nama_pemilik" placeholder="Nama Pemilik Rekening" type="text" name="nama_pemilik" required>
                </div>

                <div class="form-group">
                    <label for="nominal">Nominal <span style="color:red;">*</span></label>
                    <input class="form-control" id="nominal" placeholder="Nominal Transfer" type="number" name="nominal" onkeypress="return onlyNumberKey(event)" required>
                </div>

                <div class="form-group">
                    <label for="tanggal_transfer">Waktu Transfer <span style="color:red;">*</span></label>
                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        <input type="text" id="tanggal_transfer" name="tanggal_transfer" class="form-control datetimepicker-input" data-target="#datetimepicker1" readonly required/>
                    </div>
                </div>

                <div class="form-group">
                    <label>Bukti Transaksi</label><br>
                    <img class="img-thumbnail" style="max-width: 15vw;" id="prevBukti" src="" alt="" srcset="">
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="bukti" name="bukti" accept="image/jpg, image/jpeg, image/png"  onchange="getNameFile(this,'labelBukti'); readURL(this,'#prevBukti','#bukti')" readonly required>
                        <label id="labelBukti" class="custom-file-label" for="bukti">Pilih Gambar Bukti Transaksi</label>
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input type="checkbox" class="custom-control-input" id="status_pembayaran"  name="status_pembayaran" value="1">
                        <label class="custom-control-label" for="status_pembayaran">Verifikasi Pembayaran</label>
                    </div>
                </div>
        
                <div class="form-group text-right">
                    <span id="text_submit"></span>
                    <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
                    <input type="submit" id="submit_btn" name="submit" value="Verifikasi NPL" class="btn btn-success" onclick="save();">          
                    <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $('.summernote').summernote(
            {height:150,disableResizeEditor:true}
        );
        // akses();
        // getopendata();        
        
        $('#datetimepicker1').datetimepicker({
            debug: true,
            format: 'YYYY-MM-DD HH:mm',
            date: "<?= date('Y-m-d H:i'); ?>",
            maxDate: moment("<?= date('Y-m-d H:i'); ?>", 'YYYY-MM-DD HH:mm'),
            buttons: {showClose:true},
            ignoreReadonly: true, 
        }); 
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $( "#myform" ).validate({
            rules: {
            }
        });
    });

    $('#submit_btn').on('click',function () {
        // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
        // // $('#submit_btn').attr('disabled',true); //set button disable
        // $('#submit_btn').attr('style','display:none;'); // hide button
        // $('#cancle_btn').attr('style','display:none;'); // hide button
    });

    $( "form" ).submit(function(e) {
        // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
        // $('#submit_btn').attr('disabled',true); //set button disable
        // $('#submit_btn').attr('style','display:none;'); // hide button
        // $('#cancle_btn').attr('style','display:none;'); // hide button
        // if ($('#hak_akses').val() == 1) {
        //   return;
        // } else {
        //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
        //     return;
        //   } else {
        //     alert('Hak akses harus dipilih!!!');
        //     e.preventDefault(e);
        //   }
        // }
    });
    $("#gambar").change(function() {
        // console.log("test");
        readURL(this,'#prevGambar','#labelGambar');
    });
</script>

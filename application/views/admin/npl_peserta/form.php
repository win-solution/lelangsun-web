<?php  // var_dump($kategori_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_pembelian_npl" id="id_pembelian_npl" value="<?= @$npl_detail[0]->$id_pembelian_npl ?>">
        <input type="hidden" name="id_peserta" id="id_peserta" value="<?= @$id_peserta ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <?php if (strtolower($proses) == "detail") { ?>
          <div class="form-group">
            <label for="npl">No NPL <span style="color:red;">*</span></label>
            <input class="form-control" id="no_npl" placeholder="NO NPL" type="text" name="no_npl" value="<?= sprintf("%'.05d\n", @$npl_detail[0]->id_npl) ?>" readonly>
          </div>

          <div class="form-group">
            <label for="npl">NPL <span style="color:red;">*</span></label>
            <input class="form-control" id="npl" placeholder="NPL" type="text" name="npl" value="<?= @$npl_detail[0]->npl ?>" readonly>
          </div>

          <div class="form-group">
            <label for="event">Event <span style="color:red;">*</span></label>
            <input class="form-control" id="event" placeholder="Event" type="text" name="event" value="<?= @$npl_detail[0]->nama_event." (".@$npl_detail[0]->waktu_event.")" ?>" readonly>
          </div>
          
          <div class="form-group">
            <label for="waktu">Waktu Pembelian <span style="color:red;">*</span></label>
            <input class="form-control" id="waktu" placeholder="Waktu Pembelian NPL" type="text" name="waktu" value="<?= @$npl_detail[0]->waktu ?>" readonly>
          </div>
          
          <div class="form-group">
            <label for="no_pembelian">No Pembelian <span style="color:red;">*</span></label>
            <input class="form-control" id="no_pembelian" placeholder="NPL" type="text" name="no_pembelian" value="<?= @$npl_detail[0]->no_pembelian ?>" readonly>
          </div>

          <div class="form-group">
              <label for="no_rek">No Rekening <span style="color:red;">*</span></label>
              <input class="form-control" id="no_rek" placeholder="No Rekening Peserta" type="text" name="no_rek" value="<?= @$npl_detail[0]->no_rek ?>" readonly>
          </div>

          <div class="form-group">
              <label for="nama_pemilik">Nama Pemilik Rekening <span style="color:red;">*</span></label>
              <input class="form-control" id="nama_pemilik" placeholder="Nama Pemilik Rekening" type="text" name="nama_pemilik" value="<?= @$npl_detail[0]->nama_pemilik ?>" readonly>
          </div>
          
          <div class="form-group">
              <label for="nominal">Nominal <span style="color:red;">*</span></label>
              <input class="form-control" id="nominal" placeholder="Nominal Transfer" type="number" minlength="7" min="5000000" name="nominal" value="<?= @$npl_detail[0]->nominal ?>" readonly>
          </div>
          
          <div class="form-group">
            <label for="tanggal_transfer">Waktu Transfer <span style="color:red;">*</span></label>
            <input class="form-control" id="tanggal_transfer" placeholder="Waktu Transfer Pembelian NPL" type="text" name="tanggal_transfer" value="<?= @$npl_detail[0]->tanggal_transfer ?>" readonly>
          </div>
          
          <div class="form-group">
            <label>Gambar</label><br>
            <img id="prevGambar" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/bukti_npl/thumb/').@$npl_detail[0]->bukti ?>" alt="" srcset="">
          </div>
        <?php } else { ?>          
          <input type="hidden" name="type_pembelian" id="type_pembelian" value="offline">
          <!-- <input type="hidden" name="type_transaksi" id="type_transaksi" value="2"> -->         

          <div class="form-group">
              <label for="no_rek">Event Lelang <span style="color:red;">*</span></label>
              <select name="id_event" id="id_event" class="form-control">
                  <?php foreach ($event_detail as $key => $val) { ?>
                  <option value="<?= $val->id_event ?>"><?= $val->nama_event." (".$val->waktu_event.")" ?></option>
                  <?php } ?>
              </select>
          </div>

          <div class="form-group">
              <label for="no_rek">No NPL <span style="color:red;">*</span></label>
              <input class="form-control" id="no_npl" placeholder="No NPL Yang Dibeli" type="text" name="no_npl" onkeypress="return onlyNumberKey(event)" required>
          </div>

          <div class="form-group">
              <label for="no_rek">No Rekening <span style="color:red;">*</span></label>
              <input class="form-control" id="no_rek" placeholder="No Rekening Peserta" type="text" name="no_rek" onkeypress="return onlyNumberKey(event)" required>
          </div>
          
          <div class="form-group">
              <label for="nominal">Nominal <span style="color:red;">*</span></label>
              <input class="form-control" id="nominal" placeholder="Nominal Transfer" type="number" minlength="7" min="5000000" name="nominal" onkeypress="return onlyNumberKey(event)" required>
          </div>

          <div class="form-group">
              <label for="tanggal_transfer">Waktu Transfer <span style="color:red;">*</span></label>
              <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                  <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                  <input type="text" id="tanggal_transfer" name="tanggal_transfer" class="form-control datetimepicker-input" data-target="#datetimepicker1" readonly required/>
              </div>
          </div>

          <div class="form-group">
            <label>Bukti Transaksi</label><br>
            <img class="img-thumbnail" style="max-width: 15vw;" id="prevBukti" src="" alt="" srcset="">
          </div>

          <div class="form-group">
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="bukti" name="bukti" accept="image/jpg, image/jpeg, image/png">
              <label id="labelBukti" class="custom-file-label" for="bukti">Pilih Gambar Bukti Transaksi</label>
            </div>
          </div>
                
          <div class="form-group">
              <div class="custom-control custom-radio custom-control-inline">
                  <input class="custom-control-input" type="radio" id="type_transaksi1" name="type_transaksi" value="1">
                  <label for="type_transaksi1" class="custom-control-label">Cash dengan admin SUN</label>
              </div>
              <div class="custom-control custom-radio custom-control-inline">
                  <input class="custom-control-input" type="radio" id="type_transaksi2" name="type_transaksi" value="2">
                  <label for="type_transaksi2" class="custom-control-label">Transfer / Mesin EDC</label>
              </div>
          </div>
        <?php } ?>
        <?php if (strtolower($proses) == "tambah") { ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
    $('.summernote').summernote(
      {height:150,disableResizeEditor:true}
    );
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
    $('.select2').select2();
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        id_peserta: { required: true, },
        bukti: { required: true, extension: "jpg,jpeg,png", maxsize: 2200000, },
      }
    });    
        
    $('#datetimepicker1').datetimepicker({
        debug: true,
        format: 'YYYY-MM-DD HH:mm',
        date: "<?= date('Y-m-d H:i'); ?>",
        maxDate: moment("<?= date('Y-m-d H:i'); ?>", 'YYYY-MM-DD HH:mm'),
        buttons: {showClose:true},
        ignoreReadonly: true, 
    });      
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#bukti").change(function() {
      // console.log("test");
      readURL(this,'#prevBukti','#labelBukti');
  });
</script>

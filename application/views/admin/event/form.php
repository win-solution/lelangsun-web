<?php  // var_dump($event_detail) 
?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_event" id="id_event" value="<?= @$event_detail[0]->id_event ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="nama_event">Event <span style="color:red;">*</span></label>
          <input class="form-control" id="nama_event" placeholder="Judul event" type="text" name="nama_event" value="<?= @$event_detail[0]->nama_event ?>" required>
        </div>

        <div class="form-group">
          <label for="waktu_event">Waktu Event <span style="color:red;">*</span></label>
          <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
            <input type="text" id="waktu_event" name="waktu_event" class="form-control datetimepicker-input" data-target="#datetimepicker1" value="<?= @$event_detail[0]->waktu_event ?>" readonly required />
          </div>
        </div>

        <?php if (strtolower($proses) == "ubah") { ?>
          <!-- <div class="form-group">
          <label>Gambar</label><br>
          <img id="prevGambar" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/event/thumb/') . @$event_detail[0]->gambar ?>" alt="" srcset="">
        </div> -->
        <?php } else { ?>
          <!-- <div class="form-group">
            <label>Gambar</label><br>
            <img class="img-thumbnail" style="max-width: 15vw;" id="prevGambar" src="" alt="" srcset="">
          </div> -->
        <?php } ?>

        <!-- <div class="form-group">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="gambar" name="gambar" accept="image/jpg, image/jpeg, image/png">
            <label id="labelGambar" class="custom-file-label" for="gambar">Pilih Gambar</label>
          </div>
        </div> -->

        <div class="form-group">
          <label for="alamat_event">Alamat Event <span style="color:red;">*</span></label>
          <textarea name="alamat_event" id="alamat_event" cols="30" rows="5" class="form-control" required><?= @$event_detail[0]->alamat_event ?></textarea>
        </div>

        <div class="form-group">
          <label for="link_maps">Link Lokasi <span style="color:red;">*</span></label>
          <input class="form-control" id="link_maps" placeholder="Link Lokasi" type="text" name="link_maps" value="<?= @$event_detail[0]->link_maps ?>" required>
        </div>

        <div class="form-group">
          <label for="deskripsi_event" class="col-sm-2 control-label">Deskripsi Event</label>
          <textarea name="deskripsi_event" id="deskripsi_event" cols="30" rows="5" class="form-control summernote" required><?= @$event_detail[0]->deskripsi_event ?></textarea>
        </div>

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function() {
    // setTimeout(() => {
    $('.summernote').summernote({
      height: 150,
      disableResizeEditor: true
    });
    $('#datetimepicker1').datetimepicker({
      debug: true,
      format: 'YYYY-MM-DD HH:mm',
      minDate: moment("<?= date('Y-m-d H:i'); ?>", 'YYYY-MM-DD HH:mm'),
      buttons: {
        showClose: true
      },
      ignoreReadonly: true,
      <?php if (strtolower($proses) == "ubah") { ?>
        date: "<?= @$event_detail[0]->waktu_event ?>",
      <?php } else { ?>
        date: new Date(),
      <?php } ?>
    });
    $('#datetimepicker2').datetimepicker({
      debug: true,
      format: 'LT'
    });
    // }, 1 * 1000);
    // akses();
    // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $("#myform").validate({
      rules: {
        nama_event: {
          required: true,
        },
        deskripsi_event: {
          required: true,
        },
        waktu_event: {
          required: true,
        },
        alamat_event: {
          required: true,
        },
        link_maps: {
          required: true,
        },
        gambar: {
          <?php if (strtolower($proses) == "tambah") { ?>
            required: true,
          <?php } ?>
          extension: "jpg,jpeg,png",
          maxsize: 2200000,
        }
      }
    });
  });

  $('#submit_btn').on('click', function() {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $("form").submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#gambar").change(function() {
    readURL(this, '#prevGambar', '#labelGambar');
  });
</script>
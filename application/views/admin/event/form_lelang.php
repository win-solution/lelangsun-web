<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.2/flexslider.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.2/jquery.flexslider.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Event</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Event</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Tempusdominus Bootstrap 4 -->
        <!-- <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <script src="<?= base_url('assets/'); ?>plugins/moment/moment.min.js"></script>
        <script src="<?= base_url('assets/'); ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js"></script>         -->
        <script>
            const apis = "<?= base_url('api/') ?>";
        </script>
        <!-- Firebase -->
        <!-- <script src="https://www.gstatic.com/firebasejs/8.6.8/firebase-app.js"></script>
		<script src="https://www.gstatic.com/firebasejs/8.6.8/firebase-analytics.js"></script>
		<script src="https://www.gstatic.com/firebasejs/8.6.8/firebase-database.js"></script> -->

        <!-- chat_realtime -->
        <!-- <script type="text/javascript" src="<?= base_url('assets/') ?>js/config.js"></script>
		<script type="text/javascript" src="<?= base_url('assets/') ?>js/chat_realtime.js"></script> -->

        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <script>
            var kelas = "";
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar Event <?= APP_NAME ?></h3>
                        </div>
                        <div class="card-body">
                            <?php // var_dump(@$_SESSION['pass_event']); 
                            ?>
                            <!-- <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content_tab_1" data-toggle="pill" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true" title="Event Aktif">Lelang Event</a>
                                </li>
                            </ul> 
                            <br> -->
                            <div class="tab-content" id="custom-tabs-four-tabContent">
                                <div class="tab-pane fade active show" id="tab_1" role="tabpanel" aria-labelledby="content_tab_1">
                                    <div class="row">
                                        <div class="col-lg-6 col-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h3 class="card-title">Detail Barang Lelang</h3>
                                                </div>
                                                <div class="card-body">
                                                    <?php if (!empty(@$lot_lelang)) { ?>
                                                        <div id="slider" class="flexslider mb-2">
                                                            <img loading="lazy" src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$lot_lelang[0]->gambar ?>" class="img-thumbnail" />
                                                        </div>
                                                        <div>
                                                            <h1 class="text-uppercase pb-1"><?= @$lot_lelang[0]->nama_barang_lelang . ' ' . @$lot_lelang[0]->transmisi ?></h1>
                                                            <h2 class="text-uppercase">Brand : <?= @$lot_lelang[0]->brand ?></h2>
                                                            <h2 class="text-uppercase">Tahun : <?= @$lot_lelang[0]->tahun ?></h2>
                                                            <h2 class="text-uppercase">No Polisi : <?= @$lot_lelang[0]->no_polisi ?></h2>
                                                            <h2 class="text-uppercase">Grade : <?= @$lot_lelang[0]->grade ?></h2>
                                                            <h2 class="mr-3 fontlights float-left text-uppercase"><i class="fas fa-tachometer-alt" style="color: #ff4500"></i> <?= @$lot_lelang[0]->odometer ?></h2>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-12">
                                            <h1 id="title-harga"><?= "Rp " . number_format(((!empty(@$lot_lelang[0]->harga_awal)) ? @$lot_lelang[0]->harga_awal : 0), 0, '', '.'); ?></h1>
                                            <h3>LOT <?= sprintf("%'.05d\n", @$lot_lelang[0]->no_lot) ?></h3>
                                            <div class="card direct-chat direct-chat-primary" style="position: relative; left: 0px; top: 0px;">
                                                <div class="card-header ui-sortable-handle">
                                                    <h3 class="card-title">Sistem Lelang</h3>
                                                </div>
                                                <div class="card-body chat_area">
                                                    <div id="div-chat-lelang" class="direct-chat-messages" style="height: 350px;">
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                    <form action="#" method="post">
                                                        <input type="hidden" id="id_event" name="id_event" value="<?= $id_event ?>">
                                                        <input type="hidden" id="kode_event" name="kode_event" value="<?= $kode_event ?>">
                                                        <!-- <input type="hidden" id="email" name="email" value="<?= $email ?>"> -->
                                                        <input type="hidden" id="email" name="email" value="Floor Bidder">
                                                        <input type="hidden" id="start_value" name="start_value" value="<?= @$lot_lelang[0]->harga_awal; ?>">
                                                        <input type="hidden" id="id_lot" name="id_lot" value="<?= @$lot_lelang[0]->id_lot; ?>">
                                                        <input type="hidden" id="curent_bid" name="curent_bid" value="<?= @$lot_lelang[0]->harga_awal; ?>">
                                                        <input type="hidden" id="allow_bid" name="allow_bid" value="<?= (!empty($_SESSION['pass_event'])) ? "true" : "false"; ?>">
                                                        <input type="hidden" id="is_start" name="is_start" value="false">
                                                        <div class="input-group justify-content-center">
                                                            <!-- <span id="start-bid" class="btn btn-primary mx-1"><i class="fas fa-flag"></i> Start Bid</span> -->
                                                            <?php if (!empty($_SESSION['pass_event'])) { ?>
                                                                <span id="start-stop-bid" class="btn btn-success btn-danger mx-1" onclick="startStopBid();"><i class="fas fa-gavel"></i> Stop Bid</span>
                                                            <?php } else { ?>
                                                                <span id="start-stop-bid" class="btn btn-success mx-1" onclick="startStopBid();"><i class="fas fa-gavel"></i> Start Bid</span>
                                                            <?php } ?>
                                                            <?php if (!empty(@$lot_lelang[1])) { ?>
                                                                <span id="next-bid" class="btn btn-primary mx-1" onclick="nextBid('<?= $id_event . '?kode_event=' . $kode_event . '&id_lot=' . @$lot_lelang[1]->id_lot; ?>')"><i class="fas fa-arrow-right"></i> Next Bid</span>
                                                            <?php } else { ?>
                                                                <span id="close-bid" class="btn btn-danger mx-1"><a href="<?= base_url('api/close_bid/') . $id_event . '?channel=channel-' . $kode_event; ?>" class="text-white"><i class="fas fa-power-off"></i> Akhiri Lelang</a></span>
                                                            <?php } ?>
                                                        </div>
                                                        <hr>
                                                        <div class="input-group justify-content-center">
                                                            <span id="up-bid" class="btn btn-success mx-1 w-75"><i class="fas fa-arrow-up"></i> Bidding</span>
                                                        </div>
                                                    </form>
                                                    <div class="row begin-countdown">
                                                        <div class="col-md-12 text-center">
                                                            <progress value="<?= $setting->bid_time ?>" max="<?= $setting->bid_time ?>" id="pageBeginCountdown"></progress>
                                                            <p> Time out in <span id="pageBeginCountdownText"><?= $setting->bid_time ?> </span> seconds</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2>Lot Lelang</h2>
                                            <div id="carousel-lot" class="flexslider mb-0">
                                                <ul class="slides">
                                                    <?php foreach ($barang_lelang as $key => $val) { ?>
                                                        <li>
                                                            <div class="card" style="width: 100%;">
                                                                <img loading="lazy" src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$val->gambar ?>" class="card-img-top" alt="<?= @$val->nama_barang_lelang . ' ' . @$val->transmisi ?>">
                                                                <div class="card-body">
                                                                    <h1 class="card-title mb-0 font-weight-bold"><?= @$val->nama_barang_lelang . ' ' . @$val->transmisi ?></h1>
                                                                    <p class="card-text font-italic"><?= @$val->brand ?></p>
                                                                    <h1 class="card-title font-weight-bold"><?= "Rp " . number_format(((!empty($val->harga_awal)) ? $val->harga_awal : 0), 0, '', '.'); ?></h1>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-admin mw-75 mx-auto">

                <!-- Modal content-->
                <div class="modal-content" style="border-radius: 15px;">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal_header"></h4>
                        <button type="button" class="close btn btn-danger" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="modal_body">
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    </div>
                </div>

            </div>
        </div>
        <script>
            var countdownTimer;
            var timeleft = <?= $setting->bid_time; ?>;
            <?php if (!empty($_SESSION['pass_event'])) { ?>
                var startStop = 1;
            <?php } else { ?>
                var startStop = 0;
            <?php } ?>
            var myChanel = 'channel-' + $('#kode_event').val(),
                myEvent = 'lelang_event';
            $(document).ready(function() {
                $('#carousel').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 150,
                    itemMargin: 5,
                    asNavFor: '#slider'
                });
                $('#carousel-lot').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 150,
                    itemMargin: 5,
                });
                $('#slider').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    sync: "#carousel"
                });


                // myEvent		= '<?= $kode_event ?>';
                Pusher.log = function(message) {
                    if (window.console && window.console.log) {
                        window.console.log(message);
                    }
                };
                console.log(myChanel);
                // var pusher = new Pusher('191dbef1780135573650');
                // var channel = pusher.subscribe('chatglobal');
                var pusher = new Pusher('<?= $this->config->item('pusher_app_key'); ?>', {
                    cluster: 'ap1'
                });
                var channel = pusher.subscribe(myChanel);

                channel.bind('lelang_event', function(data) {
                    // channel.bind('<?= $kode_event ?>', function(data) {
                    timeleft = <?= $setting->bid_time; ?>;
                    clearInterval(countdownTimer);
                    // ProgressCountdown(<?= $setting->bid_time ?>, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => alert(`Page has started: ${value}.`)); 
                    ProgressCountdown('pageBeginCountdown', 'pageBeginCountdownText').then(value => startStopBid());
                    sendmessage(data);
                });

                channel.bind('appendponline', function(data) {
                    appendponline(data);
                });
                channel.bind('set_session_pass_event', function(data) {
                    // console.log("set_session_pass_event",data);
                    $.ajax({
                        url: "<?= base_url(); ?>api/set_session_pass_event/",
                        type: 'POST',
                        dataType: "JSON",
                        data: {
                            pass_event: data.pass_event,
                        },
                        success: function(data) {
                            // $('#message').val('');
                            console.log(data);
                            // Toggle BTN
                            if (data.status) {
                                // ProgressCountdown(<?= $setting->bid_time ?>, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => alert(`Page has started: ${value}.`)); 
                                ProgressCountdown('pageBeginCountdown', 'pageBeginCountdownText').then(value => startStopBid());
                                $("#start-stop-bid").addClass("btn-danger");
                                $("#start-stop-bid").html('<i class="fas fa-gavel"></i> Stop Bid');
                                $('#allow_bid').val("true");
                            } else {
                                $("#start-stop-bid").removeClass("btn-danger");
                                $("#start-stop-bid").html('<i class="fas fa-gavel"></i> Start Bid');
                                $('#allow_bid').val("false");

                                timeleft = <?= $setting->bid_time; ?>;
                                clearInterval(countdownTimer);
                                Swal.fire({
                                    title: 'Lelang telah selesai!',
                                    text: 'Lelang dimenangkan oleh ' + $(".direct-chat-name").last().text() + ' dengan harga ' + $('#title-harga').text(),
                                    icon: 'success',
                                    confirmButtonText: 'Ke Lot Berikutnya.'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        // setTimeout(() => {                                            
                                        <?php if (!empty(@$lot_lelang[1])) { ?>
                                            nextBid('<?= $id_event . '?kode_event=' . $kode_event . '&id_lot=' . @$lot_lelang[1]->id_lot; ?>');
                                        <?php } else { ?>
                                            window.location = '<?= base_url('api/close_bid/') . $id_event . '?channel=channel-' . $kode_event . '&id_lot=' . @$lot_lelang[0]->id_lot; ?>';
                                        <?php } ?>
                                        // }, 2 * 1000);
                                    }
                                });
                            }
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                });
                channel.bind('next_bid', function(data) {
                    window.location = '<?= base_url('admin/event/bidsys/') ?>' + data;
                });

                function appendponline(data) {
                    html = '';
                    html += '<li class="left clearfix">';
                    html += ' <span class="chat-img pull-left">';
                    html += ' <img loading="lazy" src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle">';
                    html += ' </span>';
                    html += '<div class="chat-body clearfix">';
                    html += '<div class="header_sec">';
                    html += ' <strong class="primary-font">' + data.username + '</strong>';
                    html += '</div>';
                    html += '</div>';
                    html += '</li>';
                    $('#appendponline').prepend(html);
                }

                function sendmessage(data, autoscroll = true) {
                    // var ses_id = <?= $this->session->userdata('id'); ?>;
                    var ses_id = document.getElementById('email').value;
                    let harga_bid = 'Rp ' + data.harga_bidding.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    $('#title-harga').html(harga_bid);
                    $('#curent_bid').val(data.harga_bidding);
                    $('#is_start').val('true');
                    // console.log('data sendmessage',data,ses_id,harga_bid);
                    if (data.email == ses_id) {
                        html = '';
                        html += '<div class="direct-chat-msg right"><div class="direct-chat-infos clearfix">';
                        html += '<span class="direct-chat-name float-right">' + data.email + '</span>';
                        html += '<span class="direct-chat-timestamp float-left">' + data.date + '</span></div>';
                        html += '<img loading="lazy" class="direct-chat-img" src="<?= base_url() ?>assets/images/profile_picture.png" alt="message user image">';
                        html += '<div class="direct-chat-text">' + harga_bid + '</div></div>';
                        $('#div-chat-lelang').prepend(html);
                        if (autoscroll) {
                            // $("html, body .chat_area").animate({ scrollTop: document.getElementById("div-chat-lelang").scrollHeight }, 1000);
                        }
                        $('#message').val("");
                    } else {
                        html = '';
                        html += '<div class="direct-chat-msg "><div class="direct-chat-infos clearfix">';
                        html += '<span class="direct-chat-name float-left">' + data.email + '</span>';
                        html += '<span class="direct-chat-timestamp float-right">' + data.date + '</span></div>';
                        html += '<img loading="lazy" class="direct-chat-img" src="<?= base_url() ?>assets/images/profile_picture.png" alt="message user image">';
                        html += '<div class="direct-chat-text">' + harga_bid + '</div></div>';
                        $('#div-chat-lelang').prepend(html);
                        if (autoscroll) {
                            // $("html, body .chat_area").animate({ scrollTop: document.getElementById("div-chat-lelang").scrollHeight }, 1000);
                        }
                    }
                    // $("html, body .chat_area").animate({ scrollTop: $(document).height() }, 1000);
                }

                function loadData() {
                    $.ajax({
                        url: "<?= base_url(); ?>api/getChat/",
                        type: 'GET',
                        dataType: "JSON",
                        data: {
                            channel: myChanel,
                            event: myEvent,
                            kode_event: $('#kode_event').val()
                        },
                        beforeSend: function() {
                            $("div.spanner").addClass("show");
                            $("div.overlay").addClass("show");
                        },
                        success: function(data) {
                            // console.log(data);
                            data.forEach(el => {
                                // console.log(el);
                                if (el.id_lot == $('#id_lot').val()) {
                                    $('#curent_bid').val(el.harga_bidding);
                                    sendmessage(el, false);
                                }
                            });
                        },
                        complete: function() {
                            setTimeout(function() {
                                $("div.spanner").removeClass("show");
                                $("div.overlay").removeClass("show");
                                // $("html, body .chat_area").animate({ scrollTop: document.getElementById("div-chat-lelang").scrollHeight }, 1000);
                            }, 5 * 1000);
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                }

                $('#up-bid').click(function() {
                    if ($('#allow_bid').val() === "true") {
                        var harga_bidding;
                        if ($('#is_start').val() == 'false') {
                            harga_bidding = parseInt($('#curent_bid').val());
                        } else {
                            harga_bidding = parseInt($('#curent_bid').val()) + 500000;
                        }
                        let btnSend = $('#up-bid'),
                            // letters = /^[0-9a-zA-Z!@#\$%\^\&*\)\(+=,<.>/?;:'"[{}|\]\_-]+$/;
                            letters = /^\s+$/;
                        $('#curent_bid').val(harga_bidding);
                        // console.log(message,message.match(letters));
                        if (!btnSend[0].classList.value.match('disabled')) {
                            // if ((!harga_bidding.match(letters)) && (harga_bidding !== "")) {
                            $.ajax({
                                url: "<?= base_url(); ?>api/chatsend/",
                                type: 'POST',
                                data: {
                                    kode_event: $('#kode_event').val(),
                                    email: $('#email').val(),
                                    id_lot: $('#id_lot').val(),
                                    id_event: $('#id_event').val(),
                                    harga_bidding: harga_bidding,
                                },
                                beforeSend: function() {
                                    btnSend.addClass("disabled");
                                },
                                success: function() {
                                    // $('#message').val('');
                                    $('#is_start').val('true');
                                },
                                complete: function() {
                                    btnSend.removeClass("disabled");

                                },
                                error: function(err) {
                                    console.log(err);
                                }
                            });
                            // } else {
                            //     $('#message').val('');
                            // }				
                        }
                    }
                });

                loadData();
            });

            function startStopBid() {
                startStop++
                // Send AJAX
                $.ajax({
                    url: "<?= base_url(); ?>api/start_stop_bid/",
                    type: 'POST',
                    dataType: "JSON",
                    data: {
                        id_event: $('#id_event').val(),
                        channel: myChanel,
                    },
                    beforeSend: function() {
                        // btnSend.addClass("disabled");
                    },
                    success: function(data) {
                        // $('#message').val('');
                    },
                    complete: function() {
                        // btnSend.removeClass("disabled");

                    },
                    error: function(err) {
                        console.log(err);
                    }
                });
            }

            function nextBid(url) {
                // Send AJAX
                $.ajax({
                    url: "<?= base_url(); ?>api/next_bid/",
                    type: 'POST',
                    dataType: "JSON",
                    data: {
                        url: url,
                        last_lot: $('#id_lot').val(),
                        channel: myChanel,
                    },
                    beforeSend: function() {
                        // btnSend.addClass("disabled");
                    },
                    success: function(data) {
                        // $('#message').val('');
                    },
                    complete: function() {
                        // btnSend.removeClass("disabled");

                    },
                    error: function(err) {
                        console.log(err);
                    }
                });
            }

            function ProgressCountdown(bar, text) {
                return new Promise((resolve, reject) => {
                    countdownTimer = setInterval(() => {
                        timeleft--;
                        document.getElementById(bar).value = timeleft;
                        document.getElementById(text).textContent = timeleft;

                        if (timeleft <= 0) {
                            clearInterval(countdownTimer);
                            resolve(true);
                        }
                    }, 1000);
                });
            }

            setInterval(function() {
                // table.ajax.reload(null,false);
                // table_trash.ajax.reload(null,false);
            }, 3 * 60 * 1000);
        </script>
    </section>
</div>
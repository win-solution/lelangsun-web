<?php  // var_dump($kategori_detail) 
?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_pembelian_npl" id="id_pembelian_npl" value="<?= @$npl_detail[0]->id_pembelian_npl ?>">
        <input type="hidden" name="id_peserta" id="id_peserta" value="<?= @$npl_detail[0]->id_peserta ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <!-- <div class="form-group">
          <label for="npl">NPL <span style="color:red;">*</span></label>
          <input class="form-control" id="npl" placeholder="NPL" type="text" name="npl" value="<?= @$npl_detail[0]->npl ?>" readonly>
        </div> -->

        <div class="form-group">
          <label for="event">Event <span style="color:red;">*</span></label>
          <input class="form-control" id="id_event" placeholder="Event" type="hidden" name="id_event" value="<?= @$npl_detail[0]->id_event ?>" readonly>
          <input class="form-control" placeholder="Event" type="text" name="event" value="<?= @$npl_detail[0]->nama_event . " (" . @$npl_detail[0]->waktu_event . ")" ?>" readonly>
        </div>

        <div class="form-group">
          <label for="waktu">Waktu Pembelian <span style="color:red;">*</span></label>
          <input class="form-control" id="waktu" placeholder="Waktu Pembelian NPL" type="text" name="waktu" value="<?= @$npl_detail[0]->waktu ?>" readonly>
        </div>

        <div class="form-group">
          <label for="no_pembelian">No Pembelian <span style="color:red;">*</span></label>
          <input class="form-control" id="no_pembelian" placeholder="NPL" type="text" name="no_pembelian" value="<?= @$npl_detail[0]->no_pembelian ?>" readonly>
        </div>

        <div class="form-group">
          <label for="no_rek">No Rekening <span style="color:red;">*</span></label>
          <input class="form-control" id="no_rek" placeholder="No Rekening Peserta" type="text" name="no_rek" value="<?= @$npl_detail[0]->no_rek ?>" readonly>
        </div>

        <div class="form-group">
          <label for="nominal">Nominal <span style="color:red;">*</span></label>
          <input class="form-control" id="nominal" placeholder="Nominal Transfer" type="text" name="nominal" value="<?= number_format(@$npl_detail[0]->nominal, 0, ',', '.') ?>" readonly>
        </div>

        <div class="form-group">
          <label for="jumlah">Jumlah Pembelian <span style="color:red;">*</span></label>
          <input class="form-control" id="jumlah" placeholder="Jumlah Pembelian" type="text" name="jumlah" value="<?= floor((@$npl_detail[0]->nominal / 5000000)) ?>" readonly>
        </div>

        <div class="form-group">
          <label for="tanggal_transfer">Waktu Transfer <span style="color:red;">*</span></label>
          <input class="form-control" id="tanggal_transfer" placeholder="Waktu Transfer Pembelian NPL" type="text" name="tanggal_transfer" value="<?= @$npl_detail[0]->tanggal_transfer ?>" readonly>
        </div>

        <div class="form-group">
          <label>Bukti Transfer</label><br>
          <img id="prevGambar" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/bukti_npl/thumb/') . @$npl_detail[0]->bukti ?>" alt="" srcset="">
        </div>

        <div class="form-group">
          <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input" id="verifikasi" name="verifikasi" value="1">
            <label class="custom-control-label" for="verifikasi">Verifikasi Pembayaran</label>
          </div>
        </div>

        <div class="form-group">
          <label for="pesan_verifikasi">Pesan Verifikasi Pembayaran</label><br>
          <textarea name="pesan_verifikasi" id="pesan_verifikasi" cols="30" rows="5" class="form-control"></textarea>
        </div>

        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="Simpan" class="btn btn-success" onclick="save();">
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>

      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function() {
    $('.summernote').summernote({
      height: 150,
      disableResizeEditor: true
    });
    $("input[data-bootstrap-switch]").each(function() {
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
    $('.select2').select2();
    // akses();
    // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $("#myform").validate({
      rules: {
        id_peserta: {
          required: true,
        },
        bukti: {
          required: true,
          extension: "jpg,jpeg,png",
          maxsize: 2200000,
        },
      }
    });

    $('#datetimepicker1').datetimepicker({
      debug: true,
      format: 'YYYY-MM-DD HH:mm',
      date: "<?= date('Y-m-d H:i'); ?>",
      maxDate: moment("<?= date('Y-m-d H:i'); ?>", 'YYYY-MM-DD HH:mm'),
      buttons: {
        showClose: true
      },
      ignoreReadonly: true,
    });
  });

  $('#submit_btn').on('click', function() {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $("form").submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#bukti").change(function() {
    // console.log("test");
    readURL(this, '#prevBukti', '#labelBukti');
  });
</script>
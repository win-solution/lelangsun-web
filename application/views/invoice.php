<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        
        <section class="page-section">

          <div class="container mw-75">

            <div class="row">
              
              <div class="col my-auto">
                <h3>PT. BALAI LELANG SUN</h3>
                <ul class="list-unstyled">
                  <li>
                    <span><b>Auction Office </b></span>
                    : Jl. HOS Cokroaminoto No.59 Ruko <br>Ciledug Mas D3 Ciledug Tanggerang
                  </li>
                </ul>
              </div>

              <div class="col d-flex justify-content-center my-auto">
                <ul class="ml-4 mb-0 fa-ul">
                  <li><span class="fa-li"><i class="fas fa-phone-alt"></i></span>Phone : (021) 22271959</li>
                  <li><span class="fa-li"><i class="fab fa-youtube"></i></span>Youtube : BalaiLelangSun</li>
                  <li><span class="fa-li"><i class="fab fa-facebook-square"></i></span>Facebook : BalaiLelangSun</li>
                  <li><span class="fa-li"><i class="fab fa-instagram-square"></i></span>Instagram : BalaiLelangSun</li>
                </ul>
              </div>

              <div class="col text-center">
                <img src="assets/images/logo.png" alt="...">
              </div>

            </div>

            <hr>

            <div class="row">
              
              <div class="col my-auto">
                <h3>PT. BALAI LELANG SUN</h3>
              </div>

              <div class="col">

                <div class="row">

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Nomor Urut </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span>20 <hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Tanggal & Jam </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span>25/03/2021    18:50:55<hr class="m-0 mb-1"></span>
                  </div>

                </div>

              </div>

            </div>

            <div class="border">
              <div>
                <h5 class="m-1">Data Peserta</h5>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="row">

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Nama Pekerja </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Nama Pekerja<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>NIK </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> NIK<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>NPWP </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> NPWP<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Alamat </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Alamat<hr class="m-0 mb-1"></span>
                    <span> Lengkap<hr class="m-0 mb-1"></span>
                    <span> Peserta<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Telpon / HP </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Telepon / HP<hr class="m-0 mb-1"></span>
                  </div>

                </div>                
              </div>
              <div class="col-lg-6">
                <div class="row">

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Bertindak Mewakili</span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 d-flex pl-0">
                    <div class="form-check mr-5">
                      <input class="form-check-input" type="checkbox" id="gridCheck1">
                      <label class="form-check-label" for="gridCheck1">
                        Pribadi
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="gridCheck1">
                      <label class="form-check-label" for="gridCheck1">
                        Perusahaan
                      </label>
                    </div>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Nama Perusahaan </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Nama Perusahaan<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>NPWP Perusahaan</span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> NPWP Perusahaan<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Alamat</span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Alamat<hr class="m-0 mb-1"></span>
                    <span> Lengkap<hr class="m-0 mb-1"></span>
                    <span> Perusahaan<hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Telp. Kantor / Email</span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span> Telp. Kantor / Email<hr class="m-0 mb-1"></span>
                  </div>

                </div>                
              </div>
            </div>

            <div class="row">
              
              <div class="col-lg-6">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                      
                      <div class="border">
                        <div>
                          <span class="m-1">Data Jaminan</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Jumlah Jaminan </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Jumlah Jaminan <hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Terbilang </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <label class="sr-only" for="inlineFormInput">Terbilang</label>
                            <input type="text" class="form-control mb-2 text-right" id="inlineFormInput" placeholder="Rupiah #">
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Bank </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Bank <hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>No. Rekening Asal </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>No. Rekening Asal <hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Atas Nama </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Atas Nama <hr class="m-0 mb-1"></span>
                          </div>

                        </div>

                      </div>

                  </div>

                  <div class="col-lg-12 mb-3">
                      
                      <div class="border">
                        <div>
                          <span class="m-1">Instruksi Pengembalian Jaminan</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Bank </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Bank <hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>No. Rekening Asal </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>No. Rekening Asal<hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-4 pr-0">
                            <div class="row">
                              <div class="col-lg-9">
                                <span>Atas Nama </span>
                              </div>
                              <div class="col-lg-3">
                                <span>:</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-8 pl-0">
                            <span>Atas Nama<hr class="m-0 mb-1"></span>
                          </div>

                          <div class="col-lg-12 text-right">
                            <span>Paraf/TTD </span>
                          </div>

                        </div>

                      </div>

                  </div>

                  <div class="col-lg-12 mw-90 mx-auto border">
                      
                    <div class="row">
                      
                      <div class="col-lg-12">
                        
                        <div class="row">
                          
                          <div class="col-lg-6">
                            
                            <h4>Pembayaran</h4>

                          </div>

                        </div>

                      </div>

                      <div class="col-lg-12">
                        
                        <div class="row">

                          <div class="col-lg-4">
                            
                            <h3></h3>

                          </div>

                          <div class="col-lg-8">
                            
                            <h4>1. Debit BCA</h4>

                          </div>

                        </div>

                      </div>

                      <div class="col-lg-12">
                        
                        <div class="row">

                          <div class="col-lg-4">
                            
                            <h3></h3>

                          </div>

                          <div class="col-lg-8">
                            
                            <h4>2. Transfer : No. Rek 5025 06 2288 <br> a.n PT. Balai Lelang SUN</h4>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

              <div class="col-lg-6">

                <div class="row">

                  <div class="col-lg-12">
                      
                      <div class="border">
                        <div class=" text-center">
                          <span class="m-1 text-uppercase">PERHATIAN</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-12 pr-0">
                            <div class="row">
                              <div class="col-lg-1">
                                <span>1</span>
                              </div>
                              <div class="col-lg-9">
                                <span>Saya telah membaca, menyetujui dan akan memenuhi ketentuan lelang yang berlaku</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 pr-0">
                            <div class="row">
                              <div class="col-lg-1">
                                <span>2</span>
                              </div>
                              <div class="col-lg-9">
                                <span>Saya akan membeli kendara lelang yang telah saya periksa dan dalam kondisi apa adanya</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 pr-0">
                            <div class="row">
                              <div class="col-lg-1">
                                <span>3</span>
                              </div>
                              <div class="col-lg-9">
                                <span>Saya telah menyetorkan uang jaminan sebesar yang telah ditentukan per nomor peserta lelang</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 pr-0">
                            <div class="row">
                              <div class="col-lg-1">
                                <span>4</span>
                              </div>
                              <div class="col-lg-9">
                                <span>Saya akan melunasi objek lelang yang saya menangkan ditambah biaya administrasi paling lambat 2 (dua) hari kerja</span>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 mw-90 mx-auto">

                            <div class="row">

                              <div class="col-lg-6 pr-0">
                                  
                                  <div class="border">
                                    <div class="text-center p-3">
                                      <span class="m-1">Peserta Lelang</span>
                                    </div>
                                  </div>

                                  <div class="border px-3">
                                    
                                    <div class="row">

                                      <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                                        <span>Peserta Lelang</span>
                                      </div>

                                    </div>

                                  </div>

                                  <div class="border">
                                    <div class=" text-center">
                                      <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                                    </div>
                                  </div>

                              </div>

                              <div class="col-lg-6 pl-0">
                                  
                                  <div class="border">
                                    <div class="text-center p-3">
                                      <span class="m-1">Auction Office</span>
                                    </div>
                                  </div>

                                  <div class="border px-3">
                                    
                                    <div class="row">

                                      <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                                        <span>Auction Office</span>
                                      </div>

                                    </div>

                                  </div>

                                  <div class="border">
                                    <div class=" text-center">
                                      <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                                    </div>
                                  </div>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                  </div>

                </div>

              </div>

            </div>

        </section>
        
    </body>
</html>
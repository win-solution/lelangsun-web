<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>

    <style type="text/css">
      /* If the screen size is 601px wide or more, set the font-size of <div> to 80px */
      @media screen and (min-width: 601px) {
        .events {
          background-image: url('assets/images/y/web-03.png');
          height: 112vh;
          background-position: left;
          background-size: cover;
          background-repeat: no-repeat;
        }
      }
      /* If the screen size is 600px wide or less, set the font-size of <div> to 30px */
      @media screen and (max-width: 600px) {
        .events {
          background-image: url('assets/images/y/mobile-03.png');
          height: 30vh;
          background-position: left;
          background-size: cover;
          background-repeat: no-repeat;
        }
      }
      .img-cover {
            height: 25vh;
            object-fit: cover;
        }

        @media screen and (max-width: 600px) {
            .img-cover {
                height: 30vh;
            }
        }
    </style>
    
    <body id="page-top" style="background-image: url('<?= base_url() ?>assets/images/dark2/dark2-03.png'); background-position: bottom;">
    	<?php include('part/nav.php') ?>

      <header class="masthead" style="background-image: url('<?= base_url() ?>assets/images/y/web-05.png'); background-position: bottom;">
          <div class="container mw-100 mh-100 events">
              <div class="row justify-content-center h-100 mw-90 mx-auto hidden-mobile">
                  <div class="col-12 col-lg-6 d-flex justify-content-center" style="margin: 17vh 0 0 0;">
                      <div>
                        <h1 class="text-uppercase text-uppercase fontbold" style="font-size: 5rem; color: #ff0000">Events</h1>
                        <span class="subheading fontlight" style="font-size: 2rem; color: #fff;">Ayo ikuti events lelang <br> secara langsung</span>
                      </div>
                  </div>
                  <div class="col-12 col-lg-6 d-flex justify-content-center text-center" style="margin: 12vh 0 0 0;">
                      <!-- <div>
                        <img src="assets/images/web-07.png" class="card-img-top" alt="...">
                      </div> -->
                  </div>
              </div>
          </div>
      </header>

    	<section class="page-section portfolio" id="about">
    	    <div class="container mw-90 mx">
                <h1 class="fontbold text-white">Event List</h1>
    	        <div class="mw-100 mx-auto">
    	            <div class="row row-cols-1 row-cols-md-3">
                    <?php foreach ($event_lelang as $key => $val) { ?>
                        <div class="col-lg-3 mb-4">
                            <a href="<?= base_url('detailevent').'/'.@$val->id_event; ?>" style="color: inherit; text-decoration: none;">
                                <div class="card h-100 text-left">

                                    <img src="<?= base_url() ?>assets/images/gadgets-auction_1284-22060.jpg" class="card-img-top img-cover" alt="<?= $val->nama_event ?>">

                                    <div class="card-body fontlight">
                                    <h5 class="card-title fontbold"><?= $val->nama_event ?></h5>
                                    <p style=" color: #333" class="mr-3"><i class="fas fa-map-marked-alt mr-3" style="color: #ff4500"></i> <?= $val->alamat_event ?></p>
                                    <p style=" color: #333" class="mr-3"><i class="fas fa-calendar mr-3" style="color: #ff4500"></i> <?= date_format(date_create($val->waktu_event),'d M Y / H:i') ?> WIB</p>
                                    </div>
                                </div>   
                            </a>
                        </div>
                    <?php } ?>
                  </div>
    	        </div>
                <div class="col-12">
                    <?= $pagingView; ?>
                </div>
    	    </div>
    	</section>

      <?php include('part/footer.php') ?>

    </body>
</html>
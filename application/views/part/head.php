<head>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <meta name="description" content="<?= $setting->metadata_deskripsi ?> Situs lelang Mobil, Motor & serba-serbi terpercaya" />

    <meta name="author" content="" />

    <title><?= APP_NAME ?></title>

    <!-- Favicon-->

    <link rel="icon" type="image/x-icon" href="<?= base_url() ?>assets/images/icon.ico" />

    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>

    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />

    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="<?= base_url('assets'); ?>/css/styles.css" rel="stylesheet" />
    <script src="<?= base_url('assets'); ?>/plugins/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.2/flexslider.css">
    <!-- SweetAlert2 -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <script src="<?= base_url('assets/'); ?>plugins/sweetalert2/sweetalert2.min.js"></script> -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Core theme JS-->
    <script src="<?= base_url() ?>assets/js/scripts.js"></script>
    <script>
        var baseURL = "<?= base_url(); ?>";
        $(document).keydown(function(e) {
            if (e.which === 123) {
                return false;
            }
        });
        $(document).bind("contextmenu", function(e) {
            e.preventDefault();
        });
    </script>
    <style type="text/css">
        @font-face {
            font-family: 'D-DIN';
            src: url('<?= base_url('assets/') ?>fonts/D-DIN.otf') format('opentype');
            /*url('../fonts/rajdhani-semibold.woff') format('woff'),
                url('../fonts/rajdhani-semibold.ttf')  format('truetype');*/
        }

        .fontlights {
            font-family: 'D-DIN', sans-serif;
        }

        @font-face {
            font-family: 'D-DIN-Bold';
            src: url('<?= base_url('assets/') ?>fonts/D-DIN-Bold.ttf') format('truetype');
            /*url('../fonts/rajdhani-semibold.woff') format('woff'),
                url('../fonts/rajdhani-semibold.ttf')  format('truetype');*/
        }

        .fontbold {
            font-family: 'D-DIN-Bold', sans-serif;
        }

        #more {
            display: none;
        }

        .circle-icon {
            vertical-align: middle;
            width: 50%;
            height: 42%;
            border-radius: 50%;
            border: 1px solid white;
        }

        .promo-box {
            position: absolute;
            top: -2.5rem;
            left: .5rem;
        }

        .grade-box {
            position: absolute;
            background: #ff4500;
            border: 1px solid #fff;
            padding: 5px;
            width: 75px;
            height: 75px;
            top: 1rem;
            border-radius: 50%;
            color: #fff;
            text-align: center;
        }

        .grade-box .text-grade {
            font-size: 22px;
            line-height: 23px;
        }

        .promo-box2 {
            position: absolute;
            top: -2.5rem;
            left: .5rem;
        }

        .grade-box2 {
            position: absolute;
            background: #ff4500;
            border: 1px solid #fff;
            padding: 5px;
            width: 75px;
            height: 75px;
            bottom: 3rem;
            left: 3rem;
            border-radius: 50%;
            color: #fff;
            text-align: center;
        }

        .grade-box2 .text-grade2 {
            font-size: 22px;
            line-height: 23px;
        }
    </style>

</head>
<!-- Footer-->

<footer class="footer text-center" id="contact" style="padding-top: 3rem;padding-bottom: unset;background-color: #252525;color: #fff;">

    <div class="container mw-90">

        <div class="row">

            <!-- Footer Location-->

            <div class="col-lg-5 mb-5 mb-lg-0 text-left">

                <h5 class="text-uppercase mb-4 text-white" >Tentang Kami</h5>
                <span class="text-white">Tentang Kami</span>

                <div class="pt-3">

<a class="btn btn-outline-light btn-social mx-1" href="<?= @$setting->ig; ?>"><i class="fab fa-fw fa-instagram"></i></a>

<a class="btn btn-outline-light btn-social mx-1" href="<?= @$setting->fb; ?>"><i class="fab fa-fw fa-facebook-f"></i></a>

<a class="btn btn-outline-light btn-social mx-1" href="<?= @$setting->tw; ?>"><i class="fab fa-fw fa-twitter"></i></a>

<a class="btn btn-outline-light btn-social mx-1" href="<?= @$setting->yt; ?>"><i class="fab fa-fw fa-youtube"></i></a>

                </div>

                <!-- <div class="copyright py-4 text-center text-white" style="font-size: 0px;">

                    <div class="container mw-90">

                        <div class="row">

                            <div class="col-12 mb-3">

                                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->ig; ?>"><i class="fab fa-fw fa-instagram"></i></a>

                                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->fb; ?>"><i class="fab fa-fw fa-facebook-f"></i></a>

                                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->tw; ?>"><i class="fab fa-fw fa-twitter"></i></a>

                            </div>

                        </div>

                    </div>

                </div> -->

            </div>

            <!-- Footer Social Icons-->

            <div class="col-lg-3 mb-5 mb-lg-0 text-left" >

                <h5 class="text-uppercase mb-4 text-white">Menu</h5>

                <ul class="links p-0" style="list-style: none;">

                    <li class="pb-3"><a href="<?= base_url(); ?>">Beranda</a></li>

                    <li class="pb-3"><a href="<?= base_url(); ?>/lot">Lot</a></li>

                    <li class="pb-3"><a href="<?= base_url(); ?>/lelang">Lelang</a></li>

                    <!-- <li><a href="<?= base_url(); ?>/booth">Booth</a></li> -->

                    <li class="pb-3"><a href="<?= base_url(); ?>/event">Event</a></li>

                    <!-- <li><a href="<?= base_url(); ?>/penghargaan">Sertifikat</a></li> -->

                    <!-- <li><a href="<?= base_url(); ?>/outlet">Outlet</a></li> -->

                </ul>

            </div>

            <!-- Footer About Text-->

            <div class="col-lg-4 mb-5 mb-lg-0 text-left" >

                <h5 class="text-uppercase mb-4 text-white" >Customer Center</h5>

                <ul class="links p-0" style="list-style: none;">

                    <li>
                        <a href="https://www.google.com/maps/place/Balai+Lelang+SUN/@-6.2259323,106.7131172,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69fb62354445c3:0x1c1460e2d8676d6a!8m2!3d-6.2259367!4d106.715303" target="_blank">
                            <div class="media">
                                <i class="fas fa-map-marker-alt pt-1 mr-3" style="font-size: 1.5rem;"></i>
                                <div class="media-body pl-1">
                                    <span>Cireundeu, Ciputat Timur, Cireundeu, Kec. Ciputat Timur. Kota Tanggeran Selatan, Banten 15419</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="tel:+<?= $setting->no_telp; ?>" target="_blank">
                            <span class="text-white"><i class="fa fa-phone mr-3"></i>08561496490</span>
                        </a>
                    </li>
                    <li>
                        <a href="mailto:<?= $setting->email; ?>" target="_blank">
                            <span class="text-white"><i class="fa fa-envelope mr-3"></i>lelangnusantara@gmail.com</span>
                        </a>
                    </li>

                </ul>

                <!-- <span><i class="fas fa-map-marker-alt"></i><a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.286297185097!2d106.71311721413772!3d-6.22593226270653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fb62354445c3%3A0x1c1460e2d8676d6a!2sBalai%20Lelang%20SUN!5e0!3m2!1sen!2sid!4v1619803047263!5m2!1sen!2sid"> <?= $setting->alamat; ?></a></span> -->

                <!-- <span><a href="https://www.google.co.id/maps/place/Ruko+Kebayoran+Arcade+2/@-6.273301,106.7159024,18.11z/data=!4m8!1m2!2m1!1sRuko+Kebayoran+Arcade+5+Block+F3%2F11+Jl.+Boulevard+Bintaro+Jaya+Sektor+7+Pondok+Aren!3m4!1s0x2e69fa9b2ffa9373:0x890816d566493bf0!8m2!3d-6.2735042!4d106.7157767"> <?= $setting->alamat; ?></a></span> -->
                <!-- <br> -->
                <!-- <a href="https://www.google.com/maps/place/Kantor+Lup+Lup+Lup+Bubble+Drink/@-6.2748826,106.7110529,17z/data=!4m8!1m2!2m1!1sLUP+LUP+BUBBLE+DRINK!3m4!1s0x2e69fba90a2d693d:0x9119e3ffbb2277f5!8m2!3d-6.2733149!4d106.7105165?hl=en"><button type="button" class="btn btn-primary btn-sm mt-2">Buka Dengan Maps</button></a> -->

            </div>

            <!-- Footer About Text-->

            <!-- <div class="col-lg-3 mb-5 mb-lg-0 text-left">

                <h5 class="text-uppercase mb-4" style="color: #ffcc00;">Kontak</h5>

                <ul class="links p-0" style="list-style: none; font-size: 16px;">

                    <li><a href="tel:+<?= $setting->no_telp; ?>"><i class="fas fa-mobile-alt"></i> +<?= $setting->no_telp; ?></a></li>

                    <li><a href="mailto:<?= $setting->email; ?>"><i class="fas fa-envelope"></i> <?= $setting->email; ?></a></li>

                </ul>

            </div> -->

        </div>

    </div>

</footer>

<!-- Copyright Section-->

<div class="copyright py-4 text-center text-white">

    <div class="container mw-90">

        <div class="row">

            <!-- <div class="col-12 mb-3">

                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->ig; ?>"><i class="fab fa-fw fa-instagram"></i></a>

                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->fb; ?>"><i class="fab fa-fw fa-facebook-f"></i></a>

                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->tw; ?>"><i class="fab fa-fw fa-twitter"></i></a>

            </div> -->

            <div class="col-12 mt-3">

                <span>2020-2021 SUN BALAI LELANG. DILINDUNGI HAK CIPTA</span>

            </div>

        </div>

    </div>

</div>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->

<div class="scroll-to-top d-lg-none position-fixed">

    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>

</div>

<?php if (!empty($setting->no_wa)) { ?>
<a href="https://api.whatsapp.com/send?phone=<?= $setting->no_wa; ?>&text=Halo min, saya ingin bertanya tentang Balai Lelang SUN." class="btn btn-outline-light btn-social mx-1 float" target="_blank">
    <i class="fab fa-fw fa-whatsapp"></i>
    <!-- <i class="fab fa-fw fa-twitter"></i> -->
</a>
<?php } ?>

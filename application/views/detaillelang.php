<!DOCTYPE html>
<html lang="en" oncontextmenu="return false">
<?php include('part/head.php') ?>

<style type="text/css">
    .direct-chat .card-body {
        overflow-x: hidden;
        padding: 0;
        position: relative;
    }

    .direct-chat.chat-pane-open .direct-chat-contacts {
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
    }

    .direct-chat.timestamp-light .direct-chat-timestamp {
        color: #30465f;
    }

    .direct-chat.timestamp-dark .direct-chat-timestamp {
        color: #cccccc;
    }

    .direct-chat-messages {
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
        height: 250px;
        overflow: auto;
        padding: 10px;
    }

    .direct-chat-msg,
    .direct-chat-text {
        display: block;
    }

    .direct-chat-msg {
        margin-bottom: 10px;
    }

    .direct-chat-msg::after {
        display: block;
        clear: both;
        content: "";
    }

    .direct-chat-messages,
    .direct-chat-contacts {
        transition: -webkit-transform .5s ease-in-out;
        transition: transform .5s ease-in-out;
        transition: transform .5s ease-in-out, -webkit-transform .5s ease-in-out;
    }

    .direct-chat-text {
        border-radius: 0.3rem;
        background-color: #d2d6de;
        border: 1px solid #d2d6de;
        color: #444;
        margin: 5px 0 0 50px;
        padding: 5px 10px;
        position: relative;
    }

    .direct-chat-text::after,
    .direct-chat-text::before {
        border: solid transparent;
        border-right-color: #d2d6de;
        content: " ";
        height: 0;
        pointer-events: none;
        position: absolute;
        right: 100%;
        top: 15px;
        width: 0;
    }

    .direct-chat-text::after {
        border-width: 5px;
        margin-top: -5px;
    }

    .direct-chat-text::before {
        border-width: 6px;
        margin-top: -6px;
    }

    .right .direct-chat-text {
        margin-left: 0;
        margin-right: 50px;
    }

    .right .direct-chat-text::after,
    .right .direct-chat-text::before {
        border-left-color: #d2d6de;
        border-right-color: transparent;
        left: 100%;
        right: auto;
    }

    .direct-chat-img {
        border-radius: 50%;
        float: left;
        height: 40px;
        width: 40px;
    }

    .right .direct-chat-img {
        float: right;
    }

    .direct-chat-infos {
        display: block;
        font-size: 0.875rem;
        margin-bottom: 2px;
    }

    .direct-chat-name {
        font-weight: 600;
    }

    .direct-chat-timestamp {
        color: #697582;
    }

    .direct-chat-contacts-open .direct-chat-contacts {
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
    }

    .direct-chat-contacts {
        -webkit-transform: translate(101%, 0);
        transform: translate(101%, 0);
        background-color: #343a40;
        bottom: 0;
        color: #fff;
        height: 250px;
        overflow: auto;
        position: absolute;
        top: 0;
        width: 100%;
    }

    .direct-chat-contacts-light {
        background-color: #f8f9fa;
    }

    .direct-chat-contacts-light .contacts-list-name {
        color: #495057;
    }

    .direct-chat-contacts-light .contacts-list-date {
        color: #6c757d;
    }

    .direct-chat-contacts-light .contacts-list-msg {
        color: #545b62;
    }

    .contacts-list {
        padding-left: 0;
        list-style: none;
    }

    .contacts-list>li {
        border-bottom: 1px solid rgba(0, 0, 0, 0.2);
        margin: 0;
        padding: 10px;
    }

    .contacts-list>li::after {
        display: block;
        clear: both;
        content: "";
    }

    .contacts-list>li:last-of-type {
        border-bottom: 0;
    }

    .contacts-list-img {
        border-radius: 50%;
        float: left;
        width: 40px;
    }

    .contacts-list-info {
        color: #fff;
        margin-left: 45px;
    }

    .contacts-list-name,
    .contacts-list-status {
        display: block;
    }

    .contacts-list-name {
        font-weight: 600;
    }

    .contacts-list-status {
        font-size: 0.875rem;
    }

    .contacts-list-date {
        color: #ced4da;
        font-weight: 400;
    }

    .contacts-list-msg {
        color: #b1bbc4;
    }

    .direct-chat-primary .right>.direct-chat-text {
        background-color: #007bff;
        border-color: #007bff;
        color: #fff;
    }

    .direct-chat-primary .right>.direct-chat-text::after,
    .direct-chat-primary .right>.direct-chat-text::before {
        border-left-color: #007bff;
    }

    .direct-chat-secondary .right>.direct-chat-text {
        background-color: #6c757d;
        border-color: #6c757d;
        color: #fff;
    }

    .direct-chat-secondary .right>.direct-chat-text::after,
    .direct-chat-secondary .right>.direct-chat-text::before {
        border-left-color: #6c757d;
    }

    .direct-chat-success .right>.direct-chat-text {
        background-color: #28a745;
        border-color: #28a745;
        color: #fff;
    }

    .direct-chat-success .right>.direct-chat-text::after,
    .direct-chat-success .right>.direct-chat-text::before {
        border-left-color: #28a745;
    }

    .direct-chat-info .right>.direct-chat-text {
        background-color: #17a2b8;
        border-color: #17a2b8;
        color: #fff;
    }

    .direct-chat-info .right>.direct-chat-text::after,
    .direct-chat-info .right>.direct-chat-text::before {
        border-left-color: #17a2b8;
    }

    .direct-chat-warning .right>.direct-chat-text {
        background-color: #ffc107;
        border-color: #ffc107;
        color: #1f2d3d;
    }

    .direct-chat-warning .right>.direct-chat-text::after,
    .direct-chat-warning .right>.direct-chat-text::before {
        border-left-color: #ffc107;
    }

    .direct-chat-danger .right>.direct-chat-text {
        background-color: #dc3545;
        border-color: #dc3545;
        color: #fff;
    }

    .direct-chat-danger .right>.direct-chat-text::after,
    .direct-chat-danger .right>.direct-chat-text::before {
        border-left-color: #dc3545;
    }

    .direct-chat-light .right>.direct-chat-text {
        background-color: #f8f9fa;
        border-color: #f8f9fa;
        color: #1f2d3d;
    }

    .direct-chat-light .right>.direct-chat-text::after,
    .direct-chat-light .right>.direct-chat-text::before {
        border-left-color: #f8f9fa;
    }

    .direct-chat-dark .right>.direct-chat-text {
        background-color: #343a40;
        border-color: #343a40;
        color: #fff;
    }

    .direct-chat-dark .right>.direct-chat-text::after,
    .direct-chat-dark .right>.direct-chat-text::before {
        border-left-color: #343a40;
    }

    .direct-chat-lightblue .right>.direct-chat-text {
        background-color: #3c8dbc;
        border-color: #3c8dbc;
        color: #fff;
    }

    .direct-chat-lightblue .right>.direct-chat-text::after,
    .direct-chat-lightblue .right>.direct-chat-text::before {
        border-left-color: #3c8dbc;
    }

    .direct-chat-navy .right>.direct-chat-text {
        background-color: #001f3f;
        border-color: #001f3f;
        color: #fff;
    }

    .direct-chat-navy .right>.direct-chat-text::after,
    .direct-chat-navy .right>.direct-chat-text::before {
        border-left-color: #001f3f;
    }

    .direct-chat-olive .right>.direct-chat-text {
        background-color: #3d9970;
        border-color: #3d9970;
        color: #fff;
    }

    .direct-chat-olive .right>.direct-chat-text::after,
    .direct-chat-olive .right>.direct-chat-text::before {
        border-left-color: #3d9970;
    }

    .direct-chat-lime .right>.direct-chat-text {
        background-color: #01ff70;
        border-color: #01ff70;
        color: #1f2d3d;
    }

    .direct-chat-lime .right>.direct-chat-text::after,
    .direct-chat-lime .right>.direct-chat-text::before {
        border-left-color: #01ff70;
    }

    .direct-chat-fuchsia .right>.direct-chat-text {
        background-color: #f012be;
        border-color: #f012be;
        color: #fff;
    }

    .direct-chat-fuchsia .right>.direct-chat-text::after,
    .direct-chat-fuchsia .right>.direct-chat-text::before {
        border-left-color: #f012be;
    }

    .direct-chat-maroon .right>.direct-chat-text {
        background-color: #d81b60;
        border-color: #d81b60;
        color: #fff;
    }

    .direct-chat-maroon .right>.direct-chat-text::after,
    .direct-chat-maroon .right>.direct-chat-text::before {
        border-left-color: #d81b60;
    }

    .direct-chat-blue .right>.direct-chat-text {
        background-color: #007bff;
        border-color: #007bff;
        color: #fff;
    }

    .direct-chat-blue .right>.direct-chat-text::after,
    .direct-chat-blue .right>.direct-chat-text::before {
        border-left-color: #007bff;
    }

    .direct-chat-indigo .right>.direct-chat-text {
        background-color: #6610f2;
        border-color: #6610f2;
        color: #fff;
    }

    .direct-chat-indigo .right>.direct-chat-text::after,
    .direct-chat-indigo .right>.direct-chat-text::before {
        border-left-color: #6610f2;
    }

    .direct-chat-purple .right>.direct-chat-text {
        background-color: #6f42c1;
        border-color: #6f42c1;
        color: #fff;
    }

    .direct-chat-purple .right>.direct-chat-text::after,
    .direct-chat-purple .right>.direct-chat-text::before {
        border-left-color: #6f42c1;
    }

    .direct-chat-pink .right>.direct-chat-text {
        background-color: #e83e8c;
        border-color: #e83e8c;
        color: #fff;
    }

    .direct-chat-pink .right>.direct-chat-text::after,
    .direct-chat-pink .right>.direct-chat-text::before {
        border-left-color: #e83e8c;
    }

    .direct-chat-red .right>.direct-chat-text {
        background-color: #dc3545;
        border-color: #dc3545;
        color: #fff;
    }

    .direct-chat-red .right>.direct-chat-text::after,
    .direct-chat-red .right>.direct-chat-text::before {
        border-left-color: #dc3545;
    }

    .direct-chat-orange .right>.direct-chat-text {
        background-color: #fd7e14;
        border-color: #fd7e14;
        color: #1f2d3d;
    }

    .direct-chat-orange .right>.direct-chat-text::after,
    .direct-chat-orange .right>.direct-chat-text::before {
        border-left-color: #fd7e14;
    }

    .direct-chat-yellow .right>.direct-chat-text {
        background-color: #ffc107;
        border-color: #ffc107;
        color: #1f2d3d;
    }

    .direct-chat-yellow .right>.direct-chat-text::after,
    .direct-chat-yellow .right>.direct-chat-text::before {
        border-left-color: #ffc107;
    }

    .direct-chat-green .right>.direct-chat-text {
        background-color: #28a745;
        border-color: #28a745;
        color: #fff;
    }

    .direct-chat-green .right>.direct-chat-text::after,
    .direct-chat-green .right>.direct-chat-text::before {
        border-left-color: #28a745;
    }

    .direct-chat-teal .right>.direct-chat-text {
        background-color: #20c997;
        border-color: #20c997;
        color: #fff;
    }

    .direct-chat-teal .right>.direct-chat-text::after,
    .direct-chat-teal .right>.direct-chat-text::before {
        border-left-color: #20c997;
    }

    .direct-chat-cyan .right>.direct-chat-text {
        background-color: #17a2b8;
        border-color: #17a2b8;
        color: #fff;
    }

    .direct-chat-cyan .right>.direct-chat-text::after,
    .direct-chat-cyan .right>.direct-chat-text::before {
        border-left-color: #17a2b8;
    }

    .direct-chat-white .right>.direct-chat-text {
        background-color: #fff;
        border-color: #fff;
        color: #1f2d3d;
    }

    .direct-chat-white .right>.direct-chat-text::after,
    .direct-chat-white .right>.direct-chat-text::before {
        border-left-color: #fff;
    }

    .direct-chat-gray .right>.direct-chat-text {
        background-color: #6c757d;
        border-color: #6c757d;
        color: #fff;
    }

    .direct-chat-gray .right>.direct-chat-text::after,
    .direct-chat-gray .right>.direct-chat-text::before {
        border-left-color: #6c757d;
    }

    .direct-chat-gray-dark .right>.direct-chat-text {
        background-color: #343a40;
        border-color: #343a40;
        color: #fff;
    }

    .direct-chat-gray-dark .right>.direct-chat-text::after,
    .direct-chat-gray-dark .right>.direct-chat-text::before {
        border-left-color: #343a40;
    }

    .dark-mode .direct-chat-text {
        background-color: #454d55;
        border-color: #4b545c;
        color: #fff;
    }

    .dark-mode .direct-chat-text::after,
    .dark-mode .direct-chat-text::before {
        border-right-color: #4b545c;
    }

    .dark-mode .direct-chat-timestamp {
        color: #adb5bd;
    }

    .dark-mode .right>.direct-chat-text::after,
    .dark-mode .right>.direct-chat-text::before {
        border-right-color: transparent;
    }

    .dark-mode .direct-chat-primary .right>.direct-chat-text {
        background-color: #3f6791;
        border-color: #3f6791;
        color: #fff;
    }

    .dark-mode .direct-chat-primary .right>.direct-chat-text::after,
    .dark-mode .direct-chat-primary .right>.direct-chat-text::before {
        border-left-color: #3f6791;
    }

    .dark-mode .direct-chat-secondary .right>.direct-chat-text {
        background-color: #6c757d;
        border-color: #6c757d;
        color: #fff;
    }

    .dark-mode .direct-chat-secondary .right>.direct-chat-text::after,
    .dark-mode .direct-chat-secondary .right>.direct-chat-text::before {
        border-left-color: #6c757d;
    }

    .dark-mode .direct-chat-success .right>.direct-chat-text {
        background-color: #00bc8c;
        border-color: #00bc8c;
        color: #fff;
    }

    .dark-mode .direct-chat-success .right>.direct-chat-text::after,
    .dark-mode .direct-chat-success .right>.direct-chat-text::before {
        border-left-color: #00bc8c;
    }

    .dark-mode .direct-chat-info .right>.direct-chat-text {
        background-color: #3498db;
        border-color: #3498db;
        color: #fff;
    }

    .dark-mode .direct-chat-info .right>.direct-chat-text::after,
    .dark-mode .direct-chat-info .right>.direct-chat-text::before {
        border-left-color: #3498db;
    }

    .dark-mode .direct-chat-warning .right>.direct-chat-text {
        background-color: #f39c12;
        border-color: #f39c12;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-warning .right>.direct-chat-text::after,
    .dark-mode .direct-chat-warning .right>.direct-chat-text::before {
        border-left-color: #f39c12;
    }

    .dark-mode .direct-chat-danger .right>.direct-chat-text {
        background-color: #e74c3c;
        border-color: #e74c3c;
        color: #fff;
    }

    .dark-mode .direct-chat-danger .right>.direct-chat-text::after,
    .dark-mode .direct-chat-danger .right>.direct-chat-text::before {
        border-left-color: #e74c3c;
    }

    .dark-mode .direct-chat-light .right>.direct-chat-text {
        background-color: #f8f9fa;
        border-color: #f8f9fa;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-light .right>.direct-chat-text::after,
    .dark-mode .direct-chat-light .right>.direct-chat-text::before {
        border-left-color: #f8f9fa;
    }

    .dark-mode .direct-chat-dark .right>.direct-chat-text {
        background-color: #343a40;
        border-color: #343a40;
        color: #fff;
    }

    .dark-mode .direct-chat-dark .right>.direct-chat-text::after,
    .dark-mode .direct-chat-dark .right>.direct-chat-text::before {
        border-left-color: #343a40;
    }

    .dark-mode .direct-chat-lightblue .right>.direct-chat-text {
        background-color: #86bad8;
        border-color: #86bad8;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-lightblue .right>.direct-chat-text::after,
    .dark-mode .direct-chat-lightblue .right>.direct-chat-text::before {
        border-left-color: #86bad8;
    }

    .dark-mode .direct-chat-navy .right>.direct-chat-text {
        background-color: #002c59;
        border-color: #002c59;
        color: #fff;
    }

    .dark-mode .direct-chat-navy .right>.direct-chat-text::after,
    .dark-mode .direct-chat-navy .right>.direct-chat-text::before {
        border-left-color: #002c59;
    }

    .dark-mode .direct-chat-olive .right>.direct-chat-text {
        background-color: #74c8a3;
        border-color: #74c8a3;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-olive .right>.direct-chat-text::after,
    .dark-mode .direct-chat-olive .right>.direct-chat-text::before {
        border-left-color: #74c8a3;
    }

    .dark-mode .direct-chat-lime .right>.direct-chat-text {
        background-color: #67ffa9;
        border-color: #67ffa9;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-lime .right>.direct-chat-text::after,
    .dark-mode .direct-chat-lime .right>.direct-chat-text::before {
        border-left-color: #67ffa9;
    }

    .dark-mode .direct-chat-fuchsia .right>.direct-chat-text {
        background-color: #f672d8;
        border-color: #f672d8;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-fuchsia .right>.direct-chat-text::after,
    .dark-mode .direct-chat-fuchsia .right>.direct-chat-text::before {
        border-left-color: #f672d8;
    }

    .dark-mode .direct-chat-maroon .right>.direct-chat-text {
        background-color: #ed6c9b;
        border-color: #ed6c9b;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-maroon .right>.direct-chat-text::after,
    .dark-mode .direct-chat-maroon .right>.direct-chat-text::before {
        border-left-color: #ed6c9b;
    }

    .dark-mode .direct-chat-blue .right>.direct-chat-text {
        background-color: #3f6791;
        border-color: #3f6791;
        color: #fff;
    }

    .dark-mode .direct-chat-blue .right>.direct-chat-text::after,
    .dark-mode .direct-chat-blue .right>.direct-chat-text::before {
        border-left-color: #3f6791;
    }

    .dark-mode .direct-chat-indigo .right>.direct-chat-text {
        background-color: #6610f2;
        border-color: #6610f2;
        color: #fff;
    }

    .dark-mode .direct-chat-indigo .right>.direct-chat-text::after,
    .dark-mode .direct-chat-indigo .right>.direct-chat-text::before {
        border-left-color: #6610f2;
    }

    .dark-mode .direct-chat-purple .right>.direct-chat-text {
        background-color: #6f42c1;
        border-color: #6f42c1;
        color: #fff;
    }

    .dark-mode .direct-chat-purple .right>.direct-chat-text::after,
    .dark-mode .direct-chat-purple .right>.direct-chat-text::before {
        border-left-color: #6f42c1;
    }

    .dark-mode .direct-chat-pink .right>.direct-chat-text {
        background-color: #e83e8c;
        border-color: #e83e8c;
        color: #fff;
    }

    .dark-mode .direct-chat-pink .right>.direct-chat-text::after,
    .dark-mode .direct-chat-pink .right>.direct-chat-text::before {
        border-left-color: #e83e8c;
    }

    .dark-mode .direct-chat-red .right>.direct-chat-text {
        background-color: #e74c3c;
        border-color: #e74c3c;
        color: #fff;
    }

    .dark-mode .direct-chat-red .right>.direct-chat-text::after,
    .dark-mode .direct-chat-red .right>.direct-chat-text::before {
        border-left-color: #e74c3c;
    }

    .dark-mode .direct-chat-orange .right>.direct-chat-text {
        background-color: #fd7e14;
        border-color: #fd7e14;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-orange .right>.direct-chat-text::after,
    .dark-mode .direct-chat-orange .right>.direct-chat-text::before {
        border-left-color: #fd7e14;
    }

    .dark-mode .direct-chat-yellow .right>.direct-chat-text {
        background-color: #f39c12;
        border-color: #f39c12;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-yellow .right>.direct-chat-text::after,
    .dark-mode .direct-chat-yellow .right>.direct-chat-text::before {
        border-left-color: #f39c12;
    }

    .dark-mode .direct-chat-green .right>.direct-chat-text {
        background-color: #00bc8c;
        border-color: #00bc8c;
        color: #fff;
    }

    .dark-mode .direct-chat-green .right>.direct-chat-text::after,
    .dark-mode .direct-chat-green .right>.direct-chat-text::before {
        border-left-color: #00bc8c;
    }

    .dark-mode .direct-chat-teal .right>.direct-chat-text {
        background-color: #20c997;
        border-color: #20c997;
        color: #fff;
    }

    .dark-mode .direct-chat-teal .right>.direct-chat-text::after,
    .dark-mode .direct-chat-teal .right>.direct-chat-text::before {
        border-left-color: #20c997;
    }

    .dark-mode .direct-chat-cyan .right>.direct-chat-text {
        background-color: #3498db;
        border-color: #3498db;
        color: #fff;
    }

    .dark-mode .direct-chat-cyan .right>.direct-chat-text::after,
    .dark-mode .direct-chat-cyan .right>.direct-chat-text::before {
        border-left-color: #3498db;
    }

    .dark-mode .direct-chat-white .right>.direct-chat-text {
        background-color: #fff;
        border-color: #fff;
        color: #1f2d3d;
    }

    .dark-mode .direct-chat-white .right>.direct-chat-text::after,
    .dark-mode .direct-chat-white .right>.direct-chat-text::before {
        border-left-color: #fff;
    }

    .dark-mode .direct-chat-gray .right>.direct-chat-text {
        background-color: #6c757d;
        border-color: #6c757d;
        color: #fff;
    }

    .dark-mode .direct-chat-gray .right>.direct-chat-text::after,
    .dark-mode .direct-chat-gray .right>.direct-chat-text::before {
        border-left-color: #6c757d;
    }

    .dark-mode .direct-chat-gray-dark .right>.direct-chat-text {
        background-color: #343a40;
        border-color: #343a40;
        color: #fff;
    }

    .dark-mode .direct-chat-gray-dark .right>.direct-chat-text::after,
    .dark-mode .direct-chat-gray-dark .right>.direct-chat-text::before {
        border-left-color: #343a40;
    }

    .card-footer {
        padding: 0.75rem 1.25rem;
        background-color: rgba(0, 0, 0, 0.03);
        border-top: 0 solid rgba(0, 0, 0, 0.125);
    }

    .card-footer:last-child {
        border-radius: 0 0 calc(0.25rem - 0) calc(0.25rem - 0);
    }

    .toolbar {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;
    }

    @keyframes live {
        0% {
            transform: scale(1, 1);
        }

        100% {
            transform: scale(3.5, 3.5);
            background-color: rgba(red, 0);
        }
    }

    @media only screen and (max-width: 500px) {
        .toolbar {
            flex-direction: column;
            padding-left: 50px;
            padding-right: 50px;
        }

        span.live-icon {
            position: absolute;
            right: 20px;
        }
    }

    @media only screen and (max-width: 350px) {}

    .img-cover {
        height: 25vh;
        object-fit: cover;
    }

    @media screen and (max-width: 600px) {
        .img-cover {
            height: 30vh;
        }
    }
</style>

<!-- Firebase -->
<!-- <script src="https://www.gstatic.com/firebasejs/8.6.8/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.6.8/firebase-analytics.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.6.8/firebase-database.js"></script> -->

<!-- chat_realtime -->
<!-- <script type="text/javascript" src="<?= base_url('assets/') ?>js/config.js"></script>
    <script type="text/javascript" src="<?= base_url('assets/') ?>js/chat_realtime.js"></script> -->
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>

<body id="page-top" style="background-image: url('<?= base_url('assets'); ?>/images/dark2/dark2-05.png');background-repeat: no-repeat;position: relative;background-size: cover;background-position: center;" oncontextmenu="return false" onkeydown="return false;" onmousedown="return false;">
    <?php include('part/nav.php') ?>
    <div id="cover-spin"></div>
    <?php
    // var_dump($_SESSION); 
    unset($_SESSION['pass_event']);
    ?>
    <header class="details">
        <div class="container mw-90 pt-5">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Detail Barang Lelang</h3>
                        </div>
                        <div class="card-body">
                            <?php if (!empty($lot_lelang)) { ?>
                                <div id="slider" class="flexslider mb-2">
                                    <img src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$lot_lelang[0]->gambar ?>" class="img-thumbnail" />
                                </div>
                                <div>
                                    <h1 class="text-uppercase pb-1">
                                        <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) {
                                            echo @$lot_lelang[0]->nama_barang_lelang . ' ' . @$lot_lelang[0]->transmisi;
                                        } else {
                                            echo @$lot_lelang[0]->nama_barang_lelang;
                                        } ?>
                                    </h1>
                                    <h2>Brand : <?= @$lot_lelang[0]->brand ?></h2>
                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                        <h2>Tahun : <?= @$lot_lelang[0]->tahun ?></h2>
                                        <h2>No Polisi : <?= @$lot_lelang[0]->no_polisi ?></h2>
                                        <h2>Grade : <?= @$lot_lelang[0]->grade ?></h2>
                                    <?php } ?>
                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) {
                                        echo '<h2 class="mr-3 fontlights float-left"><i class="fas fa-tachometer-alt" style="color: #ff4500"></i>' . @$lot_lelang[0]->odometer . '</h2>';
                                    } else {
                                        echo '<h2 class="text-uppercase pb-1">' . @$lot_lelang[0]->odometer . '</h2>';
                                    } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="card p-5 border" style="background-color: #ffffff57; border-radius: 20px;">

                        <div class="text-center text-white">
                            <h2><i class="fas fa-hand-holding-usd" style="color: #ff4500"></i></h2>
                            <hr>
                            <h1 id="title-harga" style="font-size: 3.6rem;"><?= "Rp " . number_format(((!empty($lot_lelang[0]->harga_awal)) ? $lot_lelang[0]->harga_awal : 0), 0, '', '.'); ?></h1>
                            <h3>LOT <?= sprintf("%'.05d\n", @$lot_lelang[0]->no_lot) ?></h3>
                        </div>

                        <div>
                            <div class="card direct-chat direct-chat-primary" style="box-shadow: none; background-position: center;">
                                <div class="card-header ui-sortable-handle">
                                    <h3 class="card-title">Sistem Lelang</h3>
                                </div>
                                <div class="card-body chat_area">
                                    <div id="div-chat-lelang" class="direct-chat-messages" style="height: 350px;">
                                    </div>
                                </div>
                                <?php if (!empty($_SESSION['npl'])) { ?>
                                    <div class="card-footer">
                                        <?php // var_dump(@$_SESSION['npl'], '<hr>', @$_SESSION['pass_event']); 
                                        ?>
                                        <form action="#" method="post">
                                            <input type="hidden" id="id_event" name="id_event" value="<?= @$id_event ?>">
                                            <input type="hidden" id="kode_event" name="kode_event" value="<?= @$kode_event ?>">
                                            <input type="hidden" id="email" name="email" value="Online Bidder">
                                            <input type="hidden" id="start_value" name="start_value" value="<?= @$lot_lelang[0]->harga_awal; ?>">
                                            <input type="hidden" id="id_lot" name="id_lot" value="<?= @$lot_lelang[0]->id_lot; ?>">
                                            <input type="hidden" id="curent_bid" name="curent_bid" value="<?= @$lot_lelang[0]->harga_awal; ?>">
                                            <input type="hidden" id="allow_bid" name="allow_bid" value="<?= (!empty($_SESSION['pass_event'])) ? "true" : "false"; ?>">
                                            <input type="hidden" id="self_bid" name="self_bid" value="false">
                                            <input type="hidden" id="is_start" name="is_start" value="false">
                                            <div class="input-group justify-content-center">
                                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success pb-2">
                                                    <input type="checkbox" class="custom-control-input" id="lock-bid-check" name="lock-bid-check" value="true" onclick="lock_bid();">
                                                    <label class="custom-control-label" for="lock-bid-check">Kunci untuk Bidding</label>
                                                </div>
                                                <hr>
                                                <span id="up-bid" class="btn btn-success btn-danger mx-1"><i class="fas fa-arrow-up"></i> Bidding</span>
                                            </div>
                                        </form>

                                        <div class="row begin-countdown">
                                            <div class="col-md-12 text-center">
                                                <progress value="<?= @$setting->bid_time ?>" max="<?= @$setting->bid_time ?>" id="pageBeginCountdown"></progress>
                                                <p> Time out in <span id="pageBeginCountdownText"><?= @$setting->bid_time ?> </span> seconds</p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <input type="hidden" id="id_event" name="id_event" value="<?= @$id_event ?>">
                                    <input type="hidden" id="kode_event" name="kode_event" value="<?= @$kode_event ?>">
                                    <input type="hidden" id="email" name="email" value="<?= @$email ?>">
                                    <input type="hidden" id="start_value" name="start_value" value="<?= @$lot_lelang[0]->harga_awal; ?>">
                                    <input type="hidden" id="id_lot" name="id_lot" value="<?= @$lot_lelang[0]->id_lot; ?>">
                                    <input type="hidden" id="curent_bid" name="curent_bid" value="<?= @$lot_lelang[0]->harga_awal; ?>">
                                    <input type="hidden" id="allow_bid" name="allow_bid" value="<?= (!empty($_SESSION['pass_event'])) ? "true" : "false"; ?>">
                                    <input type="hidden" id="self_bid" name="self_bid" value="false">
                                <?php } ?>
                            </div>
                            <!--/.direct-chat -->

                        </div>

                        <div>
                            <!-- <div class="mx-auto text-center">
                                <a href="<?= base_url(); ?>/lelang"><span class="btn btn-primary btn-lg btn-block" onclick="checkAuth();" style="border-radius: 3rem;">Bidding</span></a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
    </header>

    <section class="portfolio" id="about" style="padding: 3rem 0;">
        <div class="container mw-90">
            <div class="row">
                <div class="col-12 col-lg-6 pb-4">
                    <div class="card-body p-2 border" style="background-color: #00000073; border-radius: 10px;">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <ul class="nav" id="custom-tabs-four-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content_tab_1" data-toggle="pill" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true" title="Details Kendaraan">
                                        <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) {
                                            echo "Details Kendaraan";
                                        } else {
                                            echo "Detail Barang";
                                        } ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="content_tab_2" data-toggle="pill" href="#tab_2" role="tab" aria-controls="tab_2" aria-selected="false" title="Details Kondisi">Details Kondisi</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="portfolio pt-0" id="detail-kendaraan" style="padding: 3rem 0;">
        <div class="container mw-90">
            <div class="row">
                <div class="col-12 col-lg-12 pb-4">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade active show" id="tab_1" role="tabpanel" aria-labelledby="content_tab_1">
                            <div class="card flex-fill border" style="background-color: #00000073; border-radius: 10px;">
                                <div class="card-body text-white">
                                    <div class="row">
                                        <div class="col-md-6" style="border-right: 1px solid white">
                                            <table class="table table-borderless text-white">
                                                <tbody>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->no_polisi)) { ?>
                                                                <th>Nomor Polisi</th>
                                                                <td><?= @$lot_lelang[0]->no_polisi ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <?php if (!empty(@$lot_lelang[0]->brand)) { ?>
                                                            <th>Brand</th>
                                                            <td><?= @$lot_lelang[0]->brand ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <?php if (!empty(@$lot_lelang[0]->nama_barang_lelang)) { ?>
                                                            <th>Seri</th>
                                                            <td><?= @$lot_lelang[0]->nama_barang_lelang ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <!-- <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->kapasitas_kendaraan)) { ?>
                                                                <th>Kapasitas Kendaraan</th>
                                                                <td><?= @$lot_lelang[0]->kapasitas_kendaraan ?></td>
                                                            <?php } ?>
                                                        </tr> -->
                                                    <?php } ?>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->tipe_model)) { ?>
                                                                <th>Model Kendaraan</th>
                                                                <td><?= @$lot_lelang[0]->tipe_model ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->transmisi)) { ?>
                                                                <th>Transmisi</th>
                                                                <td><?= @$lot_lelang[0]->transmisi ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <?php if (!empty(@$lot_lelang[0]->tahun)) { ?>
                                                            <th>Tahun</th>
                                                            <td><?= @$lot_lelang[0]->tahun ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table table-borderless text-white">
                                                <tbody>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->no_rangka)) { ?>
                                                                <th>Nomor Rangka</th>
                                                                <td><?= @$lot_lelang[0]->no_rangka ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->no_mesin)) { ?>
                                                                <th>Nomor Mesin</th>
                                                                <td><?= @$lot_lelang[0]->no_mesin ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->odometer)) { ?>
                                                                <th>Kilometer</th>
                                                                <td><?= @$lot_lelang[0]->odometer ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->bahan_bakar)) { ?>
                                                                <th>Bahan Bakar</th>
                                                                <td><?= @$lot_lelang[0]->bahan_bakar ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->warna)) { ?>
                                                                <th>Warna Fisik</th>
                                                                <td><?= @$lot_lelang[0]->warna ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->valid_stnk)) { ?>
                                                                <th>Tanggal STNK</th>
                                                                <td><?= @$lot_lelang[0]->valid_stnk ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_2" role="tabpane2" aria-labelledby="content_tab_2">
                            <div class="card flex-fill border" style="background-color: #00000073; border-radius: 10px;">
                                <div class="card-body text-white">
                                    <div class="row">
                                        <div class="col-md-6" style="border-right: 1px solid white">
                                            <table class="table table-borderless text-white">
                                                <tbody>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <th>STNK</th>
                                                            <td><?= $foto_stnk = ((@$lot_lelang[0]->stnk == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>BPKB</th>
                                                            <td><?= $foto_bpkb = ((@$lot_lelang[0]->bpkb == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>FAKTUR</th>
                                                            <td><?= $foto_faktur = ((@$lot_lelang[0]->faktur == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>KTP PEMILIK</th>
                                                            <td><?= $foto_ktp_pemilik = ((@$lot_lelang[0]->ktp_pemilik == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>KWITANSI</th>
                                                            <td><?= $foto_kwitansi = ((@$lot_lelang[0]->kwitansi == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>SPH</th>
                                                            <td><?= $foto_ktp_pemilik = ((@$lot_lelang[0]->sph == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>KIR</th>
                                                            <td><?= $foto_kwitansi = ((@$lot_lelang[0]->kir == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table table-borderless text-white">
                                                <tbody>
                                                    <tr>
                                                        <?php if (!empty(@$lot_lelang[0]->grade)) { ?>
                                                            <th>Grade Utama</th>
                                                            <td><?= @$lot_lelang[0]->grade ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->grade_mesin)) { ?>
                                                                <th>Grade Mesin</th>
                                                                <td><?= @$lot_lelang[0]->grade_mesin ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->grade_interior)) { ?>
                                                                <th>Grade Interior</th>
                                                                <td><?= @$lot_lelang[0]->grade_interior ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->grade_exterior)) { ?>
                                                                <th>Grade Exterior</th>
                                                                <td><?= @$lot_lelang[0]->grade_exterior ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="portfolio pt-0" id="about" style="padding: 3rem 0;">
        <div class="container mh-100 mw-90">
            <div class="row">
                <div class="col-12 col-lg-12 pb-4">
                    <div class="card flex-fill border" style="background-color: #00000073; border-radius: 10px;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-white">
                                    <h2 class="font-weight-bold fontbold text-uppercase">Deskripsi</h2>
                                    <p><?= @$lot_lelang[0]->deskripsi ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="row pb-5">
        <div class="container">
            <div class="mx-auto text-center pb-3">
                <a href="<?= base_url(); ?>/lelang"><span class="btn btn-primary btn-xl" onclick="checkAuth();">Back</span></a>
            </div>
        </div>
    </div>

    <!-- ======= Lup Lup ======= -->

    <?php include('part/footer.php') ?>
    <script>
        const email = '<?= $_SESSION['email'] ?>';
        const maskEmail = (email = '') => {
            const [name, domain] = email.split('@');
            const {
                length: len
            } = name;
            const maskedName = name[0] + name[1] + '***' + name[len - 1];
            const maskedEmail = maskedName + '@' + domain;
            return maskedEmail;
        };
        $('#email').val(maskEmail);
        var countdownTimer;
        var timeleft = <?= @$setting->bid_time; ?>;
        <?php if (!empty($_SESSION['pass_event'])) { ?>
            var startStop = 1;
        <?php } else { ?>
            var startStop = 0;
        <?php } ?>
        var myChanel = 'channel-' + $('#kode_event').val(),
            myEvent = 'lelang_event';
        $(document).ready(function() {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 150,
                itemMargin: 5,
                asNavFor: '#slider'
            });
            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel"
            });
            // myEvent		= '<?= @$kode_event ?>';
            Pusher.log = function(message) {
                if (window.console && window.console.log) {
                    // window.console.log(message);
                }
            };
            // console.log(myChanel);
            var pusher = new Pusher('<?= $this->config->item('pusher_app_key'); ?>', {
                cluster: 'ap1'
            });
            var channel = pusher.subscribe(myChanel);

            channel.bind('lelang_event', function(data) {
                // channel.bind('<?= @$kode_event ?>', function(data) {
                timeleft = <?= @$setting->bid_time; ?>;
                clearInterval(countdownTimer);
                // ProgressCountdown(<?= @$setting->bid_time ?>, 'pageBeginCountdown', 'pageBeginCountdownText').then(value => Swal.fire(`Page has started: ${value}.`)); 
                ProgressCountdown('pageBeginCountdown', 'pageBeginCountdownText').then(value => startStopBid());
                sendmessage(data);
            });

            channel.bind('appendponline', function(data) {
                appendponline(data);
            });
            channel.bind('set_session_pass_event', function(data) {
                // console.log("set_session_pass_event",data);
                $.ajax({
                    url: "<?= base_url(); ?>api/set_session_pass_event/",
                    type: 'POST',
                    dataType: "JSON",
                    data: {
                        pass_event: data.pass_event,
                    },
                    success: function(data) {
                        // $('#message').val('');
                        // console.log(data);
                        // Toggle BTN
                        if (data.status) {
                            ProgressCountdown('pageBeginCountdown', 'pageBeginCountdownText').then(value => startStopBid());
                            $("#up-bid").removeClass("btn-danger");
                            $('#allow_bid').val("true");
                        } else {
                            $("#up-bid").addClass("btn-danger");
                            $('#allow_bid').val("false");

                            timeleft = <?= @$setting->bid_time; ?>;
                            clearInterval(countdownTimer);
                            if ($(".direct-chat-name").last().text() == $("#email").val()) {
                                Swal.fire({
                                    title: 'Selamat Anda Menang!',
                                    text: 'Lelang dimenangkan oleh ' + $(".direct-chat-name").last().text() + ' dengan harga ' + $('#title-harga').text() + ', Silahkan lunasi sisa pembayaran sebelum jatuh tempo.',
                                    icon: 'success',
                                });
                            } else {
                                Swal.fire({
                                    title: 'Lelang telah selesai!',
                                    text: 'Lelang dimenangkan oleh ' + $(".direct-chat-name").last().text() + ' dengan harga ' + $('#title-harga').text(),
                                    icon: 'success',
                                });
                            }
                            // setTimeout(() => {
                            //   window.location = '<?= base_url('front/lelang/'); ?>';
                            // }, 3 * 1000);
                        }
                    },
                    error: function(err) {
                        console.log(err);
                    }
                });
            });
            channel.bind('next_bid', function(data) {
                window.location = '<?= base_url('front/detaillelang/') ?>' + data;
            });

            function appendponline(data) {
                html = '';
                html += '<li class="left clearfix">';
                html += ' <span class="chat-img pull-left">';
                html += ' <img loading="lazy" src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle">';
                html += ' </span>';
                html += '<div class="chat-body clearfix">';
                html += '<div class="header_sec">';
                html += ' <strong class="primary-font">' + data.username + '</strong>';
                html += '</div>';
                html += '</div>';
                html += '</li>';
                $('#appendponline').prepend(html);
            }

            function sendmessage(data, autoscroll = true) {
                // var ses_id = <?= $this->session->userdata('id'); ?>;
                var ses_id = document.getElementById('email').value;
                let harga_bid = 'Rp ' + data.harga_bidding.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                $('#title-harga').html(harga_bid);
                $('#curent_bid').val(data.harga_bidding);
                $('#is_start').val('true');
                // console.log(data.email == ses_id,data,ses_id,harga_bid);
                if (data.email == ses_id) {
                    html = '';
                    html += '<div class="direct-chat-msg right"><div class="direct-chat-infos clearfix">';
                    html += '<span class="direct-chat-name float-right">' + data.email + '</span>';
                    html += '<span class="direct-chat-timestamp float-left">' + data.date + '</span></div>';
                    html += '<img loading="lazy" class="direct-chat-img" src="<?= base_url() ?>assets/images/profile_picture.png" alt="message user image">';
                    html += '<div class="direct-chat-text">' + harga_bid + '</div></div>';
                    $('#div-chat-lelang').prepend(html);
                    if (autoscroll) {
                        // $("html, body .chat_area").animate({ scrollTop: document.getElementById("div-chat-lelang").scrollHeight }, 1000);
                    }
                    $('#message').val("");
                    $('#self_bid').val('true');
                } else {
                    html = '';
                    html += '<div class="direct-chat-msg "><div class="direct-chat-infos clearfix">';
                    html += '<span class="direct-chat-name float-left">' + data.email + '</span>';
                    html += '<span class="direct-chat-timestamp float-right">' + data.date + '</span></div>';
                    html += '<img loading="lazy" class="direct-chat-img" src="<?= base_url() ?>assets/images/profile_picture.png" alt="message user image">';
                    html += '<div class="direct-chat-text">' + harga_bid + '</div></div>';
                    $('#div-chat-lelang').prepend(html);
                    if (autoscroll) {
                        // $("html, body .chat_area").animate({ scrollTop: document.getElementById("div-chat-lelang").scrollHeight }, 1000);
                    }
                    $('#self_bid').val('false');
                }
                // $("html, body .chat_area").animate({ scrollTop: $(document).height() }, 1000);
            }

            function loadData() {
                $.ajax({
                    url: "<?= base_url(); ?>api/getChat/",
                    type: 'GET',
                    dataType: "JSON",
                    data: {
                        channel: myChanel,
                        event: myEvent,
                        kode_event: $('#kode_event').val()
                    },
                    beforeSend: function() {
                        $("div.spanner").addClass("show");
                        $("div.overlay").addClass("show");
                    },
                    success: function(data) {
                        // console.log(data);
                        data.forEach(el => {
                            // console.log(el);                                
                            if (el.id_lot == $('#id_lot').val()) {
                                $('#curent_bid').val(el.harga_bidding);
                                sendmessage(el, false);
                            }
                        });
                    },
                    complete: function() {
                        setTimeout(function() {
                            $("div.spanner").removeClass("show");
                            $("div.overlay").removeClass("show");
                            // $("html, body .chat_area").animate({ scrollTop: document.getElementById("div-chat-lelang").scrollHeight }, 1000);
                        }, 5 * 1000);
                    },
                    error: function(err) {
                        console.log(err);
                    }
                });
            }

            $('#up-bid').click(function() {
                <?php if (!empty($_SESSION['npl'])) { ?>
                    if ($('#allow_bid').val() !== "true") {
                        return false;
                    }

                    if ($('#lock-bid-check').val() !== "true") {
                        Swal.fire('Anda mengunci tombol bidding, silahkan uncheck "Kunci untuk Bidding"!');
                        return false;
                    }
                    if ($('#self_bid').val() == 'true') {
                        Swal.fire('Tunggu peserta lain untuk melakukan bidding!');
                        return false;
                    }
                    var harga_bidding;
                    if ($('#is_start').val() == 'false') {
                        harga_bidding = parseInt($('#curent_bid').val());
                    } else {
                        harga_bidding = parseInt($('#curent_bid').val()) + 500000;
                    }
                    let btnSend = $('#up-bid'),
                        // letters = /^[0-9a-zA-Z!@#\$%\^\&*\)\(+=,<.>/?;:'"[{}|\]\_-]+$/;
                        letters = /^\s+$/;
                    $('#curent_bid').val(harga_bidding);
                    // console.log(message,message.match(letters));
                    if (!btnSend[0].classList.value.match('disabled')) {
                        // if ((!harga_bidding.match(letters)) && (harga_bidding !== "")) {
                        $.ajax({
                            url: "<?= base_url(); ?>api/chatsend/",
                            type: 'POST',
                            data: {
                                kode_event: $('#kode_event').val(),
                                email: $('#email').val(),
                                id_lot: $('#id_lot').val(),
                                id_event: $('#id_event').val(),
                                harga_bidding: harga_bidding,
                            },
                            beforeSend: function() {
                                btnSend.addClass("disabled");
                            },
                            success: function() {
                                // $('#message').val('');
                                $('#self_bid').val('true');
                                $('#is_start').val('true');
                            },
                            complete: function() {
                                btnSend.removeClass("disabled");
                            },
                            error: function(err) {
                                console.log(err);
                            }
                        });
                        // } else {
                        //     $('#message').val('');
                        // }				
                    }
                <?php } else { ?>
                    Swal.fire('Anda belum memiliki NPL, Silahkan beli NPL terlebih dahulu!');
                <?php } ?>
            });

            loadData();
        });

        function startStopBid() {
            startStop++
            // Send AJAX
            $.ajax({
                url: "<?= base_url(); ?>api/start_stop_bid/",
                type: 'POST',
                dataType: "JSON",
                data: {
                    id_event: $('#id_event').val(),
                    channel: myChanel,
                },
                beforeSend: function() {
                    // btnSend.addClass("disabled");
                },
                success: function(data) {
                    // $('#message').val('');
                },
                complete: function() {
                    // btnSend.removeClass("disabled");

                },
                error: function(err) {
                    console.log(err);
                }
            });
        }

        function nextBid(url) {
            // Send AJAX
            $.ajax({
                url: "<?= base_url(); ?>api/next_bid/",
                type: 'POST',
                dataType: "JSON",
                data: {
                    url: url,
                    last_lot: $('#id_lot').val(),
                    channel: myChanel,
                    kode_event: $('#kode_event').val(),
                    <?php if (!empty($_SESSION['npl'])) { ?>
                        npl: '<?= @$_SESSION['npl'][0] ?>',
                    <?php } ?>
                },
                beforeSend: function() {
                    // btnSend.addClass("disabled");
                },
                success: function(data) {
                    // $('#message').val('');
                },
                complete: function() {
                    // btnSend.removeClass("disabled");

                },
                error: function(err) {
                    console.log(err);
                }
            });
        }

        function ProgressCountdown(bar, text) {
            return new Promise((resolve, reject) => {
                countdownTimer = setInterval(() => {
                    timeleft--;
                    document.getElementById(bar).value = timeleft;
                    document.getElementById(text).textContent = timeleft;

                    if (timeleft <= 0) {
                        clearInterval(countdownTimer);
                        resolve(true);
                    }
                }, 1000);
            });
        }

        function lock_bid() {
            if ($('#lock-bid-check').val() == 'false') {
                $('#lock-bid-check').val('true');
            } else {
                $('#lock-bid-check').val('false');

            }
            // console.log($('#lock-bid-check').val());
        }
    </script>

    <!-- <script type="text/javascript">

          (function () {
            const second = 1000,
                  minute = second * 60,
                  hour = minute * 60,
                  day = hour * 24;

            let birthday = "Sep 30, 2021 00:00:00",
                countDown = new Date(birthday).getTime(),
                x = setInterval(function() {    

                  let now = new Date().getTime(),
                      distance = countDown - now;

                  document.getElementById("days").innerText = Math.floor(distance / (day)),
                    document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                    document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                    document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

                  //do something later when date is reached
                  if (distance < 0) {
                    let headline = document.getElementById("headline"),
                        countdown = document.getElementById("countdown"),
                        content = document.getElementById("content");

                    headline.innerText = "It's my birthday!";
                    countdown.style.display = "none";
                    content.style.display = "block";

                    clearInterval(x);
                  }
                  //seconds
                }, 0)
            }());
          
        </script> -->

</body>

</html>
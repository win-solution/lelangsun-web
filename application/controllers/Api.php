<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->model_squrity->getsqurity(); 
        $this->load->library('ci_pusher');
    }

    public function index()
    {
    }

    public function start_stop_bid()
    {
        if (!empty($_SESSION['login_admin'])) {
            $data = array();
            if (empty($_SESSION['pass_event'])) {
                // Create pass_event by event code
                // Start lelang
                $data['pass_event'] = sha1(date('YmdHis') . random_code());
                $event['data']['pass_event'] = $data['pass_event'];
                $event['data']['status'] = 1;
                $event['data']['updated_at'] = date('Y-m-d H:i:s');
                $event['data']['updated_by'] = $_SESSION['id_user_admin'];
                $event['table']        = "t_event";
                $event['where'][0]     = array('id_event', $_POST['id_event']);
                $this->model_global->updateData($event);
            } else {
                // Stop lelang
            }
            // Triget create session login_admin ke semua user yang subscribe ke chanel
            $pusher = $this->ci_pusher->get_pusher();
            $event = $pusher->trigger($_POST['channel'], 'set_session_pass_event', $data);
        }
    }

    public function next_bid()
    {
        list($id_event, $url_parameter) = explode("?", $_POST['url']);
        // var_dump($id_event,"<hr>");
        // var_dump($url_parameter,"<hr>");
        // var_dump($_POST,"<hr>");
        // jika ada data bid maka ubah status_lot di t_lot menjadi 2 (lelang)             
        $bidding_lot['select'] = "l.id_lot, b.*, bi.id_bidding, bi.id_lot, bi.id_peserta, bi.npl, bi.harga_bidding";
        $bidding_lot['from'] = "t_bidding as bi";
        // $bidding_lot['join'][]	= array("t_lot as l",'l.id_lot = bi.id_lot AND l.status = 0 AND l.status_lot = 2');
        $bidding_lot['join'][]    = array("t_lot as l", 'l.id_lot = bi.id_lot');
        $bidding_lot['join'][]    = array("m_barang_lelang as b", "b.id_barang_lelang = l.id_barang_lelang AND b.status = 1");
        // $bidding_lot['group_by'] = "kode_event";
        $bidding_lot['limit'] = 1;
        $bidding_lot['order'][] = array('harga_bidding', 'DESC');
        $bidding_lot['where'] = "bi.id_event = " . $id_event . " AND bi.id_lot = " . $_POST['last_lot'];
        $bidding_lot_detail = $this->model_global->getData($bidding_lot);
        // var_dump($bidding_lot_detail,"<hr>");
        // die;
        if (!empty($_SESSION['id_user_admin'])) {
            if (!empty($bidding_lot_detail[0])) {
                // Ubah status_lot di t_lot menjadi 2
                $event['data']['status'] = 0;
                $event['data']['status_lot'] = 2;
                $event['data']['updated_at'] = date('Y-m-d H:i:s');
                $event['data']['updated_by'] = $_SESSION['id_user_admin'];
                $event['table']        = "t_lot";
                $event['where'][0]     = array('id_lot', @$bidding_lot_detail[0]->id_lot);
                $this->model_global->updateData($event);

                // Set pemenang t_pemenang_lelang
                $pemenang_lelang['data']['id_bidding'] = @$bidding_lot_detail[0]->id_bidding;
                $pemenang_lelang['data']['npl'] = (!empty($bidding_lot_detail[0]->npl)) ? $bidding_lot_detail[0]->npl : "";
                $pemenang_lelang['data']['created_at'] = date('Y-m-d H:i:s');
                $pemenang_lelang['data']['created_by'] = $_SESSION['id_user_admin'];
                $pemenang_lelang['data']['status'] = 1;
                // var_dump($event);
                // die();
                $pemenang_lelang['table']        = "t_pemenang_lelang";
                $this->model_global->addData($pemenang_lelang);
                if ($bidding_lot_detail[0]->id_peserta !== 0) {
                    $notifikasi['data']['pengirim'] = 0;
                    $notifikasi['data']['penerima'] = $bidding_lot_detail[0]->id_peserta;
                    $notifikasi['data']['type_user'] = "peserta";
                    $notifikasi['data']['type'] = "menang";
                    $notifikasi['data']['judul'] = "Pemenang Barang Lelang";
                    $notifikasi['data']['pesan'] = "Segera lunasi pembayaran pelunasan barang lelang " . $bidding_lot_detail[0]->nama_barang . "dengan harga akhir " . $bidding_lot_detail[0]->harga_bidding . "";
                    $notifikasi['data']['is_read'] = 0;
                    $notifikasi['data']['deskripsi'] = json_encode($bidding_lot_detail);
                    $notifikasi['data']['created_at'] = date('Y-m-d H:i:s');
                    set_notifikasi($notifikasi['data']);
                }
                if (!empty($bidding_lot_detail[0]->npl)) {
                    // update status = 0 m_npl berdasarkan npl
                    $update_npl['data']['status_npl'] = 0;
                    $update_npl['data']['updated_at'] = date('Y-m-d H:i:s');
                    $update_npl['data']['updated_by'] = $_SESSION['id_peserta'];
                    $update_npl['table']        = "m_npl";
                    $update_npl['where'][0]     = array('id_npl', $bidding_lot_detail[0]->npl);
                    $this->model_global->updateData($update_npl);
                }
            }
        } else {
            // jika yang menang peserta, nonaktifkan NPL dan hapus dari session
            if (!empty($bidding_lot_detail[0])) {
                if ($bidding_lot_detail[0]->id_peserta == $_SESSION['id_peserta']) {
                    foreach ($_SESSION['npl'] as $key => $val) {
                        if ($val == $bidding_lot_detail[0]->npl) {
                            // var_dump('Sama nih di '.$key.' sama dengan '.$val,"<hr>");
                            // hapus session npl
                            unset($_SESSION['npl'][$key]);
                            $new_session_npl = array_values($_SESSION['npl']);
                            // var_dump($new_session_npl,"<hr>");
                            // update status=0 m_npl berdasarkan npl						
                            $update_npl['data']['status_npl'] = 0;
                            $update_npl['data']['updated_at'] = date('Y-m-d H:i:s');
                            $update_npl['data']['updated_by'] = $_SESSION['id_peserta'];
                            $update_npl['table']        = "m_npl";
                            $update_npl['where'][0]     = array('npl', $val);
                            $this->model_global->updateData($update_npl);
                            // recreate session npl
                            $this->session->unset_userdata('npl');
                            $newdata = array(
                                'npl'            => $new_session_npl,
                            );
                            $this->session->set_userdata($newdata);
                            break;
                        }
                    }
                }
            }
        }
        // die;
        // Triget create session login_admin ke semua user yang subscribe ke chanel
        $pusher = $this->ci_pusher->get_pusher();
        $pusher->trigger($_POST['channel'], 'next_bid', $_POST['url']);
    }

    public function close_bid($id)
    {
        $bidding_lot = array();
        $bidding_lot['select'] = "l.id_lot, b.*, bi.id_bidding, bi.id_lot, bi.id_peserta, bi.npl, bi.harga_bidding";
        $bidding_lot['from'] = "t_bidding as bi";
        // $bidding_lot['join'][]	= array("t_lot as l",'l.id_lot = bi.id_lot AND l.status = 0 AND l.status_lot = 2');
        $bidding_lot['join'][]    = array("t_lot as l", 'l.id_lot = bi.id_lot');
        $bidding_lot['join'][]    = array("m_barang_lelang as b", "b.id_barang_lelang = l.id_barang_lelang AND b.status = 1");
        // $bidding_lot['join'][]	= array("m_peserta as p","p.id_peserta = bi.id_peserta AND p.status = 1");	
        // $bidding_lot['group_by'] = "kode_event";
        $bidding_lot['limit'] = 1;
        $bidding_lot['order'][] = array('harga_bidding', 'DESC');
        $bidding_lot['where'] = "bi.id_event = " . $id . " AND bi.id_lot = " . $_GET['id_lot'];

        $bidding_lot_detail = $this->model_global->getData($bidding_lot);
        if (!empty($bidding_lot_detail)) {
            // Ubah status_lot di t_lot menjadi 2
            $event = $pemenang_lelang = array();
            $event['data']['status'] = 0;
            $event['data']['status_lot'] = 2;
            $event['data']['updated_at'] = date('Y-m-d H:i:s');
            $event['data']['updated_by'] = $_SESSION['id_user_admin'];
            $event['table']        = "t_lot";
            $event['where'][0]     = array('id_lot', @$bidding_lot_detail[0]->id_lot);
            $this->model_global->updateData($event);

            $pemenang_lelang['data']['id_bidding'] = @$bidding_lot_detail[0]->id_bidding;
            $pemenang_lelang['data']['npl'] = (!empty($bidding_lot_detail[0]->npl)) ? $bidding_lot_detail[0]->npl : "";
            $pemenang_lelang['data']['created_at'] = date('Y-m-d H:i:s');
            $pemenang_lelang['data']['created_by'] = $_SESSION['id_user_admin'];
            $pemenang_lelang['data']['status'] = 1;
            // var_dump($event);
            // die();
            $pemenang_lelang['table']        = "t_pemenang_lelang";
            $this->model_global->addData($pemenang_lelang);

            if ($bidding_lot_detail[0]->id_peserta !== 0) {
                $notifikasi['data']['pengirim'] = 0;
                $notifikasi['data']['penerima'] = $bidding_lot_detail[0]->id_peserta;
                $notifikasi['data']['type_user'] = "peserta";
                $notifikasi['data']['type'] = "menang";
                $notifikasi['data']['judul'] = "Pemenang Barang Lelang";
                // $notifikasi['data']['pesan'] = "Segera lunasi pembayaran pelunasan barang lelang "."dengan harga akhir "."";
                $notifikasi['data']['pesan'] = "Segera lunasi pembayaran pelunasan barang lelang " . $bidding_lot_detail[0]->nama_barang . "dengan harga akhir " . $bidding_lot_detail[0]->harga_bidding;
                $notifikasi['data']['is_read'] = 0;
                $notifikasi['data']['deskripsi'] = json_encode($bidding_lot_detail);
                $notifikasi['data']['created_at'] = date('Y-m-d H:i:s');
                set_notifikasi($notifikasi['data']);
            }
        }
        $event = $lot_event = array();
        $event['data']['status'] = 0;
        $event['data']['updated_at'] = date('Y-m-d H:i:s');
        $event['data']['updated_by'] = $_SESSION['id_user_admin'];
        $event['table']        = "t_event";
        $event['where'][0]     = array('id_event', $id);
        $this->model_global->updateData($event);

        // nonaktifkan status lot yang barangnya tidak laku
        $lot_event['data']['status'] = 0;
        $lot_event['table']  = "t_lot";
        $lot_event['where']  = "id_event = " . $id . " AND status_lot=1 AND status=1";
        $this->model_global->updateData($lot_event);

        // Refund semua NPL yang tidak terpakai
        $npl_event['data']['status_npl'] = 0;
        $npl_event['data']['status'] = 0;
        $npl_event['table']  = "m_npl";
        $npl_event['where']  = "id_event = " . $id . " AND status_npl=1 AND status=1";
        $this->model_global->updateData($npl_event);

        // Triget create session login_admin ke semua user yang subscribe ke chanel
        if (!empty($_GET['channel'])) {
            $pusher = $this->ci_pusher->get_pusher();
            $event = $pusher->trigger($_GET['channel'], 'set_session_pass_event', $_SESSION['pass_event']);
        }
        redirect('admin/event');
    }

    public function set_session_pass_event()
    {
        if (empty($_SESSION['pass_event'])) {
            // set session pass_event
            $this->session->set_userdata('pass_event', $_POST['pass_event']);
            // echo "Create session pass_event";
            echo json_encode(array('status' => true, 'msg' => "Create session pass_event"));
        } else {
            // destroy session pass_event
            $this->session->unset_userdata('pass_event');
            // echo "Destroy session pass_event";
            echo json_encode(array('status' => false, 'msg' => "Destroy session pass_event"));
        }
    }

    public function chatsend()
    {
        // Check pass_event
        if (empty($_SESSION['pass_event'])) {
            echo "Tidak punya pass_event";
            die;
        }
        $pusher = $this->ci_pusher->get_pusher();
        // $data['channel'] = 'my-channel';
        // $data['channel'] = 'channel-'.$_POST['kode_event'].'-'.$_POST['id_lot'];
        $data['channel'] = 'channel-' . $_POST['kode_event'];
        $data['event'] = 'lelang_event';
        $data['kode_event'] = $_POST['kode_event'];
        $data['email'] = $_POST['email'];
        $data['id_lot'] = $_POST['id_lot'];
        $data['harga_bidding'] = $_POST['harga_bidding'];
        $data['date'] = date('d M Y H:i:s A');
        $data['id_user'] = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('user_name');

        // $event = $pusher->trigger('chatglobal', 'my_event', $data);		
        $event = $pusher->trigger($data['channel'], $data['event'], $data);
        // var_dump($event);die;
        if ($event) {
            // unset($data['username']);
            // $data['date'] = "";
            // $this->chat_model->add_chat($data);
            $bidding['data']['kode_event'] = $_POST['kode_event'];
            $bidding['data']['email'] = $_POST['email'];
            $bidding['data']['id_event'] = $_POST['id_event'];
            if (!empty($_SESSION['id_peserta'])) {
                $bidding['data']['id_peserta'] = $_SESSION['id_peserta'];
            }
            if (!empty($_SESSION['npl'])) {
                $bidding['data']['npl'] = $_SESSION['npl'][0];
            }
            $bidding['data']['id_lot'] = $_POST['id_lot'];
            $bidding['data']['harga_bidding'] =  $_POST['harga_bidding'];
            $bidding['data']['waktu'] = date('d M Y H:i:s A');
            $bidding['data']['created_at'] = date('Y-m-d H:i:s');
            $bidding['data']['created_by'] = $_SESSION['id_user_admin'];
            $bidding['data']['status'] = 1;
            $bidding['table']        = "t_bidding";
            $this->model_global->addData($bidding);
        }
    }

    public function getChat()
    {
        // var_dump($_GET);
        // die;
        $bidding_detail['select']    = "k.*, k.waktu as date";
        $bidding_detail['table']    = "t_bidding as k";
        $bidding_detail['where']    = "k.kode_event = '" . $_GET['kode_event'] . "' and k.status = 1";

        $result     = $this->model_global->getData($bidding_detail);
        // var_dump(json_encode($result));
        echo json_encode($result);
    }

    // public function index() {
    //     // include_once('php/chat_realtime.php');
    //     $db_config = $this->db;
    //     // var_dump($db_config->database);die;
    //     // Image dir
    //     $imageDir = "image";

    //     // Replace with: your database account
    //     // $username 	= $db_config['username'];
    //     // $password 	= $db_config['username'];
    //     // $host  		= $db_config['username'];
    //     // $dbName    	= $db_config['username'];

    //     include "./application/libraries/chat_realtime.php";
    //     $chat = new Chat_realtime($db_config->database, $db_config->hostname, $db_config->username, $db_config->password, $imageDir);

    //     $data = array();

    //     if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
    //         if(!empty($_POST['data'])){

    //             if($_POST['data'] == 'cek'){
    //                 // if(isset($_SESSION['user']) && isset($_SESSION['avatar'])){
    //                 // if(isset($_SESSION['user'])){
    //                     $data['status'] = 'success';
    //                     $data['user'] 	= random_code(10,'alphabet').'@mail.com';
    //                     $data['avatar'] = base_url('assets/images/profile_picture.png');
    //                 // }else{
    //                 //     $data['status'] = 'error';
    //                 // }
    //             // }else if($_POST['data'] == 'login'){
    //             //     if(!empty($_POST['name']) && !empty($_POST['avatar'])){
    //             //         $data = $chat->user_login($_POST['name'], $_POST['avatar']);
    //             //         if($data['status'] == 'success'){
    //             //             $_SESSION['user'] = $_POST['name'];
    //             //             $_SESSION['avatar'] = $_POST['avatar'];
    //             //         }
    //             //     }
    //             }else if($_POST['data'] == 'message'){
    //                 if(!empty($_POST['ke']) && !empty($_POST['tipe'])){
    //                     $data = $chat->get_message($_POST['tipe'], $_POST['ke'], $_SESSION['user']);
    //                 }			
    //             // }else if($_POST['data'] == 'user'){
    //             //     $data = $chat->get_user($_SESSION['user']);
    //             }else if($_POST['data'] == 'send'){
    //                 if(isset($_SESSION['user']) && !empty($_POST['ke']) && !empty($_POST['date']) && !empty($_POST['avatar']) && !empty($_POST['tipe']) && isset($_POST['message']) && isset($_POST['images'])){
    //                     $images = json_decode($_POST['images']);
    //                     if(!empty($_POST['message']) && count($images) < 1){
    //                         $data = $chat->send_message($_SESSION['user'], $_POST['ke'], $_POST['message'], "", $_POST['date'], $_POST['avatar'], $_POST['tipe']);
    //                     }else if(!empty($_POST['message']) && count($images) > 0){
    //                         $h = 0;
    //                         foreach($images as $image){
    //                             $n = $chat->arrayToBinaryString($image->binary);
    //                             $chat->createImg($n, $image->name, 'image/png');
    //                             if($h == 0){
    //                                 $data = $chat->send_message($_SESSION['user'], $_POST['ke'], $_POST['message'], $image->name, $_POST['date'], $_POST['avatar'], $_POST['tipe']);
    //                             }else{	
    //                                 $data = $chat->send_message($_SESSION['user'], $_POST['ke'], "", $image->name, $_POST['date'], $_POST['avatar'], $_POST['tipe']);
    //                             }
    //                             $h++;
    //                         }
    //                     }else if(empty($_POST['message']) && count($images) > 0){
    //                         foreach($images as $image){
    //                             $n = $chat->arrayToBinaryString($image->binary);
    //                             $chat->createImg($n, $image->name, 'image/png');
    //                             $data = $chat->send_message($_SESSION['user'], $_POST['ke'], "", $image->name, $_POST['date'], $_POST['avatar'], $_POST['tipe']);
    //                         }
    //                     }
    //                 }
    //             // }else if($_POST['data'] == 'logout'){
    //             //     $data = $chat->user_logout($_SESSION['user']);
    //             //     if($data['status'] == 'success'){
    //             //         session_destroy();
    //             //     }
    //             }
    //         }
    //     }else{
    //         $data["aa"] = "bb";
    //     }

    //     header('Content-Type: application/json');
    //     header('Access-Control-Allow-Origin: *');
    //     echo json_encode($data);
    // }

}

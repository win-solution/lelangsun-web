<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
	}

	public function index()
	{
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}

		if (empty($_SESSION['login_admin'])) {
			$this->load->view('auth/login');
		} else {
			redirect('admin');
		}
	}

	public function check_auth()
	{
		// var_dump($_POST,'<hr>');
		// die;
		// session_destroy();
		$detail_user['select'] = 'u.id_user, u.nama_user, u.email, u.role';
		$detail_user['from'] = 'm_user as u';
		$detail_user['where'] = "u.email='" . $_POST['email'] . "' AND u.PASSWORD='" . sha1($_POST['password']) . "' AND u.STATUS = 1";
		$user = $this->model_global->getData($detail_user);
		// $user = $this->model_global->getDataQuery("SELECT id_user, nama_user, email, role FROM m_user WHERE email='".$_POST['email']."' AND PASSWORD='".sha1($_POST['password'])."' AND STATUS = 1");
		// var_dump($user,'<hr>');
		// die;
		if (!empty($user)) {
			$role_menu_detail['select'] = "rm.*, m.*";
			$role_menu_detail['from'] = "m_role_menu as rm";
			$role_menu_detail['join'][] = array('m_menu as m', 'm.id_menu = rm.id_menu');
			$role_menu_detail['join'][] = array('m_role as r', 'r.id_role = rm.id_role');
			$role_menu_detail['where'] = "rm.id_role = " . $user[0]->role . " and m.status = 1 and rm.status = 1 and r.status = 1";
			$role_menu_detail['order'][] = array('m.order', 'asc');
			$role_menu_detail['order'][] = array('m.header', 'desc');
			$role_menu_detail['order'][] = array('m.id_menu', 'asc');

			$role_menu = $this->model_global->getData($role_menu_detail);
			$menu = build_menu($role_menu);
			$newdata = array(
				'id_user_admin'	=> $user[0]->id_user,
				'nama'  		=> $user[0]->nama_user,
				'email'     	=> $user[0]->email,
				'login_admin' 	=> TRUE,
				'menu' 			=> $menu,
				'akses' 		=> $user[0]->role
			);
			// var_dump($newdata,'<hr>');
			// die;
			$this->session->set_userdata($newdata);
			redirect('admin');
		} else {
			$this->session->set_flashdata('msg_flash', 'Kombinasi Email & Password yang anda masukan salah.');
			redirect('auth');
		}
	}

	public function test_inject($password = '')
	{
		// $password = (empty($_GET['q'])) ? 'password' : $_GET['q'];
		die(md5($password));
	}

	public function logout()
	{
		session_destroy();
		redirect('auth');
	}
}

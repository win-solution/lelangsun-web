<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Front extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_setting');

		$setting = $this->model_setting->getListSetting();
		$arrSetting = array();
		foreach ($setting as $key => $val) {
			$arrSetting[$val->name_setting] = $val->value;
		}
		$this->setting = (object) $arrSetting;
		// var_dump($_SESSION);
	}

	public function index()
	{
		$data = array();
		$data['page'] = "index";
		$data['setting'] = $this->setting;
		if (!empty($_SESSION['id_peserta'])) {
			$this->rest_api('rebuild_npl', $_SESSION['id_peserta']);
			$data['notifikasi'] = get_notifikasi($_SESSION['id_peserta']);
		}
		$banner['select'] = "judul, gambar, deskripsi";
		$banner['from'] = "t_banner";
		$banner['where'] = "status = 1";
		$banner['order'][] = array('created_at', 'desc');
		$banner['limit'] = "5";
		$data['banner'] 	= $this->model_global->getData($banner);
		// var_dump($_SESSION);
		// die;
		$this->load->view('index', $data, false);
	}

	public function login()
	{ // Done
		if (!empty($_POST['submit'])) {
			// session_destroy();
			// $detail_peserta['select'] = 'u.id_peserta, u.nama, u.email, n.npl, n.id_npl';
			$detail_peserta['select'] = 'u.id_peserta, u.nama, u.email';
			$detail_peserta['from'] = 'm_peserta as u';
			// $detail_peserta['join'][] = array('m_npl as n', 'n.id_peserta = u.id_peserta AND n.status = 1 AND n.status_npl = 1', 'left');
			// $detail_peserta['where'] = "u.email='".$_POST['email']."' AND u.PASSWORD='".hash('sha256',$_POST['password'])."' AND u.is_aktif = 1 AND u.STATUS = 1";
			$detail_peserta['where'] = "u.email='" . $_POST['email'] . "' AND u.password = '" . md5($_POST['password']) . "' AND u.is_aktif = 1 AND u.STATUS = 1";
			$peserta = $this->model_global->getData($detail_peserta);
			// $user = $this->model_global->getDataQuery("SELECT id_user, nama_user, email, role FROM m_user WHERE email='".$_POST['email']."' AND PASSWORD='".sha1($_POST['password'])."' AND STATUS = 1");
			// var_dump($peserta,'<hr>');
			// die;
			// if (password_verify(password_hash($_POST['password'], PASSWORD_DEFAULT), $peserta[0]->password)) {

			$detail_npl['select'] = 'n.npl, n.id_npl';
			$detail_npl['from'] = 'm_npl as n';
			$detail_npl['where'] = "n.id_peserta ='" . $peserta[0]->id_peserta . "' AND n.status = 1 AND n.status_npl = 1";
			$npl_peserta = $this->model_global->getData($detail_npl);
			$npl = array();
			foreach ($npl_peserta as $key => $val) {
				// var_dump($val);die;
				if (!empty($val->id_npl)) {
					// $npl[] = $val->npl;
					$npl[] = $val->id_npl;
				}
			}
			// var_dump($npl);die;
			if (!empty($peserta)) {
				$newdata = array(
					'id_peserta'	=> $peserta[0]->id_peserta,
					'nama'  		=> $peserta[0]->nama,
					'email'     	=> $peserta[0]->email,
					'login_peserta' 	=> TRUE,
					'npl'			=> $npl,
				);
				$this->session->set_userdata($newdata);
				redirect('/');
			} else {
				redirect('login');
			}
			// } else {
			// var_dump(password_hash($_POST['password'], PASSWORD_DEFAULT), $peserta[0]->password);
			// die;
			// redirect('login');
			// }
		} else {
			$data = array();
			$data['page'] = "login";
			$data['setting'] = $this->setting;
			// var_dump($data);
			// die;
			$this->load->view('login', $data, false);
		}
	}

	public function logout()
	{ // Done
		session_destroy();
		redirect('/');
	}

	public function registrasi()
	{ // Done
		if (!empty($_POST['submit'])) {
			// var_dump($_POST);
			// die;
			// Cek email duplikasi atau tidak
			$detail_peserta = array();
			$detail_peserta['select'] = 'u.id_peserta, u.nama, u.email';
			$detail_peserta['from'] = 'm_peserta as u';
			$detail_peserta['where'] = "u.email='" . $_POST['email'] . "' and u.status = 1";
			$peserta = $this->model_global->getData($detail_peserta);
			if (empty($peserta)) {
				// Simpan data peserta		
				$peserta = array();
				$peserta['data']['nama'] = @$_POST['nama'];
				$peserta['data']['email'] = @$_POST['email'];
				$peserta['data']['alamat'] = @$_POST['alamat'];
				$peserta['data']['token_aktifasi'] = hash('sha256', @$_POST['email'] . date('Y-m-d H:i:s'));
				$peserta['data']['is_aktif'] = 1;
				// $peserta['data']['password'] = hash('sha256',$_POST['password']);
				$peserta['data']['password'] = md5(@$_POST['password']);
				// $peserta['data']['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
				$peserta['data']['created_at'] = date('Y-m-d H:i:s');
				$peserta['table']		= "m_peserta";
				if ($this->model_global->addData($peserta)) {
					// Get Data Peserta
					$detail_peserta = array();
					// $detail_peserta['select'] = 'u.id_peserta, u.nama, u.email, n.npl, n.id_npl';
					$detail_peserta['select'] = 'u.id_peserta, u.nama, u.email';
					$detail_peserta['from'] = 'm_peserta as u';
					// $detail_peserta['join'][] = array('m_npl as n','n.id_peserta = u.id_peserta AND n.status = 1 AND n.status_npl = 1','left');
					$detail_peserta['where'] = "u.email='" . $_POST['email'] . "' AND u.is_aktif = 1 AND u.STATUS = 1";
					$peserta = $this->model_global->getData($detail_peserta);

					// $npl = array();
					// foreach ($peserta as $key => $val) {
					// 	if (!empty($val->id_npl)) {
					// 		// $npl[] = $val->npl;
					// 		$npl[] = $val->id_npl;
					// 	}
					// }
					$newdata = array(
						'id_peserta'	=> $peserta[0]->id_peserta,
						'nama'  		=> $peserta[0]->nama,
						'email'     	=> $peserta[0]->email,
						'login_peserta' 	=> TRUE,
						// 'npl'			=> $npl,
					);
					$this->session->set_userdata($newdata);
					// Kirim email disini
					$this->session->set_flashdata('msg_flash', 'Terima kasih sudah mendaftar di website ' . APP_NAME . '. <br>Silahkan aktifasi akun anda dengan cara mengklik link yang kami kirim ke email anda.');
					redirect('/');
				} else {
					$this->session->set_flashdata('msg_flash', 'Terjadi kesalaha, proses pendaftaran anda gagal. <br>Silahkan mencoba lagi atau hubungi admin website ' . APP_NAME);
					redirect('/registrasi');
				}
			} else {
				$this->session->set_flashdata('msg_flash', 'Email yang anda masukan sudah terpakai di sistem Balai Lelang SUN');
				redirect('/registrasi');
			}
		} else {
			$data = array();
			$data['page'] = "registrasi";
			$data['setting'] = $this->setting;
			// var_dump($data);
			// die;
			$this->load->view('registrasi', $data, false);
		}
	}

	public function kontak()
	{ // Done
		if (isset($_POST['submit'])) {
			// var_dump($_POST);
			// die;
			$mitra['data']['nama'] = $_POST['nama'];
			$mitra['data']['email'] = $_POST['email'];
			$mitra['data']['no_tlp'] = $_POST['no_tlp'];
			$mitra['data']['pesan'] = $_POST['pesan'];
			$mitra['data']['created_at'] = date('Y-m-d H:i:s');
			$mitra['data']['status'] = 1;
			// var_dump($berita);
			// die();
			$mitra['table']		= "t_kontak";
			// $json['id_berita'] 	= $this->model_global->addData($data);
			// redirect('admin/berita');
			if ($this->model_global->addData($mitra)) {
				$this->session->set_flashdata('pesan', 'Pesan anda telah terkirim, admin kami akan segera menghubungi anda untuk proses berikutnya.');
			}
			redirect('kontak');
		} else {
			$data = array();
			$data['page'] = "kontak";
			$data['setting'] = $this->setting;
			// var_dump($data);
			// die;
			$this->load->view('kontak', $data, false);
		}
	}

	public function profile()
	{ // Done
		if (empty($_SESSION['id_peserta'])) {
			redirect('/');
		}
		if (!empty($_POST['submit'])) {
			// die;
			$peserta = array();
			$peserta['data']['nama'] = $_POST['nama'];
			$peserta['data']['email'] = $_POST['email'];
			if (!empty($_POST['password'])) {
				$peserta['data']['password'] = md5($_POST['password']);
			}
			$peserta['data']['no_rek'] = $_POST['no_rek'];
			$peserta['data']['nama_rek'] = $_POST['nama_rek'];
			$peserta['data']['alamat'] = $_POST['alamat'];
			$peserta['data']['no_hp'] = $_POST['no_hp'];
			$peserta['data']['nik'] = $_POST['nik'];
			$peserta['data']['npwp'] = $_POST['npwp'];
			try {
				if (!empty($_FILES['foto_ktp']['name'])) {
					$peserta['data']['foto_ktp']	= unggah_gambar('./assets/uploads/ktp_peserta', 'foto_ktp', 'png|jpg|jpeg');
				};
			} catch (\Throwable $th) {
				var_dump($th);
				die;
			}

			try {
				if (!empty($_FILES['foto_npwp']['name'])) {
					$peserta['data']['foto_npwp']	= unggah_gambar('./assets/uploads/npwp_peserta', 'foto_npwp', 'png|jpg|jpeg');
				};
			} catch (\Throwable $th) {
				var_dump($th);
				die;
			}
			$peserta['data']['updated_at'] = date('Y-m-d H:i:s');
			$peserta['data']['updated_by'] = $_SESSION['id_peserta'];
			$peserta['table']		= "m_peserta";
			$peserta['where']		= 'id_peserta = ' . $_SESSION['id_peserta'];
			if ($this->model_global->updateData($peserta)) {
				// Kirim email disini
				$this->session->set_flashdata('msg_flash', 'Berhasil, data anda sudah diperbaharuai.');
			} else {
				$this->session->set_flashdata('msg_flash', 'Terjadi kesalaha, proses pembaruan data anda gagal. <br>Silahkan mencoba lagi atau hubungi admin website ' . APP_NAME);
			}
			redirect('profile');
		} else {
			$data = $peserta_detail = $npl_detail = $pemenang_detail = $notifikasi_detail = array();
			if (!empty($_SESSION['id_peserta'])) {
				$this->rest_api('rebuild_npl', $_SESSION['id_peserta']);
				$data['notifikasi'] = get_notifikasi($_SESSION['id_peserta']);
			}
			$id_peserta = (!empty($_SESSION['id_peserta'])) ? $_SESSION['id_peserta'] : 0;
			$data['page'] = "profile";
			$data['setting'] = $this->setting;
			$peserta_detail['select'] = "*";
			$peserta_detail['from'] = "m_peserta";
			$peserta_detail['where'] = "id_peserta = '" . $id_peserta . "' and status = 1 and is_aktif = 1";
			$data['peserta'] = $this->model_global->getData($peserta_detail);

			$npl_detail['select']	= "n.id_npl, n.npl, n.no_npl, n.status_npl, e.nama_event, e.waktu_event, p.verifikasi, p.pesan_verifikasi, r.status_refund";
			// $npl_detail['select']	= "n.id_npl, n.npl, n.no_npl, n.status_npl, e.nama_event, e.waktu_event, p.verifikasi, p.pesan_verifikasi";
			$npl_detail['table']	= "m_peserta as m";
			$npl_detail['join'][]	= array("t_pembelian_npl as p", 'p.id_peserta = m.id_peserta');
			$npl_detail['join'][]	= array("m_npl as n", 'n.id_pembelian_npl = p.id_pembelian_npl AND n.status = 1', 'left');
			$npl_detail['join'][]	= array("t_event as e", 'e.id_event = p.id_event', 'left');
			$npl_detail['join'][]	= array("t_refund_npl as r", 'r.id_npl = n.id_npl', 'left');
			$npl_detail['where']	= "m.id_peserta = '" . $id_peserta . "' and m.status = 1";
			$data['npl_detail'] 	= $this->model_global->getData($npl_detail);

			$pemenang_detail['select']	= "pl.*, b.nama_barang_lelang, b.brand, bi.harga_bidding";
			$pemenang_detail['table']	= "t_pemenang_lelang as pl";
			$pemenang_detail['join'][]	= array("t_bidding as bi", 'bi.id_bidding = pl.id_bidding AND bi.status = 1');
			$pemenang_detail['join'][]	= array("t_lot as l", 'l.id_lot = bi.id_lot AND l.status_lot = 2');
			$pemenang_detail['join'][]	= array("m_barang_lelang as b", "b.id_barang_lelang = l.id_barang_lelang AND b.status = 1");
			$pemenang_detail['where']	= "bi.id_peserta = '" . $id_peserta . "' and bi.status = 1 AND pl.status_pembayaran = 0";
			$data['pemenang_detail'] 	= $this->model_global->getData($pemenang_detail);

			$notifikasi_detail['select']	= "n.*";
			$notifikasi_detail['table']	= "m_peserta as m";
			$notifikasi_detail['join'][]	= array("t_notifikasi as n", 'm.id_peserta = n.penerima AND n.status = 1');
			$notifikasi_detail['where']	= "m.id_peserta = '" . $id_peserta . "' and m.status = 1";
			$notifikasi_detail['order'][]	= array('n.created_at', 'desc');
			$data['notifikasi_detail'] 	= $this->model_global->getData($notifikasi_detail);
			// var_dump($data['notifikasi']);
			// die;
			$this->load->view('profile', $data, false);
		}
	}

	public function beli_npl()
	{ // Done
		if (!empty($_POST['submit'])) {
			// var_dump($_POST);
			// die;
			try {
				$pembelian_npl['data']['id_peserta'] = @$_POST['id_peserta'];
				$pembelian_npl['data']['id_event'] = @$_POST['id_event'];
				$pembelian_npl['data']['no_rek'] = @$_POST['no_rek'];
				$pembelian_npl['data']['nama_pemilik'] = @$_POST['nama_pemilik'];
				$pembelian_npl['data']['type_pembelian'] = @$_POST['type_pembelian'];
				$pembelian_npl['data']['type_transaksi'] = @$_POST['type_transaksi'];
				$pembelian_npl['data']['nominal'] = @$_POST['nominal'];
				$pembelian_npl['data']['tanggal_transfer'] = date_format(date_create(@$_POST['tanggal_transfer']), 'Y-m-d H:i:s');
				$pembelian_npl['data']['waktu'] = date('Y-m-d H:i:s');
				$pembelian_npl['data']['no_pembelian'] = time() . '.' . @$_POST['id_peserta'];

				if (!empty($_FILES['bukti']['name'])) {
					$pembelian_npl['data']['bukti']	= unggah_gambar('./assets/uploads/bukti_npl', 'bukti', 'png|jpg|jpeg');
				};

				$pembelian_npl['data']['created_at'] = date('Y-m-d H:i:s');
				// $pembelian_npl['data']['created_by'] = '';
				$pembelian_npl['data']['status'] = 1;
				// var_dump($peserta);
				// die();
				$pembelian_npl['table']		= "t_pembelian_npl";
				// $json['id_peserta_lot'] 	= $this->model_global->addData($data);
				$id_pembelian_npl = $this->model_global->addData($pembelian_npl);
				$this->session->set_flashdata('msg_flash', 'Pembelian NPL berhasil, data anda sedang diverifikasi oleh admin.');
				redirect('profile');
			} catch (\Throwable $th) {
				$json['status'] = FALSE;
				$json['pesan'] = $th;
				echo json_encode($json);
			}
		} else {
			$data = array();
			$data['proses'] = ucwords("Beli npl");

			$event_detail['select']	= "*";
			$event_detail['table']	= "t_event";
			$event_detail['where']	= "waktu_event BETWEEN '" . date('Y-m-d', strtotime(date('Y-m-d'))) . "' AND '" . date('Y-m-d', strtotime(date('Y-m-d') . "+1 day")) . "' AND status = 1";
			$event_detail['order'][]	= array('waktu_event', 'desc');
			$data['event_detail'] 	= $this->model_global->getData($event_detail);

			$data['content'] 	= $this->load->view('form_beli_npl', $data, TRUE);
			echo json_encode($data);
		}
	}

	public function pelunasan_lelang()
	{ // Done
		if (!empty($_POST['submit'])) {
			// var_dump($_POST);
			// die;
			try {
				$pemenang_lelang['data']['no_rek'] = @$_POST['no_rek'];
				$pemenang_lelang['data']['nama_pemilik'] = @$_POST['nama_pemilik'];
				$pemenang_lelang['data']['nominal'] = @$_POST['nominal'];
				$pemenang_lelang['data']['tipe_pelunasan'] = @$_POST['tipe_pelunasan'];
				$pemenang_lelang['data']['tanggal_transfer'] = date_format(date_create(@$_POST['tanggal_transfer']), 'Y-m-d H:i:s');

				if (!empty($_FILES['bukti']['name'])) {
					$pemenang_lelang['data']['bukti']	= unggah_gambar('./assets/uploads/pelunasan_lelang', 'bukti', 'png|jpg|jpeg');
				};

				$pemenang_lelang['data']['updated_at'] = date('Y-m-d H:i:s');
				// $pemenang_lelang['data']['updated_by'] = $_SESSION['id_user_admin'];
				// var_dump($peserta);
				// die();
				$pemenang_lelang['table']		= "t_pemenang_lelang";
				$pemenang_lelang['where']		= 'id_pemenang_lelang = ' . $_POST['id_pemenang_lelang'];
				// $json['id_peserta_lot'] 	= $this->model_global->addData($data);
				$this->model_global->updateData($pemenang_lelang);
				$this->session->set_flashdata('msg_flash', 'Pelunasan barang lelang berhasil, data anda sedang diverifikasi oleh admin.');
				redirect('profile');
			} catch (\Throwable $th) {
				$json['status'] = FALSE;
				$json['pesan'] = $th;
				echo json_encode($json);
			}
		} else {
			$data = array();
			$data['id_pemenang_lelang'] = $_GET['id_pemenang_lelang'];
			$pemenang['select'] = "bi.*";
			$pemenang['from'] = "t_bidding as bi";
			$pemenang['where'] = "p.id_pemenang_lelang = " . $_GET['id_pemenang_lelang'];
			$pemenang['limit'] = 1;
			$pemenang['join'][] = array('t_pemenang_lelang as p', "p.id_bidding = bi.id_bidding");
			$data['detail_pemenang'] = $this->model_global->getData($pemenang);

			$data['proses'] = ucwords("pelunasan barang lelang");
			$data['content'] 	= $this->load->view('form_pelunasan_lelang', $data, TRUE);
			echo json_encode($data);
		}
	}

	public function detaillot($id = null)
	{ // Done
		$data = array();
		$data['page'] = "lot";
		$data['setting'] = $this->setting;
		$star_date = date('Y-m-d H:i:s');
		$end_date = date('Y-m-d H:i:s');
		$barang_lelang['select'] = "b.*, f.gambar, e.waktu_event, e.alamat_event, e.link_maps, l.id_lot, l.harga_awal";
		$barang_lelang['from'] = "t_lot as l";
		$barang_lelang['join'][] = array('m_barang_lelang as b', 'l.id_barang_lelang = b.id_barang_lelang and b.status = 1');
		$barang_lelang['join'][] = array('m_foto_barang_lelang as f', 'f.id_barang_lelang = b.id_barang_lelang and f.status = 1', 'left');
		// $barang_lelang['join'][] = array('t_event as e','e.id_event = l.id_event and e.status = 1');
		$barang_lelang['join'][] = array('t_event as e', 'e.id_event = l.id_event');
		// $barang_lelang['group_by'] = "l.id_barang_lelang";
		// $barang_lelang['where'][] = " l.status = 1 AND l.id_lot = ".$id;
		$barang_lelang['where'][] = "l.id_lot = " . $id;

		$data['lot_lelang'] = $this->model_global->getData($barang_lelang);
		// var_dump($data);
		// die;
		$this->load->view('detaillot', $data, false);
	}

	public function detaillelang($id = null)
	{ // Done
		// var_dump($_SESSION['npl']);
		$data = array();
		// // Set Data Session NPL Peserta
		$this->session->unset_userdata('npl');
		$detail_peserta = array();
		$detail_peserta['select'] = 'u.id_peserta, u.nama, u.email, n.npl, n.id_npl, n.npl, n.no_npl';
		$detail_peserta['from'] = 'm_peserta as u';
		$detail_peserta['join'][] = array('m_npl as n', 'n.id_peserta = u.id_peserta AND n.id_event = ' . $id . ' AND n.status = 1 AND n.status_npl = 1', 'left');
		if (!empty($_SESSION['id_peserta'])) {
			$detail_peserta['where'] = "u.id_peserta='" . $_SESSION['id_peserta'] . "'";
		}
		$detail_peserta['where'] = "u.is_aktif = 1 AND u.STATUS = 1";
		$peserta = $this->model_global->getData($detail_peserta);
		$npl = array();
		// Jika ada id_peserta maka create session NPL
		if (!empty($_SESSION['id_peserta'])) {
			foreach ($peserta as $key => $val) {
				if (!empty($val->id_npl)) {
					// $npl[] = $val->npl;
					$npl[] = $val->id_npl;
				}
			}
		}
		$newdata = array('npl' => $npl);
		$this->session->set_userdata($newdata);
		// if (empty($_SESSION['email'])) { redirect('lelang'); }
		if ((!empty($_SESSION['id_peserta'])) && (!empty($_SESSION['npl']))) {
			if (!empty($_SESSION['id_peserta'])) {
				// $this->rest_api('rebuild_npl', $_SESSION['id_peserta']);
				$data['notifikasi'] = get_notifikasi($_SESSION['id_peserta']);
			}
			// cari siapa yang memenangkan bidding
			$last_bidding['select'] = '*';
			$last_bidding['from'] = 't_bidding';
			$last_bidding['limit'] = 1;
			$last_bidding['where'] = 'kode_event = "' . $_GET['kode_event'] . '" AND status = 1';
			$last_bidding['order'][] = array('created_at', 'DESC');
			$pemenang_bidding = $this->model_global->getData($last_bidding);
			// var_dump(json_encode($pemenang_bidding),"<hr>");
			if (!empty($pemenang_bidding)) {
				// var_dump($_SESSION['npl'],"<hr>");
				// hapus npl dari session jika npl pemenang bidding sama
				foreach ($_SESSION['npl'] as $key => $val) {
					if ($val == $pemenang_bidding[0]->npl) {
						// var_dump('Sama nih di '.$key.' sama dengan '.$val,"<hr>");
						// hapus session npl
						unset($_SESSION['npl'][$key]);
						$new_session_npl = array_values($_SESSION['npl']);
						// var_dump($new_session_npl,"<hr>");
						// update status=0 m_npl berdasarkan npl						
						$update_npl['data']['status_npl'] = 0;
						$update_npl['data']['updated_at'] = date('Y-m-d H:i:s');
						$update_npl['data']['updated_by'] = $_SESSION['id_peserta'];
						$update_npl['table']		= "m_npl";
						$update_npl['where'][0] 	= array('npl', $val);
						$this->model_global->updateData($update_npl);
						// recreate session npl
						$this->session->unset_userdata('npl');
						$newdata = array(
							'npl'			=> $new_session_npl,
						);
						$this->session->set_userdata($newdata);
						break;
					}
				}
				// var_dump($_SESSION['npl'],"<hr>");
				// die;
				// if ($pemenang_bidding[0]->npl) {
				// 	# code...
				// }
				// recreate list npl baru di session

			}
		}
		$event_detail['select'] = "kode_event";
		$event_detail['from'] = 't_event';
		$event_detail['where'][] = "id_event = '" . $id . "' AND status = 1";
		$data_event = $this->model_global->getData($event_detail);

		// ambil id_lot untuk tau current lot berdasarkan kode_event di t_bidding
		$bidding['select'] = 'id_lot';
		$bidding['from'] = 't_bidding';
		$bidding['where'][] = "id_event = '" . $id . "' AND status = 1";
		$bidding['group_by'] = array('id_lot');
		$bidding['limit'] = 1;
		$bidding['order'][] = array('id_lot', 'desc');
		$data['current_lot'] = $this->model_global->getData($bidding);
		// var_dump($data);die;
		if (@$_GET['id_lot'] < @$data['current_lot'][0]->id_lot) {
			// $url = ;
			// var_dump($url);die;
			redirect("front/detaillelang/{$id}?kode_event={$_GET['kode_event']}&id_lot={$data['current_lot'][0]->id_lot}");
		}
		// ambil pass_event dari t_event (belum)
		$event['select'] = 'pass_event';
		$event['from'] = 't_event';
		$event['where'][] = "id_event = '" . $id . "' AND status = 1";
		$data['pass_event'] = $this->model_global->getData($event);
		// jika pass_event ada & lelang masih berlangsung maka buat session pass_event (belum)
		if (!empty($data['pass_event'][0]->pas_event)) {
			// create session pass_event
			$this->session->set_userdata('pass_event', $data['pass_event'][0]->pas_event);
		}
		$this->load->config('pusher');
		$data['id_event'] = $id;
		// $data['kode_event'] = @$_GET['kode_event'];
		$data['kode_event'] = $data_event[0]->kode_event;
		$data['email'] = @$_SESSION['email'];
		$data['page'] = "lelang";
		$data['setting'] = $this->setting;

		$lot_lelang['select'] = "b.*, f.gambar, l.id_lot, l.harga_awal, l.no_lot";
		$lot_lelang['from'] = "t_lot as l";
		$lot_lelang['join'][] = array('m_barang_lelang as b', 'l.id_barang_lelang = b.id_barang_lelang and b.status = 1');
		$lot_lelang['join'][] = array('m_foto_barang_lelang as f', 'f.id_barang_lelang = b.id_barang_lelang and f.status = 1', 'left');
		$lot_lelang['join'][] = array('t_event as e', 'e.id_event = l.id_event and e.status = 1');
		$lot_lelang['group_by'] = "l.id_barang_lelang";
		$lot_lelang['where'][] = "e.id_event = '" . $data['id_event'] . "' AND l.status = 1 AND l.status_lot = 1";
		$lot_lelang['order'][] = array('l.id_lot', 'asc');
		$lot_lelang['limit'] = 2;
		if (!empty($_GET['id_lot'])) {
			$lot_lelang['where'][] = "l.id_lot >= '" . $_GET['id_lot'] . "'";
		}
		$data['lot_lelang'] = $this->model_global->getData($lot_lelang);

		// var_dump($_SESSION);
		// die;
		$this->load->view('detaillelang', $data, false);
	}

	public function detailevent($id = null)
	{ // Done
		$data = array();
		$data['id_event'] = $id;
		$data['kode_event'] = @$_GET['kode_event'];
		$data['email'] = @$_SESSION['email'];
		$data['page'] = "lelang";
		$data['setting'] = $this->setting;

		$lot_lelang['select'] = "b.*, f.gambar, l.id_lot, l.harga_awal, e.nama_event, e.deskripsi_event, e.waktu_event, e.alamat_event";
		$lot_lelang['from'] = "t_lot as l";
		$lot_lelang['join'][] = array('m_barang_lelang as b', 'l.id_barang_lelang = b.id_barang_lelang and b.status = 1');
		$lot_lelang['join'][] = array('m_foto_barang_lelang as f', 'f.id_barang_lelang = b.id_barang_lelang and f.status = 1', 'left');
		$lot_lelang['join'][] = array('t_event as e', 'e.id_event = l.id_event and e.status = 1');
		$lot_lelang['group_by'] = "l.id_barang_lelang";
		$lot_lelang['where'][] = "e.id_event = '" . $data['id_event'] . "' AND l.status = 1";
		$lot_lelang['order'][] = array('l.no_lot', 'asc');
		// $lot_lelang['limit'] = 2;
		if (!empty($_GET['id_lot'])) {
			$lot_lelang['where'][] = "l.id_lot >= '" . $_GET['id_lot'] . "'";
		}
		$data['lot_lelang'] = $this->model_global->getData($lot_lelang);
		// var_dump($data);
		// die;
		$this->load->view('detailevent', $data, false);
	}

	public function lot($page = 1)
	{ // Done
		$data = array();
		if (!empty($_SESSION['id_peserta'])) {
			$this->rest_api('rebuild_npl', $_SESSION['id_peserta']);
			$data['notifikasi'] = get_notifikasi($_SESSION['id_peserta']);
		}
		$data['page'] = "lot";
		$data['curent_page'] = $page;
		$data['setting'] = $this->setting;
		$star_date = date('Y-m-d H:i:s');
		$end_date = date('Y-m-d H:i:s');
		$barang_lelang['select'] = "b.id_barang_lelang, b.nama_barang_lelang, b.brand, b.tahun, b.lokasi_barang, b.transmisi, b.no_polisi, b.odometer, b.grade, f.gambar, e.id_event, e.nama_event, e.waktu_event, e.alamat_event, e.link_maps, l.id_lot, l.no_lot, l.harga_awal, l.status_lot, bi.id_bidding, bi.harga_bidding";
		$barang_lelang['from'] = "t_lot as l";
		$barang_lelang['join'][] = array('m_barang_lelang as b', 'l.id_barang_lelang = b.id_barang_lelang');
		$barang_lelang['join'][] = array('m_foto_barang_lelang as f', 'f.id_barang_lelang = b.id_barang_lelang and f.status = 1', 'left');
		$barang_lelang['join'][] = array('t_bidding as bi', 'l.id_lot = bi.id_lot', 'left');
		$barang_lelang['join'][] = array('t_pemenang_lelang as pl', 'pl.id_bidding = bi.id_bidding', 'left');
		// $barang_lelang['join'][] = array('t_event as e', 'e.id_event = l.id_event AND e.status = 1');
		$barang_lelang['join'][] = array('t_event as e', 'e.id_event = l.id_event');
		$barang_lelang['group_by'] = "l.id_barang_lelang";
		if (!empty($_GET['q'])) {
			$barang_lelang['where'][] = "(b.nama_barang_lelang like '%" . $_GET['q'] . "%' OR b.transmisi like '%" . $_GET['q'] . "%' OR b.no_polisi like '%" . $_GET['q'] . "%' OR e.waktu_event like '%" . $_GET['q'] . "%' OR e.alamat_event like '%" . $_GET['q'] . "%')";
		}
		if (!empty($_GET['brand'])) {
			$barang_lelang['where'][] = "b.brand = '" . strtolower($_GET['brand']) . "'";
		}
		if (!empty($_GET['tahun'])) {
			$barang_lelang['where'][] = "b.tahun = " . $_GET['tahun'];
		}
		if (!empty($_GET['harga'])) {
			switch ($_GET['harga']) {
				case '+100000000':
					$barang_lelang['where'][] = "l.harga_awal >= 100000000";
					break;

				default:
					$barang_lelang['where'][] = "l.harga_awal <= " . $_GET['harga'];
					break;
			}
		}
		// $barang_lelang['where'][] = "e.waktu_event BETWEEN '" . date('Y-m-d', strtotime(date('Y-m-d') . "-10 day")) . "' AND '" . date('Y-m-d', strtotime(date('Y-m-d') . "+10 day")) . "' AND l.status = 1";
		$barang_lelang['where'][] = "e.waktu_event BETWEEN '" . date('Y-m-d', strtotime(date('Y-m-d'))) . "' AND '" . date('Y-m-d', strtotime(date('Y-m-d') . "+10 day")) . "' AND l.status = 1";
		// $barang_lelang['where'][] = "e.waktu_event BETWEEN '" . date('Y-m-d', strtotime(date('Y-m-d'))) . "' AND '" . date('Y-m-d', strtotime(date('Y-m-d') . "+10 day")) . "'";
		$barang_lelang['order'][] = array('e.waktu_event', 'desc');
		$barang_lelang['order'][] = array('l.no_lot', 'asc');
		// $barang_lelang['order'][] = array('bi.harga_bidding', 'desc');

		$jumlah_data = $this->model_global->getDataRows($barang_lelang);

		$this->load->helper('pagging_helper');
		$configPaging['base_url'] = base_url() . 'lot';
		$configPaging['total_rows'] = $jumlah_data;
		$configPaging['per_page'] = 12;
		$data['per_page'] = $configPaging['per_page'];
		$configPaging['num_links'] = 2;
		$configPaging['reuse_query_string'] = true; // untuk membuat query string di url
		$from = ($page * $configPaging['per_page']) - $configPaging['per_page'];
		$data['pagingView'] = create_pagging($configPaging);

		$barang_lelang['limit'] = array($configPaging['per_page'], $from);
		$data['barang_lelang'] = $this->model_global->getData($barang_lelang);

		if (empty($_SESSION['filter_brand'])) {
			$filter_brand['select'] = 'DISTINCT(brand) as brand';
			$filter_brand['from'] = 'm_barang_lelang';
			$filter_brand['where'] = 'status = 1';
			$data['filter_brand'] = $this->model_global->getData($filter_brand);
			$this->session->set_userdata('filter_brand', $data['filter_brand']);
		}

		if (empty($_SESSION['filter_tahun'])) {
			$filter_tahun['select'] = 'DISTINCT(tahun) as tahun';
			$filter_tahun['from'] = 'm_barang_lelang';
			$filter_tahun['where'] = 'status = 1';
			$filter_tahun['order'][] = array('tahun', 'asc');
			$data['filter_tahun'] = $this->model_global->getData($filter_tahun);
			$this->session->set_userdata('filter_tahun', $data['filter_tahun']);
		}
		// var_dump($data);
		// die;
		$this->load->view('lot', $data, false);
	}

	public function event($page = 1)
	{ // Done
		$data = array();
		if (!empty($_SESSION['id_peserta'])) {
			$this->rest_api('rebuild_npl', $_SESSION['id_peserta']);
			$data['notifikasi'] = get_notifikasi($_SESSION['id_peserta']);
		}
		$data['page'] = "event";
		$data['setting'] = $this->setting;
		$data['curent_page'] = $page;

		$event_lelang['select'] = "*";
		$event_lelang['from'] = "t_event";
		$event_lelang['where'][] = "waktu_event >= '" . date_format(date_create(), 'Y-m-d') . "' AND status = 1";
		$event_lelang['order'][] = array('waktu_event', 'asc');

		$jumlah_data = $this->model_global->getDataRows($event_lelang);

		$this->load->helper('pagging_helper');
		$configPaging['base_url'] = base_url() . 'event';
		$configPaging['total_rows'] = $jumlah_data;
		$configPaging['per_page'] = 12;
		$data['per_page'] = $configPaging['per_page'];
		$configPaging['num_links'] = 2;
		$configPaging['reuse_query_string'] = true; // untuk membuat query string di url
		$from = ($page * $configPaging['per_page']) - $configPaging['per_page'];
		$data['pagingView'] = create_pagging($configPaging);

		$event_lelang['limit'] = array($configPaging['per_page'], $from);
		$data['event_lelang'] = $this->model_global->getData($event_lelang);
		// var_dump($data);
		// die;
		$this->load->view('event', $data, false);
	}

	public function lelang($page = 1)
	{ // Done
		$data = array();
		if (!empty($_SESSION['id_peserta'])) {
			$this->rest_api('rebuild_npl', $_SESSION['id_peserta']);
			$data['notifikasi'] = get_notifikasi($_SESSION['id_peserta']);
		}
		$data['page'] = "lelang";
		$data['setting'] = $this->setting;
		$data['curent_page'] = $page;

		$event_lelang['select'] = "*";
		$event_lelang['from'] = "t_event";
		$event_lelang['where'][] = "DATE(waktu_event) BETWEEN '" . date_format(date_create(), 'Y-m-d') . "' AND '" . date('Y-m-d', strtotime('+1 month')) . "' AND status = 1";
		// $event_lelang['where'][] = "DATE(waktu_event) BETWEEN '".date_format(date_create(),'Y-m-d')."' AND '".date('Y-m-d', strtotime('+7 days'))."' AND status = 1";
		$event_lelang['order'][] = array('waktu_event', 'asc');

		$jumlah_data = $this->model_global->getDataRows($event_lelang);

		$this->load->helper('pagging_helper');
		$configPaging['base_url'] = base_url() . 'lelang';
		$configPaging['total_rows'] = $jumlah_data;
		$configPaging['per_page'] = 4;
		$data['per_page'] = $configPaging['per_page'];
		$configPaging['num_links'] = 2;
		$configPaging['reuse_query_string'] = true; // untuk membuat query string di url
		$from = ($page * $configPaging['per_page']) - $configPaging['per_page'];
		$data['pagingView'] = create_pagging($configPaging);

		$event_lelang['limit'] = array($configPaging['per_page'], $from);
		$data['event_lelang'] = $this->model_global->getData($event_lelang);
		// var_dump($data);
		// die;
		$this->load->view('lelang', $data, false);
	}

	public function invoice()
	{
		$data = array();
		$data['page'] = "invoice";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('invoice', $data, false);
	}

	public function invoice2()
	{
		$data = array();
		$data['page'] = "invoice2";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('invoice2', $data, false);
	}

	public function invoice3()
	{
		$data = array();
		$data['page'] = "invoice3";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('invoice3', $data, false);
	}

	public function display()
	{
		$data = array();
		$data['page'] = "display";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('display', $data, false);
	}

	public function berita($page = 1)
	{
		$data = array();
		$data['page'] = "berita";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;

		$berita['select'] = "*";
		$berita['from'] = "t_berita";
		$berita['where'] = "status = 1 and tgl_publish < '" . date('Y-m-d H:i:s') . "'";
		$berita['order'] = array('tgl_publish', 'desc');

		$jumlah_data = $this->model_global->getDataRows($berita);

		$this->load->helper('pagging_helper');
		$configPaging['base_url'] = base_url() . 'berita/';
		$configPaging['total_rows'] = $jumlah_data;
		$configPaging['per_page'] = 5;
		$configPaging['num_links'] = 2;
		$from = ($page * $configPaging['per_page']) - $configPaging['per_page'];
		$data['pagingView'] = create_pagging($configPaging);
		// $from = $this->uri->segment(3);
		$berita['limit'] = array($configPaging['per_page'], $from);
		$data['berita'] = $this->model_global->getData($berita);

		$this->load->view('berita', $data, false);
	}

	public function detailberita($link = '')
	{
		$data = array();
		if ($link !== '') {
			// var_dump($link,"<hr>");
			// die;
			// get id_berita from link_berita
			$link_berita['select'] = "id_link_berita, id_berita, hit";
			$link_berita['from'] = "t_link_berita";
			$link_berita['where'] = "status = 1 and link_berita like '%" . $link . "%'";
			$link_berita['limit'] = "1";

			$data['link_berita'] = $this->model_global->getData($link_berita);
			if (!empty($data)) {
				$data['page'] = "berita";
				$data['setting'] = $this->setting;
				// var_dump($data,"<hr>");
				// get detail berita
				$detail_berita['select'] = "*";
				$detail_berita['from'] = "t_berita";
				$detail_berita['where'] = "status = 1 and id_berita = " . $data['link_berita'][0]->id_berita;
				$detail_berita['limit'] = "1";

				$data['detail_berita'] = $this->model_global->getData($detail_berita);

				// update hit link_berita
				$data_link_berita['data']['hit'] = $data['link_berita'][0]->hit + 1;
				$data_link_berita['table']		= "t_link_berita";
				$data_link_berita['where'][0] 	= array('id_link_berita', $data['link_berita'][0]->id_link_berita);

				$this->model_global->updateData($data_link_berita);
				// die;
				$this->load->view('detailberita', $data, false);
			} else {
				redirect('berita');
			}
		} else {
			redirect('berita');
		}
	}

	public function history()
	{
		$data = array();
		$data['page'] = "history";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('history', $data, false);
	}

	public function rest_api($type, $id = null)
	{
		$result = "";
		switch ($type) {
			case 'menu':
				$menu['select'] = "m.id_menu as id, m.nama_menu as nama, m.gambar, m.deskripsi, m.isbest, k.id_kategori_menu, k.nama_kategori_menu";
				$menu['from'] = "m_menu as m";
				$menu['join'][0] = array('m_kategori_menu as k', "m.id_kategori_menu LIKE CONCAT('%', k.id_kategori_menu, '%')");
				$menu['where'] = "m.status = 1 AND k.status = 1 AND m.id_kategori_menu like '%" . $_GET['id_kategori'] . "%' AND k.id_kategori_menu = " . $_GET['id_kategori'];
				$result = $this->model_global->getData($menu);
				break;
			case 'outlet':
				// $outlet['select'] = "id_mitra as id, nama_mitra as nama, email, no_tlp, alamat_mitra, gambar";
				// $outlet['from'] = "t_mitra";
				// $outlet['where'] = "status = 1 AND kode_prop = ".$_POST['no_prop'];
				$outlet['select'] = "o.kode_outlet, o.nama_outlet, o.kode_prop, o.nomor_outlet, o.gambar, o.link_maps, m.alamat_mitra, k.kode, k.nama_prop";
				$outlet['from'] = "t_outlet as o";
				$outlet['join'][0] = array('t_mitra as m', 'm.id_mitra = o.id_mitra');
				$outlet['join'][1] = array('m_provinsi as k', 'k.kode = o.kode_prop');
				$outlet['where'] = "o.status = 1 AND o.kode_prop like '%" . $_GET['no_prop'] . "%'";
				// $outlet['join'] = "t_mitra "
				$result = $this->model_global->getData($outlet);
				break;
			case 'rebuild_npl':
				$this->session->unset_userdata('npl');
				$npl_detail['select'] = "id_npl, npl";
				$npl_detail['from'] = "m_npl";
				$npl_detail['where'] = "id_peserta = " . $id . " AND status_npl = 1";
				$data_npl = $this->model_global->getData($npl_detail);

				$npl = array();
				foreach ($data_npl as $key => $val) {
					// var_dump($val);die;
					if (!empty($val->npl)) {
						$npl[] = $val->id_npl;
					}
				}
				// var_dump($npl);die;
				$newdata = array(
					'npl' => $npl,
				);
				$this->session->set_userdata($newdata);
				// var_dump($_SESSION['npl']);
				// exit;
				// continue;
				break;
			case 'see_all_notification':
				$notification['data']['is_read'] = 1;
				$notification['table']		= "t_notifikasi";
				$notification['where'][] 	= array('penerima', $_SESSION['id_peserta']);
				$this->model_global->updateData($notification);
				$result = "berhasil";
				break;
			case 'see_notification':
				$notification['data']['is_read'] = 1;
				$notification['table']		= "t_notifikasi";
				$notification['where'][] 	= array('id_notifikasi', $_POST['id_notifikasi']);
				$this->model_global->updateData($notification);
				$result = "berhasil";
				break;
			case 'read_all_notification':
				// var_dump($_POST);
				// die;
				$notification['data']['is_read'] = 1;
				$notification['table']		= "t_notifikasi";
				$notification['where'][] 	= array('penerima', $_POST['penerima']);
				$this->model_global->updateData($notification);
				$result = "berhasil";
				break;
			case 'refund':
				// var_dump($_POST);
				$event['data']['id_npl'] = $_POST['id_npl'];
				$event['data']['status_refund'] = 1;
				$event['data']['created_at'] = date('Y-m-d H:i:s');
				$event['data']['status'] = 1;
				// var_dump($event);
				// die();
				$event['table']		= "t_refund_npl";
				// redirect('admin/event');
				if ($this->model_global->addData($event)) {
					$result = array('code' => 200, 'msg' => 'Proses Berhasil', 'status' => true);
				} else {
					$result = array('code' => 500, 'msg' => 'Proses Gagal', 'status' => false);
				}
				break;
			default:
				# code...
				break;
		}
		if ($result !== "") {
			echo json_encode($result);
		}
	}

	public function inspect_session()
	{
		var_dump(json_encode($_SESSION));
		die;
	}
}

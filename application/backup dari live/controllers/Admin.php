<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $setting;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_setting');
		$this->load->model('model_datatable');
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		if (empty($_SESSION['login_admin'])) {
			if ($_SESSION['login_admin'] !== TRUE) {
				redirect('auth');
			}
		}

		$setting = $this->model_setting->getListSetting();
		$arrSetting = array();
		foreach ($setting as $key => $val) {
			$arrSetting[$val->name_setting] = $val->value;
		}
		$this->setting = (object) $arrSetting;
		// var_dump($_SESSION['menu']['event']);
	}

	public function index()
	{
		$data = array();
		$data['page'] = "home";
		$view['content'] = $this->load->view('admin/home', $data, true);
		$this->load->view('admin/layout', $view, false);
	}

	public function dashboard()
	{
		validate_access($_SESSION['menu']['dashboard']);
		$data = array();
		$data['page'] = "dashboard";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		$view['content'] = $this->load->view('admin/dashboard', $data, true);
		$this->load->view('admin/layout', $view, false);
	}

	public function banner($action = null, $id = null)
	{ // Done
		validate_access($_SESSION['menu']['banner']);
		$data = array();
		$data['page'] = "banner";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'banner', 'c' => '' );

		$data['back']	= base_url('admin/banner');
		$data['proses'] = ucwords($action);

		switch ($action) {
			case 'tambah':
				validate_access($_SESSION['menu']['banner']['create']);
				$data['action'] = base_url('admin/banner/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					$banner['data']['judul'] = $_POST['judul'];
					if (!empty($_FILES['gambar']['name'])) {
						$banner['data']['gambar']	= unggah_gambar('./assets/uploads/banner', 'gambar', 'png|jpg|jpeg');
					};
					$banner['data']['deskripsi'] = $_POST['deskripsi'];
					// var_dump($banner['data'],"<hr>");						
					// die();
					$banner['data']['created_at'] = date('Y-m-d H:i:s');
					$banner['data']['created_by'] = $_SESSION['id_user_admin'];
					$banner['data']['status'] = 1;
					// var_dump($banner);
					// die();
					$banner['table']		= "t_banner";
					// redirect('admin/banner');
					if ($this->model_global->addData($banner)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil tambah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					// var_dump($_SESSION);
					$data['page'] 	= array('p' => 'banner', 'c' => 'tambah banner lot');
					$data['content'] 	= $this->load->view('admin/banner/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'ubah':
				validate_access($_SESSION['menu']['banner']['update']);
				$data['action'] = base_url('admin/banner/ubah');
				$banner_detail['select']	= "k.*";
				$banner_detail['table']		= "t_banner as k";

				if (isset($id)) {
					$banner_detail['where']	= "k.id_banner = '" . $id . "' and k.status = 1";
				} else {
					$banner_detail['where']	= "k.id_banner = '" . $_POST['id_banner'] . "' and k.status = 1";
				}

				$data['banner_detail'] 	= $this->model_global->getData($banner_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['banner_detail'],"<hr>");
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					// die();

					$banner['data']['judul'] = $_POST['judul'];
					if (!empty($_FILES['gambar']['name'])) {
						$banner['data']['gambar']	= unggah_gambar('./assets/uploads/banner', 'gambar', 'png|jpg|jpeg');
						if ($banner['data']['gambar'] == "Gagal Upload") {
							$json['status'] = FALSE;
							// $json['msg'] = ;

							echo json_encode($json);
							die;
						}
					};
					$banner['data']['deskripsi'] = $_POST['deskripsi'];
					// var_dump($banner['data'],"<hr>");						
					// die();
					$banner['data']['updated_at'] = date('Y-m-d H:i:s');
					$banner['data']['updated_by'] = $_SESSION['id_user_admin'];
					$banner['table']		= "t_banner";
					$banner['where'][0] 	= array('id_banner', $_POST['id_banner']);
					// $this->model_global->updateData($banner);
					// redirect('admin/banner');
					if ($this->model_global->updateData($banner)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					//var_dump($data['banner_detail']);
					$data['page'] 	= array('p' => 'banner', 'c' => 'ubah kata kunci');
					$data['content'] 	= $this->load->view('admin/banner/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'hapus':
				validate_access($_SESSION['menu']['banner']['delete']);
				// $data['id']		= $_POST['id'];
				// $data['search']	= "id_banner";
				// $data['table']	= "t_banner";

				// $this->model_global->delById($data);
				redirect('api/close_bid/' . $id);
				// echo json_encode(array('url' => base_url('admin/banner')));
				break;

			case 'restore':
				validate_access($_SESSION['menu']['banner']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_banner";
				$data['table']	= "t_banner";

				$this->model_global->restorById($data);
				redirect('admin/banner');
				break;

			default:
				// $data['page'] 		= array('p' => 'banner', 'c' => '' );

				$view['content'] = $this->load->view('admin/banner/banner', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function event($action = null, $id = null)
	{ // Done
		validate_access($_SESSION['menu']['event']);
		$data = array();
		$data['page'] = "event";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'event', 'c' => '' );

		$data['back']	= base_url('admin/event');
		$data['proses'] = ucwords($action);

		switch ($action) {
			case 'tambah':
				validate_access($_SESSION['menu']['event']['create']);
				$data['action'] = base_url('admin/event/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					$event['data']['nama_event'] = $_POST['nama_event'];
					$event['data']['waktu_event'] = date_format(date_create($_POST['waktu_event']), 'Y-m-d H:i:s');
					if (!empty($_FILES['gambar']['name'])) {
						$event['data']['gambar']	= unggah_gambar('./assets/uploads/event', 'gambar', 'png|jpg|jpeg');
					};
					$event['data']['alamat_event'] = $_POST['alamat_event'];
					$event['data']['link_maps'] = $_POST['link_maps'];
					$event['data']['deskripsi_event'] = $_POST['deskripsi_event'];
					$event['data']['kode_event'] = sha1($event['data']['waktu_event'] . random_code());
					// var_dump($event['data'],"<hr>");						
					// die();
					$event['data']['created_at'] = date('Y-m-d H:i:s');
					$event['data']['created_by'] = $_SESSION['id_user_admin'];
					$event['data']['status'] = 1;
					// var_dump($event);
					// die();
					$event['table']		= "t_event";
					// redirect('admin/event');
					if ($this->model_global->addData($event)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil tambah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					// var_dump($_SESSION);
					$data['page'] 	= array('p' => 'event', 'c' => 'tambah event lot');
					$data['content'] 	= $this->load->view('admin/event/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'ubah':
				validate_access($_SESSION['menu']['event']['update']);
				$data['action'] = base_url('admin/event/ubah');
				$event_detail['select']	= "k.*";
				$event_detail['table']		= "t_event as k";

				if (isset($id)) {
					$event_detail['where']	= "k.id_event = '" . $id . "' and k.status = 1";
				} else {
					$event_detail['where']	= "k.id_event = '" . $_POST['id_event'] . "' and k.status = 1";
				}

				$data['event_detail'] 	= $this->model_global->getData($event_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['event_detail'],"<hr>");
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					// die();

					$event['data']['nama_event'] = $_POST['nama_event'];
					$event['data']['waktu_event'] = date_format(date_create($_POST['waktu_event']), 'Y-m-d H:i:s');
					if (!empty($_FILES['gambar']['name'])) {
						$event['data']['gambar']	= unggah_gambar('./assets/uploads/event', 'gambar', 'png|jpg|jpeg');
						if ($event['data']['gambar'] == "Gagal Upload") {
							$json['status'] = FALSE;
							// $json['msg'] = ;

							echo json_encode($json);
							die;
						}
					};
					$event['data']['alamat_event'] = $_POST['alamat_event'];
					$event['data']['link_maps'] = $_POST['link_maps'];
					$event['data']['deskripsi_event'] = $_POST['deskripsi_event'];
					// $event['data']['kode_event'] = sha1($event['data']['waktu_event'].random_code());
					// var_dump($event['data'],"<hr>");						
					// die();
					$event['data']['updated_at'] = date('Y-m-d H:i:s');
					$event['data']['updated_by'] = $_SESSION['id_user_admin'];
					$event['table']		= "t_event";
					$event['where'][0] 	= array('id_event', $_POST['id_event']);
					// $this->model_global->updateData($event);
					// redirect('admin/event');
					if ($this->model_global->updateData($event)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					//var_dump($data['event_detail']);
					$data['page'] 	= array('p' => 'event', 'c' => 'ubah kata kunci');
					$data['content'] 	= $this->load->view('admin/event/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'bidsys':
				if (!empty($_SESSION['pass_event'])) {
					// destroy session pass_event
					$this->session->unset_userdata('pass_event');
				}
				$this->load->config('pusher');
				$data['id_event'] = $id;
				$data['kode_event'] = $_GET['kode_event'];
				$data['email'] = 'adminsun@mail.com';

				// ambil id_lot untuk tau current lot berdasarkan kode_event di t_bidding
				$bidding['select'] = 'id_lot';
				$bidding['from'] = 't_bidding';
				$bidding['where'][] = "id_event = '" . $id . "' AND status = 1";
				$bidding['group_by'] = array('id_lot');
				$bidding['limit'] = 1;
				$bidding['order'][] = array('id_lot', 'desc');
				$data['current_lot'] = $this->model_global->getData($bidding);
				// var_dump($data);die;
				if (@$_GET['id_lot'] < @$data['current_lot'][0]->id_lot) {
					// $url = ;
					// var_dump($url);die;
					redirect("admin/event/bidsys/{$id}?kode_event={$_GET['kode_event']}&id_lot={$data['current_lot'][0]->id_lot}");
				}
				// List Lot di event ini
				$barang_lelang['select'] = "b.id_barang_lelang, b.nama_barang_lelang, b.brand, b.tahun, b.transmisi, b.no_polisi, b.odometer, b.grade, f.gambar, e.waktu_event, e.alamat_event, e.link_maps, l.id_lot, l.harga_awal";
				$barang_lelang['from'] = "t_lot as l";
				$barang_lelang['join'][] = array('m_barang_lelang as b', 'l.id_barang_lelang = b.id_barang_lelang and b.status = 1');
				$barang_lelang['join'][] = array('m_foto_barang_lelang as f', 'f.id_barang_lelang = b.id_barang_lelang and f.status = 1', 'left');
				$barang_lelang['join'][] = array('t_event as e', 'e.id_event = l.id_event and e.status = 1');
				$barang_lelang['group_by'] = "l.id_barang_lelang";
				$barang_lelang['where'][] = "e.id_event = '" . $data['id_event'] . "' AND l.status = 1 AND l.status_lot = 1";
				$barang_lelang['order'][] = array('l.id_lot', 'asc');
				$data['barang_lelang'] = $this->model_global->getData($barang_lelang);

				// lot sekarang
				$lot_lelang['select'] = "b.id_barang_lelang, b.nama_barang_lelang, b.brand, b.tahun, b.transmisi, b.no_polisi, b.odometer, b.grade, f.gambar, l.id_lot, l.harga_awal, l.no_lot";
				$lot_lelang['from'] = "t_lot as l";
				$lot_lelang['join'][] = array('m_barang_lelang as b', 'l.id_barang_lelang = b.id_barang_lelang and b.status = 1');
				$lot_lelang['join'][] = array('m_foto_barang_lelang as f', 'f.id_barang_lelang = b.id_barang_lelang and f.status = 1', 'left');
				$lot_lelang['join'][] = array('t_event as e', 'e.id_event = l.id_event and e.status = 1');
				$lot_lelang['group_by'] = "l.id_barang_lelang";
				$lot_lelang['where'][] = "e.id_event = '" . $data['id_event'] . "' AND l.status = 1 AND l.status_lot = 1";
				$lot_lelang['order'][] = array('l.id_lot', 'asc');
				$lot_lelang['limit'] = 2;
				if (!empty($_GET['id_lot'])) {
					$lot_lelang['where'][] = "l.id_lot >= '" . $_GET['id_lot'] . "'";
				}
				$data['lot_lelang'] = $this->model_global->getData($lot_lelang);

				// next id_lot
				// var_dump($data);die;
				$data['setting'] = $this->setting;
				$view['content'] = $this->load->view('admin/event/form_lelang', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;

			case 'hapus':
				validate_access($_SESSION['menu']['event']['delete']);
				// $data['id']		= $_POST['id'];
				// $data['search']	= "id_event";
				// $data['table']	= "t_event";

				// $this->model_global->delById($data);
				redirect('api/close_bid/' . $id);
				// echo json_encode(array('url' => base_url('admin/event')));
				break;

			case 'restore':
				validate_access($_SESSION['menu']['event']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_event";
				$data['table']	= "t_event";

				$this->model_global->restorById($data);
				redirect('admin/event');
				break;

			default:
				// $data['page'] 		= array('p' => 'event', 'c' => '' );

				$view['content'] = $this->load->view('admin/event/event', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function kontak($action = null, $id = null)
	{ // Postpone
		validate_access($_SESSION['menu']['kontak']);
		$data = array();
		$data['page'] = "kontak";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'kontak', 'c' => '' );

		$data['back']	= base_url('admin/kontak');
		$data['proses'] = ucwords($action);

		switch ($action) {
			case 'ubah':
				validate_access($_SESSION['menu']['kontak']['update']);
				$data['proses'] = "Respon Kontak";
				$data['action'] = base_url('admin/kontak/ubah');
				$kontak_detail['select']	= "k.*";
				$kontak_detail['table']		= "t_kontak as k";

				if (isset($id)) {
					$kontak_detail['where']	= "k.id_kontak = '" . $id . "' and k.status = 1";
				} else {
					$kontak_detail['where']	= "k.id_kontak = '" . $_POST['id_kontak'] . "' and k.status = 1";
				}

				$data['kontak_detail'] 	= $this->model_global->getData($kontak_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['kontak_detail'],"<hr>");
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					// die();						

					$kontak['data']['nama'] = $_POST['nama'];
					$kontak['data']['email'] = $_POST['email'];
					$kontak['data']['no_tlp'] = $_POST['no_tlp'];
					$kontak['data']['pesan'] = $_POST['pesan'];
					$kontak['data']['updated_at'] = date('Y-m-d H:i:s');
					$kontak['data']['updated_by'] = $_SESSION['id_user_admin'];
					$kontak['table']		= "t_kontak";
					$kontak['where'][0] 	= array('id_kontak', $_POST['id_kontak']);
					// $this->model_global->updateData($data);
					// redirect('admin/kontak');
					if ($this->model_global->updateData($kontak)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$data['page'] 	= array('p' => 'kontak', 'c' => 'ubah kontak');
					$data['content'] 	= $this->load->view('admin/kontak/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'hapus':
				validate_access($_SESSION['menu']['kontak']['delete']);
				$data['id']		= $_POST['id'];
				$data['search']	= "id_kontak";
				$data['table']	= "t_kontak";

				$this->model_global->delById($data);
				echo json_encode(array('url' => base_url('admin/kontak')));
				break;

			case 'restore':
				validate_access($_SESSION['menu']['kontak']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_kontak";
				$data['table']	= "t_kontak";

				$this->model_global->restorById($data);
				redirect('admin/kontak');
				break;

			default:
				// $data['page'] 		= array('p' => 'kontak', 'c' => '' );

				$view['content'] = $this->load->view('admin/kontak/kontak', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function peserta($action = null, $id = null)
	{ // Done
		validate_access($_SESSION['menu']['peserta']);
		$data = array();
		$data['page'] = "peserta";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'peserta', 'c' => '' );

		$data['back']	= base_url('admin/peserta');
		$data['proses'] = ucwords($action);

		switch ($action) {
			case 'tambah':
				validate_access($_SESSION['menu']['peserta']['create']);
				$data['action'] = base_url('admin/peserta/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					// die();
					$peserta['data']['nama'] = $_POST['nama'];
					$peserta['data']['email'] = $_POST['email'];
					$peserta['data']['no_hp'] = $_POST['no_hp'];
					$peserta['data']['alamat'] = $_POST['alamat'];
					$peserta['data']['nik'] = $_POST['nik'];
					$peserta['data']['npwp'] = $_POST['npwp'];
					$peserta['data']['is_aktif'] = 1;
					// $peserta['data']['bertindak_mewakili'] = $_POST['bertindak_mewakili'];						
					// $peserta['data']['nama_perusahaan'] = $_POST['nama_perusahaan'];
					// $peserta['data']['npwp_perusahaan'] = $_POST['npwp_perusahaan'];						
					// $peserta['data']['alamat_perusahaan'] = $_POST['alamat_perusahaan'];						
					// $peserta['data']['telp_kantor'] = $_POST['telp_kantor'];						
					// $peserta['data']['email_kantor'] = $_POST['email_kantor'];

					$peserta['data']['created_at'] = date('Y-m-d H:i:s');
					$peserta['data']['created_by'] = $_SESSION['id_user_admin'];
					$peserta['data']['status'] = 1;
					// var_dump($peserta);
					// die();
					$peserta['table']		= "m_peserta";
					// $json['id_peserta_lot'] 	= $this->model_global->addData($data);
					// redirect('admin/peserta');
					if ($this->model_global->addData($peserta)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil tambah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					// var_dump($_SESSION);
					$data['page'] 	= array('p' => 'peserta', 'c' => 'tambah peserta lot');
					$data['content'] 	= $this->load->view('admin/peserta/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'ubah':
				validate_access($_SESSION['menu']['peserta']['update']);
				$data['action'] = base_url('admin/peserta/ubah');
				$peserta_detail['select']	= "k.*";
				$peserta_detail['table']		= "m_peserta as k";

				if (isset($id)) {
					$peserta_detail['where']	= "k.id_peserta = '" . $id . "' and k.status = 1";
				} else {
					$peserta_detail['where']	= "k.id_peserta = '" . $_POST['id_peserta'] . "' and k.status = 1";
				}

				$data['peserta_detail'] 	= $this->model_global->getData($peserta_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['peserta_detail'],"<hr>");
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					// die();

					$peserta['data']['nama'] = $_POST['nama'];
					$peserta['data']['email'] = $_POST['email'];
					$peserta['data']['no_hp'] = $_POST['no_hp'];
					$peserta['data']['alamat'] = $_POST['alamat'];
					$peserta['data']['nik'] = $_POST['nik'];
					$peserta['data']['npwp'] = $_POST['npwp'];
					$peserta['data']['bertindak_mewakili'] = $_POST['bertindak_mewakili'];
					$peserta['data']['nama_perusahaan'] = $_POST['nama_perusahaan'];
					$peserta['data']['npwp_perusahaan'] = $_POST['npwp_perusahaan'];
					$peserta['data']['alamat_perusahaan'] = $_POST['alamat_perusahaan'];
					$peserta['data']['telp_kantor'] = $_POST['telp_kantor'];
					$peserta['data']['email_kantor'] = $_POST['email_kantor'];

					$peserta['data']['updated_at'] = date('Y-m-d H:i:s');
					$peserta['data']['updated_by'] = $_SESSION['id_user_admin'];
					$peserta['table']		= "m_peserta";
					$peserta['where'][0] 	= array('id_peserta', $_POST['id_peserta']);
					// $this->model_global->updateData($data);
					// redirect('admin/peserta');
					if ($this->model_global->updateData($peserta)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$data['page'] 	= array('p' => 'peserta', 'c' => 'ubah peserta');
					$data['content'] 	= $this->load->view('admin/peserta/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'verifikasi_npl':
				validate_access($_SESSION['menu']['peserta']['update']);
				$data['action'] = base_url('admin/peserta/ubah');
				$npl_detail['select']	= "p.*, n.*, e.nama_event, e.waktu_event";
				$npl_detail['from']		= "m_peserta as p";
				$npl_detail['join'][] 	= array('t_pembelian_npl as n', 'n.id_peserta = p.id_peserta AND n.verifikasi = 0');
				$npl_detail['join'][] 	= array('t_event as e', 'e.id_event = n.id_event AND e.status = 1', 'left');

				if (isset($id)) {
					$npl_detail['where']	= "n.id_pembelian_npl = '" . $id . "' and p.status = 1";
				} else {
					$npl_detail['where']	= "n.id_pembelian_npl = '" . $_POST['id_pembelian_npl'] . "' and p.status = 1";
				}

				$data['npl_detail'] 	= $this->model_global->getData($npl_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['npl_detail'],"<hr>");
					// var_dump($_POST, "<hr>");
					// var_dump($_FILES,"<hr>");
					// die();
					$pembelian_npl = array();
					$pembelian_npl['data']['verifikasi'] = (!empty($_POST['verifikasi'])) ? $_POST['verifikasi'] : 2;
					$pembelian_npl['data']['pesan_verifikasi'] = @$_POST['pesan_verifikasi'];
					$pembelian_npl['data']['updated_at'] = date('Y-m-d H:i:s');
					$pembelian_npl['data']['updated_by'] = $_SESSION['id_user_admin'];
					$pembelian_npl['table']		= "t_pembelian_npl";
					$pembelian_npl['where'][0] 	= array('id_pembelian_npl', $_POST['id_pembelian_npl']);
					// var_dump($pembelian_npl);
					// die;
					// $this->model_global->updateData($data);
					// redirect('admin/peserta');
					if ($this->model_global->updateData($pembelian_npl)) {
						if (!empty($_POST['verifikasi'])) {
							try {
								$event_detail['select']	= "n.id_npl";
								$event_detail['from']	= "m_npl as n";
								$event_detail['join'][] 	= array('t_pembelian_npl as p', 'n.id_event = p.id_event');
								$event_detail['where']	= "n.id_event = '" . $data['npl_detail'][0]->id_event . "' AND p.type_pembelian = 'online'";
								$jumlah_event	= $this->model_global->getDataRows($event_detail);
								for ($i = 0; $i < $_POST['jumlah']; $i++) {
									$jumlah_event++;
									$npl = array();
									$npl['data']['id_peserta'] = $_POST['id_peserta'];
									$npl['data']['id_event'] = $_POST['id_event'];
									$npl['data']['no_npl'] = "A_" . sprintf("%'.05d\n", $jumlah_event);
									$npl['data']['id_pembelian_npl'] = $_POST['id_pembelian_npl'];
									$npl['data']['npl'] = hash('sha256', time() . '.' . $_POST['id_peserta'] . '-' . random_code(10));
									$npl['data']['status_npl'] = 1;

									$npl['data']['created_at'] = date('Y-m-d H:i:s');
									$npl['data']['created_by'] = $_SESSION['id_user_admin'];
									$npl['data']['status'] = 1;
									// var_dump($peserta);
									// die();
									$npl['table']		= "m_npl";
									$this->model_global->addData($npl);
									sleep(1);
									$notifikasi = array();
									$notifikasi['data']['pengirim'] = 0;
									$notifikasi['data']['penerima'] = $_POST['id_peserta'];
									$notifikasi['data']['type_user'] = "peserta";
									$notifikasi['data']['type'] = "verifikasi";
									$notifikasi['data']['judul'] = "Pembelian NPL Berhasil";
									$notifikasi['data']['pesan'] = "Selamat, pembelia NPL anda berhasil untuk event lelang" . " dengan nomer " . $npl['data']['no_npl'];
									$notifikasi['data']['is_read'] = 0;
									// $notifikasi['data']['deskripsi'] = json_encode($bidding_lot_detail);
									$notifikasi['data']['created_at'] = date('Y-m-d H:i:s');
									set_notifikasi($notifikasi['data']);
								}
								// $json['id_peserta_lot'] 	= $this->model_global->addData($data);

								if (true) {
									// Kirim Email Invoice
									$json['status'] = TRUE;
									$json['status_add'] = "berhasil tambah";
									echo json_encode($json);
								} else {
									$json['msg'] = "Gagal Tambah NPL";
									$json['status'] = FALSE;
									echo json_encode($json);
								}
							} catch (\Throwable $th) {
								$json['status'] = FALSE;
								$json['pesan'] = $th;
								echo json_encode($json);
							}
						} else {
							$json['msg'] = "Pembelian NPL Ditolak";
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						$json['msg'] = "Gagal Update Pembelian NPL";
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {

					$data['page'] 	= array('p' => 'peserta', 'c' => 'verifikasi NPL');
					$data['content'] 	= $this->load->view('admin/peserta/form_verifikasi', $data, TRUE);
					echo json_encode($data);
				}
				break;

				// case 'list_npl':
				// 	validate_access($_SESSION['menu']['peserta']['update']);
				// 	$data['action'] = base_url('admin/peserta/list_npl');
				// 	$data['id_peserta'] = $id;
				// 	$peserta_detail['select']	= "k.*";
				// 	$peserta_detail['table']	= "m_peserta as k";
				// 	$peserta_detail['where']	= "k.id_peserta = '".$id."' and k.status = 1";
				// 	$data['peserta_detail'] 	= $this->model_global->getData($peserta_detail);
				// 	// $data['page'] 	= array('p' => 'peserta', 'c' => 'ubah peserta' );
				// 	$view['content'] 	= $this->load->view('admin/npl_peserta/npl_peserta',$data,TRUE);
				// 	$this->load->view('admin/layout',$view,false);
				// break;

			case 'hapus':
				validate_access($_SESSION['menu']['peserta']['delete']);
				$data['id']		= $_POST['id'];
				$data['search']	= "id_peserta";
				$data['table']	= "m_peserta";

				$this->model_global->delById($data);
				echo json_encode(array('url' => base_url('admin/peserta')));
				break;

			case 'restore':
				validate_access($_SESSION['menu']['peserta']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_peserta";
				$data['table']	= "m_peserta";

				$this->model_global->restorById($data);
				redirect('admin/peserta');
				break;

			default:
				// $data['page'] 		= array('p' => 'peserta', 'c' => '' );

				$view['content'] = $this->load->view('admin/peserta/peserta', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function npl_peserta($action = null)
	{ // Done
		validate_access($_SESSION['menu']['peserta']);
		$data = array();
		$data['page'] = "peserta";
		$data['id_peserta'] = @$_GET['id_peserta'];
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'peserta', 'c' => '' );

		$data['back']	= base_url('admin/npl_peserta');
		$data['proses'] = ucwords($action);

		switch ($action) {
			case 'tambah':
				validate_access($_SESSION['menu']['peserta']['create']);
				$data['action'] = base_url('admin/npl_peserta/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					// die();	
					try {
						$pembelian_npl['data']['id_peserta'] = @$_POST['id_peserta'];
						$pembelian_npl['data']['id_event'] = @$_POST['id_event'];
						$pembelian_npl['data']['waktu'] = date('Y-m-d H:i:s');
						$pembelian_npl['data']['type_pembelian'] = @$_POST['type_pembelian'];
						$pembelian_npl['data']['type_transaksi'] = @$_POST['type_transaksi'];
						$pembelian_npl['data']['no_rek'] = @$_POST['no_rek'];
						$pembelian_npl['data']['nama_pemilik'] = @$_POST['nama_pemilik'];
						$pembelian_npl['data']['nominal'] = @$_POST['nominal'];
						$pembelian_npl['data']['no_pembelian'] = time() . '.' . @$_POST['id_peserta'];
						$pembelian_npl['data']['verifikasi'] = 1;

						if (!empty($_FILES['bukti']['name'])) {
							$pembelian_npl['data']['bukti']	= unggah_gambar('./assets/uploads/bukti_npl', 'bukti', 'png|jpg|jpeg');
						};

						$pembelian_npl['data']['created_at'] = date('Y-m-d H:i:s');
						$pembelian_npl['data']['created_by'] = $_SESSION['id_user_admin'];
						$pembelian_npl['data']['status'] = 1;
						// var_dump($peserta);
						// die();
						$pembelian_npl['table']		= "t_pembelian_npl";
						// $json['id_peserta_lot'] 	= $this->model_global->addData($data);
						$id_pembelian_npl = $this->model_global->addData($pembelian_npl);
					} catch (\Throwable $th) {
						$json['status'] = FALSE;
						$json['pesan'] = $th;
						echo json_encode($json);
					}

					try {
						$npl['data']['id_peserta'] = @$_POST['id_peserta'];
						$npl['data']['id_event'] = @$_POST['id_event'];
						$npl['data']['no_npl'] = "B_" . sprintf("%'.05d\n", @$_POST['no_npl']);
						$npl['data']['id_pembelian_npl'] = $id_pembelian_npl;
						$npl['data']['npl'] = hash('sha256', time() . '.' . @$_POST['id_peserta']);
						$npl['data']['status_npl'] = 1;

						$npl['data']['created_at'] = date('Y-m-d H:i:s');
						$npl['data']['created_by'] = $_SESSION['id_user_admin'];
						$npl['data']['status'] = 1;
						// var_dump($peserta);
						// die();
						$npl['table']		= "m_npl";
						// $json['id_peserta_lot'] 	= $this->model_global->addData($data);

						if ($this->model_global->addData($npl)) {
							// Kirim Email Invoice
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil tambah";
							echo json_encode($json);
						} else {
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} catch (\Throwable $th) {
						$json['status'] = FALSE;
						$json['pesan'] = $th;
						echo json_encode($json);
					}
				} else {
					// var_dump($_SESSION);
					$event_detail['select']	= "*";
					$event_detail['table']	= "t_event";
					$event_detail['where']	= "waktu_event BETWEEN '" . date('Y-m-d', strtotime(date('Y-m-d'))) . "' AND '" . date('Y-m-d', strtotime(date('Y-m-d') . "+1 day")) . "' AND status = 1";
					$event_detail['order'][]	= array('waktu_event', 'desc');
					$data['event_detail'] 	= $this->model_global->getData($event_detail);

					$data['page'] 	= array('p' => 'peserta', 'c' => 'tambah NPL peserta');
					$data['content'] 	= $this->load->view('admin/npl_peserta/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'verifikasi':
				# code...
				break;

			case 'detail':
				validate_access($_SESSION['menu']['peserta']['create']);
				$data['action'] = base_url('admin/npl_peserta/detail');

				$npl_detail['select']	= "m.id_peserta, n.id_npl, n.no_npl, n.npl, p.waktu, p.no_pembelian, p.bukti, p.no_rek, p.nominal, p.nama_pemilik, p.tanggal_transfer, e.id_event, e.nama_event, e.waktu_event";
				$npl_detail['table']	= "m_peserta as m";
				$npl_detail['join'][]	= array("m_npl as n", 'n.id_peserta = m.id_peserta');
				$npl_detail['join'][]	= array("t_pembelian_npl as p", 'p.id_peserta = m.id_peserta');
				$npl_detail['join'][]	= array("t_event as e", 'e.id_event = n.id_event', 'left');
				$npl_detail['where']	= "n.id_npl = '" . $_GET['id_npl'] . "' and m.status = 1";
				$data['npl_detail'] 	= $this->model_global->getData($npl_detail);

				$data['page'] 	= array('p' => 'peserta', 'c' => 'detail NPL peserta');
				$data['content'] 	= $this->load->view('admin/npl_peserta/form', $data, TRUE);
				echo json_encode($data);
				break;

			default:
				// $data['page'] 		= array('p' => 'peserta', 'c' => '' );

				$view['content'] = $this->load->view('admin/npl_peserta/npl_peserta', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function barang($action = null, $id = null)
	{ // Done
		validate_access($_SESSION['menu']['barang']);
		$data = array();
		$data['page'] = "barang";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'lot', 'c' => '' );

		$data['back']	= base_url('admin/barang');
		$data['proses'] = ucwords($action);
		switch ($action) {
			case 'tambah':
				validate_access($_SESSION['menu']['barang']['create']);
				$data['action'] = base_url('admin/barang/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($data['lot_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					// die();
					$barang = array();
					$barang['data']['id_kategori'] = @$_POST['id_kategori'];
					$barang['data']['nama_barang_lelang'] = strtolower(@$_POST['nama_barang_lelang']);
					$barang['data']['brand'] = strtolower(@$_POST['brand']);
					$barang['data']['warna'] = strtolower(@$_POST['warna']);
					$barang['data']['deskripsi'] = @$_POST['deskripsi'];
					$barang['data']['lokasi_barang'] = @$_POST['lokasi_barang'];
					switch ($_POST['id_kategori']) {
						case 1:
						case 2:
							$barang['data']['no_rangka'] = @$_POST['no_rangka'];
							$barang['data']['no_mesin'] = @$_POST['no_mesin'];
							$barang['data']['tipe_model'] = @$_POST['tipe_model'];
							$barang['data']['transmisi'] = @$_POST['transmisi'];
							$barang['data']['bahan_bakar'] = @$_POST['bahan_bakar'];
							$barang['data']['odometer'] = @$_POST['odometer'];

							$barang['data']['grade_interior'] = @$_POST['grade_interior'];
							$barang['data']['grade_exterior'] = @$_POST['grade_exterior'];
							$barang['data']['grade_mesin'] = @$_POST['grade_mesin'];
							$barang['data']['grade'] = @$_POST['grade'];

							$barang['data']['no_polisi'] = @$_POST['no_polisi'];
							$barang['data']['valid_stnk'] = date_format(date_create(@$_POST['valid_stnk']), 'Y-m-d');
							$barang['data']['tahun'] = @$_POST['tahun'];
							$barang['data']['kapasitas_kendaraan'] = @$_POST['kapasitas_kendaraan'];

							// upload file Image stnk
							$barang['data']['stnk'] = (!empty($_POST['stnk'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['stnk']['name'])) {
							// 		$path = "./assets/uploads/stnk";
							// 		$barang['data']['stnk'] = unggah_gambar($path, 'stnk', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }

							// upload file Image bpkb
							$barang['data']['bpkb'] = (!empty($_POST['bpkb'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['bpkb']['name'])) {
							// 		$path = "./assets/uploads/bpkb";
							// 		$barang['data']['bpkb'] = unggah_gambar($path, 'bpkb', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }

							// upload file Image faktur
							$barang['data']['faktur'] = (!empty($_POST['faktur'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['faktur']['name'])) {
							// 		$path = "./assets/uploads/faktur";
							// 		$barang['data']['faktur'] = unggah_gambar($path, 'faktur', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }

							// upload file Image ktp_pemilik
							$barang['data']['ktp_pemilik'] = (!empty($_POST['ktp_pemilik'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['ktp_pemilik']['name'])) {
							// 		$path = "./assets/uploads/ktp_pemilik";
							// 		$barang['data']['ktp_pemilik'] = unggah_gambar($path, 'ktp_pemilik', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }

							// upload file Image kwitansi
							$barang['data']['kwitansi'] = (!empty($_POST['kwitansi'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['kwitansi']['name'])) {
							// 		$path = "./assets/uploads/kwitansi";
							// 		$barang['data']['kwitansi'] = unggah_gambar($path, 'kwitansi', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }
							$barang['data']['sph'] = (!empty($_POST['sph'])) ? "ADA" : "TIDAK ADA";
							$barang['data']['kir'] = (!empty($_POST['kir'])) ? "ADA" : "TIDAK ADA";

							break;

						default:
							# code...
							break;
					}

					$barang['data']['created_at'] = date('Y-m-d H:i:s');
					$barang['data']['created_by'] = $_SESSION['id_user_admin'];
					$barang['table']		= "m_barang_lelang";
					$id_barang_lelang = $this->model_global->addData($barang);


					for ($i = 0; $i < count($_FILES['gambar']['name']); $i++) {
						// upload file Image Barang Lelang
						$foto_barang = array();
						try {
							if (!empty($_FILES['gambar']['name'][$i])) {
								$foto_barang['data']['id_barang_lelang'] = $id_barang_lelang;
								$path = "./assets/uploads/gambar_barang_lelang";
								if (!file_exists($path)) {
									mkdir($path, 0777, true);
								}
								$new_name = time() . "-" . str_replace([" "], "_", $_FILES['gambar']['name'][$i]);
								move_uploaded_file($_FILES['gambar']['tmp_name'][$i], $path . "/origin/" . $new_name);
								$foto_barang['data']['gambar'] = $new_name;
								$foto_barang['table']		= "m_foto_barang_lelang";
								$id_foto_barang_lelang = $this->model_global->addData($foto_barang);
							}
						} catch (\Throwable $th) {
						}
					}
					// $this->model_global->updateData($data);
					// redirect('admin/lot');
					if (!empty($id_barang_lelang)) {
						// simpan gambar barang lelang
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$kategori['select']	= "*";
					$kategori['table']	= "m_kategori";
					$kategori['where']	= "status = 1";
					$data['kategori_detail'] 	= $this->model_global->getData($kategori);

					$data['page'] 	= array('p' => 'barang', 'c' => 'tambah barang');
					$data['content'] 	= $this->load->view('admin/barang/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'ubah':
				validate_access($_SESSION['menu']['barang']['update']);
				$data['action'] = base_url('admin/barang/ubah');
				$id = (!empty($id)) ? $id : ((!empty($_POST['id_barang_lelang'])) ? $_POST['id_barang_lelang'] : 0);

				$barang_lelang_detail['select']	= "k.*";
				$barang_lelang_detail['table']		= "m_barang_lelang as k";
				// $barang_lelang_detail['join'][0]		= array('m_foto_barang_lelang as m', 'm.id_barang_lelang = k.id_barang_lelang','left');
				$barang_lelang_detail['where']	= "k.id_barang_lelang = '" . $id . "' and k.status = 1";

				$data['barang_lelang_detail'] 	= $this->model_global->getData($barang_lelang_detail);

				$foto_barang_lelang['select']	= "k.gambar as id, CONCAT('" . base_url('assets/uploads/gambar_barang_lelang/origin/') . "',k.gambar) as src";
				$foto_barang_lelang['table']	= "m_foto_barang_lelang as k";
				$foto_barang_lelang['where']	= "k.id_barang_lelang = '" . $id . "' and k.status = 1";
				$data['foto_barang_lelang'] 	= $this->model_global->getData($foto_barang_lelang);

				// var_dump($data['foto_barang_lelang']);die;

				if (isset($_POST['submit'])) {
					// var_dump($data['lot_detail'],"<hr>");
					// var_dump($_POST, "<hr>");
					// var_dump($_FILES,"<hr>");
					// die();
					$barang = array();
					$barang['data']['id_kategori'] = $_POST['id_kategori'];
					$barang['data']['nama_barang_lelang'] = strtolower($_POST['nama_barang_lelang']);
					$barang['data']['brand'] = strtolower($_POST['brand']);
					$barang['data']['warna'] = strtolower($_POST['warna']);
					$barang['data']['deskripsi'] = $_POST['deskripsi'];
					$barang['data']['lokasi_barang'] = @$_POST['lokasi_barang'];
					switch ($_POST['id_kategori']) {
						case 1:
						case 2:
							$barang['data']['no_rangka'] = $_POST['no_rangka'];
							$barang['data']['no_mesin'] = $_POST['no_mesin'];
							$barang['data']['tipe_model'] = $_POST['tipe_model'];
							$barang['data']['transmisi'] = $_POST['transmisi'];
							$barang['data']['bahan_bakar'] = $_POST['bahan_bakar'];
							$barang['data']['odometer'] = $_POST['odometer'];

							$barang['data']['grade_interior'] = $_POST['grade_interior'];
							$barang['data']['grade_exterior'] = $_POST['grade_exterior'];
							$barang['data']['grade_mesin'] = $_POST['grade_mesin'];
							$barang['data']['grade'] = $_POST['grade'];

							$barang['data']['no_polisi'] = $_POST['no_polisi'];
							$barang['data']['valid_stnk'] = date_format(date_create($_POST['valid_stnk']), 'Y-m-d');
							$barang['data']['tahun'] = $_POST['tahun'];
							$barang['data']['kapasitas_kendaraan'] = @$_POST['kapasitas_kendaraan'];

							// upload file Image stnk
							$barang['data']['stnk'] = (!empty($_POST['stnk'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['stnk']['name'])) {
							// 		$path = "./assets/uploads/stnk";
							// 		$barang['data']['stnk'] = unggah_gambar($path, 'stnk', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }

							// upload file Image bpkb
							$barang['data']['bpkb'] = (!empty($_POST['bpkb'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['bpkb']['name'])) {
							// 		$path = "./assets/uploads/bpkb";
							// 		$barang['data']['bpkb'] = unggah_gambar($path, 'bpkb', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }

							// upload file Image faktur
							$barang['data']['faktur'] = (!empty($_POST['faktur'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['faktur']['name'])) {
							// 		$path = "./assets/uploads/faktur";
							// 		$barang['data']['faktur'] = unggah_gambar($path, 'faktur', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }

							// upload file Image ktp_pemilik
							$barang['data']['ktp_pemilik'] = (!empty($_POST['ktp_pemilik'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['ktp_pemilik']['name'])) {
							// 		$path = "./assets/uploads/ktp_pemilik";
							// 		$barang['data']['ktp_pemilik'] = unggah_gambar($path, 'ktp_pemilik', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }

							// upload file Image kwitansi
							$barang['data']['kwitansi'] = (!empty($_POST['kwitansi'])) ? "ADA" : "TIDAK ADA";
							// try {
							// 	if (!empty($_FILES['kwitansi']['name'])) {
							// 		$path = "./assets/uploads/kwitansi";
							// 		$barang['data']['kwitansi'] = unggah_gambar($path, 'kwitansi', 'png|jpg|jpeg');
							// 	}
							// } catch (\Throwable $th) {
							// }
							$barang['data']['sph'] = (!empty($_POST['sph'])) ? "ADA" : "TIDAK ADA";
							$barang['data']['kir'] = (!empty($_POST['kir'])) ? "ADA" : "TIDAK ADA";

							break;

						default:
							# code...
							break;
					}

					$barang['data']['updated_at'] = date('Y-m-d H:i:s');
					$barang['data']['updated_by'] = $_SESSION['id_user_admin'];
					$barang['table']		= "m_barang_lelang";
					$barang['where'][0] 	= array('id_barang_lelang', $_POST['id_barang_lelang']);

					// Update status 0 untuk gambar lama yang tidak ada di $_POST['old-gambar[]']
					foreach ($data['foto_barang_lelang'] as $key => $val) {
						$data_foto_barang = array();
						// var_dump($val->id, "<br>", in_array($val->id, $_POST['old-gambar']), "<hr>");
						// break;
						if (!in_array($val->id, $_POST['old-gambar'])) {
							$data_foto_barang['data']['status']		= 0;
							$data_foto_barang['where'][]		= array('id_barang_lelang', $_POST['id_barang_lelang']);
							$data_foto_barang['where'][]		= array('gambar', $val->id);
							$data_foto_barang['table']	= "m_foto_barang_lelang";

							$this->model_global->updateData($data_foto_barang);
						}
						// die;
					}

					for ($i = 0; $i < count($_FILES['gambar']['name']); $i++) {
						// upload file Image Barang Lelang
						$foto_barang = array();
						try {
							if (!empty($_FILES['gambar']['name'][$i])) {
								$foto_barang['data']['id_barang_lelang'] = $_POST['id_barang_lelang'];
								$path = "./assets/uploads/gambar_barang_lelang";
								if (!file_exists($path . '/origin')) {
									mkdir($path . '/origin', 0777, true);
								}
								$new_name = time() . "-" . str_replace([" "], "_", $_FILES['gambar']['name'][$i]);
								move_uploaded_file($_FILES['gambar']['tmp_name'][$i], $path . "/origin/" . $new_name);
								$foto_barang['data']['gambar'] = $new_name;
								$foto_barang['table']		= "m_foto_barang_lelang";
								$id_foto_barang_lelang = $this->model_global->addData($foto_barang);
							}
						} catch (\Throwable $th) {
						}
					}

					// $this->model_global->updateData($data);
					// redirect('admin/lot');
					if ($this->model_global->updateData($barang)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$kategori['select']	= "*";
					$kategori['table']	= "m_kategori";
					$kategori['where']	= "status = 1";
					$data['kategori_detail'] 	= $this->model_global->getData($kategori);

					$data['page'] 	= array('p' => 'barang', 'c' => 'ubah barang');
					$data['content'] 	= $this->load->view('admin/barang/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'import_data':
				validate_access($_SESSION['menu']['barang']['create']);
				switch ($_GET['type']) {
					case 'download':
						$this->load->helper('download');
						force_download('assets/downloads/Form import barang lelang.xls', NULL);
						break;
					case 'upload':
						$data['action'] = base_url('admin/barang/import_data?type=upload');
						if (!empty($_POST['submit'])) {
							include "./application/libraries/php-excel-reader-2.21/excel_reader2.php";
							// upload file xls
							$path = "./assets/uploads/excel_import";
							if (!file_exists($path)) {
								mkdir($path, 0777, true);
							}
							$new_name = time() . "-" . str_replace([" "], "_", $_FILES['file_import']['name']);
							move_uploaded_file($_FILES['file_import']['tmp_name'], $path . "/" . $new_name);

							// beri permisi agar file xls dapat di baca
							// chmod($_FILES['file_import']['name'],0777);
							chmod($path . "/" . $new_name, 0777);
							// var_dump(file_exists($path."/".$new_name));
							// die;
							if (file_exists($path . "/" . $new_name)) {
								// mengambil isi file xls
								$data = new Spreadsheet_Excel_Reader($path . "/" . $new_name, false);
							} else {
								var_dump("File Tidak terupload");
								die;
							}
							// menghitung jumlah baris data yang ada
							$jumlah_baris = $data->rowcount($sheet_index = 0);

							// jumlah default data yang berhasil di import
							$berhasil = 0;
							$data_insert = array();
							$counter = 0;
							for ($i = 2; $i <= $jumlah_baris; $i++) {
								// var_dump($data->sheets[0]['cells'][1][1]);
								// die;
								// menangkap data dan memasukkan ke variabel sesuai dengan kolumnya masing-masing
								for ($j = 1; $j <= count($data->sheets[0]['cells'][1]); $j++) {
									// var_dump($data->sheets[0]['cells'][1][$j]);
									// die;
									if (strtolower($data->sheets[0]['cells'][1][$j]) == 'kategori') {
										if (!empty($data->sheets[0]['cells'][$i][$j])) {
											switch (strtolower($data->sheets[0]['cells'][$i][$j])) {
												case 'mobil':
													$data_insert['data'][$counter]['id_kategori']     = 1;
													break;
												case 'motor':
													$data_insert['data'][$counter]['id_kategori']     = 2;
													break;
												case 'rumah':
													$data_insert['data'][$counter]['id_kategori']     = 3;
													break;
												case 'aset kantor':
													$data_insert['data'][$counter]['id_kategori']     = 4;
													break;
												case 'serba serbi':
													$data_insert['data'][$counter]['id_kategori']     = 5;
													break;

												default:
													$data_insert['data'][$counter]['id_kategori']     = 0;
													break;
											}
										} else {
											continue;
											// $data_insert['data'][$counter]['id_kategori']     = 0;
										}
									} else {
										// deploy data to header field
										switch ($data->sheets[0]['cells'][1][$j]) {
											case 'valid_stnk':
												$data_insert['data'][$counter][$data->sheets[0]['cells'][1][$j]]     = (!empty($data->sheets[0]['cells'][$i][$j])) ? date_format(date_create($data->sheets[0]['cells'][$i][$j]), 'Y-m-d') : "";
												break;

											default:
												$data_insert['data'][$counter][$data->sheets[0]['cells'][1][$j]]     = (!empty($data->sheets[0]['cells'][$i][$j])) ? $data->sheets[0]['cells'][$i][$j] : "";
												break;
										}
									}
								}
								// var_dump($data_insert['data']);
								// die;
								// $data_insert['data'][$counter]['id_kategori'] = 1;
								$data_insert['data'][$counter]['created_at'] = date('Y-m-d H:i:s');
								$data_insert['data'][$counter]['created_by'] = $_SESSION['id_user_admin'];
								$data_insert['data'][$counter]['status'] = 1;
								$counter++;
								// buat bulk insert

								// if($nama != "" && $alamat != "" && $telepon != ""){
								// 	// input data ke database (table data_pegawai)
								// 	mysqli_query($koneksi,"INSERT into data_pegawai values('','$nama','$alamat','$telepon')");
								// 	$berhasil++;
								// }
							}
							$data_insert['table'] = "m_barang_lelang";
							// var_dump($data_insert);die;
							// insert data bulk ke tabel
							$this->model_global->addBulkData($data_insert);
							// hapus kembali file .xls yang di upload tadi
							// unlink($_FILES['filepeExcel']['name']);

							// alihkan halaman ke index.php
							// header("location:index.php?berhasil=$berhasil");
							// redirect('admin/barang');
							if (true) {
								// simpan gambar barang lelang
								$json['status'] = TRUE;
								$json['status_add'] = "berhasil inport";
								echo json_encode($json);
							} else {
								$json['status'] = FALSE;
								echo json_encode($json);
							}
						} else {
							$data['page'] 	= array('p' => 'barang', 'c' => 'import data');
							$data['content'] 	= $this->load->view('admin/barang/form_upload', $data, TRUE);
							echo json_encode($data);
						}
						break;

					default:
						# code...
						break;
				}
				break;

			case 'hapus':
				validate_access($_SESSION['menu']['barang']['delete']);
				$data['id']		= $_POST['id'];
				$data['search']	= "id_barang_lelang";
				$data['table']	= "m_barang_lelang";

				$this->model_global->delById($data);
				echo json_encode(array('url' => base_url('admin/barang')));
				break;

			case 'restore':
				validate_access($_SESSION['menu']['barang']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_barang_lelang";
				$data['table']	= "m_barang_lelang";

				$this->model_global->restorById($data);
				redirect('admin/barang');
				break;

			default:
				// $data['page'] 		= array('p' => 'lot', 'c' => '' );

				$view['content'] = $this->load->view('admin/barang/barang', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function lot($action = null, $id = null)
	{ // Done
		validate_access($_SESSION['menu']['lot']);
		$data = array();
		$data['page'] = "lot";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'lot', 'c' => '' );

		$data['back']	= base_url('admin/lot');
		$data['proses'] = ucwords($action);
		switch ($action) {
			case 'ubah':
				validate_access($_SESSION['menu']['lot']['update']);
				$data['action'] = base_url('admin/lot/ubah');

				$event_detail['select'] = "id_event, nama_event, waktu_event, alamat_event, link_maps, deskripsi_event";
				$event_detail['from'] 	= "t_event";
				$event_detail['where']	= "id_event = '" . $id . "' and status = 1";
				$data['event_detail'] 	= $this->model_global->getData($event_detail);

				$barang_detail['select'] = "b.id_barang_lelang as id, b.nama_barang_lelang, b.brand, b.no_polisi, b.id_kategori, k.id_event, k.harga_awal, k.status_lot, k.status";
				$barang_detail['from'] 	= "m_barang_lelang as b";
				// $barang_detail['where']	= "b.status = 1 and k.id_event = '".$id."'";
				$barang_detail['where']	= "b.status = 1 AND b.id_barang_lelang IS NOT NULL";
				$barang_detail['join'][]	= array("t_lot as k", "k.id_barang_lelang = b.id_barang_lelang AND k.status = 1 AND k.status_lot = 1", "left");
				$barang_detail['join'][]	= array("t_event as e", "e.id_event = k.id_event AND e.id_event = '" . $id . "'", "left");
				$barang_detail['order'][]	= array('b.id_kategori', 'ASC');
				$barang_detail['order'][]	= array('b.nama_barang_lelang', 'ASC');
				$barang_lelang 	= $this->model_global->getData($barang_detail);
				$data['barang_lelang'] = array();
				foreach ($barang_lelang as $key => $val) {
					$data['barang_lelang'][$val->id] = $val;
				}
				$kategori_lelang['select'] = "id_kategori, kategori";
				$kategori_lelang['from'] 	= "m_kategori";
				$kategori_lelang['where']	= "status = 1";
				$kategori_lelang['order'][]	= array('id_kategori', 'ASC');
				$data['kategori_lelang'] 	= $this->model_global->getData($kategori_lelang);

				$lot_detail['select']	= "k.*";
				$lot_detail['table']	= "t_lot as k";
				$lot_detail['join'][]	= array("t_event as e", "e.id_event = k.id_event and e.status = 1");
				// $lot_detail['join'][]		= array("m_barang_lelang as e","e.id_barang_lelang = k.id_barang_lelang");

				if (isset($id)) {
					$lot_detail['where']	= "k.id_event = '" . $id . "' and k.status = 1 and e.status = 1";
				} else {
					$lot_detail['where']	= "k.id_event = '" . $_POST['id_event'] . "' and k.status = 1 and e.status = 1";
				}

				$data['lot_detail'] 	= $this->model_global->getData($lot_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['lot_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// var_dump($_FILES,"<hr>");
					$lot['data']['id_event'] = $_POST['id_event'];
					$lot['data']['tanggal'] = $_POST['tanggal'];
					$lot['table']		= "t_lot";
					try {
						if (!empty($_POST['id_barang_lelang'])) {
							$i = 0;
							foreach ($_POST['id_barang_lelang'] as $key => $val) {
								$i++;
								$lot['data'] = array();
								$lot['where'] = array();
								$lot['data']['id_event'] = $_POST['id_event'];
								$lot['data']['tanggal'] = $_POST['tanggal'];
								$lot['data']['no_lot'] = $i;
								$lot['data']['id_barang_lelang'] = $val;
								$lot['data']['harga_awal'] = (!empty($_POST['harga_id_barang_lelang'][$key])) ? $_POST['harga_id_barang_lelang'][$key] : "0";
								// $lot['data']['harga_awal'] = $_POST['harga_id_barang_lelang'][$key];
								if ($data['barang_lelang'][$val]->status == null) {
									// Insert
									$lot['data']['created_at'] = date('Y-m-d H:i:s');
									$lot['data']['created_by'] = $_SESSION['id_user_admin'];
									$this->model_global->addData($lot);
								} else {
									// Update
									$lot['data']['updated_at'] = date('Y-m-d H:i:s');
									$lot['data']['updated_by'] = $_SESSION['id_user_admin'];
									// die;
									$lot['where'][] 	= array('id_event', $_POST['id_event']);
									$lot['where'][] 	= array('id_barang_lelang', $val);
									// var_dump($key,"<hr>",$lot['data']['id_event'],$val,$lot['data']['harga_awal'],"<hr>");
									$this->model_global->updateData($lot);
								}
							}
						} else {
							$_POST['id_barang_lelang'] = array();
						}
					} catch (\Throwable $th) {
						$json['message'] = $th;
						$json['status'] = FALSE;
						echo json_encode($json);
						die;
					}
					// die;
					// Remove from selected list lot
					foreach ($data['barang_lelang'] as $key => $val2) {
						$lot = array();
						if ($val2->id_event == $_POST['id_event']) {
							if (!in_array($val2->id, $_POST['id_barang_lelang'])) {
								// var_dump($val2,"<hr>");
								$lot['data']['status'] = 0;
								$lot['table']		= "t_lot";
								$lot['where'][] 	= array('id_event', $_POST['id_event']);
								$lot['where'][] 	= array('id_barang_lelang', $val2->id);
								// var_dump($lot,"<hr>");
								// die;
								$this->model_global->updateData($lot);
							}
						}
					}
					if (true) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$data['proses'] = ucwords("list lot lelang");
					$data['page'] 	= array('p' => 'lot', 'c' => 'list lot lelang');
					$data['content'] 	= $this->load->view('admin/lot/form', $data, TRUE);
					echo json_encode($data);
				}
				break;
				/*
			Tidak Terpakai
			case 'hapus':
				validate_access($_SESSION['menu']['lot']['delete']);
				$data['id']		= $_POST['id'];
				$data['search']	= "id_lot";
				$data['table']	= "m_lot";

				$this->model_global->delById($data);
				echo json_encode(array('url' => base_url('admin/lot')));
			break;

			case 'restore':
				validate_access($_SESSION['menu']['lot']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_lot";
				$data['table']	= "m_lot";

				$this->model_global->restorById($data);
				redirect('admin/lot');
			break;
			*/
			default:
				// $data['page'] 		= array('p' => 'lot', 'c' => '' );

				$view['content'] = $this->load->view('admin/lot/lot', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function pemenang($action = null, $id = null)
	{
		validate_access($_SESSION['menu']['pemenang']);
		$data = array();
		$data['page'] = "pemenang";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'kategori', 'c' => '' );

		$data['back']	= base_url('admin/kategori');
		$data['proses'] = ucwords($action);

		switch ($action) {
			case 'verifikasi':
				validate_access($_SESSION['menu']['pemenang']['update']);
				$data['action'] = base_url('admin/pemenang/verifikasi');

				$pemenang_detail['select']	= "pl.*,l.id_lot, b.id_barang_lelang, b.nama_barang_lelang, b.brand, bi.harga_bidding, p.id_peserta";
				$pemenang_detail['table']	= "t_pemenang_lelang as pl";
				$pemenang_detail['join'][]	= array("t_bidding as bi", 'bi.id_bidding = pl.id_bidding AND bi.status = 1');
				$pemenang_detail['join'][]	= array("t_lot as l", 'l.id_lot = bi.id_lot AND l.status = 0 AND l.status_lot = 2');
				$pemenang_detail['join'][]	= array("m_barang_lelang as b", "b.id_barang_lelang = l.id_barang_lelang AND b.status = 1");
				$pemenang_detail['join'][]	= array("m_peserta as p", "p.id_peserta = bi.id_peserta AND p.status = 1");

				if (isset($id)) {
					$pemenang_detail['where']	= "pl.id_pemenang_lelang = '" . $id . "' and bi.status = 1 AND pl.status_pembayaran = 0";
				} else {
					$pemenang_detail['where']	= "pl.id_pemenang_lelang = '" . $_POST['id_pemenang_lelang'] . "' and bi.status = 1 AND pl.status_pembayaran = 0";
				}

				$data['pemenang_detail'] 	= $this->model_global->getData($pemenang_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['pemenang_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// die();						
					if (!empty($_POST['status_pembayaran'])) {
						$pelunasan['data']['status_pembayaran'] = $_POST['status_pembayaran'];
					} else {
						$pelunasan['data']['no_rek'] = NULL;
						$pelunasan['data']['nama_pemilik'] = NULL;
						$pelunasan['data']['nominal'] = NULL;
						$pelunasan['data']['tipe_pelunasan'] = NULL;
						$pelunasan['data']['tanggal_transfer'] = NULL;
						$pelunasan['data']['bukti'] = NULL;
						$pelunasan['data']['status_pembayaran'] = 0;
					}

					$pelunasan['data']['updated_at'] = date('Y-m-d H:i:s');
					$pelunasan['data']['updated_by'] = $_SESSION['id_user_admin'];
					$pelunasan['table']		= "t_pemenang_lelang";
					$pelunasan['where'][0] 	= array('id_pemenang_lelang', $_POST['id_pemenang_lelang']);

					if ($this->model_global->updateData($pelunasan)) {
						if (!empty($_POST['status_pembayaran'])) {
							// Ubah status_lot di t_lot jadi 3 (sold)
							$lot['data']['status_lot'] = 3;
							$lot['data']['updated_at'] = date('Y-m-d H:i:s');
							$lot['data']['updated_by'] = $_SESSION['id_user_admin'];
							$lot['table']		= "t_lot";
							$lot['where'][0] 	= array('id_lot', $data['pemenang_detail'][0]->id_lot);
							$this->model_global->updateData($lot);

							// Ubah status di m_barang_lelang jadi 0 (tidak aktif)
							$barang_lelang = array();
							$barang_lelang['data']['status'] = 0;
							$barang_lelang['data']['updated_at'] = date('Y-m-d H:i:s');
							$barang_lelang['data']['updated_by'] = $_SESSION['id_user_admin'];
							$barang_lelang['table']		= "m_barang_lelang";
							$barang_lelang['where'][0] 	= array('id_barang_lelang', $data['pemenang_detail'][0]->id_barang_lelang);
							$this->model_global->updateData($barang_lelang);

							$notifikasi['data']['pengirim'] = 0;
							$notifikasi['data']['penerima'] = $data['pemenang_detail'][0]->id_peserta;
							$notifikasi['data']['type_user'] = "peserta";
							$notifikasi['data']['type'] = "pelunasan";
							$notifikasi['data']['judul'] = "Pelunasan Barang Lelang";
							$notifikasi['data']['pesan'] = "Pembayaran sudah terverifikasi, silahkan datang ke kantor untuk mengambil barang lelang.";
							$notifikasi['data']['is_read'] = 0;
							$notifikasi['data']['deskripsi'] = json_encode($data['pemenang_detail']);
							$notifikasi['data']['created_at'] = date('Y-m-d H:i:s');
							set_notifikasi($notifikasi['data']);
						}
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$data['page'] 	= array('p' => 'pemenang', 'c' => 'Verifikasi Pembayaran Online');
					$data['content'] 	= $this->load->view('admin/pemenang/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'verifikasi_offline':
				validate_access($_SESSION['menu']['pemenang']['update']);
				$data['action'] = base_url('admin/pemenang/verifikasi');

				$pemenang_detail['select']	= "pl.*,l.id_lot, b.id_barang_lelang, b.nama_barang_lelang, b.brand, bi.harga_bidding";
				$pemenang_detail['table']	= "t_pemenang_lelang as pl";
				$pemenang_detail['join'][]	= array("t_bidding as bi", 'bi.id_bidding = pl.id_bidding AND bi.status = 1');
				$pemenang_detail['join'][]	= array("t_lot as l", 'l.id_lot = bi.id_lot AND l.status = 0 AND l.status_lot = 2');
				$pemenang_detail['join'][]	= array("m_barang_lelang as b", "b.id_barang_lelang = l.id_barang_lelang AND b.status = 1");


				if (isset($id)) {
					$pemenang_detail['where']	= "pl.id_pemenang_lelang = '" . $id . "' and bi.status = 1 AND pl.status_pembayaran = 0";
				} else {
					$pemenang_detail['where']	= "pl.id_pemenang_lelang = '" . $_POST['id_pemenang_lelang'] . "' and bi.status = 1 AND pl.status_pembayaran = 0";
				}

				$data['pemenang_detail'] 	= $this->model_global->getData($pemenang_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['pemenang_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// die();						
					if (!empty($_POST['status_pembayaran'])) {
						// Accept Pembayaran
						$pelunasan['data']['status_pembayaran'] = $_POST['status_pembayaran'];
						$pelunasan['data']['no_rek'] = $_POST['no_rek'];
						$pelunasan['data']['nama_pemilik'] = $_POST['nama_pemilik'];
						// $pelunasan['data']['tipe_pelunasan'] = $_POST['tipe_pelunasan'];
						$pelunasan['data']['tipe_pelunasan'] = 1;
						$pelunasan['data']['nominal'] = $_POST['nominal'];
						$pelunasan['data']['tanggal_transfer'] = date_format(date_create(@$_POST['tanggal_transfer']), 'Y-m-d H:i:s');

						if (!empty($_FILES['bukti']['name'])) {
							$pelunasan['data']['bukti']	= unggah_gambar('./assets/uploads/pelunasan_lelang', 'bukti', 'png|jpg|jpeg');
						};
					} else {
						// Reject Pembayaran
						$pelunasan['data']['no_rek'] = NULL;
						$pelunasan['data']['nama_pemilik'] = NULL;
						$pelunasan['data']['nominal'] = NULL;
						$pelunasan['data']['tanggal_transfer'] = NULL;
						$pelunasan['data']['bukti'] = NULL;
						$pelunasan['data']['status_pembayaran'] = 0;
					}

					$pelunasan['data']['updated_at'] = date('Y-m-d H:i:s');
					$pelunasan['data']['updated_by'] = $_SESSION['id_user_admin'];
					$pelunasan['table']		= "t_pemenang_lelang";
					$pelunasan['where'][0] 	= array('id_pemenang_lelang', $_POST['id_pemenang_lelang']);

					// Get ID NPL berdasarkan No NPL terakhir
					$get_npl['select'] = "id_npl";
					$get_npl['from'] = "m_npl";
					$get_npl['limit'] = 1;
					$get_npl['where'] = "no_npl = " . $_POST['no_npl'];
					$get_npl['order'][]	= array('id_npl', 'DESC');
					$detail_npl = $this->model_global->getData($get_npl);

					if (!empty($detail_npl)) {
						// update status = 0 m_npl berdasarkan no_npl
						$update_npl['data']['status_npl'] = 0;
						$update_npl['data']['updated_at'] = date('Y-m-d H:i:s');
						$update_npl['data']['updated_by'] = $_SESSION['id_user_admin'];
						$update_npl['table']		= "m_npl";
						$update_npl['where'][0] 	= array('id_npl', $detail_npl[0]->id_npl);
						$this->model_global->updateData($update_npl);
					}

					if ($this->model_global->updateData($pelunasan)) {
						if (!empty($_POST['status_pembayaran'])) {
							// Ubah status_lot di t_lot jadi 3 (sold)
							$lot['data']['status_lot'] = 3;
							$lot['data']['updated_at'] = date('Y-m-d H:i:s');
							$lot['data']['updated_by'] = $_SESSION['id_user_admin'];
							$lot['table']		= "t_lot";
							$lot['where'][0] 	= array('id_lot', $data['pemenang_detail'][0]->id_lot);
							$this->model_global->updateData($lot);

							// Ubah status di m_barang_lelang jadi 0 (tidak aktif)
							$barang_lelang = array();
							$barang_lelang['data']['status'] = 0;
							$barang_lelang['data']['updated_at'] = date('Y-m-d H:i:s');
							$barang_lelang['data']['updated_by'] = $_SESSION['id_user_admin'];
							$barang_lelang['table']		= "m_barang_lelang";
							$barang_lelang['where'][0] 	= array('id_barang_lelang', $data['pemenang_detail'][0]->id_barang_lelang);
							$this->model_global->updateData($barang_lelang);
						}
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$data['page'] 	= array('p' => 'pemenang', 'c' => 'Verifikasi Pembayaran Offline');
					$data['content'] 	= $this->load->view('admin/pemenang/form_offline', $data, TRUE);
					echo json_encode($data);
				}
				break;

			default:
				// $data['page'] 		= array('p' => 'pemenang', 'c' => '' );

				$view['content'] = $this->load->view('admin/pemenang/pemenang', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function kategori($action = null, $id = null)
	{ // Done
		validate_access($_SESSION['menu']['kategori']);
		$data = array();
		$data['page'] = "kategori";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'kategori', 'c' => '' );

		$data['back']	= base_url('admin/kategori');
		$data['proses'] = ucwords($action);

		switch ($action) {
			case 'tambah':
				validate_access($_SESSION['menu']['kategori']['create']);
				$data['action'] = base_url('admin/kategori/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					// die();
					$kategori['data']['kategori'] = $_POST['kategori'];
					// if (!empty($_FILES['gambar']['name'])) {
					// 	$kategori['data']['gambar']	= unggah_gambar('./assets/uploads/kategori_lot', 'gambar', 'png|jpg|jpeg');
					// };						
					// $kategori['data']['deskripsi'] = $_POST['deskripsi'];
					$kategori['data']['created_at'] = date('Y-m-d H:i:s');
					$kategori['data']['created_by'] = $_SESSION['id_user_admin'];
					$kategori['data']['status'] = 1;
					// var_dump($kategori);
					// die();
					$kategori['table']		= "m_kategori";
					// $json['id_kategori'] 	= $this->model_global->addData($data);
					// redirect('admin/kategori');
					if ($this->model_global->addData($kategori)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil tambah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					// var_dump($_SESSION);
					$data['page'] 	= array('p' => 'kategori', 'c' => 'tambah kategori lot');
					$data['content'] 	= $this->load->view('admin/kategori/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'ubah':
				validate_access($_SESSION['menu']['kategori']['update']);
				$data['action'] = base_url('admin/kategori/ubah');
				$kategori_detail['select']	= "k.*";
				$kategori_detail['table']		= "m_kategori as k";

				if (isset($id)) {
					$kategori_detail['where']	= "k.id_kategori = '" . $id . "' and k.status = 1";
				} else {
					$kategori_detail['where']	= "k.id_kategori = '" . $_POST['id_kategori'] . "' and k.status = 1";
				}

				$data['kategori_detail'] 	= $this->model_global->getData($kategori_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['kategori_detail'],"<hr>");
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					// die();						

					$kategori['data']['kategori'] = $_POST['kategori'];
					// if (!empty($_FILES['gambar']['name'])) {
					// 	$kategori['data']['gambar']	= unggah_gambar('./assets/uploads/kategori_lot', 'gambar', 'png|jpg|jpeg');
					// };
					// $kategori['data']['deskripsi'] = $_POST['deskripsi'];
					$kategori['data']['updated_at'] = date('Y-m-d H:i:s');
					$kategori['data']['updated_by'] = $_SESSION['id_user_admin'];
					$kategori['table']		= "m_kategori";
					$kategori['where'][0] 	= array('id_kategori', $_POST['id_kategori']);
					// $this->model_global->updateData($data);
					// redirect('admin/kategori');
					if ($this->model_global->updateData($kategori)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$data['page'] 	= array('p' => 'kategori', 'c' => 'ubah kata kunci');
					$data['content'] 	= $this->load->view('admin/kategori/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'hapus':
				validate_access($_SESSION['menu']['kategori']['delete']);
				$data['id']		= $_POST['id'];
				$data['search']	= "id_kategori";
				$data['table']	= "m_kategori";

				$this->model_global->delById($data);
				echo json_encode(array('url' => base_url('admin/kategori')));
				break;

			case 'restore':
				validate_access($_SESSION['menu']['kategori']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_kategori";
				$data['table']	= "m_kategori";

				$this->model_global->restorById($data);
				redirect('admin/kategori');
				break;

			default:
				// $data['page'] 		= array('p' => 'kategori', 'c' => '' );

				$view['content'] = $this->load->view('admin/kategori/kategori', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function user($action = null, $id = null)
	{ // Done
		validate_access($_SESSION['menu']['user']);
		$data = array();
		$data['page'] = "user";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'user', 'c' => '' );

		$data['back']	= base_url('admin/user');
		$data['proses'] = ucwords($action);

		$role_detail['select'] = "*";
		$role_detail['from'] = "m_role";
		$role_detail['where'] = "status = 1";
		$data['role'] = $this->model_global->getData($role_detail);

		switch ($action) {
			case 'tambah':
				validate_access($_SESSION['menu']['user']['create']);
				$data['action'] = base_url('admin/user/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>");
					// die();
					// qecLAJgqcT
					$user['data']['role'] = (!empty($_POST['role'])) ? $_POST['role'] : 2;
					$user['data']['nama_user'] = $_POST['nama_user'];
					$user['data']['email'] = $_POST['email'];
					$user['data']['password'] = sha1($_POST['password']);
					$user['data']['created_at'] = date('Y-m-d H:i:s');
					$user['data']['status'] = 1;
					// var_dump($user);
					// die();
					$user['table']		= "m_user";
					// $json['id_user'] 	= $this->model_global->addData($data);
					// redirect('admin/user');
					if ($this->model_global->addData($user)) {
						// $email_setting = (object) array();
						// $email_setting->email_from = $this->config->item('smtp_user');
						// $email_setting->email_to = $_POST['email'];
						// $email_setting->subject = "Access to news aggregator JN1";
						// $email_setting->message = "Berikut adalah password untuk login ke situs ".base_url()."<hr>";
						// $email_setting->message .= "Password : <b>".$_POST['password']."</b><hr>";
						// $email_setting->message .= "<b>*NB</b> : Diharap untuk menjaga kerahasiaan akses ini.";

						// send_email($email_setting);
						// $json['status'] = send_email($email_setting);
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil tambah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					//var_dump($_SESSION);
					$data['page'] 	= array('p' => 'user', 'c' => 'tambah user');
					$data['content'] 	= $this->load->view('admin/user/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'ubah':
				validate_access($_SESSION['menu']['user']['update']);
				$data['action'] = base_url('admin/user/ubah');
				$user_detail['select']	= "k.*";
				$user_detail['table']		= "m_user as k";

				if (isset($id)) {
					$user_detail['where']	= "k.id_user = '" . $id . "' and k.status = 1";
				} else {
					$user_detail['where']	= "k.id_user = '" . $_POST['id_user'] . "' and k.status = 1";
				}

				$data['user_detail'] 	= $this->model_global->getData($user_detail);

				if (isset($_POST['submit'])) {
					// var_dump($data['user_detail'],"<hr>");
					// var_dump($_POST,"<hr>");
					// die();
					$user['data']['role'] = (!empty($_POST['role'])) ? $_POST['role'] : 2;
					$user['data']['nama_user'] 	= $_POST['nama_user'];
					$user['data']['email'] 		= $_POST['email'];
					if (!empty($_POST['password'])) {
						$user['data']['password'] 	= sha1($_POST['password']);
					}
					$user['data']['updated_at'] = date('Y-m-d H:i:s');
					$user['data']['updated_by'] = $_SESSION['id_user_admin'];
					$user['table']		= "m_user";
					$user['where'][0] 	= array('id_user', $_POST['id_user']);
					// $this->model_global->updateData($data);
					// redirect('admin/user');
					if ($this->model_global->updateData($user)) {
						// $email_setting = (object) array();
						// $email_setting->email_from = $this->config->item('smtp_user');
						// $email_setting->email_to = $_POST['email'];
						// $email_setting->subject = "Update access to news aggregator JN1";
						// $email_setting->message = "Berikut adalah password untuk login ke situs ".base_url()."<hr>";
						// $email_setting->message .= "Password : <b>".$_POST['password']."</b><hr>";
						// $email_setting->message .= "<b>*NB</b> : Diharap untuk menjaga kerahasiaan akses ini.";
						// send_email($email_setting);
						// $json['status'] = send_email($email_setting);
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					//var_dump($data['berita_detail']);
					$data['page'] 	= array('p' => 'user', 'c' => 'ubah kata kunci');
					$data['content'] 	= $this->load->view('admin/user/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'hapus':
				validate_access($_SESSION['menu']['user']['delete']);
				$data['id']		= $_POST['id'];
				$data['search']	= "id_user";
				$data['table']	= "m_user";

				$this->model_global->delById($data);
				echo json_encode(array('url' => base_url('admin/user')));
				break;

			case 'restore':
				validate_access($_SESSION['menu']['user']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_user";
				$data['table']	= "m_user";

				$this->model_global->restorById($data);
				redirect('admin/user');
				break;

			default:
				// $data['page'] 		= array('p' => 'user', 'c' => '' );

				$view['content'] = $this->load->view('admin/user/user', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function role($action = null, $id = null)
	{ // Done
		validate_access($_SESSION['menu']['role']);
		$data = array();
		$data['page'] = "role";
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		// $data['page'] 	= array('p' => 'role', 'c' => '' );

		$data['back']	= base_url('admin/role');
		$data['proses'] = ucwords($action);

		switch ($action) {
			case 'tambah':
				validate_access($_SESSION['menu']['role']['create']);
				$data['action'] = base_url('admin/role/tambah');
				if (isset($_POST['submit'])) {
					// var_dump($_POST,"<hr>",$_FILES,"<hr>");
					$role['data']['nama_role'] = $_POST['nama_role'];
					$role['data']['created_at'] = date('Y-m-d H:i:s');
					$role['data']['status'] = 1;
					// var_dump($role);
					// die();
					$role['table']		= "m_role";

					// redirect('admin/role');
					if ($this->model_global->addData($role)) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil tambah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					// var_dump($_SESSION);
					$data['page'] 	= array('p' => 'role', 'c' => 'tambah role user');
					$data['content'] 	= $this->load->view('admin/role/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'ubah':
				validate_access($_SESSION['menu']['role']['update']);
				$data['action'] = base_url('admin/role/ubah');
				$role_detail['select']	= "r.*";
				$role_detail['table']		= "m_role as r";
				if (isset($id)) {
					$role_detail['where']	= "r.id_role = '" . $id . "' and r.status = 1";
				} else {
					$role_detail['where']	= "r.id_role = '" . $_POST['id_role'] . "' and r.status = 1";
				}
				$data['role_detail'] 	= $this->model_global->getData($role_detail);

				$role_menu['select']		= "m.*, rm.id_role_menu, rm.id_role, rm.create, rm.update, rm.delete, rm.restore, rm.status";
				$role_menu['from']		= "m_menu as m";
				$role_menu['join'][0]	=  array('m_role_menu as rm', 'm.id_menu = rm.id_menu');
				if (isset($id)) {
					$role_menu['where']	= "rm.id_role = '" . $id . "'";
				} else {
					$role_menu['where']	= "rm.id_role = '" . $_POST['id_role'] . "'";
				}
				$data['role_menu'] 	= $this->model_global->getData($role_menu);
				foreach ($data['role_menu'] as $val) {
					$data['role_menu_detail'][$val->id_menu] = $val;
				}

				if (isset($_POST['submit'])) {
					// var_dump($data['role_detail'],"<hr>");
					// var_dump(json_encode($data['role_menu_detail']),"<hr>");
					// var_dump(json_encode($_POST),"<hr>");
					$tmpIdMenu = array();
					foreach ($_POST['menu'] as $key => $val) {
						// array_push($tmpIdMenu,$key);
						// var_dump($key,"<hr>",$val,"<hr>");
						// die;						
						$role_menu_data = array();
						$role_menu_data['data']['id_role'] = $_POST['id_role'];
						$role_menu_data['data']['id_menu'] = $key;
						$role_menu_data['data']['status'] = (!empty($val['status'])) ? "1" : "0";
						$role_menu_data['data']['create'] = (!empty($val['create'])) ? "1" : "0";
						$role_menu_data['data']['update'] = (!empty($val['update'])) ? "1" : "0";
						$role_menu_data['data']['delete'] = (!empty($val['delete'])) ? "1" : "0";
						$role_menu_data['data']['restore'] = (!empty($val['restore'])) ? "1" : "0";
						$role_menu_data['table']		= "m_role_menu";
						if (!empty($data['role_menu_detail'][$key])) {
							// var_dump("update<hr>");
							$role_menu_data['where'][0] 	= array('id_role_menu', $val['id_role_menu']);
							// $role_menu_data['where'][1] 	= array('id_menu', $key);
							// var_dump($role_menu_data,"<hr>");
							$this->model_global->updateData($role_menu_data);
							continue;
						}
						// if(empty($val['id_menu'])) {
						// 	// var_dump("update<hr>");
						// 	$role_menu_data['data']['status'] = 0;
						// 	$role_menu_data['where'][0] 	= array('id_role', $_POST['id_role']);
						// 	$role_menu_data['where'][1] 	= array('id_menu', $key);
						// 	// var_dump($role_menu_data,"<hr>");
						// 	$this->model_global->updateData($role_menu_data);
						// 	continue;
						// }
						if (empty($data['role_menu_detail'][$key])) {
							// var_dump("create<hr>");
							$this->model_global->addData($role_menu_data);
							continue;
						}
					}
					// die();
					// $this->model_global->updateData($role);

					// redirect('admin/role');
					if (true) {
						$json['status'] = TRUE;
						$json['status_add'] = "berhasil ubah";
						echo json_encode($json);
					} else {
						$json['status'] = FALSE;
						echo json_encode($json);
					}
				} else {
					$menu_detail['select'] = "*";
					$menu_detail['from'] = "m_menu";
					$menu_detail['where'] = "status = 1";
					$menu_detail['order'][0] = array('order', 'asc');
					$menu_detail['order'][1] = array('id_menu', 'asc');
					$data['menu_detail'] 	= $this->model_global->getData($menu_detail);

					// var_dump($data);
					$data['page'] 	= array('p' => 'role', 'c' => 'ubah kata kunci');
					$data['content'] 	= $this->load->view('admin/role/form', $data, TRUE);
					echo json_encode($data);
				}
				break;

			case 'hapus':
				validate_access($_SESSION['menu']['role']['delete']);
				$data['id']		= $_POST['id'];
				$data['search']	= "id_role";
				$data['table']	= "m_role";

				$this->model_global->delById($data);
				echo json_encode(array('url' => base_url('admin/role')));
				break;

			case 'restore':
				validate_access($_SESSION['menu']['role']['restore']);
				$data['id']		= $id;
				$data['search']	= "id_role";
				$data['table']	= "m_role";

				$this->model_global->restorById($data);
				redirect('admin/role');
				break;

			default:
				// $data['page'] 		= array('p' => 'role', 'c' => '' );

				$view['content'] = $this->load->view('admin/role/role', $data, true);
				$this->load->view('admin/layout', $view, false);
				break;
		}
	}

	public function setting()
	{ // Done
		validate_access($_SESSION['menu']['setting']);
		$data = array();
		$data['page'] = 'setting';
		if (empty($_SESSION['menu'][$data['page']])) {
			redirect('admin');
		}
		$setting = $this->model_setting->getListSetting();
		$arrSetting = array();
		foreach ($setting as $key => $val) {
			$arrSetting[$val->name_setting] = $val->value;
		}
		$data['setting'] = (object) $arrSetting;
		if (!empty($_POST['submit'])) {
			$value = array();
			// var_dump($_POST,"<hr>");
			// var_dump(array_keys($arrSetting),"<hr>");
			// die;
			foreach ($_POST as $key => $val) {
				// var_dump(array_keys($arrSetting),"<hr>",$key,"<hr>",in_array($key, array_keys($arrSetting)));die;
				switch ($key) {
					case 'metadata_title':
						$value['name_setting'] = 'metadata_title';
						$value['value'] = $_POST['metadata_title'];
						break;
					case 'metadata_keyword':
						$value['name_setting'] = 'metadata_keyword';
						$value['value'] = $_POST['metadata_keyword'];
						break;
					case 'metadata_deskripsi':
						$value['name_setting'] = 'metadata_deskripsi';
						$value['value'] = $_POST['metadata_deskripsi'];
						break;
					case 'no_telp':
						$value['name_setting'] = 'no_telp';
						$value['value'] = $_POST['no_telp'];
						break;
					case 'no_wa':
						$value['name_setting'] = 'no_wa';
						$value['value'] = $_POST['no_wa'];
						break;
					case 'bid_time':
						$value['name_setting'] = 'bid_time';
						$value['value'] = $_POST['bid_time'];
						break;
					case 'email':
						$value['name_setting'] = 'email';
						$value['value'] = $_POST['email'];
						break;
					case 'alamat':
						$value['name_setting'] = 'alamat';
						$value['value'] = $_POST['alamat'];
						break;
					case 'ig':
						$value['name_setting'] = 'ig';
						$value['value'] = $_POST['ig'];
						break;
					case 'fb':
						$value['name_setting'] = 'fb';
						$value['value'] = $_POST['fb'];
						break;
					case 'tw':
						$value['name_setting'] = 'tw';
						$value['value'] = $_POST['tw'];
						break;
					case 'yt':
						$value['name_setting'] = 'yt';
						$value['value'] = $_POST['yt'];
						break;

					default:
						break;
				}
				// var_dump($value);
				// die;	
				if ($key !== 'submit') {
					if (in_array($key, array_keys($arrSetting))) {
						var_dump("masuk update");
						$this->model_setting->update('m_setting', $value, array('name_setting', $key));
					} else {
						var_dump("masuk save");
						$this->model_setting->save($value);
					}
				}
			}

			$this->session->set_flashdata('msg', 'Data Setting Berhasil Tersimpan.');
			redirect('admin/setting');
		} else {
			// var_dump(json_encode(array('fileIMG' => '', 'bulk' => '1',)));
			$data['page'] = "setting";
			// var_dump($data['setting']);die;
			$view['content'] = $this->load->view('admin/setting', $data, true);
			$this->load->view('admin/layout', $view, false);
		}
	}

	public function rest_api($type)
	{
		switch ($type) {
			case 'dataNewUser':
				$value = $this->db->query('SELECT COUNT(id_user) as result FROM m_user WHERE DATE(created_at) = CURRENT_DATE() AND status = 1')->row();
				echo json_encode($value->result);
				break;

			case 'getProp':
				$prop['select']	= "*";
				$prop['table']	= "m_provinsi";
				$prop['order']	= array('NAMA_PROP', 'ASC');

				$value 	= $this->model_global->getData($prop);
				foreach ($value as $val) {
					$response[] = array(
						"id" => $val->NO_PROP,
						"text" => $val->NAMA_PROP
					);
				}

				return ($response);
				// exit();
				break;

			case 'getKab':
				$prop['select']	= "*";
				$prop['table']	= "m_kabupaten_kota";
				$prop['where']	= "NO_PROP = '" . $_POST['kode_prop'] . "'";
				$prop['order']	= array('NAMA_KAB', 'ASC');

				$value 	= $this->model_global->getData($prop);
				foreach ($value as $val) {
					$response[] = array(
						"id" => $val->NO_KAB,
						"text" => $val->NAMA_KAB
					);
				}

				echo json_encode($response);
				// exit();
				break;

			case 'dtUser': // Done
				// var_dump($_POST);die;
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_user'];
					$row[] = $l['email'];
					$row[] = $l['nama_role'];
					$btnUpdate = $btnDelete = $btnRestore = '';
					if ($_SESSION['menu']['user']['update']) {
						$btnUpdate .= '<button class="btn btn-warning" type="button" title="Ubah Data User" onclick="edit(' . $l['id_user'] . ');"><i class="fas fa-pen"></i></button>';
					}
					if ($_SESSION['menu']['user']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`user`,' . $l['id_user'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data User"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['user']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/user/restore') . '/' . $l['id_user'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data User"><i class="fas fa-recycle"></i></button></a>';
					}

					if ($_GET['type'] == 'user') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtRole': // Done
				// var_dump($_POST);die;
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_role'];

					$btnUpdate = $btnDelete = $btnRestore = '';
					if ($_SESSION['menu']['role']['update']) {
						$btnUpdate .= '<button class="btn btn-warning" type="button" title="Ubah Data Role" onclick="edit(' . $l['id_role'] . ');"><i class="fas fa-pen"></i></button>';
					}
					if ($_SESSION['menu']['role']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`role`,' . $l['id_role'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Role"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['role']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/role/restore') . '/' . $l['id_role'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Role"><i class="fas fa-recycle"></i></button></a>';
					}

					if ($_GET['type'] == 'role') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;
			case 'dtBanner': // Done
				// var_dump($_POST);die;
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['judul'];
					$row[] = '<div class="w-100"><img class="img-thumbnail" src="' . base_url('assets/uploads/banner/origin/' . @$l['gambar']) . '" title="' . $l['judul'] . '" alt="' . $l['judul'] . '" srcset="" style="max-width: 15vw;"></div>';

					$btnUpdate = $btnDelete = $btnRestore = '';
					if ($_SESSION['menu']['banner']['update']) {
						$btnUpdate .= '<button class="btn btn-warning" type="button" title="Ubah Data Role" onclick="edit(' . $l['id_banner'] . ');"><i class="fas fa-pen"></i></button>';
					}
					if ($_SESSION['menu']['banner']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`banner`,' . $l['id_banner'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Role"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['banner']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/banner/restore') . '/' . $l['id_banner'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Role"><i class="fas fa-recycle"></i></button></a>';
					}

					if ($_GET['type'] == 'banner') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtEvent': // Done
				// var_dump($_POST);die;
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_event'];
					$row[] = $l['waktu_event'];
					$row[] = '<a href="' . $l['link_maps'] . '" target="_blank" alt="lokasi alamat event">' . $l['alamat_event'] . '</a>';
					$btnUpdate = $btnDelete = $btnRestore = '';
					if ($_SESSION['menu']['event']['update']) {
						// $btnUpdate .= ' <button class="btn btn-primary" type="button" title="List Lot Lelang" onclick="detail_lot('.$l['id_event'].');"><i class="fas fa-list"></i> '.$l['jumlah_lot'].'</button>';							
						$btnUpdate .= ' <button class="btn btn-warning" type="button" title="Ubah Data Event" onclick="edit(' . $l['id_event'] . ');"><i class="fas fa-pen"></i></button>';
						if (date_format(date_create($l['waktu_event']), 'Y-m-d') == date('Y-m-d')) {
							$btnUpdate .= ' <a href="' . base_url('admin/event/bidsys') . '/' . $l['id_event'] . '?kode_event=' . $l['kode_event'] . '" target="_blank"><button class="btn btn-success" type="button" title="sistem lelang"><i class="fas fa-hand-paper"></i></button></a>';
						}
					}
					if ($_SESSION['menu']['event']['delete']) {
						$btnDelete .= '<a href="' . base_url('admin/event/hapus') . '/' . $l['id_event'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Berita Lot"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['event']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/event/restore') . '/' . $l['id_event'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Event"><i class="fas fa-recycle"></i></button></a>';
					}

					if ($_GET['type'] == 'event') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtKategori': // Done
				// var_dump($_POST);die;
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['kategori'];
					$btnUpdate = $btnDelete = $btnRestore = '';
					if ($_SESSION['menu']['kategori']['update']) {
						$btnUpdate .= '<button class="btn btn-warning" type="button" title="Ubah Data Kategori Lot" onclick="edit(' . $l['id_kategori'] . ');"><i class="fas fa-pen"></i></button>';
					}
					if ($_SESSION['menu']['kategori']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`kategori`,' . $l['id_kategori'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Kategori Lot"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['kategori']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/kategori/restore') . '/' . $l['id_kategori'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Kategori Lot"><i class="fas fa-recycle"></i></button></a>';
					}

					if ($_GET['type'] == 'kategori') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtBarang': // Done
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_barang_lelang'];
					$row[] = $l['kategori'];
					$row[] = '<div class="w-100"><img class="img-thumbnail" src="' . base_url('assets/uploads/gambar_barang_lelang/origin/' . @$l['gambar']) . '" title="' . $l['nama_barang_lelang'] . '" alt="' . $l['nama_barang_lelang'] . '" srcset="" style="max-width: 15vw;"></div>';
					switch (strtolower($l['kategori'])) {
						case 'mobil':
						case 'motor':
							$foto_stnk = (($l['stnk'] == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>';
							$foto_bpkb = (($l['bpkb'] == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>';
							$foto_faktur = (($l['faktur'] == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>';
							$foto_ktp_pemilik = (($l['ktp_pemilik'] == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>';
							$foto_kwitansi = (($l['kwitansi'] == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>';
							$sph = (($l['sph'] == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>';
							$kir = (($l['kir'] == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>';
							$row[] = "<ol>
							<li>STNK : " . $foto_stnk . "</li>
							<li>BPKB : " . $foto_bpkb . "</li>
							<li>FAKTUR : " . $foto_faktur . "</li>
							<li>KTP PEMILIK : " . $foto_ktp_pemilik . "</li>
							<li>KWITANSI : " . $foto_kwitansi . "</li>
							<li>SPH : " . $sph . "</li>
							<li>KIR : " . $kir . "</li>
							</ol>";
							break;

						default:
							$row[] = "";
							break;
					}
					$btnUpdate = $btnDelete = $btnRestore = '';
					if ($_SESSION['menu']['barang']['update']) {
						$btnUpdate .= '<button class="btn btn-warning" type="button" title="Ubah Data Peserta" onclick="edit(' . $l['id_barang_lelang'] . ');"><i class="fas fa-pen"></i></button>';
					}
					if ($_SESSION['menu']['barang']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`barang`,' . $l['id_barang_lelang'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Peserta"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['barang']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/barang/restore') . '/' . $l['id_barang_lelang'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Peserta"><i class="fas fa-recycle"></i></button></a>';
					}

					if ($_GET['type'] == 'barang') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}
					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtLot':
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_event'];
					$row[] = $l['waktu_event'];
					$row[] = '<a href="' . $l['link_maps'] . '" target="_blank" alt="lokasi alamat event">' . $l['alamat_event'] . '</a>';
					$row[] = $l['jumlah_lot'];
					$btnUpdate = $btnDelete = $btnRestore = '';
					if ($_SESSION['menu']['event']['update']) {
						$btnUpdate .= '<button class="btn btn-primary" type="button" title="List Lot Lelang" onclick="edit(' . $l['id_event'] . ');"><i class="fas fa-list"></i> ' . $l['jumlah_lot'] . '</button>';
						// $btnUpdate .= ' <a href="'.base_url('admin/lot/listlot').'/'.$l['id_event'].'" target="_blank"><button class="btn btn-primary" type="button" title="List Lot Lelang"><i class="fas fa-list"></i></button></a>';
					}
					if ($_SESSION['menu']['event']['delete']) {
						// $btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`event`,'.$l['id_event'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Berita Lot"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['event']['restore']) {
						// $btnRestore .= '<a href="'.base_url('admin/event/restore').'/'.$l['id_event'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Event"><i class="fas fa-recycle"></i></button></a>';
					}

					if ($_GET['type'] == 'lot') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}
					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtPeserta': // Done
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$btnUpdate = $btnDelete = $btnRestore = $btnListNPL = '';
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama'];
					$row[] = '<a target="_blank" href="mailto:' . $l['email'] . '">' . $l['email'] . '</a>';
					$row[] = '<a target="_blank" href="tel:' . $l['no_hp'] . '">' . $l['no_hp'] . '</a>';
					$row[] = $l['alamat'];

					if ($_SESSION['menu']['peserta']['update']) {
						$btnUpdate .= '<button class="btn btn-warning" type="button" title="Detail Data Peserta" onclick="edit(' . $l['id_peserta'] . ');"><i class="fas fa-eye"></i></button>';
						$btnListNPL .= '<a href="' . base_url('admin/npl_peserta?id_peserta=') . $l['id_peserta'] . '" ><button class="btn btn-success" type="button" title="List Data NPL Peserta"><i class="fas fa-list"> ' . $l['jumlah_npl'] . '</i></button></a>';
					}
					if ($_SESSION['menu']['peserta']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`peserta`,' . $l['id_peserta'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Peserta"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['peserta']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/peserta/restore') . '/' . $l['id_peserta'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Peserta"><i class="fas fa-recycle"></i></button></a>';
					}
					$row[] = $btnListNPL;

					// $verifikasi = (is_null($l['id_barang']))?'<a href="'.base_url('admin/peserta/active').'/'.$l['id_peserta'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Data Peserta Ini???`)" ><button class="btn btn-success" type="button" title="Verifikasi Data Calon Peserta"><i class="fas fa-check"></i></button></a>':'';


					if ($_GET['type'] == 'peserta') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtPemenang': // Done
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$btnUpdate = $btnDelete = $btnRestore = $btnListNPL = '';
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = (!empty($l['nama'])) ? $l['nama'] : "ADMIN";
					$row[] = (!empty($l['email'])) ? '<a target="_blank" href="mailto:' . $l['email'] . '">' . $l['email'] . '</a>' : '-';
					$row[] = $l['nama_barang_lelang'].'- <b>[lot '.$l['no_lot'].']</b>';
					$row[] = $l['nama_event'] . ' (' . $l['waktu_event'] . ')';
					$row[] = $l['harga_bidding'];
					$row[] = (!empty($l['npl'])) ? "<span class='btn btn-success'>Online</span>" : "<span class='btn btn-secondary'>Offline</span>";

					if ($_SESSION['menu']['pemenang']['update']) {
						if (empty($l['npl'])) {
							// Pelunasan Offline
							$btnUpdate .= '<button class="btn btn-primary" type="button" title="Verifikasi Pelunasan Pembayaran" onclick="edit2(' . $l['id_pemenang_lelang'] . ');"><i class="fas fa-check"></i></button>';
						} else {
							// Pelunasan Online
							if ((!empty($l['no_rek'])) && (!empty($l['nominal'])) && (!empty($l['bukti']))) {
								$btnUpdate .= '<button class="btn btn-primary" type="button" title="Verifikasi Pelunasan Pembayaran" onclick="edit(' . $l['id_pemenang_lelang'] . ');"><i class="fas fa-check"></i></button>';
							}
						}
						// $btnListNPL .= '<a href="'.base_url('admin/npl_peserta?id_peserta=').$l['id_peserta'].'" ><button class="btn btn-success" type="button" title="List Data NPL Peserta"><i class="fas fa-list"> '.$l['jumlah_npl'].'</i></button></a>';
					}
					// if ($_SESSION['menu']['pemenang']['delete']) {
					// 	$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`peserta`,'.$l['id_peserta'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Peserta"><i class="fas fa-trash"></i></button></a>';
					// }
					// if ($_SESSION['menu']['pemenang']['restore']) {
					// 	$btnRestore .= '<a href="'.base_url('admin/peserta/restore').'/'.$l['id_peserta'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Peserta"><i class="fas fa-recycle"></i></button></a>';
					// }
					// $row[] = $btnListNPL;

					// $verifikasi = (is_null($l['id_barang']))?'<a href="'.base_url('admin/peserta/active').'/'.$l['id_peserta'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Data Peserta Ini???`)" ><button class="btn btn-success" type="button" title="Verifikasi Data Calon Peserta"><i class="fas fa-check"></i></button></a>':'';


					if ($_GET['type'] == 'pemenang') {
						// $row[] = $btnUpdate.' '.$btnDelete;
						$row[] = $btnUpdate;
					} else {
						$row[] = '<span class="btn btn-success">LUNAS</span>';
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtNPLPeserta': // Done
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$btnUpdate = $btnDelete = $btnRestore = $btnListNPL = '';
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama'];
					$row[] = '<a target="_blank" href="mailto:' . $l['email'] . '">' . $l['email'] . '</a>';
					$row[] = '<a target="_blank" href="tel:' . $l['no_hp'] . '">' . $l['no_hp'] . '</a>';
					// $row[] = (!empty($l['id_npl'])) ? sprintf("%'.04d\n", $l['id_npl']) : "-";
					$row[] = (!empty($l['no_npl'])) ? $l['no_npl'] : "-";

					if ($_SESSION['menu']['peserta']['update']) {
						$btnUpdate .= '<button class="btn btn-warning" type="button" title="Detail Data Peserta" onclick="detail(' . $l['id_npl'] . ');"><i class="fas fa-eye"></i></button>';
						$btnListNPL .= '<a href="' . base_url('admin/npl_peserta?id_peserta=') . $l['id_peserta'] . '" ><button class="btn btn-success" type="button" title="List Data NPL Peserta"><i class="fas fa-list"></i></button></a>';
					}
					if ($_SESSION['menu']['peserta']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`peserta`,' . $l['id_peserta'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Peserta"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['peserta']['restore']) {
						$btnRestore .= '<button class="btn btn-warning" type="button" title="Detail Data Peserta" onclick="detail(' . $l['id_npl'] . ');"><i class="fas fa-eye"></i></button>';
						// $btnRestore .= '<a href="' . base_url('admin/peserta/restore') . '/' . $l['id_peserta'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Peserta"><i class="fas fa-recycle"></i></button></a>';
					}
					// $row[] = $btnListNPL;

					// $verifikasi = (is_null($l['id_barang']))?'<a href="'.base_url('admin/peserta/active').'/'.$l['id_peserta'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Data Peserta Ini???`)" ><button class="btn btn-success" type="button" title="Verifikasi Data Calon Peserta"><i class="fas fa-check"></i></button></a>':'';


					if ($_GET['type'] == 'npl_peserta') {
						$row[] = $btnUpdate;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtNPL':
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$btnUpdate = $btnDelete = $btnRestore = $btnListNPL = '';
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama'];
					$row[] = '<a target="_blank" href="mailto:' . $l['email'] . '">' . $l['email'] . '</a>';
					$row[] = $l['tanggal_transfer'];
					$row[] = "Rp" . number_format($l['nominal'], 0, ',', '.');

					if ($_SESSION['menu']['peserta']['update']) {
						$btnUpdate .= '<button class="btn btn-success" type="button" title="Verifikasi NPL" onclick="verifikasi(' . $l['id_pembelian_npl'] . ');"><i class="fas fa-check"></i></button>';
						// $btnListNPL .= '<a href="'.base_url('admin/npl_peserta?id_peserta=').$l['id_peserta'].'" ><button class="btn btn-success" type="button" title="List Data NPL Peserta"><i class="fas fa-list"></i></button></a>';
					}
					if ($_SESSION['menu']['peserta']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`peserta`,' . $l['id_peserta'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Peserta"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['peserta']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/peserta/restore') . '/' . $l['id_peserta'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Peserta"><i class="fas fa-recycle"></i></button></a>';
					}
					// $row[] = $btnListNPL;

					// $verifikasi = (is_null($l['id_barang']))?'<a href="'.base_url('admin/peserta/active').'/'.$l['id_peserta'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Data Peserta Ini???`)" ><button class="btn btn-success" type="button" title="Verifikasi Data Calon Peserta"><i class="fas fa-check"></i></button></a>':'';


					if ($_GET['type'] == 'npl_verifikasi') {
						$row[] = $btnUpdate;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtKontak':
				$list = $this->model_datatable->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama'];
					$row[] = '<a target="_blank" href="mailto:' . $l['email'] . '">' . $l['email'] . '</a>';
					$row[] = '<a target="_blank" href="tel:' . $l['no_tlp'] . '">' . $l['no_tlp'] . '</a>';
					$row[] = $l['pesan'];
					$btnUpdate = $btnDelete = $btnRestore = '';
					if ($_SESSION['menu']['kontak']['update']) {
						$btnUpdate .= '<button class="btn btn-primary" type="button" title="Respon Data Kontak" onclick="edit(' . $l['id_kontak'] . ');"><i class="fas fa-reply"></i></button>';
					}
					if ($_SESSION['menu']['kontak']['delete']) {
						$btnDelete .= '<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`kontak`,' . $l['id_kontak'] . ')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Kontak"><i class="fas fa-trash"></i></button></a>';
					}
					if ($_SESSION['menu']['kontak']['restore']) {
						$btnRestore .= '<a href="' . base_url('admin/kontak/restore') . '/' . $l['id_kontak'] . '" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Kontak"><i class="fas fa-recycle"></i></button></a>';
					}

					if ($_GET['type'] == 'kontak') {
						$row[] = $btnUpdate . ' ' . $btnDelete;
					} else {
						$row[] = $btnRestore;
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_datatable->count_all($_GET['type']),
					"recordsFiltered" => $this->model_datatable->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			default:
				# code...
				break;
		}
	}

	public function inject()
	{
		echo "<span>start time : " . microtime(true) . "</span><br>";
		$data = array();
		$data['limit'] = 1000;
		$query = $this->db->query('SELECT id_user, nama_user, email, kode_uniqlo, kelas FROM m_user AS p WHERE p.kelas LIKE "%26-07:15-yoga%" OR p.kelas LIKE "%20-08:30-yoga%" OR p.kelas LIKE "%20-16:00-strong%" LIMIT ' . $data['limit']);
		$data['num_rows'] = $query->num_rows();
		$data['query'] = $query->result();
		foreach ($data['query'] as $val) {
			// var_dump($val);
			$val->kelas = str_replace('26-07:15-yoga', '26-07:15-strong', $val->kelas);
			$val->kelas = str_replace('20-08:30-yoga', '21-08:30-yoga', $val->kelas);
			$val->kelas = str_replace('20-16:00-strong', '21-16:00-strong', $val->kelas);

			$this->db->set('kelas', $val->kelas);
			$this->db->where('id_user', $val->id_user);
			$this->db->update('m_user');

			// break;
		}
		// var_dump($data);
		echo "<span>end time : " . microtime(true) . "</span><br>";
		$this->load->view('admin/inject', $data, false);
	}

	public function sand_box($type)
	{
		switch ($type) {
			case 'send_email':
				try {
					$this->load->helper('email');
					$email_setting = (object) array();
					$email_setting->email_from = $this->config->item('smtp_user');
					$email_setting->email_to = $_GET['email'];
					$email_setting->subject = "Test Email";
					$email_setting->message = "Test kirim email dari cpanel spero.id";
					send_email($email_setting);
					echo "sampai sini";
				} catch (\Throwable $th) {
					echo "Error <hr>" . $th;
				}
				break;

			default:
				# code...
				break;
		}
	}
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_global extends CI_Model {   

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getDataQuery($querySQL) {
        $query = $this->db->query($querySQL);
        return $query->result();
    }    

    public function _getData($data) {
        $this->db->select($data['select']);

        if (isset($data['table'])) {
            $this->db->from($data['table']);
        } elseif (isset($data['from'])) {
            $this->db->from($data['from']);
        }

        if (isset($data['where'])) {
            if (is_array($data['where'])) {
                foreach ($data['where'] as $where) {
                    if (is_array($where)) {
                        $this->db->where($where['0'],$where['1']);
                    } else {
                        $this->db->where($where);
                    }
                }
            } else {
                $this->db->where($data['where']);
            }            
        }

        if (isset($data['join'])) {
            foreach ($data['join'] as $join) {
                if (!empty($join['2'])) {
                    $this->db->join($join['0'],$join['1'],$join['2']);
                } else {
                    $this->db->join($join['0'],$join['1']);
                }
            }
        }

        if (isset($data['limit'])) {
            if (is_array($data['limit'])) {
                $this->db->limit($data['limit']['0'],$data['limit']['1']);
            } else {
                $this->db->limit($data['limit']);
            }
        }
        
        if (isset($data['group_by'])) {
            $this->db->group_by($data['group_by']);
        }
        
        if (isset($data['order'])) {
            foreach ($data['order'] as $order) {
                $this->db->order_by($order['0'],$order['1']);
            }
        }
    }

    public function getData($data) {
        $this->_getData($data);
        return $this->db->get()->result();
    }

    public function getDataRows($data) {
        $this->_getData($data);
        return $this->db->get()->num_rows();
    } 

    public function addData($value=null) {
        $this->db->insert($value['table'],$value['data']);
        $id = $this->db->insert_id();
        return $id;
    } 

    public function addBulkData($value=null) {
        $this->db->insert_batch($value['table'],$value['data']);
        // $id = $this->db->insert_id();
        return true;
    }
    
    public function updateData($value) {
        if (isset($value['where'])) {
            if (is_array($value['where'])) {
                foreach ($value['where'] as $where) {
                    $this->db->where($where['0'],$where['1']);
                }
            } else {
                $this->db->where($value['where']);
            }
        }        
        $this->db->set($value['data']);
        $this->db->update($value['table']);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    // public function save($table, $data) {
    //     $this->db->insert($table, $data);
    //     return $this->db->insert_id();
    // } 

    // public function edit($table, $data, $where) {
    //     $this->db->set($data);
    //     $this->db->where($where['0'],$where['1']);
    //     $this->db->update($table);
    //     return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    // }

    public function delById($value) {
        $data = array('status' => '0',);

        if (isset($value['field'])) {
            $this->db->where($value['field'], $value['id']);
        } else {
            $this->db->where($value['search'], $value['id']);
        }        
        
        $this->db->update($value['table'], $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    public function restorById($value) {
        $data = array('status' => '1',);        

        if (isset($value['field'])) {
            $this->db->where($value['field'], $value['id']);
        } else {
            $this->db->where($value['search'], $value['id']);
        }

        $this->db->update($value['table'], $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }
}
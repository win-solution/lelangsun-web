<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_datatable extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($type = null, $id = null)
    {
        switch ($type) {
            case 'user':
            case 'del_user':
                $column_order = array('p.id_user', 'p.nama_user', 'p.email', 'r.nama_role'); //set column field database for datatable orderable
                $column_search = array('p.nama_user', 'p.email', 'r.nama_role'); //set column field database for datatable searchable
                $order = array('p.id_user' => 'desc'); // default order

                $this->db->select("p.*,r.nama_role");
                $this->db->from("m_user as p");
                $this->db->join("m_role as r", "r.id_role = p.role and r.status = 1");
                break;
            case 'role':
            case 'del_role':
                $column_order = array('p.id_role', 'p.nama_role'); //set column field database for datatable orderable
                $column_search = array('p.nama_role'); //set column field database for datatable searchable
                $order = array('p.id_role' => 'desc'); // default order

                $this->db->select("p.*");
                $this->db->from("m_role as p");
                break;
            case 'event':
            case 'del_event':
                $column_order = array('b.id_event', 'b.nama_event', 'b.waktu_event', 'b.alamat_event'); //set column field database for datatable orderable
                $column_search = array('b.nama_event', 'b.waktu_event', 'b.alamat_event'); //set column field database for datatable searchable
                $order = array('b.id_event' => 'desc'); // default order

                $this->db->select("b.*");
                $this->db->from("t_event as b");
                break;
            case 'banner':
            case 'del_banner':
                $column_order = array('b.id_event', 'b.judul', 'b.gambar'); //set column field database for datatable orderable
                $column_search = array('b.judul', 'b.gambar'); //set column field database for datatable searchable
                $order = array('b.id_banner' => 'desc'); // default order

                $this->db->select("b.*");
                $this->db->from("t_banner as b");
                break;
            case 'kategori':
            case 'del_kategori':
                $column_order = array('k.id_kategori', 'k.kategori'); //set column field database for datatable orderable
                $column_search = array('k.kategori'); //set column field database for datatable searchable
                $order = array('k.id_kategori' => 'desc'); // default order

                $this->db->select("k.*");
                $this->db->from("m_kategori as k");
                break;
            case 'barang':
            case 'del_barang':
                $column_order = array('m.id_barang_lelang', 'm.nama_barang_lelang', 'k.kategori'); //set column field database for datatable orderable
                $column_search = array('m.nama_barang_lelang', 'k.kategori'); //set column field database for datatable searchable
                $order = array('m.id_barang_lelang' => 'desc'); // default order

                $this->db->select("m.*,k.kategori,g.gambar");
                $this->db->from("m_barang_lelang as m");
                $this->db->join("m_kategori as k", "k.id_kategori = m.id_kategori and k.status = 1");
                $this->db->join("m_foto_barang_lelang as g", "g.id_barang_lelang = m.id_barang_lelang and g.status = 1", "left");
                // $this->db->where("g.status = 1");
                $this->db->group_by('m.id_barang_lelang');
                break;
            case 'lot':
            case 'del_lot':
                $column_order = array('m.id_event', 'm.nama_event', 'm.waktu_event', 'm.alamat_event', 'jumlah_lot'); //set column field database for datatable orderable
                $column_search = array('m.nama_event', 'm.waktu_event', 'm.alamat_event'); //set column field database for datatable searchable
                $order = array('m.id_event' => 'desc'); // default order

                $this->db->select("m.*, count(l.id_event) as jumlah_lot");
                $this->db->from("t_event as m");
                $this->db->join("t_lot as l", "l.id_event = m.id_event AND l.status = 1 AND l.status_lot = 1", "left");
                $this->db->group_by('m.id_event');
                break;
            case 'peserta':
            case 'del_peserta':
                $column_order = array('m.id_peserta', 'm.nama', 'm.email', 'm.no_hp', 'm.alamat'); //set column field database for datatable orderable
                $column_search = array('m.nama', 'm.email', 'm.no_hp', 'm.alamat'); //set column field database for datatable searchable
                $order = array('m.id_peserta' => 'desc'); // default order

                $this->db->select("m.*,COUNT(n.id_npl) as jumlah_npl");
                $this->db->from("m_peserta as m");
                $this->db->join("m_npl as n", "n.id_peserta = m.id_peserta AND n.status_npl = 1", "left");
                $this->db->group_by('m.id_peserta');
                break;
            case 'npl_verifikasi':
                $column_order = array('n.id_pembelian_npl', 'p.nama', 'p.email', 'n.tanggal_transfer', 'n.nominal', ''); //set column field database for datatable orderable
                $column_search = array('p.nama', 'p.email', 'n.tanggal_transfer', 'n.nominal'); //set column field database for datatable searchable
                $order = array('n.id_pembelian_npl' => 'desc'); // default order

                $this->db->select("n.*, p.nama, p.email");
                $this->db->from("t_pembelian_npl as n");
                $this->db->join("m_peserta as p", "p.id_peserta = n.id_peserta AND p.status = 1");
                break;
            case 'pemenang':
            case 'del_pemenang':
                $column_order = array('n.id_pemenang_lelang', 'p.nama', 'p.email', 'b.nama_barang_lelang', 'bi.harga_bidding', 'n.npl', ''); //set column field database for datatable orderable
                $column_search = array('p.nama', 'p.email', 'b.nama_barang_lelang', 'bi.harga_bidding', 'n.npl'); //set column field database for datatable searchable
                $order = array('n.id_pemenang_lelang' => 'desc'); // default order

                $this->db->select("n.*, p.nama, p.email, b.nama_barang_lelang, bi.harga_bidding, e.nama_event, e.waktu_event,l.no_lot");
                $this->db->from("t_pemenang_lelang as n");
                $this->db->join("t_bidding as bi", "bi.id_bidding = n.id_bidding AND bi.status = 1");
                $this->db->join("t_event as e", "e.id_event = bi.id_event");
                $this->db->join("m_peserta as p", "p.id_peserta = bi.id_peserta AND p.status = 1", 'left');
                $this->db->join("t_lot as l", "l.id_lot = bi.id_lot AND l.status_lot != 1", 'left');
                $this->db->join("m_barang_lelang as b", "b.id_barang_lelang = l.id_barang_lelang", 'left');
                break;
            case 'npl_peserta':
            case 'del_npl_peserta':
                $column_order = array('m.id_peserta', 'm.nama', 'm.email', 'm.no_hp', 'n.no_npl'); //set column field database for datatable orderable
                $column_search = array('m.nama', 'm.email', 'm.no_hp', 'n.no_npl'); //set column field database for datatable searchable
                $order = array('m.id_peserta' => 'desc'); // default order

                $this->db->select("m.*, n.id_npl, n.npl, n.no_npl");
                $this->db->from("m_peserta as m");
                $this->db->join("m_npl as n", 'n.id_peserta = m.id_peserta');
                $this->db->where('m.id_peserta = "' . $_GET['id_peserta'] . '"');
                break;
            case 'outlet':
            case 'del_outlet':
                $column_order = array('o.id_outlet', 'o.nama_outlet', 'mi.alamat_peserta'); //set column field database for datatable orderable
                $column_search = array('o.nama_outlet', 'mi.nama_peserta', 'mi.email', 'mi.no_tlp', 'mi.alamat_peserta'); //set column field database for datatable searchable
                $order = array('o.id_outlet' => 'desc'); // default order

                $this->db->select("o.*, mi.id_peserta, mi.nama_peserta, mi.email, mi.no_tlp, mi.alamat_peserta");
                $this->db->from("t_outlet as o");
                $this->db->join("t_peserta as mi", "mi.id_peserta = o.id_peserta and mi.status = 1", "left");
                break;
            case 'kontak':
            case 'del_kontak':
                $column_order = array('m.id_kontak', 'm.nama', 'm.email', 'm.no_tlp', 'm.pesan'); //set column field database for datatable orderable
                $column_search = array('m.nama', 'm.email', 'm.no_tlp', 'm.pesan'); //set column field database for datatable searchable
                $order = array('m.id_kontak' => 'desc'); // default order

                $this->db->select("m.*");
                $this->db->from("t_kontak as m");
                break;

            default:
                # code...
                break;
        }

        $i = 0;
        foreach ($column_search as $item) // loop column 
        {
            if (isset($_POST['search'])) {
                if ($_POST['search']['value']) // if datatable send POST for search
                {
                    if ($i === 0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }

                    if (count($column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
        }

        if (!isset($order_req)) {
            if (isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else if (isset($order)) {
                foreach ($order as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }
    }

    public function get_datatables($type = null, $id = null)
    {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where("p.status = 1");
                break;
            case 'del_user':
                $this->db->where("p.status = 0");
                break;

            case 'role':
                $this->db->where("p.status = 1");
                break;
            case 'del_role':
                $this->db->where("p.status = 0");
                break;

            case 'banner':
                $this->db->where("b.status = 1");
                break;
            case 'del_banner':
                $this->db->where("b.status = 0");
                break;

            case 'event':
                // $this->db->where("b.waktu_event >= '" . date('Y-m-d') . "'");
                $this->db->where("b.status = 1");
                break;
            case 'del_event':
                // $this->db->where("b.waktu_event <= '" . date('Y-m-d') . "'");
                $this->db->where("b.status = 0");
                break;

            case 'kategori':
                $this->db->where("k.status = 1");
                break;
            case 'del_kategori':
                $this->db->where("k.status = 0");
                break;

            case 'lot':
                $this->db->where("m.waktu_event >= '" . date('Y-m-d') . "'");
                $this->db->where("m.status = 1");
                break;
            case 'del_lot':
                $this->db->where("m.waktu_event <= '" . date('Y-m-d') . "'");
                $this->db->where("m.status = 0");
                break;

            case 'barang':
                $this->db->where("m.status = 1");
                break;
            case 'del_barang':
                $this->db->where("m.status = 0");
                break;

            case 'peserta':
                $this->db->where("m.status = 1");
                break;
            case 'del_peserta':
                $this->db->where("m.status = 0");
                break;

            case 'pemenang':
                $this->db->where("n.status_pembayaran = 0 AND l.status_lot = 2");
                break;
            case 'del_pemenang':
                $this->db->where("n.status_pembayaran = 1  AND l.status_lot = 3");
                break;

            case 'npl_verifikasi':
                $this->db->where("n.verifikasi = 0 and n.status = 1");
                break;

            case 'npl_peserta':
                $this->db->where("m.status = 1 and n.status_npl = 1");
                break;
            case 'del_npl_peserta':
                $this->db->where("m.status = 1 and n.status_npl = 0");
                break;

            case 'outlet':
                $this->db->where("o.status = 1");
                break;
            case 'del_outlet':
                $this->db->where("o.status = 0");
                break;

            case 'kontak':
                $this->db->where("m.status = 1");
                break;
            case 'del_kontak':
                $this->db->where("m.status = 0");
                break;

            default:
                # code...
                break;
        }

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function count_filtered($type = null, $id = null)
    {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where("p.status = 1");
                break;
            case 'del_user':
                $this->db->where("p.status = 0");
                break;

            case 'role':
                $this->db->where("p.status = 1");
                break;
            case 'del_role':
                $this->db->where("p.status = 0");
                break;

            case 'banner':
                $this->db->where("b.status = 1");
                break;
            case 'del_banner':
                $this->db->where("b.status = 0");
                break;

            case 'event':
                // $this->db->where("b.waktu_event >= '" . date('Y-m-d') . "'");
                $this->db->where("b.status = 1");
                break;
            case 'del_event':
                // $this->db->where("b.waktu_event <= '" . date('Y-m-d') . "'");
                $this->db->where("b.status = 0");
                break;

            case 'kategori':
                $this->db->where("k.status = 1");
                break;
            case 'del_kategori':
                $this->db->where("k.status = 0");
                break;

            case 'lot':
                $this->db->where("m.waktu_event >= '" . date('Y-m-d') . "'");
                $this->db->where("m.status = 1");
                break;
            case 'del_lot':
                $this->db->where("m.waktu_event <= '" . date('Y-m-d') . "'");
                $this->db->where("m.status = 0");
                break;

            case 'barang':
                $this->db->where("m.status = 1");
                break;
            case 'del_barang':
                $this->db->where("m.status = 0");
                break;

            case 'peserta':
                $this->db->where("m.status = 1");
                break;
            case 'del_peserta':
                $this->db->where("m.status = 0");
                break;

            case 'pemenang':
                $this->db->where("n.status_pembayaran = 0 AND l.status_lot = 2");
                break;
            case 'del_pemenang':
                $this->db->where("n.status_pembayaran = 1  AND l.status_lot = 3");
                break;

            case 'npl_verifikasi':
                $this->db->where("n.verifikasi = 0 and n.status = 1");
                break;

            case 'npl_peserta':
                $this->db->where("m.status = 1 and n.status_npl = 1");
                break;
            case 'del_npl_peserta':
                $this->db->where("m.status = 1 and n.status_npl = 0");
                break;

            case 'outlet':
                $this->db->where("o.status = 1");
                break;
            case 'del_outlet':
                $this->db->where("o.status = 0");
                break;

            case 'kontak':
                $this->db->where("m.status = 1");
                break;
            case 'del_kontak':
                $this->db->where("m.status = 0");
                break;

            default:
                # code...
                break;
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($type = null, $id = null)
    {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->db->where("p.status = 1");
                break;
            case 'del_user':
                $this->db->where("p.status = 0");
                break;

            case 'role':
                $this->db->where("p.status = 1");
                break;
            case 'del_role':
                $this->db->where("p.status = 0");
                break;

            case 'banner':
                $this->db->where("b.status = 1");
                break;
            case 'del_banner':
                $this->db->where("b.status = 0");
                break;

            case 'event':
                // $this->db->where("b.waktu_event >= '" . date('Y-m-d') . "'");
                $this->db->where("b.status = 1");
                break;
            case 'del_event':
                // $this->db->where("b.waktu_event <= '" . date('Y-m-d') . "'");
                $this->db->where("b.status = 0");
                break;

            case 'kategori':
                $this->db->where("k.status = 1");
                break;
            case 'del_kategori':
                $this->db->where("k.status = 0");
                break;

            case 'lot':
                $this->db->where("m.waktu_event >= '" . date('Y-m-d') . "'");
                $this->db->where("m.status = 1");
                break;
            case 'del_lot':
                $this->db->where("m.waktu_event <= '" . date('Y-m-d') . "'");
                $this->db->where("m.status = 0");
                break;

            case 'barang':
                $this->db->where("m.status = 1");
                break;
            case 'del_barang':
                $this->db->where("m.status = 0");
                break;

            case 'peserta':
                $this->db->where("m.status = 1");
                break;
            case 'del_peserta':
                $this->db->where("m.status = 0");
                break;

            case 'pemenang':
                $this->db->where("n.status_pembayaran = 0 AND l.status_lot = 2");
                break;
            case 'del_pemenang':
                $this->db->where("n.status_pembayaran = 1  AND l.status_lot = 3");
                break;

            case 'npl_verifikasi':
                $this->db->where("n.verifikasi = 0 and n.status = 1");
                break;

            case 'npl_peserta':
                $this->db->where("m.status = 1 and n.status_npl = 1");
                break;
            case 'del_npl_peserta':
                $this->db->where("m.status = 1 and n.status_npl = 0");
                break;

            case 'outlet':
                $this->db->where("o.status = 1");
                break;
            case 'del_outlet':
                $this->db->where("o.status = 0");
                break;

            case 'kontak':
                $this->db->where("m.status = 1");
                break;
            case 'del_kontak':
                $this->db->where("m.status = 0");
                break;

            default:
                # code...
                break;
        }

        return $this->db->count_all_results();
    }
}

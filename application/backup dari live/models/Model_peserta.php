<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_peserta extends CI_Model {   

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($type=null,$id=null) {
        switch ($type) {
            case 'peserta':
            case 'del_peserta':
                $column_order = array('p.id_peserta', 'p.nama_peserta', 'p.email', 'p.created_at'); //set column field database for datatable orderable
                $column_search = array('p.nama_peserta', 'p.email', 'p.kelas', 'p.created_at'); //set column field database for datatable searchable
                $order = array('p.id_peserta' => 'desc'); // default order

                $this->db->select("p.*");
                $this->db->from("m_peserta as p");
                break;

            default:
                # code...
                break;
        }

        $i = 0;
        if(!empty($_POST['kelas'])) {
            $this->db->like('kelas', $_POST['kelas'], 'both');
        }
        foreach ($column_search as $item) // loop column
        {
            if (!empty($_POST['search'])) {
                if($_POST['search']['value']) // if datatable send POST for search
                {
                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                        // $this->db->like($item, $_POST['search']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                        // $this->db->or_like($item, $_POST['search']);
                    }

                    if(count($column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
        }

        if (!empty($order_req)) {
            if(empty($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            else if(empty($order))
            {
                foreach ($order as $key => $value) {
                    $this->db->order_by($key, $value);
                }
            }
        }
    }

    public function get_datatables($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'peserta': $this->db->where("p.status = 1"); break;
            case 'del_peserta': $this->db->where("p.status = 0"); break;

            default:
                # code...
                break;
        }

        if($_POST['length'] != -1){
        	$this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function count_filtered($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'peserta': $this->db->where("p.status = 1"); break;
            case 'del_peserta': $this->db->where("p.status = 0"); break;

            default:
                # code...
                break;
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'peserta': $this->db->where("p.status = 1"); break;
            case 'del_peserta': $this->db->where("p.status = 0"); break;

            default:
                # code...
                break;
        }

        return $this->db->count_all_results();
    }

    public function getListPeserta()
    {
        $this->db->select('id_peserta, nama_peserta, email, created_at');
        $this->db->from('m_peserta');
        $this->db->where('status = 1');
        $this->db->order_by('id_peserta','desc');
        return $this->db->get()->result();
    }

    public function save($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    } 

}
<?php 
function imageProses($option) {
    // var_dump("imageProses option <hr>",$option);
    // die;
	$CI = get_instance();
    $config['image_library']='gd2';
    $config['source_image']= $option['source_image'];
    $config['quality']= '75%';
    $config['new_image']= $option['new_image'];
    switch ($option['type']) {
        case 'resize':
            // $config['create_thumb']= TRUE;
            // $config['thumb_marker']= TRUE;
            $config['width']=  $option['width'];
            $config['maintain_ratio']= TRUE;
            break;
        case 'watermark':
            $config['wm_text'] = WM_TEXT;
            $config['wm_type'] = 'text';
            $config['wm_font_path'] = './system/fonts/texb.ttf';
            $config['wm_font_size'] = '16';
            $config['wm_font_color'] = 'ffffff';
            $config['wm_vrt_alignment'] = 'center';
            $config['wm_hor_alignment'] = 'center';
            // $config['wm_padding'] = '20';
            break;
        case 'crop':
            $config['width']= 500;
            $config['height']= 400;
            $config['x_axis']= 500;
            $config['y_axis']= 400;
            $config['maintain_ratio']= TRUE;
            break;
        
        default:
            # code...
            break;
    }
    
    
    $CI->load->library('image_lib', $config);
    if ( !$CI->image_lib->resize()) {
        echo $CI->image_lib->display_errors();
    }
}
?>
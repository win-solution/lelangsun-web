<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
    	<?php include('part/nav.php') ?>

    	<header class="masthead" >
    	    <div class="container mw-100 mh-100" style=" background-image: url('assets/images/backgroundlot1.png'); height: 90vh; background-position: center; background-size: cover;">
    	        <div class="row justify-content-center h-100">
    	            <div class="col-12 col-lg-12 my-auto d-flex justify-content-center text-center">
    	                <!-- <div class="row"> -->
    	                    <div>
	                            <h1 class="text-uppercase text-white text-uppercase" style="font-size: 80px;">Event</h1>
	                            <span class="subheading text-white" style="font-size: 30px;">Ayo ikuti event lelang secara langsung !</span>
	                        </div>

    	                <!-- </div> -->
    	            </div>
    	        </div>
    	    </div>
    	</header>

    	<section class="page-section portfolio" id="about" style="background-image: url('assets/images/backgroundlot1bawah.png'); background-size: cover; position: relative;">
    	    <div class="container mh-100 mw-90">
    	        <div class="mw-90 mx-auto">
    	            <div class="row row-cols-1 row-cols-md-3">
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil2.png" class="card-img-top" alt="...">
    	                  </div>
    	                  <div class="card-body">
    	                    <h3 class="card-title">Vios 1.5 G M/T</h3>
    	                    <span style="font-size: 20px; color: #333" class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i>   22 April 2021 / 13.00 WIB</span>
    	                    <br>
    	                    <span style="font-size: 20px; color: #333" class=""><i class="fas fa-map-marker-alt" style="color: #ff4500"></i>  Jawa Tengah</span>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil3.jpg" class="card-img-top" alt="...">
    	                  </div>
    	                  <div class="card-body">    	                  	
    	                    <h3 class="card-title">APV 1.5 DLX M/T 2012</h3>
    	                    <span style="font-size: 20px; color: #333" class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i>   10 Juni 2021 / 13.00 WIB</span>
    	                    <br>
    	                    <span style="font-size: 20px; color: #333" class=""><i class="fas fa-map-marker-alt" style="color: #ff4500"></i>  Jawa Tengah</span>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil4.png" class="card-img-top" alt="...">
    	              	  </div>
    	                  <div class="card-body">
    	                    <h3 class="card-title">Ertiga 1.4 GX M/T 2012</h3>
    	                    <span style="font-size: 20px; color: #333" class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i>   25 Oktober 2021 / 13.00 WIB</span>
    	                    <br>
    	                    <span style="font-size: 20px; color: #333" class=""><i class="fas fa-map-marker-alt" style="color: #ff4500"></i>  Jawa Tengah</span>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil6.jpg" class="card-img-top" alt="...">
    	                  </div>
    	                  <div class="card-body">
    	                    <h3 class="card-title">JUKE 2011</h3>
    	                    <span style="font-size: 20px; color: #333" class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i>   12 November 2021 / 13.00 WIB</span>
    	                    <br>
    	                    <span style="font-size: 20px; color: #333" class=""><i class="fas fa-map-marker-alt" style="color: #ff4500"></i>  Jawa Tengah</span>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil2.png" class="card-img-top" alt="...">
    	              	  </div>
    	                  <div class="card-body">
    	                    <h3 class="card-title">Vios 1.5 G M/T</h3>
    	                    <span style="font-size: 20px; color: #333" class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i>   22 November 2021 / 13.00 WIB</span>
    	                    <br>
    	                    <span style="font-size: 20px; color: #333" class=""><i class="fas fa-map-marker-alt" style="color: #ff4500"></i>  Jawa Tengah</span>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil3.jpg" class="card-img-top" alt="...">
    	                  </div>
    	                  <div class="card-body">
    	                    <h3 class="card-title">APV 1.5 DLX M/T 2012</h3>
    	                    <span style="font-size: 20px; color: #333" class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i>   22 Desember 2021 / 13.00 WIB</span>
    	                    <br>
    	                    <span style="font-size: 20px; color: #333" class=""><i class="fas fa-map-marker-alt" style="color: #ff4500"></i>  Jawa Tengah</span>
    	                  </div>
    	                </div>
    	              </div>
    	            </div>
    	        </div>
    	    </div>
    	</section>

    </body>
</html>
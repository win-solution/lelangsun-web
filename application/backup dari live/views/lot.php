<!DOCTYPE html>
<html lang="en">
<?php include('part/head.php') ?>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelectorAll('.sidebar .nav-link').forEach(function(element) {
            element.addEventListener('click', function(e) {
                let nextEl = element.nextElementSibling;
                let parentEl = element.parentElement;
                if (nextEl) {
                    e.preventDefault();
                    let mycollapse = new bootstrap.Collapse(nextEl);

                    if (nextEl.classList.contains('show')) {
                        mycollapse.hide();
                    } else {
                        mycollapse.show();
                        // find other submenus with class=show
                        var opened_submenu = parentEl.parentElement.querySelector('.submenu.show');
                        // if it exists, then close all of them
                        if (opened_submenu) {
                            new bootstrap.Collapse(opened_submenu);
                        }
                    }
                }
            }); // addEventListener
        }) // forEach
    });
    // DOMContentLoaded  end
</script>

<style type="text/css">
    input[type=text] {
        box-sizing: border-box;
        border: 2px solid #ccc;
        border-radius: 4px;
        font-size: 16px;
        background-color: white;
        background-image: url('assets/images/magnifying-glass.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        padding: 10px 20px 10px 40px;
        -webkit-transition: width 0.4s ease-in-out;
        transition: width 0.4s ease-in-out;
    }

    /* If the screen size is 601px wide or more, set the font-size of <div> to 80px */
    @media screen and (min-width: 601px) {
        .lot {
            background-image: url('assets/images/y/web-01.png');
            height: 112vh;
            background-position: left;
            background-size: cover;
            background-repeat: no-repeat;
        }
    }

    /* If the screen size is 600px wide or less, set the font-size of <div> to 30px */
    @media screen and (max-width: 600px) {
        .lot {
            background-image: url('assets/images/y/mobile-01.png');
            height: 30vh;
            background-position: left;
            background-size: cover;
            background-repeat: no-repeat;
        }
    }

    .img-cover {
        height: 25vh;
        object-fit: cover;
    }

    @media screen and (max-width: 600px) {
        .img-cover {
            height: 30vh;
        }
    }

    /* common */
    .ribbon {
        width: 150px;
        height: 150px;
        overflow: hidden;
        position: absolute;
    }

    .ribbon::before,
    .ribbon::after {
        position: absolute;
        z-index: -1;
        content: '';
        display: block;
        border: 5px solid #2980b9;
    }

    .ribbon span {
        position: absolute;
        display: block;
        width: 225px;
        padding: 15px 0;
        background-color: #3498db;
        box-shadow: 0 5px 10px rgba(0, 0, 0, .1);
        color: #fff;
        font: 700 18px/1 'Lato', sans-serif;
        text-shadow: 0 1px 1px rgba(0, 0, 0, .2);
        text-transform: uppercase;
        text-align: center;
    }

    /* top left*/
    .ribbon-top-left {
        top: -10px;
        left: -10px;
    }

    .ribbon-top-left::before,
    .ribbon-top-left::after {
        border-top-color: transparent;
        border-left-color: transparent;
    }

    .ribbon-top-left::before {
        top: 0;
        right: 0;
    }

    .ribbon-top-left::after {
        bottom: 0;
        left: 0;
    }

    .ribbon-top-left span {
        right: -25px;
        top: 30px;
        transform: rotate(-45deg);
    }

    /* top right*/
    .ribbon-top-right {
        top: -10px;
        right: -10px;
    }

    .ribbon-top-right::before,
    .ribbon-top-right::after {
        border-top-color: transparent;
        border-right-color: transparent;
    }

    .ribbon-top-right::before {
        top: 0;
        left: 0;
    }

    .ribbon-top-right::after {
        bottom: 0;
        right: 0;
    }

    .ribbon-top-right span {
        left: -25px;
        top: 30px;
        transform: rotate(45deg);
    }

    /* bottom left*/
    .ribbon-bottom-left {
        bottom: -10px;
        left: -10px;
    }

    .ribbon-bottom-left::before,
    .ribbon-bottom-left::after {
        border-bottom-color: transparent;
        border-left-color: transparent;
    }

    .ribbon-bottom-left::before {
        bottom: 0;
        right: 0;
    }

    .ribbon-bottom-left::after {
        top: 0;
        left: 0;
    }

    .ribbon-bottom-left span {
        right: -25px;
        bottom: 30px;
        transform: rotate(225deg);
    }

    /* bottom right*/
    .ribbon-bottom-right {
        bottom: -10px;
        right: -10px;
    }

    .ribbon-bottom-right::before,
    .ribbon-bottom-right::after {
        border-bottom-color: transparent;
        border-right-color: transparent;
    }

    .ribbon-bottom-right::before {
        bottom: 0;
        left: 0;
    }

    .ribbon-bottom-right::after {
        top: 0;
        right: 0;
    }

    .ribbon-bottom-right span {
        left: -25px;
        bottom: 30px;
        transform: rotate(-225deg);
    }
</style>

<body id="page-top" style="background-image: url('<?= base_url() ?>assets/images/dark2/darkkkkkk.png'); background-position: bottom;">
    <?php include('part/nav.php') ?>

    <header class="masthead" style="background-image: url('<?= base_url() ?>assets/images/y/web-05.png'); background-position: bottom;">
        <div class="container mw-100 mh-100 lot">
            <div class="row justify-content-center h-100 mw-90 mx-auto hidden-mobile">
                <div class="col-12 col-lg-6" style="margin: 23vh 0 0 0;">
                    <div>
                        <h1 class="text-uppercase text-uppercase fontbold" style="font-size: 5rem; color: #ff0000">Lot</h1>
                        <span class="subheading fontlight" style="font-size: 2rem; color: #fff;"><?= date('d M Y') ?></span>
                    </div>
                </div>
                <div class="col-12 col-lg-6 d-flex justify-content-center text-center hidden-mobile" style="margin: 12vh 0 0 0;">
                    <!-- <div>
                            <img src="assets/images/web-05.png" class="card-img-top" alt="...">
                        </div> -->
                </div>
            </div>
        </div>
    </header>

    <section class="page-section portfolio" id="about">
        <div class="container mw-90">
            <div class="row">
                <div class="col-12 offset-sm-2 col-md-3 mx-auto pb-4">
                    <div class="card-body" style="background-color: #31869b;">
                        <h4 class="text-uppercase text-white p-5 text-center fontbold" style="color: #333;">Filter</h4>
                        <div class="pb-4">
                            <div class="nav flex-column nav-pills fontlight" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <!-- <a class="nav-link active mb-3 dropdown-toggle" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Merek</a> -->
                                <form action="" method="get" class="mt-2 mb-4">
                                    <input class="form-control form-control-lg col mr-sm-2" type="text" name="q" id="q" placeholder="Search" aria-label="Search" value="<?= @$_GET['q'] ?>">
                                    <!-- <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button> -->
                                    <li class="nav-item dropdown mb-3 active">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Brand</a>
                                        <div class="dropdown-menu">
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="brand" id="radioBrand" value="" checked>
                                                <label class="form-check-label" for="radioBrand">Semua Brand</label>
                                            </div>
                                            <?php foreach ($_SESSION['filter_brand'] as $key => $val) {
                                                if (empty($val->brand)) {
                                                    continue;
                                                }
                                                $checked = "";
                                                if ($val->brand == @$_GET['brand']) {
                                                    $checked = "checked";
                                                } ?>
                                                <div class="form-check dropdown-item">
                                                    <input class="form-check-input" type="radio" name="brand" id="radioBrand-<?= $val->brand ?>" value="<?= $val->brand ?>" <?= $checked ?>>
                                                    <label class="form-check-label" for="radioBrand-<?= $val->brand ?>"><?= strtoupper($val->brand) ?></label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown mb-3 active">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tahun</a>
                                        <div class="dropdown-menu">
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="tahun" id="radioTahun" value="" checked>
                                                <label class="form-check-label" for="radioTahun">Semua Tahun</label>
                                            </div>
                                            <?php foreach ($_SESSION['filter_tahun'] as $key => $val) {
                                                if (empty($val->tahun)) {
                                                    continue;
                                                }
                                                $checked = "";
                                                if ($val->tahun == @$_GET['tahun']) {
                                                    $checked = "checked";
                                                } ?>
                                                <div class="form-check dropdown-item">
                                                    <input class="form-check-input" type="radio" name="tahun" id="radioTahun-<?= $val->tahun ?>" value="<?= $val->tahun ?>" <?= $checked ?>>
                                                    <label class="form-check-label" for="radioTahun-<?= $val->tahun ?>"><?= $val->tahun ?></label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown mb-3 active">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Harga Awal</a>
                                        <div class="dropdown-menu">
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="harga" value="" id="radioHarga" checked>
                                                <label class="form-check-label" for="radioHarga">Semua Harga</label>
                                            </div>
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="harga" id="radioHarga-+100000000" value="+100000000" <?= (@$_GET['harga'] == "+100000000") ? "checked" : ""; ?>>
                                                <label class="form-check-label" for="radioHarga-+100000000">Diatas Rp 100 Juta</label>
                                            </div>
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="harga" id="radioHarga-100000000" value="100000000" <?= (@$_GET['harga'] == "100000000") ? "checked" : ""; ?>>
                                                <label class="form-check-label" for="radioHarga-100000000">Dibawah Rp 100 Juta</label>
                                            </div>
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="harga" id="radioHarga-70000000" value="70000000" <?= (@$_GET['harga'] == "70000000") ? "checked" : ""; ?>>
                                                <label class="form-check-label" for="radioHarga-70000000">Dibawah Rp 70 Juta</label>
                                            </div>
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="harga" id="radioHarga-60000000" value="60000000" <?= (@$_GET['harga'] == "60000000") ? "checked" : ""; ?>>
                                                <label class="form-check-label" for="radioHarga-60000000">Dibawah Rp 60 Juta</label>
                                            </div>
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="harga" id="radioHarga-50000000" value="50000000" <?= (@$_GET['harga'] == "50000000") ? "checked" : ""; ?>>
                                                <label class="form-check-label" for="radioHarga-50000000">Dibawah Rp 50 Juta</label>
                                            </div>
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="harga" id="radioHarga-40000000" value="40000000" <?= (@$_GET['harga'] == "40000000") ? "checked" : ""; ?>>
                                                <label class="form-check-label" for="radioHarga-40000000">Dibawah Rp 40 Juta</label>
                                            </div>
                                            <div class="form-check dropdown-item">
                                                <input class="form-check-input" type="radio" name="harga" id="radioHarga-30000000" value="30000000" <?= (@$_GET['harga'] == "30000000") ? "checked" : ""; ?>>
                                                <label class="form-check-label" for="radioHarga-30000000">Dibawah Rp 30 Juta</label>
                                            </div>
                                        </div>
                                    </li>
                                    <div class="container d-flex justify-content-center align-items-center">
                                        <button class="btn btn-success w-75" type="submit">Filter</button>
                                    </div>
                                </form>
                            </div>
                            <!-- <div class="container d-flex justify-content-center align-items-center">
                                    <span class="btn btn-success w-75" onclick="alert('hai');">Filter</span>
                                </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12">
                            <?php if (!empty($_GET['q'])) {
                                echo '<h3 class="d-inline"><span class="badge badge-primary">Kata Pencarian : ' . $_GET['q'] . '</span></h3>';
                            } ?>
                            <?php if (!empty($_GET['brand'])) {
                                echo '<h3 class="d-inline"><span class="badge badge-primary">Brand : ' . $_GET['brand'] . '</span></h3>';
                            } ?>
                            <?php if (!empty($_GET['tahun'])) {
                                echo '<h3 class="d-inline"><span class="badge badge-primary">Tahun : ' . $_GET['tahun'] . '</span></h3>';
                            } ?>
                            <?php if (!empty($_GET['harga'])) {
                                echo '<h3 class="d-inline"><span class="badge badge-primary">Harga : ' . $_GET['harga'] . '</span></h3>';
                            } ?>
                        </div>
                    </div>
                    <br>
                    <div class="row mb-2">
                        <?php $lot = ($curent_page - 1) * $per_page;
                        foreach ($barang_lelang as $key => $val) {
                            $lot++;  ?>
                            <div class="col-lg-4 col-md-4 col-12 mb-4 pl-2 pr-0">
                                <a href="<?= base_url('detaillot') . '/' . @$val->id_lot; ?>" style="color: inherit; text-decoration: none;">
                                    <div class="card h-100 text-left">
                                        <?= (($val->status_lot !== "1") && (!empty($val->harga_bidding))) ? '<div class="ribbon ribbon-top-right"><span>SOLD</span></div>' : ''; ?>
                                        <div class="img">
                                            <img src="<?= base_url() ?>assets/uploads/gambar_barang_lelang/origin/<?= @$val->gambar ?>" class="card-img-top img-cover" alt="<?= @$val->nama_barang_lelang ?>">
                                        </div>
                                        <div class="card-body">
                                            <div class="promo-box"></div>
                                            <?= (!empty($val->grade)) ? '<div class="grade-box"><p class="mb-0 mt-1">Grade</p><p class="text-grade mb-0">' . $val->grade . '</p></div>' : ''; ?>
                                            <p class="card-text">Lot <?= sprintf("%'.05d\n", @$val->no_lot) ?></p>
                                            <h2 class="text-uppercase">
                                                <?php if ((@$barang_lelang[0]->id_kategori == 1) or (@$barang_lelang[0]->id_kategori == 2)) {
                                                    echo @$val->nama_barang_lelang . ' ' . @$val->transmisi;
                                                } else {
                                                    echo @$val->nama_barang_lelang;
                                                } ?>
                                            </h2>
                                            <p class="card-text"><?= @$val->no_polisi ?></p>
                                            <!-- <h2 class="card-title"><?= (empty($val->harga_bidding)) ? 'Rp ' . number_format(((!empty($val->harga_awal)) ? $val->harga_awal : 0), 0, '', '.') : 'Terjual dengan harga<br>Rp ' . number_format(((!empty($val->harga_bidding)) ? $val->harga_bidding : 0), 0, '', '.'); ?></h2> -->
                                            <h2 class="card-title"><?= 'Rp ' . number_format(((!empty($val->harga_awal)) ? $val->harga_awal : 0), 0, '', '.'); ?></h2>
                                            <?= (!empty($val->odometer)) ? '<span style=" color: #333" class="mr-3"><i class="fas fa-tachometer-alt" style="color: #ff4500"></i> ' . $val->odometer . '</span><br>' : ''; ?>
                                            <?= (!empty($val->alamat_event)) ? '<span style=" color: #333" class="mr-3 text-uppercase"><i class="fas fa-map-marked-alt" style="color: #ff4500"></i> ' . $val->lokasi_barang . '</span>' : ''; ?>
                                            <br>
                                            <?= (!empty($val->waktu_event)) ? '<span style=" color: #333" class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i> ' . date_format(date_create($val->waktu_event), 'd M Y / H:i') . ' WIB</span>' : '' ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>

                        <div class="col-12">
                            <?= $pagingView; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('part/footer.php') ?>

</body>

</html>
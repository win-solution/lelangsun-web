<!DOCTYPE html>
<html lang="en">
<?php include('part/head.php') ?>

<body id="page-top" style="background-image: url('<?= base_url('assets') ?>/images/dark2/dark2-06.png');background-repeat: no-repeat;position: relative;background-size: cover;background-position: center;">
    <!-- Place either at the bottom in the <head> of your page or inside your CSS -->
    <style type="text/css">
        .flex-caption {
            width: 96%;
            padding: 2%;
            left: 0;
            bottom: 0;
            background: rgba(0, 0, 0, .5);
            color: #fff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, .3);
            font-size: 14px;
            line-height: 18px;
        }

        li.css a {
            border-radius: 0;
        }

        #carousel .slides li {
            cursor: pointer;
        }

        #carousel .slides li.flex-active-slide {
            cursor: default;
        }
    </style>
    <?php include('part/nav.php') ?>
    <!-- Masthead-->
    <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('<?= base_url('assets') ?>/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
    <header class="details" style="">
        <div class="container mh-100 mw-90">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div id="slider" class="flexslider mb-3">
                        <ul class="slides">
                            <?php foreach ($lot_lelang as $key => $value) { ?>
                                <li><img src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$value->gambar ?>" class="card-img-top img-cover" alt="<?= @$val->nama_barang_lelang ?>"></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div id="carousel" class="flexslider mb-3">
                        <ul class="slides">
                            <?php foreach ($lot_lelang as $key => $value) { ?>
                                <li><img src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$value->gambar ?>" class="card-img-top img-cover" alt="<?= @$val->nama_barang_lelang ?>"></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <div class="col-12 col-lg-6 px-5 text-white" style="padding-top: 7%;">
                    <h1 class="text-uppercase pb-1">
                        <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) {
                            echo @$lot_lelang[0]->nama_barang_lelang . ' ' . @$lot_lelang[0]->transmisi;
                        } else {
                            echo @$lot_lelang[0]->nama_barang_lelang;
                        } ?>
                    </h1>
                    <h2><?= @$lot_lelang[0]->tahun ?></h2>
                    <h2><?= @$lot_lelang[0]->no_polisi ?></h2>
                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) {
                        echo '<h2 class="mr-3 fontlights float-left"><i class="fas fa-tachometer-alt" style="color: #ff4500"></i>' . @$lot_lelang[0]->odometer . '</h2>';
                    } else {
                        echo '<h2 class="text-uppercase pb-1">' . @$lot_lelang[0]->odometer . '</h2>';
                    } ?>
                    <h2 class="mx-3 fontlights"><i class="fas fa-map-marker-alt" style="color: #ff4500"></i><?= @$lot_lelang[0]->lokasi_barang ?></h2>
                    <h2 class="mr-3 fontlights"><i class="fas fa-calendar" style="color: #ff4500"></i><?= @$lot_lelang[0]->waktu_event ?></h2>
                </div>
            </div>
        </div>
    </header>
    <section class="portfolio" id="about" style="padding: 3rem 0;">
        <div class="container mh-100 mw-90">
            <div class="row">
                <div class="col-12 col-lg-6 pb-4">
                    <div class="card-body p-2 border" style="background-color: #00000073; border-radius: 10px;">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <ul class="nav" id="custom-tabs-four-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content_tab_1" data-toggle="pill" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true" title="Details Kendaraan">
                                        <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) {
                                            echo "Details Kendaraan";
                                        } else {
                                            echo "Detail Barang";
                                        } ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="content_tab_2" data-toggle="pill" href="#tab_2" role="tab" aria-controls="tab_2" aria-selected="false" title="Details Kondisi">Details Kondisi</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="portfolio pt-0" id="about" style="padding: 3rem 0;">
        <div class="container mh-100 mw-90">
            <div class="row">
                <div class="col-12 col-lg-12 pb-4">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade active show" id="tab_1" role="tabpanel" aria-labelledby="content_tab_1">
                            <div class="card flex-fill border" style="background-color: #00000073; border-radius: 10px;">
                                <div class="card-body text-white">
                                    <div class="row">
                                        <div class="col-md-6" style="border-right: 1px solid white">
                                            <table class="table table-borderless text-white">
                                                <tbody>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->no_polisi)) { ?>
                                                                <th>Nomor Polisi</th>
                                                                <td><?= @$lot_lelang[0]->no_polisi ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <?php if (!empty(@$lot_lelang[0]->brand)) { ?>
                                                            <th>Brand</th>
                                                            <td><?= @$lot_lelang[0]->brand ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <?php if (!empty(@$lot_lelang[0]->nama_barang_lelang)) { ?>
                                                            <th>Seri</th>
                                                            <td><?= @$lot_lelang[0]->nama_barang_lelang ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <!-- <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->kapasitas_kendaraan)) { ?>
                                                                <th>Kapasitas Kendaraan</th>
                                                                <td><?= @$lot_lelang[0]->kapasitas_kendaraan ?></td>
                                                            <?php } ?>
                                                        </tr> -->
                                                    <?php } ?>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->tipe_model)) { ?>
                                                                <th>Model Kendaraan</th>
                                                                <td><?= @$lot_lelang[0]->tipe_model ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->transmisi)) { ?>
                                                                <th>Transmisi</th>
                                                                <td><?= @$lot_lelang[0]->transmisi ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <?php if (!empty(@$lot_lelang[0]->tahun)) { ?>
                                                            <th>Tahun</th>
                                                            <td><?= @$lot_lelang[0]->tahun ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table table-borderless text-white">
                                                <tbody>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->no_rangka)) { ?>
                                                                <th>Nomor Rangka</th>
                                                                <td><?= @$lot_lelang[0]->no_rangka ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->no_mesin)) { ?>
                                                                <th>Nomor Mesin</th>
                                                                <td><?= @$lot_lelang[0]->no_mesin ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->odometer)) { ?>
                                                                <th>Kilometer</th>
                                                                <td><?= @$lot_lelang[0]->odometer ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->bahan_bakar)) { ?>
                                                                <th>Bahan Bakar</th>
                                                                <td><?= @$lot_lelang[0]->bahan_bakar ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->warna)) { ?>
                                                                <th>Warna Fisik</th>
                                                                <td><?= @$lot_lelang[0]->warna ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                            <?php if ((@$lot_lelang[0]->stnk == "ADA")) { ?>
                                                                <tr>
                                                                    <?php if (!empty(@$lot_lelang[0]->valid_stnk)) { ?>
                                                                        <th>Tanggal STNK</th>
                                                                        <td><?= @$lot_lelang[0]->valid_stnk ?></td>
                                                                    <?php } ?>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_2" role="tabpane2" aria-labelledby="content_tab_2">
                            <div class="card flex-fill border" style="background-color: #00000073; border-radius: 10px;">
                                <div class="card-body text-white">
                                    <div class="row">
                                        <div class="col-md-6" style="border-right: 1px solid white">
                                            <table class="table table-borderless text-white">
                                                <tbody>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <th>STNK</th>
                                                            <td><?= $foto_stnk = ((@$lot_lelang[0]->stnk == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>BPKB</th>
                                                            <td><?= $foto_bpkb = ((@$lot_lelang[0]->bpkb == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>FAKTUR</th>
                                                            <td><?= $foto_faktur = ((@$lot_lelang[0]->faktur == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>KTP PEMILIK</th>
                                                            <td><?= $foto_ktp_pemilik = ((@$lot_lelang[0]->ktp_pemilik == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>KWITANSI</th>
                                                            <td><?= $foto_kwitansi = ((@$lot_lelang[0]->kwitansi == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>SPH</th>
                                                            <td><?= $foto_ktp_pemilik = ((@$lot_lelang[0]->sph == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>KIR</th>
                                                            <td><?= $foto_kwitansi = ((@$lot_lelang[0]->kir == "ADA")) ? '<span class="badge badge-success">ADA</span>' : '<span class="badge badge-danger">TIDAK ADA</span>'; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table table-borderless text-white">
                                                <tbody>
                                                    <tr>
                                                        <?php if (!empty(@$lot_lelang[0]->grade)) { ?>
                                                            <th>Grade Utama</th>
                                                            <td><?= @$lot_lelang[0]->grade ?></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php if ((@$lot_lelang[0]->id_kategori == 1) or (@$lot_lelang[0]->id_kategori == 2)) { ?>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->grade_mesin)) { ?>
                                                                <th>Grade Mesin</th>
                                                                <td><?= @$lot_lelang[0]->grade_mesin ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->grade_interior)) { ?>
                                                                <th>Grade Interior</th>
                                                                <td><?= @$lot_lelang[0]->grade_interior ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php if (!empty(@$lot_lelang[0]->grade_exterior)) { ?>
                                                                <th>Grade Exterior</th>
                                                                <td><?= @$lot_lelang[0]->grade_exterior ?></td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="portfolio pt-0" id="about" style="padding: 3rem 0;">
        <div class="container mh-100 mw-90">
            <div class="row">
                <div class="col-12 col-lg-12 pb-4">
                    <div class="card flex-fill border" style="background-color: #00000073; border-radius: 10px;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 text-white">
                                    <h2 class="font-weight-bold fontbold text-uppercase">Deskripsi</h2>
                                    <p><?= @$lot_lelang[0]->deskripsi ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="row pb-5">
        <div class="container">
            <div class="mw-35 mx-auto text-center pb-3">
                <a href="<?= base_url(); ?>/lot"><span class="btn btn-primary btn-xl" onclick="checkAuth();" style="">Back</span></a>
            </div>
        </div>
    </div>

    <!-- ======= Lup Lup ======= -->

    <?php include('part/footer.php') ?>

    <script>
        $(function() {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 150,
                itemMargin: 5,
                asNavFor: '#slider'
            });
            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel"
            });
        });
    </script>

</body>

</html>
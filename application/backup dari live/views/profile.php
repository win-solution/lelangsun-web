<!DOCTYPE html>
<?php // var_dump($npl_detail); 
?>
<html lang="en">
<?php include('part/head.php') ?>
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<script src="<?= base_url('assets/'); ?>plugins/moment/moment.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js"></script>

<!-- Image Upload -->
<link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/image-uploader-master/src/image-uploader.css">
<script src="<?= base_url('assets/'); ?>plugins/image-uploader-master/src/image-uploader.js"></script>
<!-- Form Validation -->
<script src="<?= base_url('assets/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/jquery-validation/additional-methods.js"></script>

<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<script src="<?= base_url() . 'assets/'; ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() . 'assets/'; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() . 'assets/'; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url() . 'assets/'; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<!-- <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/buttons.flash.min.js"></script> -->
<script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>

<body id="page-top">

    <style type="text/css">
        .profile {
            text-align: center;
        }

        .profile .img-profile {
            margin: 0 auto;
            -webkit-transform: translate3d(0, -50%, 0);
            -moz-transform: translate3d(0, -50%, 0);
            -o-transform: translate3d(0, -50%, 0);
            -ms-transform: translate3d(0, -50%, 0);
            transform: translate3d(0, -50%, 0);
        }

        .img-raised {
            box-shadow: 0 5px 15px -8px rgba(0, 0, 0, .24), 0 8px 10px -5px rgba(0, 0, 0, .2);
        }

        .rounded-circle {
            border-radius: 50% !important;
        }

        .img-fluid,
        .img-thumbnail {
            max-width: 100%;
            height: auto;
        }

        .img-raised {
            box-shadow: 0 5px 15px -8px rgba(0, 0, 0, .24), 0 8px 10px -5px rgba(0, 0, 0, .2);
        }

        .rounded-circle {
            border-radius: 50% !important;
        }

        .img-fluid,
        .img-thumbnail {
            max-width: 100%;
            height: auto;
        }

        .avatar-profile {
            vertical-align: middle;
            width: 13vw;
            height: 20vh;
            border-radius: 50%;
        }

        .picker-switch a {
            color: red;
        }
    </style>

    <?php include('part/nav.php') ?>

    <header class="masthead">
        <div class="container mw-100 mh-100" style=" background-image: url('<?= base_url('assets/') ?>images/baru/bg-01.png'); height: 40vh; background-position: center; background-size: cover;">
        </div>
    </header>

    <div class="profile-content" style="background-image: url('<?= base_url('assets/') ?>images/y/web-06.png'); background-size: cover; background-position: top;">
        <!-- <div class="container"> -->
        <div class="row">
            <div class="col-md-10 ml-auto mr-auto">
                <div class="profile">
                    <div class="avatar-profile mx-auto">
                        <img src="<?= base_url('assets/') ?>images/profile_picture.png" alt="Circle Image" class="img-raised rounded-circle img-fluid img-profile">
                    </div>
                    <div class="name text-white">
                        <h2 class="fontbold"><?= @$peserta[0]->nama; ?></h2>
                        <p class="mb-3" style="font-size: 20px;"><?= @$peserta[0]->email; ?></p>
                    </div>
                </div>
                <div class="col-12 mb-5">
                    <div class="card card-primary card-outline card-outline-tabs">
                        <div class="card-header p-0 border-bottom-0">
                            <ul class="nav nav-tabs nav-pills nav-fill" id="custom-tabs-four-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link text-dark active" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark" id="custom-tabs-four-npl-tab" data-toggle="pill" href="#custom-tabs-four-npl" role="tab" aria-controls="custom-tabs-four-npl" aria-selected="false">NPL</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark" id="custom-tabs-four-pelunasan-tab" data-toggle="pill" href="#custom-tabs-four-pelunasan" role="tab" aria-controls="custom-tabs-four-pelunasan" aria-selected="false">Pelunasan Barang Lelang</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark" id="custom-tabs-four-notifikasi-tab" data-toggle="pill" href="#custom-tabs-four-notifikasi" role="tab" aria-controls="custom-tabs-four-notifikasi" aria-selected="false">Notifikasi</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-four-tabContent">
                                <div class="tab-pane fade active show" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                                    <form id="formProfile" name="formProfile" action="" method="post" class="text-dark" enctype="multipart/form-data">
                                        <input type="hidden" name="submit" value="submit">
                                        <input type="hidden" name="id_peserta" value="<?= $_SESSION['id_peserta'] ?>">
                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group has-feedback">
                                                    <label for="exampleInputNama" class="form-label">Nama</label>
                                                    <input type="text" name="nama" id="nama" class="form-control input-borderless" placeholder="Nama" required="" value="<?= @$peserta[0]->nama; ?>">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label for="exampleInputEmail1" class="form-label">Email</label>
                                                    <input type="text" name="email" id="email" class="form-control input-borderless" placeholder="Email" required="" value="<?= @$peserta[0]->email; ?>">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label for="password" class="form-label">Password Baru</label>
                                                    <input type="text" name="password" id="password" class="form-control input-borderless" placeholder="Password Baru">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                    <div class="alert alert-warning" role="alert">
                                                        Juka ingin merubah password silahkan isi bagian ini
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group has-feedback">
                                                    <label for="exampleInputNama" class="form-label">No HP</label>
                                                    <input type="text" name="no_hp" id="no_hp" class="form-control input-borderless numeric" placeholder="No HP" required="" value="<?= @$peserta[0]->no_hp; ?>" onkeypress="return onlyNumberKey(event)">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label for="exampleInputEmail1" class="form-label">Alamat</label>
                                                    <textarea name="alamat" id="alamat" class="form-control input-borderless" placeholder="Alamat" cols="30" rows="5"><?= @$peserta[0]->alamat; ?></textarea>
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group has-feedback">
                                                    <label for="exampleInputNoRek" class="form-label">No Rekening</label>
                                                    <input type="text" name="no_rek" id="no_rek" class="form-control input-borderless" placeholder="No Rekening" required="" value="<?= @$peserta[0]->no_rek; ?>">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label for="exampleInputNamaRek" class="form-label">Nama Pemilik Rekening</label>
                                                    <input type="text" name="nama_rek" id="nama_rek" class="form-control input-borderless" placeholder="Nama Pemilik Rekening" required="" value="<?= @$peserta[0]->nama_rek; ?>">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group has-feedback">
                                                    <label for="exampleInputEmail1" class="form-label">NIK</label>
                                                    <input type="text" name="nik" id="nik" class="form-control input-borderless numeric" placeholder="NIK" required="" value="<?= @$peserta[0]->nik; ?>" onkeypress="return onlyNumberKey(event)">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Upload Foto KTP</label><br>
                                                    <?php if (!empty(@$peserta[0]->foto_ktp)) { ?>
                                                        <img id="prevKTP" class="img-thumbnail" style="max-width: 20vw;" src="<?= base_url('assets/uploads/ktp_peserta/origin/') . @$peserta[0]->foto_ktp ?>" alt="" srcset="">
                                                    <?php } else { ?>
                                                        <img class="img-thumbnail" style="max-width: 20vw;" id="prevKTP" src="" alt="" srcset="">
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <!-- <label>Upload Foto KTP</label> -->
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="foto_ktp" name="foto_ktp" accept="image/jpg, image/jpeg, image/png" onchange="getNameFile(this,'labelfoto_ktp'); readURL(this,'#prevKTP','#foto_ktp')" readonly>
                                                        <label id="labelfoto_ktp" class="custom-file-label" for="foto_ktp">Pilih Foto KTP</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <!-- <div class="form-group has-feedback">
                                                    <label for="exampleInputEmail1" class="form-label">NPWP</label>
                                                    <input type="text" name="npwp" id="npwp" class="form-control input-borderless numeric" placeholder="NPWP" required="" value="<?= @$peserta[0]->npwp; ?>" onkeypress="return onlyNumberKey(event)">
                                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                </div>
                                                <div class="form-group">
                                                    <label>Upload Foto NPWP</label><br>
                                                    <?php if (!empty(@$peserta[0]->foto_npwp)) { ?>
                                                        <img id="prevNPWP" class="img-thumbnail" style="max-width: 20vw;" src="<?= base_url('assets/uploads/npwp_peserta/origin/') . @$peserta[0]->foto_npwp ?>" alt="" srcset="">
                                                    <?php } else { ?>
                                                        <img class="img-thumbnail" style="max-width: 20vw;" id="prevNPWP" src="" alt="" srcset="">
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="foto_npwp" name="foto_npwp" accept="image/jpg, image/jpeg, image/png" onchange="getNameFile(this,'labelfoto_npwp'); readURL(this,'#prevNPWP','#foto_npwp')" readonly>
                                                        <label id="labelfoto_npwp" class="custom-file-label" for="foto_npwp">Pilih Foto KTP</label>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <div class="col-12">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="alert alert-warning col-md-6" role="alert">
                                                            Cara untuk mengecilkan ukuran foto dalam HP Android, <br>ikuti langkah-langkah berikut. <br>
                                                            <ol>
                                                                <li>Ketuk <b>Kamera</b></li>
                                                                <li>Buka <b>Menu</b></li>
                                                                <li>Ketuk <b>Pengaturan</b></li>
                                                                <li>Ketuk <b>Kualitas Gambar</b></li>
                                                                <li>Tetapkan Pengaturan <b>Rendah</b></li>
                                                            </ol>
                                                        </div>
                                                        <div class="alert alert-warning col-md-6" role="alert">
                                                            Cara untuk mengecilkan ukuran foto dalam IOS, <br>ikuti langkah-langkah berikut. <br>
                                                            <ol>
                                                                <li>Buka <b>Pengaturan</b> di iPhone Anda</li>
                                                                <li>Swipe ke bawah hingga menemukan opsi <b>Kamera</b></li>
                                                                <li>Ketuk <b>Format</b></li>
                                                                <li>Tetapkan Pengaturan <b>Efisiensi Tinggi</b></li>
                                                            </ol>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="mb-3 form-check pb-4 text-center">
                                                    <a href="forgot.html" class="">Lupa Kata Sandi?</a><span> / </span><a href="forgot.html" class="">Registrasi</a>
                                                    </div> -->
                                        <!-- <h5 class="pull-right">Lupa Password?</h5> -->
                                        <div class="row pt-5">
                                            <div class="container">
                                                <div class="col-12 pull-right d-flex justify-content-center pb-2">
                                                    <!-- <span id="submit_btn" class="btn btn-primary btn-block btn-flat bg-red" onclick="save();">Daftar</span> -->
                                                    <input type="submit" value="Simpan" id="submit_btn" class="btn btn-primary btn-block btn-flat bg-red">
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="tab-pane fade overflow-auto" id="custom-tabs-four-npl" role="tabpanel" aria-labelledby="custom-tabs-four-npl-tab">
                                    <div class="mw-35 mx-auto text-center pt-3">
                                        <span class="btn btn-primary btn-xl" onclick="beli_npt();" style="background-color: #ff6e07; border-color: #ff6e07;"><i class="fas fa-plus-square mr-3"></i>Beli NPL</span>
                                    </div>
                                    <table id="tableNPL" class="table table-bordered table-light table-striped tableList mt-3 mb-5">
                                        <thead style="background-color: #31869b;">
                                            <tr class="text-white">
                                                <th>No</th>
                                                <th>Event</th>
                                                <th>Kode NPL</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody style="overflow-wrap: anywhere;">
                                            <?php $i = 0;
                                            foreach ($npl_detail as $key => $val) {
                                                // var_dump($val);
                                                $i++;
                                                $status_aksi = "";
                                                switch ($val->verifikasi) {
                                                    case '1':
                                                        $status_verifikasi = '<span class="badge badge-success btn" onclick="Swal.fire(`Aktif`,`NPL telah aktif`,`info`)">Aktif</span>';
                                                        break;
                                                    case '2':
                                                        $status_verifikasi = '<span class="badge badge-danger btn" onclick="Swal.fire(`Ditolak`,`' . $val->pesan_verifikasi . '`,`info`)">Ditolak</span>';
                                                        break;

                                                    default:
                                                        $status_verifikasi = '<span class="badge badge-warning btn" onclick="Swal.fire(`Verifikasi`,`Sedang proses verifikasi oleh admin SUN`,`info`)">Verifikasi</span>';
                                                        break;
                                                }
                                                switch ($val->status_npl) {
                                                    case '0':
                                                        $status_aksi = '<span class="btn btn-success">Terpakai</span>';
                                                        break;

                                                    default:
                                                        if (!empty($val->status_refund)) {
                                                            if ($val->status_refund == 1) {
                                                                $status_aksi = '<span class="btn btn-warning">Proses Refund</span>';
                                                            } else {
                                                                $status_aksi = '<span class="btn btn-success">Berhasil Refund</span>';
                                                            }
                                                        } else {
                                                            $status_aksi = ((@$val->waktu_event <= date("Y-m-d h:i")) && ($val->verifikasi == 1)) ? '<span class="btn btn-primary" onclick="refund(`' . $val->id_npl . '`);">Refund</span>' : '-';
                                                        }

                                                        break;
                                                }
                                            ?>
                                                <tr>
                                                    <td data-id="<?= $val->id_npl ?>"><?= $i ?></td>
                                                    <td><?= @$val->nama_event . ' (' . @$val->waktu_event . ')'; ?></td>
                                                    <!-- <td><?= (empty($val->no_npl)) ? sprintf("%'.05d\n", @$val->no_npl) : @$val->no_npl; ?></td> -->
                                                    <td><?= (empty($val->no_npl)) ? "-" : @$val->no_npl; ?></td>
                                                    <td><?= $status_verifikasi; ?></td>
                                                    <td><?= $status_aksi; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade overflow-auto" id="custom-tabs-four-pelunasan" role="tabpanel" aria-labelledby="custom-tabs-four-pelunasan-tab">
                                    <table id="tableBarang" class="table table-bordered table-light table-striped tableList mt-3 mb-5">
                                        <thead style="background-color: #31869b;">
                                            <tr class="text-white">
                                                <th>No</th>
                                                <th>Nama Barang Lelang</th>
                                                <th>Harga</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 0;
                                            foreach ($pemenang_detail as $key => $val) {
                                                $i++; ?>
                                                <tr>
                                                    <td><?= $i ?></td>
                                                    <td><?= $val->nama_barang_lelang . " (" . $val->brand . ")" ?></td>
                                                    <td><?= "Rp " . number_format(((!empty($val->harga_bidding)) ? $val->harga_bidding : 0), 0, ',', '.') ?></td>
                                                    <td class="d-flex justify-content-center align-content-center">
                                                        <?php if ((empty($val->no_rek)) && (empty($val->nominal)) && (empty($val->bukti))) { ?>
                                                            <span class="btn btn-primary btn-xl" onclick="pelunasan_lelang(<?= $val->id_pemenang_lelang ?>);" style="background-color: #ff6e07; border-color: #ff6e07;"><i class="fas fa-hand-holding-usd mr-3"></i>Pelunasan Lelang</span>
                                                        <?php } else { ?>
                                                            <span class="btn btn-success btn-xl"><i class="fas fa-hourglass-half mr-3"></i>Proses Verifikasi</span>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade overflow-auto" id="custom-tabs-four-notifikasi" role="tabpanel" aria-labelledby="custom-tabs-four-notifikasi-tab">
                                    <table id="tableBarang" class="table table-bordered table-light table-striped tableList mt-3 mb-5">
                                        <thead style="background-color: #31869b;">
                                            <tr class="text-white">
                                                <th>No</th>
                                                <th>Judul Pesan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 0;
                                            foreach ($notifikasi_detail as $key => $val) {
                                                $i++; ?>
                                                <tr>
                                                    <td><?= $i ?></td>
                                                    <td><?= $val->judul ?></td>
                                                    <td class="d-flex justify-content-center align-content-center">
                                                        <span class="btn btn-success btn-xl" onclick="Swal.fire('<?= $val->judul ?>','<?= $val->pesan ?>','info')"><i class="fas fa-eye mr-3"></i> Lihat</span>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-admin mw-75 mx-auto">

            <!-- Modal content-->
            <div class="modal-content" style="border-radius: 15px;">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_header"></h4>
                    <button type="button" class="close btn btn-danger" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="modal_body">
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                </div>
            </div>

        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#formProfile").validate({
                rules: {
                    nama: {
                        required: true,
                    },
                    alamat: {
                        required: true,
                    },
                    no_rek: {
                        required: true,
                    },
                    nama_rek: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    no_hp: {
                        required: true,
                        digits: true,
                    },
                    nik: {
                        required: true,
                        digits: true,
                    },
                    // npwp: {
                    //     required: true,
                    //     digits: true,
                    // },
                    foto_ktp: {
                        // required:true,
                        extension: "jpg,jpeg,png",
                        maxsize: 2200000,
                    },
                    // foto_npwp: {
                    //     // required:true,
                    //     extension: "jpg,jpeg,png",
                    //     maxsize: 2200000,
                    // },
                },
                messages: {
                    foto_ktp: {
                        // required: "Foto KTP diperlukan",
                        extension: "Format file yang diijinkan .jpg, .jpeg, .png",
                        maxsize: "File gambar tidak boleh lebih dari 2 MB"
                    },
                    // foto_npwp: {
                    //     // required: "Foto KTP diperlukan",
                    //     extension: "Format file yang diijinkan .jpg, .jpeg, .png",
                    //     maxsize: "File gambar tidak boleh lebih dari 2 MB"
                    // },
                }
            });

            $('#datetimepicker1').datetimepicker({
                debug: true,
                format: 'YYYY-MM-DD HH:mm',
                defaultDate: "<?= date('Y-m-d H:i'); ?>",
                maxDate: moment("<?= date('Y-m-d H:i'); ?>", 'YYYY-MM-DD HH:mm'),
                buttons: {
                    showClose: true
                },
                ignoreReadonly: true,
            });
            $('.tableList').DataTable({
                "dom": 'frtip'
            });
            // $('#tableNPL').DataTable();
            // $('#tableBarang').DataTable();
        });

        function refund(id) {
            Swal.fire({
                title: 'Apakah anda ingin melakukan refund?',
                showCancelButton: true,
                confirmButtonText: 'Iya',
                cancelButtonText: 'Tidak',
                icon: 'question'
            }).then((result) => {
                // Ajax set refund npl
                if (result.isConfirmed) {
                    return new Promise((resolve, reject) => {
                        refundAjax(id).then((resolve) => {
                            if (resolve) {
                                Swal.fire('Pengajuan Refund Berhasil!', '', 'success');
                            } else {
                                Swal.fire('Gagal memproses data!', '', 'error');
                            }
                            setTimeout(() => {
                                location.reload();
                            }, 5 * 1000);
                        }).catch((err) => {
                            Swal.fire('Gagal memproses data!', '', 'error');
                        });
                    });
                }
            })
        }

        function refundAjax(id_npl) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: "<?php echo base_url('front/rest_api/refund/') ?>",
                    type: "POST",
                    data: {
                        id_npl: id_npl,
                    },
                    dataType: "JSON",
                    async: false,
                    success: function(data) {
                        resolve(data.status);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        reject();
                    }
                });
            })
        }


        function beli_npt() {
            action = "<?= base_url('front/beli_npl') ?>";
            action_label = "Tambah";
            $.ajax({
                url: "<?= base_url('front/beli_npl') ?>",
                type: "GET",
                dataType: "JSON",
                async: false,
                success: function(data) {
                    $('#myModal #modal_header').html("");
                    $('#myModal #modal_body').html("");

                    $('#myModal #modal_header').append(data.proses);
                    $('#myModal #modal_body').append(data.content);
                    $('#myModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    newSWAL("Gagal memperoleh data dari ajax!", "error");
                }
            });
        }

        function pelunasan_lelang($id) {
            action = "<?= base_url('front/pelunasan_lelang') ?>";
            action_label = "Tambah";
            $.ajax({
                url: "<?= base_url('front/pelunasan_lelang') ?>",
                type: "GET",
                dataType: "JSON",
                data: {
                    id_pemenang_lelang: $id,
                },
                async: false,
                success: function(data) {
                    $('#myModal #modal_header').html("");
                    $('#myModal #modal_body').html("");

                    $('#myModal #modal_header').append(data.proses);
                    $('#myModal #modal_body').append(data.content);
                    $('#myModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    newSWAL("Gagal memperoleh data dari ajax!", "error");
                }
            });
        }

        <?php if (!empty($_SESSION['msg_flash'])) { ?>
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: '<?= @$_SESSION['msg_flash'] ?>',
            });
        <?php }
        $this->session->unset_userdata('msg_flash'); ?>
    </script>
    <?php include('part/footer.php') ?>
</body>

</html>
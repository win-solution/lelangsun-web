<!DOCTYPE html>
<html lang="en">
<?php include('part/head.php') ?>

<style type="text/css">
    /* If the screen size is 601px wide or more, set the font-size of <div> to 80px */
    @media screen and (min-width: 601px) {
        .events {
            background-image: url('<?= base_url('assets') ?>/images/revisi/etios-valco_1224.jpg');
            height: 70vh;
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
    }

    /* If the screen size is 600px wide or less, set the font-size of <div> to 30px */
    @media screen and (max-width: 600px) {
        .events {
            background-image: url('<?= base_url('assets') ?>/images/revisi/etios-valco_1224.jpg');
            height: 30vh;
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
    }

    @media screen and (max-width: 600px) {
        .img-cover {
            height: 30vh;
        }
    }

    /* If the screen size is 601px wide or more, set the font-size of <div> to 80px */
    @media screen and (min-width: 601px) {
        .text-desk {
            font-size: 25px;
        }
    }

    .flex-caption {
        width: 96%;
        padding: 2%;
        left: 0;
        bottom: 0;
        background: rgba(0, 0, 0, .5);
        color: #fff;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .3);
        font-size: 14px;
        line-height: 18px;
    }

    li.css a {
        border-radius: 0;
    }

    #carousel .slides li {
        cursor: pointer;
    }

    #carousel .slides li.flex-active-slide {
        cursor: default;
    }
</style>

<body id="page-top" style="background-image: url('<?= base_url('assets') ?>/images/dark2/dark2-06.png'); background-position: center;">
    <?php include('part/nav.php') ?>
    <header class="details">
        <!-- <div class="container mw-100 mh-100 events">                
            </div> -->
    </header>

    <section>
        <div class="container mh-100 mw-90">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div id="slider" class="flexslider mb-3">
                        <ul class="slides">
                            <?php foreach ($lot_lelang as $key => $value) { ?>
                                <li><img src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$value->gambar ?>" class="card-img-top img-cover" alt="<?= @$val->nama_barang_lelang ?>"></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div id="carousel" class="flexslider mb-3">
                        <ul class="slides">
                            <?php foreach ($lot_lelang as $key => $value) { ?>
                                <li><img src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$value->gambar ?>" class="card-img-top img-cover" alt="<?= @$val->nama_barang_lelang ?>"></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div id="detail_lot" class="flexslider mb-3" style="background: none;">
                        <ul class="slides">
                            <?php foreach ($lot_lelang as $key => $value) { ?>
                                <li>
                                    <div class="col-12 col-lg-6 px-5 text-white" style="padding-top: 7%;">
                                        <h1 class="text-uppercase pb-1 "><?= @$value->nama_barang_lelang . ' ' . @$value->transmisi ?></h1>
                                        <h2><?= @$value->tahun ?></h2>
                                        <h2><?= @$value->no_polisi ?></h2>
                                        <h2 class="mr-3 fontlights"><i class="fas fa-tachometer-alt" style="color: #ff4500"></i> <?= @$value->odometer ?></h2>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row d-flex align-content-center justify-content-center">
                <div class="col-12 col-lg-6">
                    <div id="slider" class="flexslider mb-3">
                        <ul class="slides">
                            <?php foreach ($lot_lelang as $key => $value) { ?>
                                <li><img src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$value->gambar ?>" class="card-img-top img-cover" alt="<?= @$val->nama_barang_lelang ?>"></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div id="carousel" class="flexslider mb-3">
                        <ul class="slides">
                            <?php foreach ($lot_lelang as $key => $value) { ?>
                                <li><img src="<?= base_url('assets/uploads/gambar_barang_lelang/origin/') . @$value->gambar ?>" class="card-img-top img-cover" alt="<?= @$val->nama_barang_lelang ?>"></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div> -->
        <div class="container mh-100 mw-90 pt-5 pb-5">
            <div class="fontlight text-white">
                <h1 class="text-uppercase pb-1 fontbold"><?= @$lot_lelang[0]->nama_event  ?></h1>
                <div class="card flex-fill border mb-3" style="background-color: #00000073; border-radius: 10px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 text-white">
                                    <h2 class="font-weight-bold fontbold text-uppercase">Deskripsi</h2>
                                    <p class="text-white"><?= @$lot_lelang[0]->deskripsi_event ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span style="font-size: 30px;" class="mr-3"><i class="fas fa-calendar mr-5" style="color: #ff4500"></i><?= @$lot_lelang[0]->waktu_event ?></span>
                <br>
                <br>
                <!-- <a href="'.@$val->link_maps.'" target="_blank"><span style=" color: #333" class="mr-3"><i class="fas fa-tachometer-alt" style="color: #ff4500"></i><?= @$lot_lelang[0]->alamat ?></span></a> -->
                <span style="font-size: 30px;" class=""><i class="fas fa-map-marker-alt mr-5" style="color: #ff4500"></i><a href="'.@$val->link_maps.'"><?= @$lot_lelang[0]->alamat_event ?></a></span>
            </div>
        </div>
    </section>

    <!-- ======= Lup Lup ======= -->

    <?php include('part/footer.php') ?>


    <script>
        $(function() {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 150,
                itemMargin: 5,
                asNavFor: "#slider",
            });
            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel",
                sync: "#detail_lot",
            });
            $('#detail_lot').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                // sync: "#slider"
            });
        });
    </script>
</body>

</html>
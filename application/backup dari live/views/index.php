<!DOCTYPE html>
<html lang="en">
<?php include('part/head.php') ?>
<style type="text/css">
    .img-cover {
        height: 25vh;
        object-fit: cover;
    }

    #more {
        display: none;
    }

    .circle-icon {
        vertical-align: middle;
        width: 50%;
        height: 42%;
        border-radius: 50%;
        border: 1px solid white;
    }

    /* If the screen size is 601px wide or more, set the font-size of <div> to 80px */
    @media screen and (min-width: 601px) {
        .beranda {
            background-image: url('assets/images/dark2/web-01.png');
            height: 149vh;
            background-position: left;
            background-size: cover;
            background-repeat: no-repeat;
        }

        .ellipsis-testimoni {
            overflow: hidden;
            display: -webkit-box;
            -webkit-line-clamp: 5;
            -webkit-box-orient: vertical;
            height: 150px;
        }
    }

    /* If the screen size is 600px wide or less, set the font-size of <div> to 30px */
    @media screen and (max-width: 600px) {
        .beranda {
            background-image: url('assets/images/baru/Mobile-01 (1).png');
            height: 52vh;
            background-position: left;
            background-size: cover;
            background-repeat: no-repeat;
        }

        .btn-lelang {
            padding-top: 22vh;
            text-align: center !important;
            margin-left: 5px;
        }

        .btn-flex {
            display: flex;
            justify-content: center;
            align-content: center;
            padding-left: 0px !important;
            padding-bottom: 0px !important;
        }

        .hidden-mobile {
            display: none;
        }

        .ellipsis-testimoni {
            overflow: hidden;
            display: -webkit-box;
            -webkit-line-clamp: 5;
            -webkit-box-orient: vertical;
            height: 260px;
        }

        span {
            font-size: 16px !important;
        }
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<?php if (!empty($_SESSION['msg_flash'])) { ?>
    <script>
        $(document).ready(function() {
            Swal.fire({
                icon: 'success',
                title: 'Selamat',
                text: '<?= @$_SESSION['msg_flash'] ?>',
                showConfirmButton: false,
                timer: 5500
            });
        });
    </script>
<?php }
$this->session->unset_userdata('msg_flash'); ?>

<body id="page-top" style="background-image: url('assets/images/dark-01.png '); background-position: center; background-size: cover; height: 26vh;">
    <?php include('part/nav.php') ?>

    <!-- Masthead-->
    <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->

    <header class="masthead">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php foreach ($banner as $key => $val) { ?>
                    <div class="carousel-item active" style="background-image: url('assets/uploads/banner/origin/<?= $val->gambar; ?>'); background-position: center; background-size: cover; height: 40vh;">
                        <div class="row h-100 mw-75 mx-auto">
                            <div class="col-12 col-md-6 my-auto text-white">
                                <h1><?= $val->judul; ?></h1>
                                <span><?= $val->deskripsi; ?></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>

    <section class="page-section beranda">
        <div class="row h-100 mx-auto">
            <div class="col-md-6 mw-90 ml-3 my-auto" style="height: 112vh;">
                <div class="hidden-mobile mw-75">
                    <span class="fontlights"><span>SUN BALAI LELANG !</span></span>
                    <hr style="background-color: red; margin-right: 70%;">
                    <h2>SUN BALAI LELANG<br> MOBIL TERPERCAYA</h2>
                    <br>
                    <span class="subheading fontlight">PT Balai Lelang SUN menyediakan opsi metode lelang dengan berbagai keunggulan masing-masing Onsite Auction, Live Auction, dan Online Auction. Yang dapat mendukung kebutuhan anda dalam mengikuti Event Lelang.</span>
                </div>
                <div class="btn-lelang">
                    <a href="<?= base_url(); ?>lelang"><span class="btn btn-xl btn btn-primary btn-xl mt-2 text-black">Ikuti Lelang</span></a>
                    <?php if (!empty($_SESSION['login_peserta'])) { ?>
                        <a href="<?= base_url(); ?>profile"><span class="btn btn-xl btn btn-primary btn-xl mt-2 text-black">Beli NPL</span></a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-5 hidden-mobile text-center text-white">
                <h3 class="text-uppercase fontlights">Etios Valco 2018</h3>
                <span class="subheading fontlights">Harga awal <b>200</b> jutaan <br> dilelang <br> jadi <b>150</b> jutaan</span>
            </div>
        </div>
    </section>

    <section class="page-section portfolio pb-5">
        <div class="container mw-90">
            <div class="text-center mb-4">
                <h1 class="text-uppercase fontbold text-white d-inline-block">Cara ikut lelang di</h1>
            </div>
            <span class="subheading fontlight text-white">PT Balai Lelang SUN menyediakan opsi metode lelang dengan berbagai keunggulan yang dapat mendukung kebutuhan anda dalam mengikuti Event Lelang.</span>
            <h3 class="text-uppercase fontbold text-white mt-4 mb-4 text-left">Video Tata Cara Lelang</h3>
            <div class="embed-responsive embed-responsive-21by9 mx-auto border" style="border-radius: 25px;">
                <iframe src="https://www.youtube.com/embed/DINwhKAs51w?start=10" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </section>

    <section class="page-section portfolio pt-0">
        <div class="container text-center mw-75">
            <div class="row">
                <div class="col-md-4 p-3 d-flex">
                    <div class="card" style="border: 1px solid white; background-color: #000000c7;">
                        <a href="#" style="color: inherit; text-decoration: none;">
                            <img src="assets/images/profile_picture.png" alt="Onsite Auction" class="card-img-top p-3 mw-50">
                            <div class="card-body text-white">
                                <h2 class="card-title">Onsite Auction</h2>
                                <span align="justify">Peserta dapat langsung melakukan penawaran atau bidding di lokasi balai lelang SUN sesuai dengan jadwal yang telah ditentukan.</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 p-3 d-flex">
                    <div class="card" style="border: 1px solid white; background-color: #000000c7;">
                        <a href="#" style="color: inherit; text-decoration: none;">
                            <img src="assets/images/profile_picture.png" alt="Onsite Auction" class="card-img-top p-3 mw-50">
                            <div class="card-body text-white">
                                <h2 class="card-title">Live Auction</h2>
                                <span align="justify">Peserta dapat mengikuti lelang melalui Website sesuai dengan jadwal yang telah ditentukan, tanpa harus datang ke lokasi lelang.</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 p-3 d-flex">
                    <div class="card" style="border: 1px solid white; background-color: #000000c7;">
                        <a href="#" style="color: inherit; text-decoration: none;">
                            <img src="assets/images/profile_picture.png" alt="Onsite Auction" class="card-img-top p-3 mw-50">
                            <div class="card-body text-white">
                                <h2 class="card-title">Timed Auction</h2>
                                <span align="justify">Peserta dapat mengikuti lelang online dimana saja, hanya dengan melalui website Balai Lelang SUN sesuai rentang waktu yang sudah ditentukan.</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="mw-90 mx-auto text-center">
        <h1 class="text-uppercase fontbold text-white d-inline-block">Kenapa harus lelang di SUN?</h1>
    </div>

    <section class="page-section portfolio">

        <div class="container mw-90 text-white">

            <div class="row p-4 mb-4" style="background-color: #000000c7;border: 1px solid white; border-radius: 20px;">

                <div class="col-12 col-md-3 col-6 text-center">
                    <img src="assets/images/dark-06.png" class="p-3 mw-50" alt="Lelang Secara Online">
                    <h2 class="mb-5">Lelang<br> Secara Online &amp; offline</h2>
                    <span class="mb-3" align="justify">Semua bisa menang lelang tanpa perlu hadir di lokasi melalui fitur Live Auction dan Timed Auction atau bisa langsung datang ke loasi untuk mengikuti lelang secara offline/floor</span>
                </div>
                <div class="col-12 col-md-3 col-6 text-center">
                    <img src="assets/images/dark-03.png" class="p-3 mw-50" alt="Lelang Secara Online">
                    <h2 class="mb-5">Transaksi Aman &amp; <br> Mudah</h2>
                    <span class="mb-3" align="justify">Pembayaran transaksi di Balai Lelang SUN lebih aman dan mudah karena pembayaran hanya melalui transfer ke Rekening Balai Lelang SUN</span>
                </div>
                <div class="col-12 col-md-3 col-6 text-center">
                    <img src="assets/images/dark-04.png" class="p-3 mw-50" alt="Lelang Secara Online">
                    <h2 class="mb-5">Info Unit <br> Transparan</h2>
                    <span class="mb-4" align="justify">Balai Lelang SUN akan memberikan informasi terkini kendaraan yang akan dilelang secara lengkap dan transparan</span>
                </div>
                <div class="col-12 col-md-3 col-6 text-center">
                    <img src="assets/images/dark-05.png" class="p-3 mw-50" alt="Lelang Secara Online">
                    <h2 class="mb-5">Jadwal<br>Lelang Rutin</h2>
                    <span class="mb-4" align="justify">Kami akan memberikan informasi secara rutin untuk jadwal lelang mingguan</span>
                </div>

            </div>

        </div>

    </section>

    <div class="mw-90 mx-auto text-center">
        <h1 class="text-uppercase fontbold text-white d-inline-block">Testimonial</h1>
    </div>

    <section class="page-section portfolio pt-3">

        <div class="container mw-90">
            <div id="testimonial" class="carousel slide" data-ride="carousel">
                <!-- <ol class="carousel-indicators">
                        <li data-target="#testimonial" data-slide-to="0" class="active"></li>
                        <li data-target="#testimonial" data-slide-to="1"></li>
                        <li data-target="#testimonial" data-slide-to="2"></li>
                    </ol> -->
                <div class="carousel-inner">
                    <div class="carousel-item active" data-interval="10000">
                        <div class="row w-100 mx-auto">
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Unit setiap lelang selalu bisa masuk harganya, Pelayanan lelang baik di lapangan serta pelayanan pelunasan sangat baik.</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Sobikin</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Unit setiap lelang selalu bisa masuk harganya, Pelayanan lelang baik di lapangan serta pelayanan pelunasan sangat baik.</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Sobikin</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Nymana, harga bersaing dan amanah</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Nasrul (Tangerang)</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item" data-interval="10000">
                        <div class="row w-100 mx-auto">
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Tempat lelang berbagai kendaraan bekas dengan kondisi apa adanya</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Henry PN (Jakarta Selatan)</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Unit setiap lelang selalu bisa masuk harganya, Pelayanan lelang baik di lapangan serta pelayanan pelunasan sangat baik.</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Samirat (Bekasi)</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Unit setiap lelang selalu bisa masuk harganya, Pelayanan lelang baik di lapangan serta pelayanan pelunasan sangat baik.</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Sobikin</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item" data-interval="10000">
                        <div class="row w-100 mx-auto">
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Unit setiap lelang selalu bisa masuk harganya, Pelayanan lelang baik di lapangan serta pelayanan pelunasan sangat baik.</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Sobikin</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Unit setiap lelang selalu bisa masuk harganya, Pelayanan lelang baik di lapangan serta pelayanan pelunasan sangat baik.</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Sobikin</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 p-3">
                                <div class="card-body card">
                                    <div class="media d-block">
                                        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png" alt="Sobikin " class="mr-3" style="width: 80px;height: 80px;object-fit: cover;border-radius: 50%;">
                                        <div class="media-body">
                                            <small class="ellipsis-testimoni">
                                                <span>Unit setiap lelang selalu bisa masuk harganya, Pelayanan lelang baik di lapangan serta pelayanan pelunasan sangat baik.</span>
                                            </small>
                                            <div class="dropdown-divider my-3"></div>
                                            <div class="media d-block">
                                                <div class="media-body text-right">
                                                    <span class="mb-0"><small>Sobikin</small></span>
                                                    <div>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star mr-2 text-warning"></i>
                                                        <i class="fas fa-star text-warning"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#testimonial" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>

    <?php include('part/footer.php') ?>
</body>

</html>
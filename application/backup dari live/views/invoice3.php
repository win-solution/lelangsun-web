<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        
        <section class="page-section">

          <div class="container mw-75 border px-0">

            <div class="row">
              
              <div class="col my-auto">
                <h3>PT. BALAI LELANG SUN</h3>
                <ul class="list-unstyled">
                  <li>
                    <span><b>Auction Office </b></span>
                    : Jl. HOS Cokroaminoto No.59 Ruko <br>Ciledug Mas D3 Ciledug Tanggerang
                  </li>
                </ul>
              </div>

              <div class="col d-flex justify-content-center my-auto">
                <ul class="ml-4 mb-0 fa-ul">
                  <li><span class="fa-li"><i class="fas fa-phone-alt"></i></span>Phone : (021) 22271959</li>
                  <li><span class="fa-li"><i class="fab fa-youtube"></i></span>Youtube : BalaiLelangSun</li>
                  <li><span class="fa-li"><i class="fab fa-facebook-square"></i></span>Facebook : BalaiLelangSun</li>
                  <li><span class="fa-li"><i class="fab fa-instagram-square"></i></span>Instagram : BalaiLelangSun</li>
                </ul>
              </div>

              <div class="col text-center">
                <img src="assets/images/logo.png" alt="...">
              </div>

            </div>

            <div class="row">
              
              <div class="col my-auto">
                <h3>Konfirmasi Pemenang Lelang</h3>
              </div>

              <div class="col">

                <div class="row">

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Nomor Urut </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span>20 <hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Tanggal & Jam </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span>25/03/2021    18:50:55<hr class="m-0 mb-1"></span>
                  </div>

                </div>

              </div>

            </div>

            <hr class="my-auto">

            <div class="row">
              
              <div class="col-lg-12 ml-1">
                <span>Konfirmasi Pemenang Lelang</span>
              </div>

              <div class="col-lg-6">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                      
                    <div class="border">
                      <div>
                        <span class="m-1">Data Pemenang Lelang</span>
                      </div>
                    </div>

                    <div class="border px-3 py-3">
                      
                      <div class="row">

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>NPL </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Nama </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Alamat </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Telepon/HP </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

              <div class="col-lg-6">

                <div class="row">

                  <div class="col-lg-12">
                      
                    <div class="border">
                      <div>
                        <span class="m-1">Data Pemenang Lelang</span>
                      </div>
                    </div>

                    <div class="border px-3 py-3">
                      
                      <div class="row">

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Nomor Polisi </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Merk / Type </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Tahun / Warna </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>No. Rangka </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>No. Mesin </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <div class="row">
              
              <div class="col-lg-12 ml-1">
                <span>Konfirmasi Pemenang Lelang</span>
              </div>

              <div class="container-fluid">

                <div class="row">

                  <div class="col-lg-12">
                      
                    <div class="border">
                      <div>
                        <span class="m-1">Data Pemenang Lelang</span>
                      </div>
                    </div>

                    <div class="border px-3 py-3">
                      
                      <div class="row">

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Harga Terjual</span>
                            </div>
                            <div class="col-lg-3 d-flex justify-content-between">
                              <span>:</span>
                              <span>Rp</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0">
                          <div class="pl-0">
                            <label class="sr-only" for="inlineFormInput">Terbilang</label>
                            <input type="text" class="form-control mb-2 text-right" id="inlineFormInput" placeholder="Rupiah #">
                          </div>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Catatan</span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 62%;">
                          <span></span>
                        </div>  

                      </div>

                    </div>

                  </div>

                </div>

              </div>

              <div class="col-lg-12">

                <div class="row">
                  
                  <div class="col-lg-6">
                      
                    <div class="col-lg-12 mw-90 mx-auto border mt-3">
                        
                      <div class="row">
                        
                        <div class="col-lg-12">
                          
                          <div class="row">
                            
                            <div class="col-lg-6">
                              
                              <h4>Pembayaran</h4>

                            </div>

                          </div>

                        </div>

                        <div class="col-lg-12">
                          
                          <div class="row">

                            <div class="col-lg-4">
                              
                              <h3></h3>

                            </div>

                            <div class="col-lg-8">
                              
                              <h4>1. Debit BCA</h4>

                            </div>

                          </div>

                        </div>

                        <div class="col-lg-12">
                          
                          <div class="row">

                            <div class="col-lg-4">
                              
                              <h3></h3>

                            </div>

                            <div class="col-lg-8">
                              
                              <h4>2. Transfer : No. Rek 5025 06 2288 <br> a.n PT. Balai Lelang SUN</h4>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                    <div class="d-flex justify-content-between font-italic">
                      <span class="small">(Putih) Auction Officer</span>
                      <span class="small">(Pink) Administrasi</span>
                      <span class="small">(Kuning) Pemenang Lelang</span>
                    </div>

                  </div>

                  <div class="col-lg-6">
                    
                    <div class="col-lg-12 mw-90 mx-auto">

                      <div class="row">

                        <div class="col-lg-6 pr-0">
                            
                            <div class="border">
                              <div class="text-center p-3">
                                <span class="m-1">Peserta Lelang</span>
                              </div>
                            </div>

                            <div class="border px-3">
                              
                              <div class="row">

                                <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                                  <span>Peserta Lelang</span>
                                </div>

                              </div>

                            </div>

                            <div class="border">
                              <div class=" text-center">
                                <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                              </div>
                            </div>

                            <span class="small">*Mohon ditulis dengan nama jelas</span>

                        </div>

                        <div class="col-lg-6 pl-0">
                            
                          <div class="border">
                            <div class="text-center p-3">
                              <span class="m-1">Auction Office</span>
                            </div>
                          </div>

                          <div class="border px-3">
                            
                            <div class="row">

                              <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                                <span>Auction Office</span>
                              </div>

                            </div>

                          </div>

                          <div class="border">
                            <div class=" text-center">
                              <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                            </div>
                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>
                
              </div>

            </div>

          </div>

        </section>
        
    </body>
</html>
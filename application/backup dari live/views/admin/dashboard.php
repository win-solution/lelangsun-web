<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard Admin <?= APP_NAME ?></a></li>
                </ol>
            </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- chartjs -->
        <script src="https://www.chartjs.org/dist/2.9.4/Chart.min.js"></script>

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
        <script src="<?= base_url().'assets/'; ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>1500</h3>

                            <p>Total Barang</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-boxes"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-6">
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3>500</h3>

                                    <p>Barang Terjual</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-shopping-bag"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-6">
                            <div class="small-box bg-olive">
                                <div class="inner">
                                    <h3>200</h3>

                                    <p>Terjual Online</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-globe"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-6">
                            <div class="small-box bg-lightblue">
                                <div class="inner">
                                    <h3>300</h3>

                                    <p>Terjual Offline</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-building"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning h-75">
                        <div class="inner">
                            <h3>1000</h3>

                            <p>Barang Belum Terjual</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-warehouse"></i>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <section class="col-lg-12 connectedSortable">
                    <div class="card bg-gradient-info">
                        <div class="card-header border-0">
                            <h3 class="card-title">
                            <i class="fas fa-th mr-1"></i>
                            Penjualan Barang Lelang
                            </h3>

                            <div class="card-tools">
                            <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn bg-info btn-sm" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <canvas class="chart" id="line-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                        <div class="card-footer bg-transparent">
                            <div class="row">
                                <div class="col-6 text-center">
                                    <input type="text" class="knob" data-readonly="true" value="70" data-width="60" data-height="60"
                                        data-fgColor="#39CCCC">

                                    <div class="text-white">Online</div>
                                </div>
                                <div class="col-6 text-center">
                                    <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60"
                                        data-fgColor="#39CCCC">

                                    <div class="text-white">Offline</div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </section>
            </div>
        </div>
        
        <script>
            var tableKelas;
            async function getData(ajaxurl) { 
                let resValue;
                const result = await $.ajax({
                    url: ajaxurl,
                    type: 'GET',
                    dataType : 'json',
                    async: true,
                }).done(function (res) { 
                    resValue = res;
                });
                return resValue;
            };
            
            const setDataNewUser = async () => {
                try {
                    let dataNewUser = getData("<?= base_url('admin/rest_api/dataNewUser') ?>");
                    dataNewUser.then(function(result) {
                        $('#dataNewUser').html(result)
                    })
                } catch (err) {
                    console.log("Error setDataKelas : ",err);
                }
            }
            
            const setDataTotalUser = async () => {
                try {
                    let dataTotalUser = getData("<?= base_url('admin/rest_api/dataTotalUser') ?>");
                    dataTotalUser.then(function(result) {
                        $('#dataTotalUser').html(result)
                    })
                } catch (err) {
                    console.log("Error setDataKelas : ",err);
                }
            }

            const setDataKelas = async () => {
                try {
                    let dataKelas = getData("<?= base_url('admin/rest_api/dataRegular') ?>");
                    dataKelas.then(function (res) {
                        let i = 0;
                        $.map(res, function (val, key) {
                            i++;
                            // console.log(val);
                            $("#tableKelas tbody").append('<tr><td>'+i+'</td><td>'+val.hari+'</td><td>'+val.tanggal+'</td><td>'+val.kelas+'</td><td>'+val.result+'</td></tr>')
                        });
                        return res;
                    }).then(function (res) { 
                        // console.log("res then 2",res);
                        tableKelas = $('#tableKelas').DataTable({
                            "responsive": true,
                            "autoWidth": false,
                            "lengthMenu": [[10, -1], [10, "All"]],
                            "pageLength": 10,
                        });
                        res.map(val => console.log(val));
                        let ctx = document.getElementById('kelas-chart').getContext('2d');
                        let myBar = new Chart(ctx, {
                            type: 'bar',
                            data: barChartData,
                            options: {
                                responsive: true,
                                legend: {
                                    position: 'top',
                                },
                                title: {
                                    display: true,
                                    text: 'Chart.js Bar Chart'
                                }
                            }
                        });
                    })
                } catch (err) {
                    console.log("Error setDataKelas : ",err);
                }
            }

            $(function () {
                // setDataNewUser();
                // setDataTotalUser();
                // setDataKelas();
            });
            setInterval(() => {
                // setDataNewUser()
                // setDataTotalUser();
            }, 5 * 60 * 1000);
            
            // jQuery('#vmap').vectorMap({
            //     map: 'indonesia_id',
            //     backgroundColor: '#',
            //     borderColor: '#333333',
            //     borderOpacity: 0.5,
            //     borderWidth: 3,
            //     color: '#a67c52',
            //     enableZoom: false,
            //     hoverColor: '#c9dfaf',
            //     hoverOpacity: null,
            //     normalizeFunction: 'linear',
            //     scaleColors: ['#b6d6ff', '#005ace'],
            //     selectedColor: '#c9dfaf',
            //     selectedRegion: null,
            //     showTooltip: true,
            //     onRegionClick: function(el, code, region, no_prop) {
            //         getDataOutletByProp(no_prop);
            //     }
            // });
            function getDataOutletByProp(no_prop) { 
                $.ajax({ url : "<?= base_url('front/rest_api/outlet')?>",
                    type: "GET",
                    dataType: "JSON",
                    data: {no_prop:no_prop},
                    async: false,        
                    beforeSend: function(){
                        // $("#cover-spin").show();
                    },
                    success: function(data){
                        // swal('Data '+type+' berhasil dihapus','success');
                        // document.location = data.url;
                        // console.log(data);
                        if (data.length == 0) {
                            Swal.fire('Data Kosong!','Data yang diperoleh kosong','info');
                        } else {
                            let valHTML = "";
                            data.map(function (e) {
                                // console.log(e);
                                valHTML += '<div class="col-lg-6 py-3"><div class="row border border-dark rounded mx-1 btn-kategori-menu"><div class="col-lg-12 my-auto text-left py-3"><h2 class="text-uppercase text-black">'+e.nama_outlet+' #'+e.kode_outlet+'</h1><p class="text-black text-capitalize">'+e.alamat_mitra+'</p><hr><a href="'+e.link_maps+'" target="_blank" style="color: #4441ff;"><i class="fas fa-map-marker-alt"></i> Lokasi Outlet</a></div></div></div>';
                            });
                            $('#modalBodyRow').html(valHTML);
                            $("#myModal").modal();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        Swal.fire('Error!','Gagal memproses data dari ajax','error');
                    }
                }).done(function() {
                    setTimeout(function(){
                        // $("#cover-spin").hide();
                        // spin();
                    },500);
                });
            }
        </script>
        <!-- jQuery UI 1.11.4 -->
        <script src="<?= base_url().'assets/'; ?>plugins/jquery-ui/jquery-ui.min.js"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 4 -->
        <script src="<?= base_url().'assets/'; ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- ChartJS -->
        <script src="<?= base_url().'assets/'; ?>plugins/chart.js/Chart.min.js"></script>
        <!-- Sparkline -->
        <script src="<?= base_url().'assets/'; ?>plugins/sparklines/sparkline.js"></script>
        <!-- JQVMap -->
        <!-- <script src="<?= base_url().'assets/'; ?>plugins/jqvmap/jquery.vmap.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
        <!-- jQuery Knob Chart -->
        <script src="<?= base_url().'assets/'; ?>plugins/jquery-knob/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="<?= base_url().'assets/'; ?>plugins/moment/moment.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="<?= base_url().'assets/'; ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- Summernote -->
        <script src="<?= base_url().'assets/'; ?>plugins/summernote/summernote-bs4.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="<?= base_url().'assets/'; ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?= base_url().'assets/'; ?>dist/js/demo.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?= base_url().'assets/'; ?>dist/js/pages/dashboard.js"></script>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Peserta</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Peserta</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <script src="<?= base_url('assets/'); ?>plugins/moment/moment.min.js"></script>
        <script src="<?= base_url('assets/'); ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js"></script>
        <!-- Select2 -->
        <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/select2/css/select2.css">
        <script src="<?= base_url('assets/'); ?>plugins/select2/js/select2.full.min.js"></script>
        <!-- summernote -->
        <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/summernote/summernote-bs4.min.css">
        <script src="<?= base_url('assets/'); ?>plugins/summernote/summernote-bs4.min.js"></script>
        <!-- Bootstrap Switch -->
        <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.css">
        <script src="<?= base_url('assets/'); ?>plugins/bootstrap-switch/js/bootstrap-switch.js"></script>
        <!-- Form Validation -->
        <script src="<?= base_url('assets/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?= base_url('assets/'); ?>plugins/jquery-validation/additional-methods.js"></script>
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url() . 'assets/'; ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= base_url() . 'assets/'; ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
        <script src="<?= base_url() . 'assets/'; ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

        <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <!-- <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/buttons.flash.min.js"></script> -->
        <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
        <script src="<?= base_url() . 'assets/'; ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
        <script>
            var kelas = "";
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar Peserta <?= APP_NAME ?></h3>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content_tab_1" data-toggle="pill" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true" title="Peserta Aktif">Peserta Aktif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="content_tab_2" data-toggle="pill" href="#tab_2" role="tab" aria-controls="tab_2" aria-selected="false" title="Peserta Tidak Aktif">Peserta Tidak Aktif</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="content_tab_3" data-toggle="pill" href="#tab_3" role="tab" aria-controls="tab_3" aria-selected="false" title="Verifikasi NPL">Verifikasi NPL</a>
                                </li>
                                <li class="nav-item d-none">
                                    <a class="nav-link" id="content_tab_4" data-toggle="pill" href="#tab_4" role="tab" aria-controls="tab_4" aria-selected="false" title="Refund NPL">Refund NPL</a>
                                </li>
                            </ul>
                            <br>
                            <div class="tab-content" id="custom-tabs-four-tabContent">
                                <div class="tab-pane fade active show" id="tab_1" role="tabpanel" aria-labelledby="content_tab_1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <!-- <h3 class="panel-title" >Custom Filter : </h3> -->
                                        </div>
                                        <div class="panel-body row">
                                            <form id="form-filter" class="form-horizontal col-12">
                                                <div class="row">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <button class="btn btn-success" onclick="add();"><i class="fas fa-plus"></i> Tambah Peserta </button>
                                    <!-- <button class="btn btn-success" onclick="alert('pop-up form peserta');"><i class="fas fa-plus"></i> Tambah Peserta  </button> -->
                                    <table id="tablePeserta" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Telpon</th>
                                                <th>Alamat</th>
                                                <th>NPL</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="tab_2" role="tabpane2" aria-labelledby="content_tab_2">
                                    <table id="tableDelPeserta" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Telpon</th>
                                                <th>Alamat</th>
                                                <th>NPL</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="tab_3" role="tabpane3" aria-labelledby="content_tab_3">
                                    <table id="tableVerNPL" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Tgl Transfer</th>
                                                <th>Nominal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade d-none" id="tab_4" role="tabpane4" aria-labelledby="content_tab_4">
                                    <table id="tableRefundNPL" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Event Lelang</th>
                                                <th>Tgl Transfer</th>
                                                <th>No Rekening</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-admin mw-75 mx-auto">

                <!-- Modal content-->
                <div class="modal-content" style="border-radius: 25px;">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal_header"></h4>
                        <button type="button" class="close btn btn-danger" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="modal_body">
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    </div>
                </div>

            </div>
        </div>
        <script>
            var table, table2, table3, table_trash, action, action_label;
            $(function() {
                table = $('#tablePeserta').DataTable({
                    "responsive": true,
                    "autoWidth": false,
                    "scrollX": false,
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "searchDelay": 1 * 1000,
                    // "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
                    // "pageLength": 25,
                    "dom": 'lBifrtip',
                    "order": [], //Initial no order.
                    "buttons": [{
                            extend: 'excelHtml5',
                            title: 'Data Peserta  Aktif <?= APP_NAME ?> ' + Date.now(),
                            exportOptions: {
                                modifier: {
                                    page: 'all',
                                    search: 'applied',
                                    order: 'applied',
                                }
                            },
                        },
                        'print',
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?= base_url('admin/rest_api/dtPeserta?type=peserta') ?>",
                        "type": "POST",
                        "data": function(data) {
                            data.kelas = $('#kelas').val();
                            // data.search = $('#search').val();
                        }
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [{
                        "targets": [0, 5, 6], //first column / numbering column
                        "orderable": false, //set not orderable
                    }, ],
                });
                table_trash = $('#tableDelPeserta').DataTable({
                    "responsive": true,
                    "autoWidth": false,
                    "scrollX": false,
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "searchDelay": 1 * 1000,
                    // "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
                    // "pageLength": 25,
                    "dom": 'lfrtip',
                    "order": [], //Initial no order.                    
                    "dom": 'lBfrtip',
                    "buttons": [{
                            extend: 'excelHtml5',
                            title: 'Data Peserta  Tidak Aktif <?= APP_NAME ?>',
                            exportOptions: {
                                modifier: {
                                    page: 'all',
                                    search: 'applied',
                                    order: 'applied',
                                }
                            },
                        },
                        'print',
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?= base_url('admin/rest_api/dtPeserta?type=del_peserta') ?>",
                        "type": "POST",
                        "data": function(data) {
                            data.kelas = $('#filterKelas').val();
                        }
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [{
                        "targets": [0, 5, 6], //first column / numbering column
                        "orderable": false, //set not orderable
                    }, ],
                });
                table2 = $('#tableVerNPL').DataTable({
                    "responsive": true,
                    "autoWidth": false,
                    "scrollX": false,
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "searchDelay": 1 * 1000,
                    // "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
                    // "pageLength": 25,
                    "dom": 'lfrtip',
                    "order": [], //Initial no order.
                    "buttons": [{
                            extend: 'excelHtml5',
                            title: 'Data NPL Belum Terverifikasi <?= APP_NAME ?>',
                            exportOptions: {
                                modifier: {
                                    page: 'all',
                                    search: 'applied',
                                    order: 'applied',
                                }
                            },
                        },
                        'print',
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?= base_url('admin/rest_api/dtNPL?type=npl_verifikasi') ?>",
                        "type": "POST",
                        "data": function(data) {
                            data.kelas = $('#filterKelas').val();
                        }
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [{
                        "targets": [0, 5], //first column / numbering column
                        "orderable": false, //set not orderable
                    }, ],
                });
                table3 = $('#tableRefundNPL').DataTable({
                    "responsive": true,
                    "autoWidth": false,
                    "scrollX": false,
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "searchDelay": 1 * 1000,
                    // "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
                    // "pageLength": 25,
                    "dom": 'lfrtip',
                    "order": [], //Initial no order.
                    "buttons": [{
                            extend: 'excelHtml5',
                            title: 'Data NPL Belum Terverifikasi <?= APP_NAME ?>',
                            exportOptions: {
                                modifier: {
                                    page: 'all',
                                    search: 'applied',
                                    order: 'applied',
                                }
                            },
                        },
                        'print',
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?= base_url('admin/rest_api/dtNPL?type=npl_verifikasi') ?>",
                        "type": "POST",
                        "data": function(data) {
                            data.kelas = $('#filterKelas').val();
                        }
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [{
                        "targets": [0, 5], //first column / numbering column
                        "orderable": false, //set not orderable
                    }, ],
                });
            });

            function add() {
                action = "<?= base_url('admin/peserta/tambah') ?>";
                action_label = "Tambah";
                $.ajax({
                    url: "<?= base_url('admin/peserta/tambah') ?>",
                    type: "GET",
                    dataType: "JSON",
                    async: false,
                    success: function(data) {
                        $('#myModal #modal_header').html("");
                        $('#myModal #modal_body').html("");

                        $('#myModal #modal_header').append(data.proses);
                        $('#myModal #modal_body').append(data.content);
                        $('#myModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        newSWAL("Gagal memperoleh data dari ajax!", "error");
                    }
                });
            }

            function edit(id) {
                action = "<?= base_url('admin/peserta/ubah') ?>";
                action_label = "Ubah";
                $.ajax({
                    url: "<?= base_url('admin/peserta/ubah/') ?>" + id,
                    type: "GET",
                    dataType: "JSON",
                    async: false,
                    success: function(data) {
                        $('#myModal #modal_header').html("");
                        $('#myModal #modal_body').html("");

                        $('#myModal #modal_header').append(data.proses);
                        $('#myModal #modal_body').append(data.content);
                        $('#myModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        newSWAL("Gagal memperoleh data dari ajax!", "error");
                    }
                });
            }

            function verifikasi(id) {
                action = "<?= base_url('admin/peserta/verifikasi_npl') ?>";
                action_label = "Verifikasi NPL";
                $.ajax({
                    url: "<?= base_url('admin/peserta/verifikasi_npl/') ?>" + id,
                    type: "GET",
                    dataType: "JSON",
                    async: false,
                    success: function(data) {
                        $('#myModal #modal_header').html("");
                        $('#myModal #modal_body').html("");

                        $('#myModal #modal_header').append(data.proses);
                        $('#myModal #modal_body').append(data.content);
                        $('#myModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        newSWAL("Gagal memperoleh data dari ajax!", "error");
                    }
                });
            }

            function detail(id) {
                action = "<?= base_url('admin/peserta/detail') ?>";
                action_label = "Detail Peserta";
                $.ajax({
                    url: "<?= base_url('admin/peserta/detail/') ?>" + id,
                    type: "GET",
                    dataType: "JSON",
                    async: false,
                    success: function(data) {
                        $('#myModal #modal_header').html("");
                        $('#myModal #modal_body').html("");

                        $('#myModal #modal_header').append(data.proses);
                        $('#myModal #modal_body').append(data.content);
                        $('#myModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        newSWAL("Gagal memperoleh data dari ajax!", "error");
                    }
                });
            }

            function save() {
                var form = $("#myform");
                var formDOM = document.getElementById('myForm');
                let formData = new FormData(form[0]);
                console.log(formData);
                form.validate();
                if (form.valid()) {
                    $("#submit_btn").attr('disabled', 'disabled');
                    $("#submit_btn").val('Sedang menyimpan data!');
                    $("#cancel_btn").attr('style', 'display:none;');
                    $.ajax({
                        url: action,
                        type: "POST",
                        // data : form.serialize(),
                        data: new FormData(form[0]),
                        dataType: "JSON",
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            table.ajax.reload(null, false);
                            table2.ajax.reload(null, false);
                            $('#myModal').modal("hide");
                            newSWAL("Data berhasil tersimpan!", "success");
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            // $("#submit_btn").removeAttr("disabled");
                            // $("#submit_btn").val(action_label);
                            // $("#cancel_btn").attr('style','display:block;');
                            newSWAL("Gagal memperoleh data dari ajax!", "error");
                        }
                    });
                } else {

                }
            }

            setInterval(function() {
                // table.ajax.reload(null,false);
                // table_trash.ajax.reload(null,false);
            }, 3 * 60 * 1000);
        </script>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
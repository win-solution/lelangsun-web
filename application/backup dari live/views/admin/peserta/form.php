<?php  // var_dump($kategori_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_peserta" id="id_peserta" value="<?= @$peserta_detail[0]->id_peserta ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="nama">Nama <span style="color:red;">*</span></label>
          <input class="form-control" id="nama" placeholder="Nama peserta" type="text" name="nama" value="<?= @$peserta_detail[0]->nama ?>" required required>
        </div>

        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label for="email">Email <span style="color:red;">*</span></label>
              <input class="form-control" id="email" placeholder="Email peserta" type="email" name="email" value="<?= @$peserta_detail[0]->email ?>" required>
            </div>
          </div>
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label for="no_hp">Telpon <span style="color:red;">*</span></label>
              <input class="form-control" id="no_hp" placeholder="No Telpon peserta" type="text" name="no_hp" value="<?= @$peserta_detail[0]->no_hp ?>" required>
            </div>
          </div>
        </div>        

        <div class="form-group">
          <label for="alamat">Alamat <span style="color:red;">*</span></label>
          <textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control" required><?= @$peserta_detail[0]->alamat ?></textarea>
        </div>
          
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label for="nik">NIK <span style="color:red;">*</span></label>
              <input class="form-control" id="nik" placeholder="NIK peserta" type="text" name="nik" value="<?= @$peserta_detail[0]->nik ?>" required>
            </div>
          </div>
          
          <div class="col-md-6 col-12">
            <div class="form-group">
                <label>Foto KTP</label><br>
                <?php if (!empty(@$peserta_detail[0]->foto_ktp)) { ?>
                <img id="prevKTP" class="img-thumbnail" style="max-width: 25vw;" src="<?= base_url('assets/uploads/ktp_peserta/origin/').@$peserta_detail[0]->foto_ktp ?>" alt="" srcset="">
                <?php } else { ?>
                <div class="form-group">
                  <img class="img-thumbnail" style="max-width: 15vw;" id="prevKTP" src="" alt="" srcset="">
                </div>

                <div class="form-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="foto_ktp" name="foto_ktp" onchange="readURL(this,'#prevKTP','#labelKTP');" accept="image/jpg, image/jpeg, image/png">
                    <label id="labelKTP" class="custom-file-label" for="foto_ktp">Pilih Gambar KTP</label>
                  </div>
                </div>
                <?php } ?>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-md-6 col-12">
            <div class="form-group">
              <label for="npwp">NPWP <span style="color:red;">*</span></label>
              <input class="form-control" id="npwp" placeholder="npwp peserta" type="text" name="npwp" value="<?= @$peserta_detail[0]->npwp ?>" required>
            </div>
          </div>
          
          <div class="col-md-6 col-12">
            <div class="form-group">
                <label>Foto NPWP</label><br>
                <?php if (!empty(@$peserta_detail[0]->foto_npwp)) { ?>
                <img id="prevNPWP" class="img-thumbnail" style="max-width: 25vw;" src="<?= base_url('assets/uploads/npwp_peserta/origin/').@$peserta_detail[0]->foto_npwp ?>" alt="" srcset="">
                <?php } else { ?>                
                <div class="form-group">
                  <img class="img-thumbnail" style="max-width: 15vw;" id="prevNPWP" src="" alt="" srcset="">
                </div>

                <div class="form-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="foto_npwp" name="foto_npwp" onchange="readURL(this,'#prevNPWP','#labelNPWP');" accept="image/jpg, image/jpeg, image/png">
                    <label id="labelNPWP" class="custom-file-label" for="foto_npwp">Pilih Gambar NPWP</label>
                  </div>
                </div>
                <?php } ?>
            </div>
          </div>
        </div>

        <?php if (strtolower($proses) == "ubah") { ?>
          <div class="row">
            <div class="col-lg-6 col-12 text-right">
              <a class="btn btn-app bg-success" target="_blank" href="tel:<?= @$peserta_detail[0]->no_hp ?>">
                <i class="fas fa-phone-alt"></i> Telpon
              </a>
            </div>

            <div class="col-lg-6 col-12">
              <a class="btn btn-app bg-success" target="_blank" href="mailto:<?= @$peserta_detail[0]->email ?>">
                <i class="fas fa-envelope"></i> Email
              </a>
            </div>
          </div>
        <?php } ?>

        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
    $('.summernote').summernote(
      {height:150,disableResizeEditor:true}
    );
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
    $('.select2').select2();
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        id_peserta: { required: true, },
        nama: { required: true, },
        email: { required: true, },
        no_hp: { required: true, },
        alamat: { required: true, },
      }
    });
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#gambar").change(function() {
      // console.log("test");
      readURL(this,'#prevGambar','#labelGambar');
  });
</script>

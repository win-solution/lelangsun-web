<?php  // var_dump($kategori_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_kontak" id="id_kontak" value="<?= @$kontak_detail[0]->id_kontak ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="nama">Nama <span style="color:red;">*</span></label>
          <input class="form-control" id="nama" placeholder="Nama" type="text" name="nama" value="<?= @$kontak_detail[0]->nama ?>" required readonly>
        </div>

        <div class="form-group">
          <label for="email">Email <span style="color:red;">*</span></label>
          <input class="form-control" id="email" placeholder="Email" type="email" name="email" value="<?= @$kontak_detail[0]->email ?>" required readonly>
        </div>

        <div class="form-group">
          <label for="no_tlp">Telpon <span style="color:red;">*</span></label>
          <input class="form-control" id="no_tlp" placeholder="Telpon" type="text" name="no_tlp" value="<?= @$kontak_detail[0]->no_tlp ?>" required readonly>
        </div>

        <div class="form-group">
            <label for="pesan" class="col-sm-2 control-label">Pesan</label>
            <textarea name="pesan" id="pesan" cols="30" rows="5" class="form-control" required readonly><?= @$kontak_detail[0]->pesan ?></textarea>
        </div> 

        <div class="row">
          <div class="col-lg-6 col-12 text-right">
            <a class="btn btn-app bg-success" target="_blank" href="tel:<?= @$kontak_detail[0]->no_tlp ?>">
              <i class="fas fa-phone-alt"></i> Telpon
            </a>
          </div>

          <div class="col-lg-6 col-12">
            <a class="btn btn-app bg-success" target="_blank" href="mailto:<?= @$kontak_detail[0]->email ?>">
              <i class="fas fa-envelope"></i> Email
            </a>
          </div>
        </div>

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">           -->
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
    $('.summernote').summernote(
      {height:150,disableResizeEditor:true}
    );
    $('.select2').select2();
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        nama: { required: true, },
        email: { required: true, },
        no_tlp: { required: true, },
        pesan: { required: true, },
        <?php if (strtolower($proses) == "tambah") { ?>
        <?php } ?>
      }
    });
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#gambar").change(function() {
        // console.log("test");
        readURL(this,'#prevGambar','#labelGambar');
    });
</script>

<?php  // var_dump($kategori_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_event" id="id_event" value="<?= @$event_detail[0]->id_event ?>">
        <input type="hidden" name="tanggal" id="tanggal" value="<?= @$event_detail[0]->waktu_event ?>">
        <input type="hidden" name="submit" id="submit" value="submit">
        <div class="row">
          <div class="col-12">
            <h3>Detail Event</h3>
            <ol>
              <li>Nama Event : <b><?= @$event_detail[0]->nama_event ?></b></li>
              <li>Deskripsi Event : <b><?= @$event_detail[0]->deskripsi_event ?></b></li>
              <li>Alamat Event : <b><?= '<a href="'.@$event_detail[0]->link_maps.'" target="_blank" rel="noopener noreferrer">'.@$event_detail[0]->alamat_event.'</a>' ?></b></li>
              <li>Waktu/Tanggal Event : <b><?= date_format(date_create(@$event_detail[0]->waktu_event),'d M Y') ?></b></li>
            </ol>
          </div>
        </div>
        <div class="form-group">
          <label for="nama_menu">List Barang <span style="color:red;">*</span></label>
          <div class="transfer"></div>
        </div>

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="Simpan" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  var groupData;
  $(document).ready(function () {
    groupData=[
      <?php 
        foreach ($kategori_lelang as $key => $val) {
      ?>
      {"groupName":"<?= $val->kategori ?>","groupData":[
        <?php foreach ($barang_lelang as $key2 => $val2) { if ($val->id_kategori == $val2->id_kategori) { 
            if ($val2->id_event == $event_detail[0]->id_event) { } else { if (!is_null($val2->id_event)) { continue; } }
        ?>
          {
            "name":"<?= $val2->brand.' - '.$val2->nama_barang_lelang.' ('.$val2->no_polisi.')' ?>",
            "value":<?= $val2->id ?>,
            <?php if ($val2->harga_awal !== null) { ?>
            "harga_awal":<?= $val2->harga_awal ?>,
            <?php } ?>
            <?php if ($val2->status == 1) { ?>
              "selected":true,
            <?php } ?>
          },
        <?php } } ?>
      ]},
      <?php } ?>
    ]
    // {"groupName":"JavaScript","groupData":[{"language":"jQuery","value":122,"selected":true},{"language":"AngularJS","value":643},{"language":"ReactJS","value":422},{"language":"VueJS","value":622}]},{"groupName":"Popular","groupData":[{"language":"JavaScript","value":132},{"language":"Java","value":112},{"language":"Python","value":124},{"language":"TypeScript","value":121},{"language":"PHP","value":432},{"language":"Ruby on Rails","value":421}]}
    var settings = {
        itemName:"name",
        groupItemName:"groupName",
        groupArrayName:"groupData",
        selectedName:"id_barang_lelang[]",
        valueName:"value",
        tabNameText:"Lot Lelang",
        rightTabNameText:"Lot Lelang Terpilih",
        searchPlaceholderText:"search",
        groupDataArray: groupData,
        // dataArray: languages,
    };
    var myTransfer = $(".transfer").transfer(settings);

    // var settings = {
    //     "inputId": "languageInput",
    //     // "data": languages,
    //     "groupData": groupData,
    //     // "itemName": "language",
    //     "groupItemName": "groupName",
    //     "groupListName" : "groupData",
    //     "container": "transfer",
    //     // "valueName": "value",
    //     "tabNameGroup": "Kategori",
    //     // "tabNameItem": "Barang",
    //     "callable" : function (data, names) {
    //         console.log("Selected ID：" + data)
    //         $("#selectedItemSpan").text(names)
    //     }
    // };

    // Transfer.transfer(settings);
    $('.summernote').summernote(
      {height:150,disableResizeEditor:true}
    );
    $('.select2').select2();
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        id_event: { required: true, },
      }
    });
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#gambar").change(function() {
        // console.log("test");
        readURL(this,'#prevGambar','#labelGambar');
    });
</script>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Inject Query</title>
    </head>
    <body>
        <h1>Inject Script</h1>
        <hr>
        <h3>Data yang didapat</h3>
        <hr>
        <span>get data : <b><?= $num_rows; ?></b></span>
        <br>
        <!-- <table>
            <thead>
                <tr>
                    <th>id_peserta</th>
                    <th>nama_peserta</th>
                    <th>email</th>
                    <th>kode_uniqlo</th>
                    <th>kelas</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table> -->
        <!-- <span>end time : <?= microtime(true); ?></span> -->
        <script>
            setTimeout(() => {
                alert('masuk nih ye');
            }, 5 * 1000);
        </script>
    </body>
</html>
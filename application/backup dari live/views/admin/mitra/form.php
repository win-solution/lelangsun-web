<?php  // var_dump($kategori_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_mitra" id="id_mitra" value="<?= @$mitra_detail[0]->id_mitra ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="nama_mitra">Nama <span style="color:red;">*</span></label>
          <input class="form-control" id="nama_mitra" placeholder="Nama mitra" type="text" name="nama_mitra" value="<?= @$mitra_detail[0]->nama_mitra ?>"  required>
        </div>

        <div class="form-group">
          <label for="email">Email <span style="color:red;">*</span></label>
          <input class="form-control" id="email" placeholder="Email mitra" type="email" name="email" value="<?= @$mitra_detail[0]->email ?>"  required>
        </div>

        <div class="form-group">
          <label for="no_tlp">Telpon <span style="color:red;">*</span></label>
          <input class="form-control" id="no_tlp" placeholder="No Telpon mitra" type="text" name="no_tlp" value="<?= @$mitra_detail[0]->no_tlp ?>"  required>
        </div>

        <div class="form-group">
          <label for="alamat_mitra">Alamat <span style="color:red;">*</span></label>
          <textarea name="alamat_mitra" id="alamat_mitra" cols="30" rows="5" class="form-control"  required><?= @$mitra_detail[0]->alamat_mitra ?></textarea>
        </div>

        <?php if (strtolower($proses) == "ubah") { ?>
          <div class="form-group">
              <label for="follow_up" class="col-sm-2 control-label">Sudah Follow Up?</label><br>
              <input type="checkbox" id="follow_up" name="follow_up" value="1" <?= (@$mitra_detail[0]->follow_up == 1)?"checked ":""; ?> data-bootstrap-switch data-off-color="danger" data-on-color="success" data-on-text="Sudah" data-off-text="Belum">
          </div> 

          <div class="form-group">
              <label for="pesan" class="col-sm-2 control-label">Pesan</label>
              <textarea name="pesan" id="pesan" cols="30" rows="5" class="form-control summernote" required><?= @$mitra_detail[0]->pesan ?></textarea>
          </div> 

          <div class="row">
            <div class="col-lg-6 col-12 text-right">
              <a class="btn btn-app bg-success" target="_blank" href="tel:<?= @$mitra_detail[0]->no_tlp ?>">
                <i class="fas fa-phone-alt"></i> Telpon
              </a>
            </div>

            <div class="col-lg-6 col-12">
              <a class="btn btn-app bg-success" target="_blank" href="mailto:<?= @$mitra_detail[0]->email ?>">
                <i class="fas fa-envelope"></i> Email
              </a>
            </div>
          </div>
        <?php } ?>

        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
    $('.summernote').summernote(
      {height:150,disableResizeEditor:true}
    );
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
    $('.select2').select2();
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        <?php if (strtolower($proses) == "ubah") { ?>
        id_mitra: { required: true, },
        <?php } ?>
        nama_mitra: { required: true, },
        email: { required: true, },
        no_tlp: { required: true, },
        alamat_mitra: { required: true, },
      }
    });
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#gambar").change(function() {
        // console.log("test");
        readURL(this,'#prevGambar','#labelGambar');
    });
</script>

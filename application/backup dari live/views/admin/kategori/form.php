<?php  // var_dump($kategori_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_kategori" id="id_kategori" value="<?= @$kategori_detail[0]->id_kategori ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="kategori">Nama Kategori <span style="color:red;">*</span></label>
          <input class="form-control" id="kategori" placeholder="Nama kategori" type="text" name="kategori" value="<?= @$kategori_detail[0]->kategori ?>" required>
        </div>

        <?php if (strtolower($proses) == "ubah") { ?>
        <!-- <div class="form-group">
          <label>Gambar</label><br>
          <img id="prevGambar" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/kategori_menu/thumb/').@$kategori_detail[0]->gambar ?>" alt="" srcset="">
        </div> -->
        <?php } else { ?>
          <!-- <div class="form-group">
            <label>Gambar</label><br>
            <img class="img-thumbnail" style="max-width: 15vw;" id="prevGambar" src="" alt="" srcset="">
          </div> -->
        <?php } ?>

        <!-- <div class="form-group">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="gambar" name="gambar" accept="image/jpg, image/jpeg, image/png">
            <label id="labelGambar" class="custom-file-label" for="gambar">Pilih Gambar</label>
          </div>
        </div> -->

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
    $('.summernote').summernote(
      {height:150,disableResizeEditor:true}
    );
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        kategori: { required: true, },       
        // <?php if (strtolower($proses) == "tambah") { ?>
        //   gambar: {
        //     required:true,
        //     extension: "jpg,jpeg,png",
        //     maxsize: 20000,
        //   }
        // <?php } else { ?>
        //   gambar: {
        //     extension: "jpg,jpeg,png",
        //     maxsize: 20000,
        //   }
        // <?php } ?>
      }
    });
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#gambar").change(function() {
        // console.log("test");
        readURL(this,'#prevGambar','#labelGambar');
    });
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Admin CMS <?= APP_NAME ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Admin CMS <?= APP_NAME ?></a></li>
                </ol>
            </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
        <script src="<?= base_url().'assets/'; ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <!-- <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.flash.min.js"></script> -->
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
        <script>
            var kelas = "";
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Admin <?= APP_NAME ?></h3>
                        </div>
                        <div class="card-body">
                            <p>Selamat datang di aplikasi admin <?= APP_NAME ?></p>
                        </div>                          
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <script>
            var table, table_trash;
            $(function () {
                
            });
            setInterval( function () {
            }, 3 * 60 * 1000 );
        </script>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

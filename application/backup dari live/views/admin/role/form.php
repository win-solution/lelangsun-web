<?php // var_dump(json_encode($role_menu_detail)) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
          <strong><?= $this->session->flashdata('error') ?></strong>
        </div>
      <?php } ?>
        <input type="hidden" name="id_role" id="id_role" value="<?= @$role_detail[0]->id_role ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="nama_role">Nama <span style="color:red;">*</span></label>
          <input class="form-control" id="nama_role" placeholder="Nama Role" type="text" name="nama_role" value="<?= @$role_detail[0]->nama_role ?>" required>
        </div>
        <?php if (strtolower($proses) == "ubah") { ?>
        <h3>Akses Menu</h3>
        <table id="tableMenu" class="table table-striped">
          <thead>
            <th>
              <td>Menu</td>
              <td>Create</td>
              <td>Update</td>
              <td>Delete</td>
              <td>Restore</td>
            </th>
          </thead>
          <tbody>
            <?php $tmpIdMenu = array(); foreach ($menu_detail as $key => $val) {
              if (!empty($role_menu_detail[$val->id_menu])) { $tmpIdMenu[] = $val->id_menu; }
            ?>
            <tr>
              <td>
                <div class="custom-control custom-switch">
                  <input type="hidden" name="menu[<?=$val->id_menu?>][id_role_menu]" value="<?= (!empty($role_menu_detail[$val->id_menu]->id_role_menu))?$role_menu_detail[$val->id_menu]->id_role_menu:0; ?>">
                  <input type="hidden" name="menu[<?=$val->id_menu?>][id_menu]" value="<?=$val->id_menu?>">
                  <input type="checkbox" class="custom-control-input" id="aktifSwitch<?=$val->id_menu?>" name="menu[<?=$val->id_menu?>][status]" value="1" <?= ((!empty($role_menu_detail[$val->id_menu]))&&($role_menu_detail[$val->id_menu]->status == 1))?"checked":"" ?> >
                  <label class="custom-control-label" for="aktifSwitch<?=$val->id_menu?>">Aktif</label>
                </div>
              </td>
              <td style="<?= ($val->header)?"font-weight: bold;":""; ?>"><?= $val->nama_menu ?><?= ($val->header)?" (Header)":""; ?></td>
              <td>
                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                  <input type="checkbox" class="custom-control-input" id="createSwitch<?=$val->id_menu?>" value="1" name="menu[<?=$val->id_menu?>][create]" <?= ((!empty($role_menu_detail[$val->id_menu]))&&($role_menu_detail[$val->id_menu]->create == 1))?"checked":"" ?>>
                  <label class="custom-control-label" for="createSwitch<?=$val->id_menu?>"></label>
                </div>
              </td>
              <td>
                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                  <input type="checkbox" class="custom-control-input" id="updateSwitch<?=$val->id_menu?>" value="1" name="menu[<?=$val->id_menu?>][update]" <?= ((!empty($role_menu_detail[$val->id_menu]))&&($role_menu_detail[$val->id_menu]->update == 1))?"checked":"" ?>>
                  <label class="custom-control-label" for="updateSwitch<?=$val->id_menu?>"></label>
                </div>
              </td>
              <td>
                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                  <input type="checkbox" class="custom-control-input" id="deleteSwitch<?=$val->id_menu?>" value="1" name="menu[<?=$val->id_menu?>][delete]" <?= ((!empty($role_menu_detail[$val->id_menu]))&&($role_menu_detail[$val->id_menu]->delete == 1))?"checked":"" ?>>
                  <label class="custom-control-label" for="deleteSwitch<?=$val->id_menu?>"></label>
                </div>
              </td>
              <td>
                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                  <input type="checkbox" class="custom-control-input" id="restoreSwitch<?=$val->id_menu?>" value="1" name="menu[<?=$val->id_menu?>][restore]" <?= ((!empty($role_menu_detail[$val->id_menu]))&&($role_menu_detail[$val->id_menu]->restore == 1))?"checked":"" ?>>
                  <label class="custom-control-label" for="restoreSwitch<?=$val->id_menu?>"></label>
                </div>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <input type="hidden" name="id_menu_awal" value="<?= implode(",",$tmpIdMenu); ?>">
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
      // akses();
      // getopendata();
    $('#tableMenu').DataTable({ 
        "responsive": true,
        "autoWidth": false,
        "scrollX": false,
        "processing"  : true, //Feature control the processing indicator.
        "serverSide"  : false, //Feature control DataTables' server-side processing mode.
        "searchDelay" : 1 * 1000,
        // "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "pageLength": "All",
        "dom": 'rti',
        "order": [], //Initial no order.
        //Set column definition initialisation properties.
        "columnDefs": [
            { 
                "targets": [ 0, 1, 2, 3, 4, 5, 6 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
    });
    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        nama: "required",
      }
    });
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
</script>

<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        
        <section class="page-section">

          <div class="container mw-75 py-3">

            <div class="row">
              
              <div class="col my-auto">
                <h3>PT. BALAI LELANG SUN</h3>
                <ul class="list-unstyled">
                  <li>
                    <span><b>Auction Office </b></span>
                    : Jl. HOS Cokroaminoto No.59 Ruko <br>Ciledug Mas D3 Ciledug Tanggerang
                  </li>
                </ul>
              </div>

              <div class="col text-right">
                <img src="assets/images/logo.png" alt="...">
              </div>

            </div>

            <div class="row border">
              
              <div class="col p-3 text-center">
                <h1 class="m-0">SURAT IZIN KELUAR KENDARAAN</h1>
              </div>

            </div>

            <br>

            <div class="row">

              <div class="col-lg-12 mb-3 px-0">

                <div class="col-lg-8 py-3">
                  
                  <div class="row">

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span>NPL </span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span></span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span>Nama </span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span></span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span>Alamat </span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span></span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <div class="row">
                        <div class="col-lg-6">
                          <span></span>
                        </div>
                        <div class="col-lg-6">
                          <span>Kode Pos :</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span></span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                  </div>

                </div>

              </div>

            </div>

            <div class="row">

              <div class="col-lg-12 px-0">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                    
                    <div class="row mx-auto text-right">
                      <div class="col-lg-9 pr-0">
                        <span>Tangerang,</span>
                      </div>
                      <div class="col pl-0 border-bottom">
                        <span></span>
                      </div>
                    </div>

                  </div>

                  <div class="col-lg-12 pr-0">

                    <div class="row">
                      
                      <div class="col-lg-4">
                        
                        <div>
                          <div class="text-center p-3">
                            <span class="m-1">Driver,</span>
                          </div>
                        </div>

                        <div class="px-3">
                          
                          <div class="row">

                            <div class="col-lg-6 mx-auto text-center border-bottom" style="padding-top: 10vh;">
                              <span></span>
                            </div>

                          </div>

                        </div>

                      </div>

                      <div class="col-lg-4">
                        
                        <div>
                          <div class="text-center p-3">
                            <span class="m-1">Disetujui,</span>
                          </div>
                        </div>

                        <div class="px-3">
                          
                          <div class="row">

                            <div class="col-lg-6 mx-auto text-center border-bottom" style="padding-top: 10vh;">
                              <span></span>
                            </div>

                          </div>

                        </div>

                      </div>

                      <div class="col-lg-4">
                        
                        <div>
                          <div class="text-center p-3">
                            <span class="m-1">Security,</span>
                          </div>
                        </div>

                        <div class="px-3">
                          
                          <div class="row">

                            <div class="col-lg-6 mx-auto text-center border-bottom" style="padding-top: 10vh;">
                              <span></span>
                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

                  

                </div>

              </div>

            </div>

          </div>

        </section>
        
    </body>
</html>
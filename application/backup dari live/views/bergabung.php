<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">        
        <?php include('part/nav.php') ?>
        <!-- Masthead-->
        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
        <header class="masthead" style="background-image: url('assets/images/Cara Franchise-01-min.png'); height: 100vh; margin-top: 104px;">
            <div class="container pt-3 mw-90">
                <div class="row w-75 mx-auto">
                    <div class="col-6 col-lg-6" style="padding: 10% 9% 0 0;">
                    <a href="<?= base_url(); ?>syarat"><img src="assets/images/Franchise r-01.png" class="d-block w-100" alt="..." loading="lazy"></a>
                    <br>
                    <a href="<?= base_url(); ?>biaya"><img src="assets/images/Franchise r2-01-01.png" class="d-block w-100" alt="..." loading="lazy"></a>
                    <br>
                    <a href="<?= base_url(); ?>bergabung"><img src="assets/images/Franchise r3-01-01-01.png" class="d-block w-100" alt="..." loading="lazy"></a>
                    </div>
                    <div class="col-6 col-lg-6" style="padding: 12% 0 0 4%;">
                        <h2 class="text-uppercase pb-1" style="color: #ffcc00;">Cara Bergabung</h2>
                        <p class="font-rajdhani" style="color: #fff; ">Jika anda ingin bergabung gengan kami, berikut adalah informasi tentang proses kerjasama yang dilakukandalam jangka waktu kurang lebih 1 bulan</p>
                    </div>
                </div>
            </div>
        </header>
        <!-- About Section-->
        <section class="page-section portfolio" id="about" style="background-color: #f2ebe4;background-size: cover;">
            <hr style="height:2px;border-width:0;color:gray;background-color:gray">
        <div class="container pt-5 mw-90">
            <div class="row w-75 mx-auto">
                <div class="col-8 col-lg-8" >
                    <img src="assets/images/Cara Bergabung-01-min.png" class="d-block w-100" alt="..." loading="lazy">
                </div>
                <div class="col-4 col-lg-4" >
                    <ul style="padding: 8% 0px 0px 10%;font-size: 20px;font-family: rajdhani, sans-serif !important;font-weight: 600;">
                      <p>1. Share lokasi usaha kemudian kita cek berdekatan atau tidak dengan mitra Lup Lup yang sudah ada.</p>
                      <p>2. Kita ada 3 macam bentuk kemitraan </p>
                      <p>3. melakukan pembayaran DP 80% dari nilai paket yang ingin diambil.</p>
                      <p>4. Pengerjaan booth dan perlengkapan maksimal 14 hari kerja.</p>
                      <p>5. Selanjutnya melakukan pelunasan 20% + Ongkos kirim, setelah lunas paket akan dikirim.</p>
                    </ul>
                </div>
            </div>
        </div>
        </section>
        <section class="page-section portfolio" id="about" style="background-color: #f2ebe4;background-size: cover;">
        <div class="container pt-5 mw-90" style="background-color: #ffcc00; margin: 60px; padding: 60px">
            <h2>Untuk pertanyaan lebih lanjut <br> dapat menghubungi kami dibawah ini.</h2>
            <hr style="height:2px;border-width:0;color:#333;background-color:gray">
            <div class="row w-75 mx-auto">
                <div class="col-4 col-lg-4" >
                    <img src="assets/images/Cara Bergabung-01-min.png" class="d-block w-100" alt="..." style="width: 350px !important;" loading="lazy">
                </div>
                <div class="col-8 col-lg-8" >
                    <ul style="padding: 5% 0 0 25%;font-size: 20px;font-family: rajdhani, sans-serif !important;font-weight: 600;">
                      <p>Jl. Ciputat Raya No.20 A Rt.08 / Rw.12, Pondok Pinang, Kebayoran Lama Kota Jakarta Selatan, DKI Jakarta 12310</p>
                      <p>Telphone:      +62 857 XXXX XXXX</p>
                      <p>Email:         Info@luplup.id</p>
                    </ul>
                </div>
            </div>
        </div>
        </section>
        <?php include('part/footer.php') ?>
    </body>
</html>
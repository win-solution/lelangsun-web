<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <style type="text/css">
      .flex-caption {
        border-top: none;
        border-left: 3px solid darkgrey;
        border-bottom: 3px solid darkgrey;
        border-right: 3px solid darkgrey;
        background-color: #fff;
        text-shadow: 0 -1px 0 rgba(0,0,0,.3);
        font-size: 14px;
        line-height: 18px;
      }
    </style>
    <style type="text/css">
        
        .direct-chat .card-body {
          overflow-x: hidden;
          padding: 0;
          position: relative;
        }

        .direct-chat.chat-pane-open .direct-chat-contacts {
          -webkit-transform: translate(0, 0);
          transform: translate(0, 0);
        }

        .direct-chat.timestamp-light .direct-chat-timestamp {
          color: #30465f;
        }

        .direct-chat.timestamp-dark .direct-chat-timestamp {
          color: #cccccc;
        }

        .direct-chat-messages {
          -webkit-transform: translate(0, 0);
          transform: translate(0, 0);
          height: 250px;
          overflow: auto;
          padding: 10px;
        }

        .direct-chat-msg,
        .direct-chat-text {
          display: block;
        }

        .direct-chat-msg {
          margin-bottom: 10px;
        }

        .direct-chat-msg::after {
          display: block;
          clear: both;
          content: "";
        }

        .direct-chat-messages,
        .direct-chat-contacts {
          transition: -webkit-transform .5s ease-in-out;
          transition: transform .5s ease-in-out;
          transition: transform .5s ease-in-out, -webkit-transform .5s ease-in-out;
        }

        .direct-chat-text {
          border-radius: 0.3rem;
          background-color: #d2d6de;
          border: 1px solid #d2d6de;
          color: #444;
          margin: 5px 0 0 50px;
          padding: 5px 10px;
          position: relative;
        }

        .direct-chat-text::after, .direct-chat-text::before {
          border: solid transparent;
          border-right-color: #d2d6de;
          content: " ";
          height: 0;
          pointer-events: none;
          position: absolute;
          right: 100%;
          top: 15px;
          width: 0;
        }

        .direct-chat-text::after {
          border-width: 5px;
          margin-top: -5px;
        }

        .direct-chat-text::before {
          border-width: 6px;
          margin-top: -6px;
        }

        .right .direct-chat-text {
          margin-left: 0;
          margin-right: 50px;
        }

        .right .direct-chat-text::after, .right .direct-chat-text::before {
          border-left-color: #d2d6de;
          border-right-color: transparent;
          left: 100%;
          right: auto;
        }

        .direct-chat-img {
          border-radius: 50%;
          float: left;
          height: 40px;
          width: 40px;
        }

        .right .direct-chat-img {
          float: right;
        }

        .direct-chat-infos {
          display: block;
          font-size: 0.875rem;
          margin-bottom: 2px;
        }

        .direct-chat-name {
          font-weight: 600;
        }

        .direct-chat-timestamp {
          color: #697582;
        }

        .direct-chat-contacts-open .direct-chat-contacts {
          -webkit-transform: translate(0, 0);
          transform: translate(0, 0);
        }

        .direct-chat-contacts {
          -webkit-transform: translate(101%, 0);
          transform: translate(101%, 0);
          background-color: #343a40;
          bottom: 0;
          color: #fff;
          height: 250px;
          overflow: auto;
          position: absolute;
          top: 0;
          width: 100%;
        }

        .direct-chat-contacts-light {
          background-color: #f8f9fa;
        }

        .direct-chat-contacts-light .contacts-list-name {
          color: #495057;
        }

        .direct-chat-contacts-light .contacts-list-date {
          color: #6c757d;
        }

        .direct-chat-contacts-light .contacts-list-msg {
          color: #545b62;
        }

        .contacts-list {
          padding-left: 0;
          list-style: none;
        }

        .contacts-list > li {
          border-bottom: 1px solid rgba(0, 0, 0, 0.2);
          margin: 0;
          padding: 10px;
        }

        .contacts-list > li::after {
          display: block;
          clear: both;
          content: "";
        }

        .contacts-list > li:last-of-type {
          border-bottom: 0;
        }

        .contacts-list-img {
          border-radius: 50%;
          float: left;
          width: 40px;
        }

        .contacts-list-info {
          color: #fff;
          margin-left: 45px;
        }

        .contacts-list-name,
        .contacts-list-status {
          display: block;
        }

        .contacts-list-name {
          font-weight: 600;
        }

        .contacts-list-status {
          font-size: 0.875rem;
        }

        .contacts-list-date {
          color: #ced4da;
          font-weight: 400;
        }

        .contacts-list-msg {
          color: #b1bbc4;
        }

        .direct-chat-primary .right > .direct-chat-text {
          background-color: #007bff;
          border-color: #007bff;
          color: #fff;
        }

        .direct-chat-primary .right > .direct-chat-text::after, .direct-chat-primary .right > .direct-chat-text::before {
          border-left-color: #007bff;
        }

        .direct-chat-secondary .right > .direct-chat-text {
          background-color: #6c757d;
          border-color: #6c757d;
          color: #fff;
        }

        .direct-chat-secondary .right > .direct-chat-text::after, .direct-chat-secondary .right > .direct-chat-text::before {
          border-left-color: #6c757d;
        }

        .direct-chat-success .right > .direct-chat-text {
          background-color: #28a745;
          border-color: #28a745;
          color: #fff;
        }

        .direct-chat-success .right > .direct-chat-text::after, .direct-chat-success .right > .direct-chat-text::before {
          border-left-color: #28a745;
        }

        .direct-chat-info .right > .direct-chat-text {
          background-color: #17a2b8;
          border-color: #17a2b8;
          color: #fff;
        }

        .direct-chat-info .right > .direct-chat-text::after, .direct-chat-info .right > .direct-chat-text::before {
          border-left-color: #17a2b8;
        }

        .direct-chat-warning .right > .direct-chat-text {
          background-color: #ffc107;
          border-color: #ffc107;
          color: #1f2d3d;
        }

        .direct-chat-warning .right > .direct-chat-text::after, .direct-chat-warning .right > .direct-chat-text::before {
          border-left-color: #ffc107;
        }

        .direct-chat-danger .right > .direct-chat-text {
          background-color: #dc3545;
          border-color: #dc3545;
          color: #fff;
        }

        .direct-chat-danger .right > .direct-chat-text::after, .direct-chat-danger .right > .direct-chat-text::before {
          border-left-color: #dc3545;
        }

        .direct-chat-light .right > .direct-chat-text {
          background-color: #f8f9fa;
          border-color: #f8f9fa;
          color: #1f2d3d;
        }

        .direct-chat-light .right > .direct-chat-text::after, .direct-chat-light .right > .direct-chat-text::before {
          border-left-color: #f8f9fa;
        }

        .direct-chat-dark .right > .direct-chat-text {
          background-color: #343a40;
          border-color: #343a40;
          color: #fff;
        }

        .direct-chat-dark .right > .direct-chat-text::after, .direct-chat-dark .right > .direct-chat-text::before {
          border-left-color: #343a40;
        }

        .direct-chat-lightblue .right > .direct-chat-text {
          background-color: #3c8dbc;
          border-color: #3c8dbc;
          color: #fff;
        }

        .direct-chat-lightblue .right > .direct-chat-text::after, .direct-chat-lightblue .right > .direct-chat-text::before {
          border-left-color: #3c8dbc;
        }

        .direct-chat-navy .right > .direct-chat-text {
          background-color: #001f3f;
          border-color: #001f3f;
          color: #fff;
        }

        .direct-chat-navy .right > .direct-chat-text::after, .direct-chat-navy .right > .direct-chat-text::before {
          border-left-color: #001f3f;
        }

        .direct-chat-olive .right > .direct-chat-text {
          background-color: #3d9970;
          border-color: #3d9970;
          color: #fff;
        }

        .direct-chat-olive .right > .direct-chat-text::after, .direct-chat-olive .right > .direct-chat-text::before {
          border-left-color: #3d9970;
        }

        .direct-chat-lime .right > .direct-chat-text {
          background-color: #01ff70;
          border-color: #01ff70;
          color: #1f2d3d;
        }

        .direct-chat-lime .right > .direct-chat-text::after, .direct-chat-lime .right > .direct-chat-text::before {
          border-left-color: #01ff70;
        }

        .direct-chat-fuchsia .right > .direct-chat-text {
          background-color: #f012be;
          border-color: #f012be;
          color: #fff;
        }

        .direct-chat-fuchsia .right > .direct-chat-text::after, .direct-chat-fuchsia .right > .direct-chat-text::before {
          border-left-color: #f012be;
        }

        .direct-chat-maroon .right > .direct-chat-text {
          background-color: #d81b60;
          border-color: #d81b60;
          color: #fff;
        }

        .direct-chat-maroon .right > .direct-chat-text::after, .direct-chat-maroon .right > .direct-chat-text::before {
          border-left-color: #d81b60;
        }

        .direct-chat-blue .right > .direct-chat-text {
          background-color: #007bff;
          border-color: #007bff;
          color: #fff;
        }

        .direct-chat-blue .right > .direct-chat-text::after, .direct-chat-blue .right > .direct-chat-text::before {
          border-left-color: #007bff;
        }

        .direct-chat-indigo .right > .direct-chat-text {
          background-color: #6610f2;
          border-color: #6610f2;
          color: #fff;
        }

        .direct-chat-indigo .right > .direct-chat-text::after, .direct-chat-indigo .right > .direct-chat-text::before {
          border-left-color: #6610f2;
        }

        .direct-chat-purple .right > .direct-chat-text {
          background-color: #6f42c1;
          border-color: #6f42c1;
          color: #fff;
        }

        .direct-chat-purple .right > .direct-chat-text::after, .direct-chat-purple .right > .direct-chat-text::before {
          border-left-color: #6f42c1;
        }

        .direct-chat-pink .right > .direct-chat-text {
          background-color: #e83e8c;
          border-color: #e83e8c;
          color: #fff;
        }

        .direct-chat-pink .right > .direct-chat-text::after, .direct-chat-pink .right > .direct-chat-text::before {
          border-left-color: #e83e8c;
        }

        .direct-chat-red .right > .direct-chat-text {
          background-color: #dc3545;
          border-color: #dc3545;
          color: #fff;
        }

        .direct-chat-red .right > .direct-chat-text::after, .direct-chat-red .right > .direct-chat-text::before {
          border-left-color: #dc3545;
        }

        .direct-chat-orange .right > .direct-chat-text {
          background-color: #fd7e14;
          border-color: #fd7e14;
          color: #1f2d3d;
        }

        .direct-chat-orange .right > .direct-chat-text::after, .direct-chat-orange .right > .direct-chat-text::before {
          border-left-color: #fd7e14;
        }

        .direct-chat-yellow .right > .direct-chat-text {
          background-color: #ffc107;
          border-color: #ffc107;
          color: #1f2d3d;
        }

        .direct-chat-yellow .right > .direct-chat-text::after, .direct-chat-yellow .right > .direct-chat-text::before {
          border-left-color: #ffc107;
        }

        .direct-chat-green .right > .direct-chat-text {
          background-color: #28a745;
          border-color: #28a745;
          color: #fff;
        }

        .direct-chat-green .right > .direct-chat-text::after, .direct-chat-green .right > .direct-chat-text::before {
          border-left-color: #28a745;
        }

        .direct-chat-teal .right > .direct-chat-text {
          background-color: #20c997;
          border-color: #20c997;
          color: #fff;
        }

        .direct-chat-teal .right > .direct-chat-text::after, .direct-chat-teal .right > .direct-chat-text::before {
          border-left-color: #20c997;
        }

        .direct-chat-cyan .right > .direct-chat-text {
          background-color: #17a2b8;
          border-color: #17a2b8;
          color: #fff;
        }

        .direct-chat-cyan .right > .direct-chat-text::after, .direct-chat-cyan .right > .direct-chat-text::before {
          border-left-color: #17a2b8;
        }

        .direct-chat-white .right > .direct-chat-text {
          background-color: #fff;
          border-color: #fff;
          color: #1f2d3d;
        }

        .direct-chat-white .right > .direct-chat-text::after, .direct-chat-white .right > .direct-chat-text::before {
          border-left-color: #fff;
        }

        .direct-chat-gray .right > .direct-chat-text {
          background-color: #6c757d;
          border-color: #6c757d;
          color: #fff;
        }

        .direct-chat-gray .right > .direct-chat-text::after, .direct-chat-gray .right > .direct-chat-text::before {
          border-left-color: #6c757d;
        }

        .direct-chat-gray-dark .right > .direct-chat-text {
          background-color: #343a40;
          border-color: #343a40;
          color: #fff;
        }

        .direct-chat-gray-dark .right > .direct-chat-text::after, .direct-chat-gray-dark .right > .direct-chat-text::before {
          border-left-color: #343a40;
        }

        .dark-mode .direct-chat-text {
          background-color: #454d55;
          border-color: #4b545c;
          color: #fff;
        }

        .dark-mode .direct-chat-text::after, .dark-mode .direct-chat-text::before {
          border-right-color: #4b545c;
        }

        .dark-mode .direct-chat-timestamp {
          color: #adb5bd;
        }

        .dark-mode .right > .direct-chat-text::after, .dark-mode .right > .direct-chat-text::before {
          border-right-color: transparent;
        }

        .dark-mode .direct-chat-primary .right > .direct-chat-text {
          background-color: #3f6791;
          border-color: #3f6791;
          color: #fff;
        }

        .dark-mode .direct-chat-primary .right > .direct-chat-text::after, .dark-mode .direct-chat-primary .right > .direct-chat-text::before {
          border-left-color: #3f6791;
        }

        .dark-mode .direct-chat-secondary .right > .direct-chat-text {
          background-color: #6c757d;
          border-color: #6c757d;
          color: #fff;
        }

        .dark-mode .direct-chat-secondary .right > .direct-chat-text::after, .dark-mode .direct-chat-secondary .right > .direct-chat-text::before {
          border-left-color: #6c757d;
        }

        .dark-mode .direct-chat-success .right > .direct-chat-text {
          background-color: #00bc8c;
          border-color: #00bc8c;
          color: #fff;
        }

        .dark-mode .direct-chat-success .right > .direct-chat-text::after, .dark-mode .direct-chat-success .right > .direct-chat-text::before {
          border-left-color: #00bc8c;
        }

        .dark-mode .direct-chat-info .right > .direct-chat-text {
          background-color: #3498db;
          border-color: #3498db;
          color: #fff;
        }

        .dark-mode .direct-chat-info .right > .direct-chat-text::after, .dark-mode .direct-chat-info .right > .direct-chat-text::before {
          border-left-color: #3498db;
        }

        .dark-mode .direct-chat-warning .right > .direct-chat-text {
          background-color: #f39c12;
          border-color: #f39c12;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-warning .right > .direct-chat-text::after, .dark-mode .direct-chat-warning .right > .direct-chat-text::before {
          border-left-color: #f39c12;
        }

        .dark-mode .direct-chat-danger .right > .direct-chat-text {
          background-color: #e74c3c;
          border-color: #e74c3c;
          color: #fff;
        }

        .dark-mode .direct-chat-danger .right > .direct-chat-text::after, .dark-mode .direct-chat-danger .right > .direct-chat-text::before {
          border-left-color: #e74c3c;
        }

        .dark-mode .direct-chat-light .right > .direct-chat-text {
          background-color: #f8f9fa;
          border-color: #f8f9fa;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-light .right > .direct-chat-text::after, .dark-mode .direct-chat-light .right > .direct-chat-text::before {
          border-left-color: #f8f9fa;
        }

        .dark-mode .direct-chat-dark .right > .direct-chat-text {
          background-color: #343a40;
          border-color: #343a40;
          color: #fff;
        }

        .dark-mode .direct-chat-dark .right > .direct-chat-text::after, .dark-mode .direct-chat-dark .right > .direct-chat-text::before {
          border-left-color: #343a40;
        }

        .dark-mode .direct-chat-lightblue .right > .direct-chat-text {
          background-color: #86bad8;
          border-color: #86bad8;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-lightblue .right > .direct-chat-text::after, .dark-mode .direct-chat-lightblue .right > .direct-chat-text::before {
          border-left-color: #86bad8;
        }

        .dark-mode .direct-chat-navy .right > .direct-chat-text {
          background-color: #002c59;
          border-color: #002c59;
          color: #fff;
        }

        .dark-mode .direct-chat-navy .right > .direct-chat-text::after, .dark-mode .direct-chat-navy .right > .direct-chat-text::before {
          border-left-color: #002c59;
        }

        .dark-mode .direct-chat-olive .right > .direct-chat-text {
          background-color: #74c8a3;
          border-color: #74c8a3;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-olive .right > .direct-chat-text::after, .dark-mode .direct-chat-olive .right > .direct-chat-text::before {
          border-left-color: #74c8a3;
        }

        .dark-mode .direct-chat-lime .right > .direct-chat-text {
          background-color: #67ffa9;
          border-color: #67ffa9;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-lime .right > .direct-chat-text::after, .dark-mode .direct-chat-lime .right > .direct-chat-text::before {
          border-left-color: #67ffa9;
        }

        .dark-mode .direct-chat-fuchsia .right > .direct-chat-text {
          background-color: #f672d8;
          border-color: #f672d8;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-fuchsia .right > .direct-chat-text::after, .dark-mode .direct-chat-fuchsia .right > .direct-chat-text::before {
          border-left-color: #f672d8;
        }

        .dark-mode .direct-chat-maroon .right > .direct-chat-text {
          background-color: #ed6c9b;
          border-color: #ed6c9b;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-maroon .right > .direct-chat-text::after, .dark-mode .direct-chat-maroon .right > .direct-chat-text::before {
          border-left-color: #ed6c9b;
        }

        .dark-mode .direct-chat-blue .right > .direct-chat-text {
          background-color: #3f6791;
          border-color: #3f6791;
          color: #fff;
        }

        .dark-mode .direct-chat-blue .right > .direct-chat-text::after, .dark-mode .direct-chat-blue .right > .direct-chat-text::before {
          border-left-color: #3f6791;
        }

        .dark-mode .direct-chat-indigo .right > .direct-chat-text {
          background-color: #6610f2;
          border-color: #6610f2;
          color: #fff;
        }

        .dark-mode .direct-chat-indigo .right > .direct-chat-text::after, .dark-mode .direct-chat-indigo .right > .direct-chat-text::before {
          border-left-color: #6610f2;
        }

        .dark-mode .direct-chat-purple .right > .direct-chat-text {
          background-color: #6f42c1;
          border-color: #6f42c1;
          color: #fff;
        }

        .dark-mode .direct-chat-purple .right > .direct-chat-text::after, .dark-mode .direct-chat-purple .right > .direct-chat-text::before {
          border-left-color: #6f42c1;
        }

        .dark-mode .direct-chat-pink .right > .direct-chat-text {
          background-color: #e83e8c;
          border-color: #e83e8c;
          color: #fff;
        }

        .dark-mode .direct-chat-pink .right > .direct-chat-text::after, .dark-mode .direct-chat-pink .right > .direct-chat-text::before {
          border-left-color: #e83e8c;
        }

        .dark-mode .direct-chat-red .right > .direct-chat-text {
          background-color: #e74c3c;
          border-color: #e74c3c;
          color: #fff;
        }

        .dark-mode .direct-chat-red .right > .direct-chat-text::after, .dark-mode .direct-chat-red .right > .direct-chat-text::before {
          border-left-color: #e74c3c;
        }

        .dark-mode .direct-chat-orange .right > .direct-chat-text {
          background-color: #fd7e14;
          border-color: #fd7e14;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-orange .right > .direct-chat-text::after, .dark-mode .direct-chat-orange .right > .direct-chat-text::before {
          border-left-color: #fd7e14;
        }

        .dark-mode .direct-chat-yellow .right > .direct-chat-text {
          background-color: #f39c12;
          border-color: #f39c12;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-yellow .right > .direct-chat-text::after, .dark-mode .direct-chat-yellow .right > .direct-chat-text::before {
          border-left-color: #f39c12;
        }

        .dark-mode .direct-chat-green .right > .direct-chat-text {
          background-color: #00bc8c;
          border-color: #00bc8c;
          color: #fff;
        }

        .dark-mode .direct-chat-green .right > .direct-chat-text::after, .dark-mode .direct-chat-green .right > .direct-chat-text::before {
          border-left-color: #00bc8c;
        }

        .dark-mode .direct-chat-teal .right > .direct-chat-text {
          background-color: #20c997;
          border-color: #20c997;
          color: #fff;
        }

        .dark-mode .direct-chat-teal .right > .direct-chat-text::after, .dark-mode .direct-chat-teal .right > .direct-chat-text::before {
          border-left-color: #20c997;
        }

        .dark-mode .direct-chat-cyan .right > .direct-chat-text {
          background-color: #3498db;
          border-color: #3498db;
          color: #fff;
        }

        .dark-mode .direct-chat-cyan .right > .direct-chat-text::after, .dark-mode .direct-chat-cyan .right > .direct-chat-text::before {
          border-left-color: #3498db;
        }

        .dark-mode .direct-chat-white .right > .direct-chat-text {
          background-color: #fff;
          border-color: #fff;
          color: #1f2d3d;
        }

        .dark-mode .direct-chat-white .right > .direct-chat-text::after, .dark-mode .direct-chat-white .right > .direct-chat-text::before {
          border-left-color: #fff;
        }

        .dark-mode .direct-chat-gray .right > .direct-chat-text {
          background-color: #6c757d;
          border-color: #6c757d;
          color: #fff;
        }

        .dark-mode .direct-chat-gray .right > .direct-chat-text::after, .dark-mode .direct-chat-gray .right > .direct-chat-text::before {
          border-left-color: #6c757d;
        }

        .dark-mode .direct-chat-gray-dark .right > .direct-chat-text {
          background-color: #343a40;
          border-color: #343a40;
          color: #fff;
        }

        .dark-mode .direct-chat-gray-dark .right > .direct-chat-text::after, .dark-mode .direct-chat-gray-dark .right > .direct-chat-text::before {
          border-left-color: #343a40;
        }

        .card-footer {
          padding: 0.75rem 1.25rem;
          background-color: rgba(0, 0, 0, 0.03);
          border-top: 0 solid rgba(0, 0, 0, 0.125);
        }

        .card-footer:last-child {
          border-radius: 0 0 calc(0.25rem - 0) calc(0.25rem - 0);
        }

    </style>
    <body id="page-top">

      <section class="page-section pt-5 pb-0 mw-90 mx-auto">
        <div class="container mw-90 text-center">
          <div class="row">
            <div class="col-12 col-lg-6 mx-auto">
              <div>
                <img src="assets/images/revisi/suzuki-ertiga-i_4121.jpg" class="w-100" />
                <p class="d-flex justify-content-between flex-caption p-4 font-black fontlight mb-0">
                  <span>Adventurer Lemon</span>
                  <span>Tahun 2019</span>
                </p>
              </div>
            </div>
            <div class="col-12 col-lg-6 my-auto">
              <div class="card flex-fill">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid black">
                        <div class="row">
                            <div class="col-6 col-md-6 fontlight">
                                <div class=" mb-3"><span class="text-black">Nomor Polisi</span></div>
                                <div class=" mb-3"><span class="text-black">Nomor Rangka</span></div>
                                <div class=" mb-3"><span class="text-black">Tipe Model</span></div>
                                <div class=" mb-3"><span class="text-black">No Mesin</span></div>
                                <div class=" mb-3"><span class="text-black">Warna</span></div>
                                <div class=" mb-3"><span class="text-black">STNK</span></div>
                                <div class=" mb-3"><span class="text-black">Tahun</span></div>
                                <div class=" mb-3"><span class="text-black">BPKB</span></div>
                            </div>
                            <div class="col-6 col-md-6 fontbold">
                                <div class=" mb-3"><span class="text-black font-weight-bold">DT 48 IB</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">JKSDJKLS765</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">KIJANG INNOVA</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">3ZRB366766</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">Ada</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">Ada Sampai 18-11-2021</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">2019</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">Ada</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="">
                        <div class="row">
                            <div class="col-6 col-md-6">
                                <div class=" mb-3"><span class="text-black">Transmisi</span></div>
                                <div class=" mb-3"><span class="text-black">Faktur</span></div>
                                <div class=" mb-3"><span class="text-black">Kapasitas Mobil</span></div>
                                <div class=" mb-3"><span class="text-black">KTP Pemilik</span></div>
                                <div class=" mb-3"><span class="text-black">Bahan Bakar </span></div>
                                <div class=" mb-3"><span class="text-black">Kwitansi</span></div>
                                <div class=" mb-3"><span class="text-black">Odometer</span></div>
                                <div class=" mb-3"><span class="text-black">Tahun</span></div>
                            </div>
                            <div class="col-6 col-md-6">
                                <div class=" mb-3"><span class="text-black font-weight-bold">M/T</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">TIdak Ada</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">3.000 cc</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">Ada</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">Bensin</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">Ada</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">10.786</span></div>
                                <div class=" mb-3"><span class="text-black font-weight-bold">2019</span></div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="page-section pt-5">
        <div class="container mw-90 text-center">
          <div class="text-center">
            <h1><!-- <i class="fas fa-hand-holding-usd" style="color: #ff4500"></i>  -->Rp. 125.500.000</h1>
          </div>
        </div>

        <div class="mw-35 mx-auto">

          <div class="card direct-chat direct-chat-primary" style="box-shadow: none; background-image: url('assets/images/baru/baru 5-01.png'); background-position: center;">
            <!-- /.card-header -->
            <div class="card-body">
              <!-- Conversations are loaded here -->
              <div class="direct-chat-messages">
                <!-- Message. Default to the left -->
                <div class="direct-chat-msg">
                  <div class="direct-chat-infos clearfix">
                    <span class="direct-chat-name float-left">NPL 350</span>
                    <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
                  </div>
                  <!-- /.direct-chat-infos -->
                  <img class="direct-chat-img" src="assets/images/profile_picture.png" alt="message user image">
                  <!-- /.direct-chat-img -->
                  <div class="direct-chat-text">
                    Rp. 70.500.000
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->

                <!-- Message to the right -->
                <div class="direct-chat-msg right">
                  <div class="direct-chat-infos clearfix">
                    <span class="direct-chat-name float-right">NPL 255</span>
                    <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
                  </div>
                  <!-- /.direct-chat-infos -->
                  <img class="direct-chat-img" src="assets/images/profile_picture.png" alt="message user image">
                  <!-- /.direct-chat-img -->
                  <div class="direct-chat-text">
                    Rp. 73.500.000
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->

                <!-- Message. Default to the left -->
                <div class="direct-chat-msg">
                  <div class="direct-chat-infos clearfix">
                    <span class="direct-chat-name float-left">NPL 444</span>
                    <span class="direct-chat-timestamp float-right">23 Jan 5:37 pm</span>
                  </div>
                  <!-- /.direct-chat-infos -->
                  <img class="direct-chat-img" src="assets/images/profile_picture.png" alt="message user image">
                  <!-- /.direct-chat-img -->
                  <div class="direct-chat-text">
                    Rp. 75.000.000
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->

                <!-- Message to the right -->
                <div class="direct-chat-msg right">
                  <div class="direct-chat-infos clearfix">
                    <span class="direct-chat-name float-right">NPL 255</span>
                    <span class="direct-chat-timestamp float-left">23 Jan 6:10 pm</span>
                  </div>
                  <!-- /.direct-chat-infos -->
                  <img class="direct-chat-img" src="assets/images/profile_picture.png" alt="message user image">
                  <!-- /.direct-chat-img -->
                  <div class="direct-chat-text">
                    Rp. 80.500.000
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->

              </div>
              <!--/.direct-chat-messages-->
            </div>
            <!-- /.card-body -->
            
          </div>
          <!--/.direct-chat -->

        </div>

        <div>
          <h6 class="text-uppercase pb-1 pt-3 text-center" style="color: #333;">Countdown Timer</h6>
          <div id="countdown" class="text-center w-100" style="display: inline-flex; align-items: center; justify-content: center;">10:00</div>
          <h2 class="text-center">NPL 32</h2>
          <!-- <div id="countdown">
            <ul>
              <li><span id="days"></span>days</li>
              <li><span id="hours"></span>Hours</li>
              <li><span id="minutes"></span>Minutes</li>
              <li><span id="seconds"></span>Seconds</li>
            </ul>
          </div> -->
        </div>

        <div>

          <div class="mx-auto text-center mw-35">
              <a href="<?= base_url(); ?>/lelang"><span class="btn btn-primary btn-lg btn-block" onclick="checkAuth();" style="border-radius: 3rem;">Bidding</span></a>
          </div>
          
        </div>
      </section>

    </body>
    <script type="text/javascript">

      const startingMinutes = 10;
      let time = startingMinutes * 60;

      const countdownEl = document.getElementById('countdown');

      setInterval(updateCountdown, 1000);

      function updateCountdown() {
        let minutes = Math.floor(time / 60);
        let seconds = time % 60;

        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;

        countdownEl.innerHTML = `<div style='border: 3px solid #4c4c4c; border-radius: 10px; padding: 5px;'><h1 class='mb-0'>${minutes}</h1></div><div class='m-2'><h1 class='mb-0'> : </h1></div><div style='border: 3px solid #4c4c4c; border-radius: 10px; padding: 5px;'><h1 class='mb-0'>${seconds}</h1></div>`;
        time--;
      }

    </script>
</html>
<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        
        <section class="page-section">

          <div class="container mw-75 py-3">

            <div class="row">
              
              <div class="col my-auto">
                <h3>PT. BALAI LELANG SUN</h3>
                <ul class="list-unstyled">
                  <li>
                    <span><b>Auction Office </b></span>
                    : Jl. HOS Cokroaminoto No.59 Ruko <br>Ciledug Mas D3 Ciledug Tanggerang
                  </li>
                </ul>
              </div>

              <!-- <div class="col d-flex justify-content-center my-auto">
                <ul class="ml-4 mb-0 fa-ul">
                  <li><span class="fa-li"><i class="fas fa-phone-alt"></i></span>Phone : (021) 22271959</li>
                  <li><span class="fa-li"><i class="fab fa-youtube"></i></span>Youtube : BalaiLelangSun</li>
                  <li><span class="fa-li"><i class="fab fa-facebook-square"></i></span>Facebook : BalaiLelangSun</li>
                  <li><span class="fa-li"><i class="fab fa-instagram-square"></i></span>Instagram : BalaiLelangSun</li>
                </ul>
              </div> -->

              <div class="col text-right">
                <img src="assets/images/logo.png" alt="...">
              </div>

            </div>

            <div class="row border">
              
              <div class="col my-auto">
                <h1>Formulir Pendaftaran Peserta Lelang</h1>
              </div>

              <div class="col">

                <div class="row">

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Nomor Urut </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span>20 <hr class="m-0 mb-1"></span>
                  </div>

                  <div class="col-lg-4 pr-0">
                    <div class="row">
                      <div class="col-lg-9">
                        <span>Tanggal & Jam </span>
                      </div>
                      <div class="col-lg-3">
                        <span>:</span>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-8 pl-0">
                    <span>25/03/2021    18:50:55<hr class="m-0 mb-1"></span>
                  </div>

                </div>

              </div>

            </div>

            <br>

            <div class="row">

              <div class="col-lg-12 mb-3 px-0">
                  
                <div class="border" style="background-color: black; color: white;">
                  <div>
                    <h4 class="col my-1">Data Peserta</h4>
                  </div>
                </div>

                <div class="border col-lg-12 py-3">
                  
                  <div class="row">

                    <div class="col-lg-6">
                      
                      <div class="row">
                        
                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Nama Peserta </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>No. KTP </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Alamat </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <div class="row">
                            <div class="col-lg-6">
                              <span></span>
                            </div>
                            <div class="col-lg-6">
                              <span>Kode Pos :</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span>Telepon/HP </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                      </div>

                    </div>

                    <div class="col-lg-6">
                      
                      <div class="row">
                        
                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9 px-0">
                              <span>Bertindak Mewakili </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9 px-0">
                              <span>Nama Perusahaan </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9 px-0">
                              <span>Alamat Perusahaan </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <div class="row">
                            <div class="col-lg-6">
                              <span></span>
                            </div>
                            <div class="col-lg-6">
                              <span>Kode Pos :</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9 px-0">
                              <span>Telp Kantor/Fax </span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                        <div class="col-lg-4 pr-0">
                          <div class="row">
                            <div class="col-lg-9">
                              <span></span>
                            </div>
                            <div class="col-lg-3">
                              <span>:</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                          <span></span>
                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

              <div class="col-lg-6 mb-3 px-0">
                  
                <div class="border" style="background-color: black; color: white;">
                  <div>
                    <h4 class="col my-1">Data Jaminan</h4>
                  </div>
                </div>

                <div class="border col py-3">
                  
                  <div class="row">

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span>Jumlah Jaminan </span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span></span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span>Nama </span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span></span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span>Alamat </span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span></span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <div class="row">
                        <div class="col-lg-6">
                          <span></span>
                        </div>
                        <div class="col-lg-6">
                          <span>Kode Pos :</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4 pr-0">
                      <div class="row">
                        <div class="col-lg-9">
                          <span></span>
                        </div>
                        <div class="col-lg-3">
                          <span>:</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                      <span></span>
                    </div>

                  </div>

                </div>

              </div>

              <div class="col-lg-6 pl-0 pr-1">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                      
                    <div class="border">
                      <div>
                        <span class="m-1">NPL Diterima</span>
                      </div>
                    </div>

                    <div class="border px-3 py-3">
                      
                      <div class="row mx-auto">

                        <div class="col-lg-12 pb-2">
                          <div class="row">
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 pb-2">
                          <div class="row">
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 pb-2">
                          <div class="row">
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 pb-2">
                          <div class="row">
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

              <div class="col-lg-6 pl-1 pr-0">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                      
                    <div class="border">
                      <div>
                        <span class="m-1">NPL Dikembalikan</span>
                      </div>
                    </div>

                    <div class="border px-3 py-3">
                      
                      <div class="row mx-auto">

                        <div class="col-lg-12 pb-2">
                          <div class="row">
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 pb-2">
                          <div class="row">
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 pb-2">
                          <div class="row">
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 pb-2">
                          <div class="row">
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                            <div class="col p-3 border">
                              <span></span>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

            <div class="row">
              
              <div class="col-lg-6 px-0">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                    
                    <div class="row mx-auto ">
                      <div class="col-lg-4 pr-0">
                        <div class="row">
                          <div class="col-lg-9">
                            <span>Diterima Tanggal & Jam </span>
                          </div>
                          <div class="col-lg-3">
                            <span>:</span>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                        <span></span>
                      </div>
                    </div>

                  </div>

                  <div class="col-lg-6 pr-0">
                      
                      <div class="border">
                        <div class="text-center p-3">
                          <span class="m-1">Penerima</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                            <span>Penerima</span>
                          </div>

                        </div>

                      </div>

                      <div class="border">
                        <div class=" text-center">
                          <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                        </div>
                      </div>

                      <span class="small">*Mohon ditulis dengan nama jelas</span>

                  </div>

                  <div class="col-lg-6 pl-0">
                      
                      <div class="border">
                        <div class="text-center p-3">
                          <span class="m-1">Auction Officer</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                            <span>Auction Officer</span>
                          </div>

                        </div>

                      </div>

                      <div class="border">
                        <div class=" text-center">
                          <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                        </div>
                      </div>

                  </div>

                </div>

              </div>

              <div class="col-lg-6 px-0">

                <div class="row">

                  <div class="col-lg-12 mb-3">
                    
                    <div class="row mx-auto ">
                      <div class="col-lg-4 pr-0">
                        <div class="row">
                          <div class="col-lg-9">
                            <span>Diterima Tanggal & Jam </span>
                          </div>
                          <div class="col-lg-3">
                            <span>:</span>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-8 pl-0 border-bottom" style="max-width: 65%;">
                        <span></span>
                      </div>
                    </div>

                  </div>

                  <div class="col-lg-6 pr-0">
                      
                      <div class="border">
                        <div class="text-center p-3">
                          <span class="m-1">Penerima</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                            <span>Penerima</span>
                          </div>

                        </div>

                      </div>

                      <div class="border">
                        <div class=" text-center">
                          <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                        </div>
                      </div>

                      <span class="small">*Mohon ditulis dengan nama jelas</span>

                  </div>

                  <div class="col-lg-6 pl-0">
                      
                      <div class="border">
                        <div class="text-center p-3">
                          <span class="m-1">Auction Officer</span>
                        </div>
                      </div>

                      <div class="border px-3">
                        
                        <div class="row">

                          <div class="col-lg-12 text-center" style="padding-top: 10vh;">
                            <span>Auction Officer</span>
                          </div>

                        </div>

                      </div>

                      <div class="border">
                        <div class=" text-center">
                          <span class="m-1 text-uppercase">25/03/2021 18:50</span>
                        </div>
                      </div>

                  </div>

                </div>

              </div>

                  

                </div>

              </div>

            </div>

          </div>

        </section>
        
    </body>
</html>
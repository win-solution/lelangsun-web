<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <style type="text/css">
      .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -35px;
        position: relative;
        z-index: 2;
      }
    </style>
    <body id="page-top">
        
        <section class="" style="background-image: url('assets/images/mobil5.jpg'); height: 89vh; background-position: center; background-repeat: no-repeat;">

            <div style=" background-color: #000000e0; height: 100%">

                <div class="row mx-auto" style="height: 100vh">
                <!-- <div class="container">   -->
                    <div class="mx-auto my-auto col-12 col-lg-4" >
                      <h1 class="d-flex justify-content-center fontbold text-white">Login Lelang</h1>
                        <form id="formLogin" name="formLogin" action="" method="post" class="text-white">
                          <div class="form-group has-feedback">
                              <label for="exampleInputEmail1" class="form-label fontlight">Email address</label>
                              <input type="text" name="email" id="email" class="form-control input-borderless" placeholder="Email" required="">
                              <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
                          </div>
                          <div class="form-group has-feedback">
                              <label for="password" class="form-label">Password</label>
                              <div class="input-group mb-3">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="">
                                <div class="input-group-append">
                                  <span toggle="#password" class="input-group-text toggle-password"><i class="fas fa-eye"></i></span>
                                </div>
                              </div>
                          </div>
                              <!-- <h5 class="pull-right">Lupa Password?</h5> -->
                          <div class="row pt-5">
                              <div class="container">
                                  <div class="pull-right d-flex justify-content-center pb-2">
                                      <!-- <span class="btn btn-primary btn-block btn-flat bg-red" onclick="checkAuth();" >Masuk</span> -->
                                      <button type="submit" id="submit" name="submit" value="submit" class="btn btn-primary btn-block btn-flat bg-red"> Masuk</button>
                                  </div>
                              </div>
                          </div>

                          <div class="mb-3 form-check pb-4 text-center">
                            <!-- <a href="forgot.html" class="">Lupa Kata Sandi?</a> -->
                            <span> Belum memiliki akun? </span>
                            <br>
                            <a href="<?= base_url(); ?>registrasi" style="color: red;">Registrasi Disini</a>
                          </div>

                        </form>
                      </div>
                    </div>
                <!-- </div> -->
                </div>

            </div>

        </section>

        <div class="copyright py-4 text-center text-white">

            <div class="container mw-90">

                <div class="row">

                    <!-- <div class="col-12 mb-3">

                        <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->ig; ?>"><i class="fab fa-fw fa-instagram"></i></a>

                        <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->fb; ?>"><i class="fab fa-fw fa-facebook-f"></i></a>

                        <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->tw; ?>"><i class="fab fa-fw fa-twitter"></i></a>

                    </div> -->

                    <div class="col-12 p-3 text-black" style="font-size: 20px;">

                        <span>2020-2021 SUN BALAI LELANG. DILINDUNGI HAK CIPTA</span>

                    </div>

                </div>

            </div>

        </div>

      <!-- SCRIPT -->

      <script type="text/javascript">
        $(".toggle-password").click(function() {
          $(this).toggleClass("fa-eye-slash");
          var input = $($(this).attr("toggle"));
          if (input.attr("type") == "password") {
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");
            input.attr("type", "text");
          } else {
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
            input.attr("type", "password");
          }
        });
      </script>

    </body>
</html>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJ6D5LH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.2/jquery.flexslider.js"></script>
<script>
    var baseURL = "<?= base_url(); ?>";
    var Toast = Swal.mixin({
        position: 'center',
        showConfirmButton: true,
        timer: 5000
    });
</script>
<style>
    .float {
        position: fixed;
        width: 60px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: #25d366;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        font-size: 30px;
        box-shadow: 2px 2px 3px #999;
        z-index: 100;
    }

    .my-float {
        margin-top: 16px;
    }

    .avatar {
        vertical-align: middle;
        width: 50px;
        height: 50px;
        border-radius: 50%;
    }
</style>

<!-- Navigation-->

<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">

    <div class="container mw-90">

        <a class="navbar-brand js-scroll-trigger" href="<?= base_url() ?>">

            <img src="<?= base_url() ?>assets/images/logo.png" alt="" class="ml-5 logo" srcset="" style="" loading="lazy">
            <!-- <img src="<?= base_url() ?>assets/images/logocimb.png" alt="" class="ml-5 logo" srcset="" style="" loading="lazy"> -->

        </a>

        <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fas fa-bars"></i>

        </button>

        <div class="collapse navbar-collapse overflow-auto" id="navbarResponsive" style="max-height: 70vh;">

            <ul class="navbar-nav ml-auto">

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "index") ? "active" : "" ?>" href="<?= base_url(); ?>">Beranda</a></li>

                <!-- <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "display") ? "active" : "" ?>" href="<?= base_url(); ?>display">Display</a></li> -->

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "lot") ? "active" : "" ?>" href="<?= base_url(); ?>lot">Lot</a></li>

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= (($page == "lelang") || ($page == "detaillelang")) ? "active" : "" ?>" href="<?= base_url(); ?>lelang">Lelang</a></li>

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= (($page == "event") || ($page == "detailevent")) ? "active" : "" ?>" href="<?= base_url(); ?>event">Events</a></li>

                <!-- <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "history") ? "active" : "" ?>" href="<?= base_url(); ?>history">History</a></li> -->

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "kontak") ? "active" : "" ?>" href="<?= base_url(); ?>kontak">Kontak</a></li>

                <?php if ((empty($_SESSION['id_peserta'])) && (empty($_SESSION['login_peserta']))) { ?>
                    <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "login") ? "active" : "" ?>" href="<?= base_url(); ?>login">Log In</a></li>
                <?php } else { ?>
                    <!-- <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "logout") ? "active" : "" ?>" href="<?= base_url(); ?>logout">Log Out</a></li> -->
                    <!-- Notifications Dropdown Menu -->
                    <li class="nav-item my-float">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="far fa-bell" style="font-size: 28px;"></i>
                            <span class="badge badge-warning navbar-badge"><?= ((!empty(@$notifikasi)) && (is_array(@$notifikasi))) ? count(@$notifikasi) : "0" ?></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right p-3" style="transform: translate3d(0,-25%,0);">
                            <div class="dropdown-divider"></div>
                            <?php if ((!empty(@$notifikasi)) && (is_array(@$notifikasi))) { ?>
                                <?php foreach (@$notifikasi as $key => $val) { ?>
                                    <a href="#" class="dropdown-item" onclick="Swal.fire('<?= $val->judul ?>','<?= $val->pesan ?>','info');see_notification(<?= $val->id_notifikasi ?>);">
                                        <i class="fas fa-envelope mr-2"></i> <?= $val->judul ?>
                                        <?= (false) ? '<span class="float-right text-muted text-sm">3 mins</span>' : ''; ?>
                                    </a>
                                <?php } ?>
                            <?php } ?>
                            <div class="dropdown-divider"></div>
                            <!-- <a href="javascript:void(0);" class="dropdown-item dropdown-footer" onclick="see_all_notification();">Mark All As Read Notification</a> -->
                            <a href="<?= base_url('front/profile') ?>" class="dropdown-item dropdown-footer">See All Notification</a>
                        </div>
                    </li>
                    <!-- <li class="nav-item mx-0 mx-lg-1 my-float">                        
                        <div class="dropdown">
                            <a class="btn dropdown-toggle py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bell"></i>
                            </a>
                            <div class="dropdown-menu position-static" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </li> -->
                    <li class="nav-item mx-0 mx-lg-1 my-float">
                        <!-- <a class="nav-link dropdown-toggle px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "profile") ? "active" : "" ?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="<?= base_url('assets/') ?>images/profile_picture.png" alt="Avatar" class="avatar">
                        </a>
                        <div class="dropdown-menu position-static">
                            <a class="dropdown-item" href="<?= base_url('profile'); ?>">Profil</a>
                            <a class="dropdown-item" href="<?= base_url(); ?>logout">
                                <span class="btn btn-danger">Log Out</span>
                            </a>
                        </div> -->

                        <a class="nav-link dropdown-toggle px-0 px-lg-3 px-1 rounded js-scroll-trigger" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="<?= base_url('assets/') ?>images/profile_picture.png" alt="Avatar" class="avatar">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right p-3" style="transform: translate3d(0,-25%,0);">
                            <a class="dropdown-item" href="<?= base_url('profile'); ?>">Profil</a>
                            <a class="dropdown-item" href="<?= base_url(); ?>logout">
                                <span class="btn btn-danger">Log Out</span>
                            </a>
                        </div>
                    </li>
                <?php } ?>

            </ul>

        </div>

    </div>

</nav>

<script>
    function see_notification(id) {
        $.ajax({
            url: "<?= base_url('front/rest_api/see_notification') ?>",
            type: "POST",
            dataType: "JSON",
            data: {
                id_notifikasi: id
            },
            async: false,
            success: function(data) {
                setTimeout(() => {
                    location.reload();
                }, 5 * 1000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                newSWAL("Gagal memperoleh data dari ajax!", "error");
            }
        });
    }
</script>
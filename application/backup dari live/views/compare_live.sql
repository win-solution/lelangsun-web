CREATE TABLE `t_pembelian_npl` (
  `id_pembelian_npl` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `no_pembelian` varchar(255) DEFAULT NULL,
  `verifikasi` tinyint(1) DEFAULT 0,
  `bukti` varchar(555) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `tanggal_transfer` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_pembelian_npl`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4

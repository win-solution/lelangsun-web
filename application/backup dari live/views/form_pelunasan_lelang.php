
<form action="<?= base_url('front/pelunasan_lelang'); ?>" method="post" id="myFormNPL" name="myFormNPL" enctype="multipart/form-data">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger">
                    <strong><?= $this->session->flashdata('error') ?></strong>
                </div>
                <?php } ?>
                <input type="hidden" name="id_pemenang_lelang" id="id_pemenang_lelang" value="<?= @$id_pemenang_lelang ?>">
                <input type="hidden" name="id_peserta" id="id_peserta" value="<?= @$_SESSION['id_peserta'] ?>">
                <input type="hidden" name="tipe_pelunasan" id="tipe_pelunasan" value="2">
                <input type="hidden" name="submit" id="submit" value="submit">
                
                <div class="form-group">
                    <label for="nominal_bayar">Nominal Yang Harus Dibayarkan<span style="color:red;">*</span></label>
                    <input class="form-control" id="nominal_bayar" placeholder="Nominal Transfer" type="text" name="nominal_bayar" value="Rp <?= number_format($detail_pemenang[0]->harga_bidding,0,',','.') ?>" readonly>
                </div>

                <hr>

                <div class="form-group">
                    <label for="no_rek">No Rekening <span style="color:red;">*</span></label>
                    <input class="form-control" id="no_rek" placeholder="No Rekening Peserta" type="text" name="no_rek" onkeypress="return onlyNumberKey(event)" required>
                </div>

                <div class="form-group">
                    <label for="nama_pemilik">Nama Pemilik Rekening <span style="color:red;">*</span></label>
                    <input class="form-control" id="nama_pemilik" placeholder="Nama Pemilik Rekening" type="text" name="nama_pemilik" required>
                </div>
                
                <div class="form-group">
                    <label for="nominal">Nominal <span style="color:red;">*</span></label>
                    <input class="form-control" id="nominal" placeholder="Nominal Transfer" type="number" name="nominal" onkeypress="return onlyNumberKey(event)" required>
                </div>

                <div class="form-group">
                    <label for="tanggal_transfer">Waktu Transfer <span style="color:red;">*</span></label>
                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        <input type="text" id="tanggal_transfer" name="tanggal_transfer" class="form-control datetimepicker-input" data-target="#datetimepicker1" readonly required/>
                    </div>
                </div>
                
                <div class="form-group">
                    <label>Bukti Transaksi</label><br>
                    <img class="img-thumbnail" style="max-width: 15vw;" id="prevBukti" src="" alt="" srcset="">
                </div>

                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="bukti" name="bukti" accept="image/jpg, image/jpeg, image/png"  onchange="getNameFile(this,'labelBukti'); readURL(this,'#prevBukti','#bukti')" readonly required>
                        <label id="labelBukti" class="custom-file-label" for="bukti">Pilih Gambar Bukti Transaksi</label>
                    </div>
                </div>

                <div class="form-group text-right">
                    <span id="text_submit"></span>
                    <input type="submit" id="submit_btn" name="submit" value="Kirim" class="btn btn-success">          
                    <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $( "#myFormNPL" ).validate({
            rules: {
                nama_pemilik: { required: true, },
                no_rek: { required: true, digits: true, },
                nominal: { required: true, digits: true, },
                tanggal_transfer: { required: true, },
                bukti: {
                    required:true,
                    extension: "jpg,jpeg,png",
                    maxsize: 2200000,
                }
            },
            messages: {
                bukti: {
                    required: "Foto bukti pelunasan diperlukan",
                    extension: "Format file yang diijinkan .jpg, .jpeg, .png",
                    maxsize: "File gambar tidak boleh lebih dari 2 MB"
                },
            }
        }); 
        
        $('#datetimepicker1').datetimepicker({
            debug: true,
            format: 'YYYY-MM-DD HH:mm',
            defaultDate: "<?= date('Y-m-d H:i'); ?>",
            maxDate: moment("<?= date('Y-m-d H:i'); ?>", 'YYYY-MM-DD HH:mm'),
            buttons: {showClose:true},
            ignoreReadonly: true, 
        });                  
    });
</script>
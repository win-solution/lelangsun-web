<!DOCTYPE html>
<html lang="en">
<?php include('part/head.php') ?>

<body id="page-top" style="background-image: url('<?= base_url() ?>assets/images/dark2/darkkkk.png'); background-position: bottom; ">

    <style type="text/css">
        /* If the screen size is 601px wide or more, set the font-size of <div> to 80px */
        @media screen and (min-width: 601px) {
            .lelang {
                background-image: url('assets/images/y/web-02.png');
                height: 112vh;
                background-position: left;
                background-size: cover;
                background-repeat: no-repeat;
            }
        }

        /* If the screen size is 600px wide or less, set the font-size of <div> to 30px */
        @media screen and (max-width: 600px) {
            .lelang {
                background-image: url('<?= base_url() ?>assets/images/y/mobile-02.png');
                height: 30vh;
                background-position: left;
                background-size: cover;
                background-repeat: no-repeat;
            }
        }
    </style>

    <?php include('part/nav.php') ?>

    <header class="masthead" style="background-image: url('<?= base_url() ?>assets/images/y/web-05.png'); background-position: bottom;">
        <div class="container mw-100 mh-100 lelang">
            <div class="row justify-content-center h-100 mw-90 mx-auto hidden-mobile">
                <div class="col-12 col-lg-6" style="margin: 22vh 0 0 0;">
                    <div>
                        <h1 class="text-uppercase text-uppercase fontbold" style="font-size: 5rem; color: #ff0000">Lelang</h1>
                        <span class="subheading fontlight" style="font-size: 2.5rem; color: #fff;">Fast Bid !</span>
                    </div>
                </div>
                <div class="col-12 col-lg-6 d-flex justify-content-center text-center" style="margin: 12vh 0 0 0;">
                    <!-- <div>
                            <img src="assets/images/web-06.png" class="card-img-top" alt="...">
                        </div> -->
                </div>
            </div>
        </div>
    </header>

    <section class="page-section portfolio" id="about">
        <h1 style="text-align: center; color: white;">Jadwal Lelang <br> (<?= date('d M Y') ?> - <?= date('d M Y', strtotime(date('d M Y') . "+1 month")) ?> )</h1>
        <?php foreach ($event_lelang as $key => $val) { ?>
            <div class="container mh-100 mw-90 mb-3">
                <div class="row">
                    <div class="col-12 offset-sm-2 col-md-6 mx-auto">
                        <img src="<?= base_url() ?>assets/images/mobil6.jpg" alt="<?= $val->nama_event ?>" class="carousel-cell-image mw-100 mb-3">
                        <div class="promo-box2"></div>
                        <!-- <div class="grade-box2">
                        <p class="mb-0 mt-1">Grade</p>
                        <p class="text-grade2 mb-0">D</p>
                    </div> -->
                    </div>
                    <div class="col-12 col-md-6 my-auto px-5 fontlight text-white">
                        <h1 class="text-uppercase pb-1 fontbold fontbold"><?= $val->nama_event ?></h1>
                        <span class="mr-3"><a href="<?= $val->link_maps ?>" target="_blank"><i class="fas fa-map-marker-alt" style="color: #ff4500"></i> <?= $val->alamat_event ?></a></span>
                        <br>
                        <span class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i> <?= date_format(date_create($val->waktu_event), 'd M Y / H:i') ?> WIB</span>
                        <?php
                        // if ((!empty($_SESSION['id_peserta'])) && ((date_format(date_create($val->waktu_event),'Y-m-d H:i:s')) <= date('Y-m-d H:i:s')) ) {
                        if (((date_format(date_create($val->waktu_event), 'Y-m-d H:i:s')) <= date('Y-m-d H:i:s'))) {
                        ?>
                            <div class="mw-50 pt-3">
                                <a href="<?= base_url('front/detaillelang/' . $val->id_event . '?kode_event=') . $val->kode_event; ?>"><span class="btn btn-primary btn-xl" style="">Masuk</span></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-12">
            <?= $pagingView; ?>
        </div>
    </section>

    <?php include('part/footer.php') ?>

</body>

</html>
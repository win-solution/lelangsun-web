<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <style>
        .error {
            color: red;
        }
    </style>
    <!-- Form Validation -->
    <script src="<?= base_url('assets/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?= base_url('assets/'); ?>plugins/jquery-validation/additional-methods.js"></script>
    <body id="page-top">
        
        <section class="" style=" background-image: url('assets/images/mobil5.jpg'); background-position: center; background-repeat: no-repeat;">

            <div style=" background-color: #000000e0;">

                <div class="row mx-auto">
                <!-- <div class="container">   -->
                    <div class="mx-auto my-auto col-12 col-lg-4" style="padding-top: 5rem; padding-bottom: 5.3rem;">
                      <h1 class="d-flex justify-content-center text-white">Registrasi</h1>
                        <form id="formRegis" name="formRegis" action="" method="post" class="text-white">
                            <input type="hidden" name="submit" value="submit">
                          <div class="form-group has-feedback">
                              <label for="nama" class="form-label">Nama</label>
                              <input type="text" name="nama" id="nama" class="form-control input-borderless" placeholder="Nama" required="">
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                          </div> 
                          <div class="form-group has-feedback">
                              <label for="email" class="form-label">Email</label>
                              <input type="email" name="email" id="email" class="form-control input-borderless" placeholder="Email" required="">
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                          </div> 
                          <div class="form-group has-feedback">
                              <label for="alamat" class="form-label">Alamat</label>
                              <textarea name="alamat" id="alamat" class="form-control input-borderless" placeholder="Alamat" cols="30" rows="5"></textarea>
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                          </div> 
                          <div class="form-group has-feedback">
                              <label for="password" class="form-label">Password</label>
                                <input type="password" name="password" id="password" class="form-control input-borderless" placeholder="Password" required="">
                                <!-- <div class="input-group-append">
                                  <span toggle="#password" class="input-group-text toggle-password"><i class="fas fa-eye"></i></span>
                                </div> -->
                              <!-- <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
                          </div>
                          <div class="form-group has-feedback">
                              <label for="repassword" class="form-label">Confirm Password</label>
                                <input type="password" name="repassword" id="repassword" class="form-control input-borderless" placeholder="Confirm Password" required="">
                                <!-- <div class="input-group-append">
                                  <span toggle="#repassword" class="input-group-text toggle-password"><i class="fas fa-eye"></i></span>
                                </div> -->
                              <!-- <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
                          </div>   
                          <!-- <div class="mb-3 form-check pb-4 text-center">
                            <a href="forgot.html" class="">Lupa Kata Sandi?</a><span> / </span><a href="forgot.html" class="">Registrasi</a>
                          </div> -->
                              <!-- <h5 class="pull-right">Lupa Password?</h5> -->
                          <div class="row pt-5">
                              <div class="container">
                                  <div class="col-12 pull-right d-flex justify-content-center pb-2">
                                      <!-- <span id="submit_btn" class="btn btn-primary btn-block btn-flat bg-red" onclick="save();">Daftar</span> -->
                                      <input type="submit" value="Daftar" id="submit_btn" class="btn btn-primary btn-block btn-flat bg-red">
                                  </div>
                              </div>
                          </div>

                          <div class="mb-3 form-check pb-4 text-center">
                            <span> Sudah memiliki akun? </span><br><a href="<?= base_url(); ?>login" style="color: red;">Login Disini</a>
                          </div>

                        </form>
                      </div>
                    </div>
                <!-- </div> -->
                </div>

            </div>

        </section>
        <script type="text/javascript">
            
            $(document).ready(function () {
                $( "#formRegis" ).validate({
                    rules: {
                        nama: { required: true, },
                        alamat: { required: true, },
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 10
                        },
                        repassword: {
                            equalTo: "#password"
                        }
                    }
                });
            });
            $(".toggle-password").click(function() {
                $(this).toggleClass("fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    $(this).removeClass("fa-eye");
                    $(this).addClass("fa-eye-slash");
                    input.attr("type", "text");
                } else {
                    $(this).removeClass("fa-eye-slash");
                    $(this).addClass("fa-eye");
                    input.attr("type", "password");
            }
            });
      </script>
    </body>
</html>
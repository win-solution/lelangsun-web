<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">    
        <?php include('part/nav.php') ?>
        <script>
            <?php if (!empty($this->session->flashdata('pesan'))) { ?>
                Toast.fire({
                    icon: "success",
                    title: "<?= $this->session->flashdata('pesan') ?>"
                });
            <?php } ?>
        </script>
        <!-- Masthead-->
        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
        <header class="masthead" >
            <div class="container mw-100 mh-100" style=" background-image: url('assets/images/baru/bg-01.png'); height: 40vh; margin-top: 3vh; background-position: center; background-size: cover;">
                <div class="row justify-content-center h-100">
                    <div class="col-12 col-lg-12 my-auto d-flex justify-content-center text-center">
                        <div>
                            <h1 class="text-uppercase text-uppercase text-white fontbold" style="">Kontak</h1>
                            <span class="subheading text-white fontlight" style="font-size: 25px;">Hubungi Kami Disini</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="page-section portfolio" id="about" style="background-image: url('assets/images/y/web-05.png'); background-size: cover; position: relative; background-position: bottom;">
            <div class="container mh-100 mw-90">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <a href="https://www.google.com/maps/place/Balai+Lelang+SUN/@-6.2259323,106.7131172,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69fb62354445c3:0x1c1460e2d8676d6a!8m2!3d-6.2259367!4d106.715303" target="_blank">
                            <div class="card-body text-center p-5 h-100" style="background-color: #31869b;">
                                <h1><i class="fas fa-map-marker-alt" style="color: #fff"></i></h1>
                                <h3 class="text-uppercase pb-1 pt-3 fontbold" style="color: #fff;">Our Address</h3>
                                <span class="subheading text-white fontlight"><?= $setting->alamat; ?></span>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-lg-3">
                        <a href="tel:+<?= $setting->no_telp; ?>" target="_blank">
                            <div class="card-body text-center py-5 h-100" style="background-color: #31869b;">
                                <h1><i class="fas fa-envelope" style="color: #fff"></i></h1>
                                <h3 class="text-uppercase pb-1 pt-3 fontbold" style="color: #fff;">Email Us</h3>
                                <span class="subheading text-white fontlight" style="overflow-wrap: break-word;"><?= $setting->email; ?></span>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-lg-3">
                        <a href="mailto:<?= $setting->email; ?>" target="_blank">
                            <div class="card-body text-center p-5 h-100" style="background-color: #31869b;">
                                <h1><i class="fas fa-phone" style="color: #fff"></i></h1>
                                <h3 class="text-uppercase pb-1 pt-3 fontbold" style="color: #fff;">Call Us</h3>
                                <span class="subheading text-white fontlight">+<?= $setting->no_telp; ?></span>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-lg-12 pt-3">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7932.572507715018!2d106.715306!3d-6.225938!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1c1460e2d8676d6a!2sBalai%20Lelang%20SUN!5e0!3m2!1sid!2sid!4v1619857772000!5m2!1sid!2sid" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>

            </div>
            
        </section>


        <?php include('part/footer.php') ?>
    </body>
</html>
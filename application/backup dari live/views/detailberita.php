<!DOCTYPE html>

<html lang="en">

    <?php include('part/head.php') ?>

    <body id="page-top">

        <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=606d6598a784de0012cc7b98&product=inline-share-buttons" async="async"></script>

        <?php include('part/nav.php') ?>

        <!-- Masthead-->

        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">

            <div class="container d-flex align-items-center flex-column" style="height: 500px;">

                <div class="row">

                    <div class="col-6 offset-6" style="color: black;">

                        <h2>Tentang Kami</h2>

                    </div>

                </div>

            </div>

        </header> -->

        <!-- ======= Lup Lup ======= -->

        <section class="page-section portfolio" id="about" style="background-image: url('<?= base_url(); ?>assets/images/Tentang Kami 7 ( )-01.png'); background-size: cover; position: relative;">

            <?php foreach ($detail_berita as $key => $val) { ?>

            <div class="container pt-5 mw-90 pb-5">

                <div class="row w-75 mx-auto">

                    <div class="col-12">

                        <h6 style="color: #ff0000;">Aktifitas Lup Lup</h6>

                        <hr>

                        <h4 class="text-black text-uppercase text-center"><?= $val->judul_berita ?></h4>

                        <br>

                    </div>

                    <div class="col-12 my-auto">

                       <img src="<?= base_url('assets/uploads/berita/origin/').$val->gambar ?>" class="d-block w-75 m-auto" alt="<?= $val->judul_berita ?>" loading="lazy">

                       <hr><h6 style="color: #ff0000;"><?= date_format(date_create($val->tgl_publish),"d M Y") ?></h6>

                       <div class="sharethis-inline-share-buttons"></div>

                       <br>

                       <?= $val->isi_berita ?>

                    </div>

                    <div class="clearfix"></div>

                </div>

            </div>

            <?php } ?>

            <br>

        </section>

        <?php include('part/footer.php') ?>

    </body>

</html>
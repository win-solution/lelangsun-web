/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : lelang_sun

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 12/05/2021 21:04:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for barang_lelang
-- ----------------------------
DROP TABLE IF EXISTS `m_barang_lelang`;
CREATE TABLE `m_barang_lelang`  (
  `id_barang_lelang` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(11) NOT NULL,
  `grade` varchar(255) NULL DEFAULT NULL,
  `no_polisi` varchar(255) NULL DEFAULT NULL,
  `no_rangka` varchar(255) NULL DEFAULT NULL,
  `tipe_model` varchar(255) NULL DEFAULT NULL,
  `no_mesin` varchar(255) NULL DEFAULT NULL,
  `warna` varchar(255) NULL DEFAULT NULL,
  `stnk` varchar(255) NULL DEFAULT NULL,
  `tahun` year NULL DEFAULT NULL,
  `bpkb` varchar(255) NULL DEFAULT NULL,
  `transmisi` varchar(255) NULL DEFAULT NULL,
  `faktur` varchar(255) NULL DEFAULT NULL,
  `kapasitas_mobil` varchar(255) NULL DEFAULT NULL,
  `ktp_pemilik` varchar(255) NULL DEFAULT NULL,
  `bahan_bakar` varchar(255) NULL DEFAULT NULL,
  `kwitansi` varchar(255) NULL DEFAULT NULL,
  `deskripsi` varchar(255) NULL DEFAULT NULL,
  `speedometer` varchar(255) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_barang_lelang`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for bidding
-- ----------------------------
DROP TABLE IF EXISTS `t_bidding`;
CREATE TABLE `t_bidding`  (
  `id_bidding` int(11) NOT NULL AUTO_INCREMENT,
  `id_npl` int(11) NOT NULL,
  `id_lot` int(11) NOT NULL,
  `harga_bidding` int(255) NULL DEFAULT NULL,
  `waktu` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_bidding`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for event
-- ----------------------------
DROP TABLE IF EXISTS `t_event`;
CREATE TABLE `t_event`  (
  `id_event` int(11) NOT NULL AUTO_INCREMENT,
  `id_pool` int(11) NULL DEFAULT NULL,
  `nama_event` varchar(255) NULL DEFAULT NULL,
  `deskripsi_event` varchar(255) NULL DEFAULT NULL,
  `waktu_event` datetime NULL DEFAULT NULL,
  `foto_event` varchar(255) NULL DEFAULT NULL,
  `alamat_event` text NULL,
  `link_maps` text NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_event`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for foto_kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `m_foto_kendaraan`;
CREATE TABLE `m_foto_kendaraan`  (
  `id_foto` int(11) NOT NULL AUTO_INCREMENT,
  `id_kendaraan` int(11) NOT NULL,
  `path_foto` text NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_foto`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for lot
-- ----------------------------
DROP TABLE IF EXISTS `t_lot`;
CREATE TABLE `t_lot`  (
  `id_lot` int(11) NOT NULL AUTO_INCREMENT,
  `id_kendaraan` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `tanggal` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `harga` int(255) NULL DEFAULT NULL,
  `status_lot` tinyint(1) NULL DEFAULT NULL COMMENT 'aktif, lelang, sold',
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_lot`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for m_kategori
-- ----------------------------
DROP TABLE IF EXISTS `m_kategori`;
CREATE TABLE `m_kategori`  (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(255) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for m_peserta
-- ----------------------------
DROP TABLE IF EXISTS `m_peserta`;
CREATE TABLE `m_peserta`  (
  `id_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NULL DEFAULT NULL,
  `email` varchar(255) NULL DEFAULT NULL,
  `no_hp` text NULL,
  `alamat` varchar(255) NULL DEFAULT NULL,
  `nik` varchar(255) NULL DEFAULT NULL,
  `npwp` varchar(255) NULL DEFAULT NULL,
  `bertindak_mewakili` varchar(255) NULL DEFAULT NULL,
  `nama_perusahaan` varchar(255) NULL DEFAULT NULL,
  `npwp_perusahaan` varchar(255) NULL DEFAULT NULL,
  `alamat_perusahaan` varchar(255) NULL DEFAULT NULL,
  `telp_kantor` text NULL,
  `email_kantor` varchar(255) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_peserta`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for m_pool
-- ----------------------------
DROP TABLE IF EXISTS `m_pool`;
CREATE TABLE `m_pool`  (
  `id_pool` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pool` varchar(255) NULL DEFAULT NULL,
  `alamat` text NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_pool`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for npl
-- ----------------------------
DROP TABLE IF EXISTS `m_npl`;
CREATE TABLE `m_npl`  (
  `id_npl` int(11) NOT NULL AUTO_INCREMENT,
  `npl` varchar(255) NULL DEFAULT NULL,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `id_pembelian_npl` int(11) NULL DEFAULT NULL,
  `status_npl` tinyint(1) NULL DEFAULT NULL COMMENT 'aktif, non aktif',
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_npl`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for pembelian_npl
-- ----------------------------
DROP TABLE IF EXISTS `t_pembelian_npl`;
CREATE TABLE `t_pembelian_npl`  (
  `id_pembelian_npl` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `waktu` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(6),
  `no_pembelian` int(255) NULL DEFAULT NULL,
  `verifikasi` varchar(255) NULL DEFAULT NULL,
  `bukti` varchar(255) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_pembelian_npl`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for pemenang_lelang
-- ----------------------------
DROP TABLE IF EXISTS `t_pemenang_lelang`;
CREATE TABLE `t_pemenang_lelang`  (
  `id_pemenang_lelang` int(11) NOT NULL AUTO_INCREMENT,
  `id_bidding` int(11) NULL DEFAULT NULL,
  `id_npl` int(11) NOT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_pemenang_lelang`) USING BTREE
) ENGINE = InnoDB;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `m_user_peserta`;
CREATE TABLE `m_user_peserta`  (
  `id_user_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) NULL DEFAULT NULL,
  `password` varchar(255) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `created_by` int(11) NULL DEFAULT NULL,
  `updated_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user_peserta`) USING BTREE
) ENGINE = InnoDB;

SET FOREIGN_KEY_CHECKS = 1;

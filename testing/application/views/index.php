<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        <?php include('part/nav.php') ?>
        <!-- Masthead-->
        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
        <header class="masthead" >
            <div class="container mw-100 mh-100" style=" background-image: url('assets/images/background_home.png'); height: 90vh; background-position: center; background-size: cover;">
                <div class="row justify-content-center h-100">
                    <div class="col-12 col-sm-4 my-auto">
                        <!-- <div class="row"> -->
                            <div class="col-sm-12">
                                <div>
                                    <h2 class="text-uppercase">SUN Balai Lelang 1</h2>
                                </div>
                                <hr style="border-bottom:4px solid red; margin-right: 20vw;">
                                <h2 class="text-uppercase" >SUN Balai Lelang <br> Mobil Terpercaya</h2>
                                <span class=" subheading alert-link" style="color: #333; font-size: 16px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</span>
                                <br>
                                <a href="<?= base_url(); ?>detailberita"><span class="btn btn-danger btn-xl btn btn-primary btn-xl mt-2 text-black">Ikuti Lelang</span></a>
                            </div>

                        <!-- </div> -->
                    </div>
                    <div class="col-12 col-sm-6 my-auto text-center" style="margin-right: 2vw">
                        <div>
                            <h1 class="text-uppercase text-white">Etios Valco</h1>
                            <span class=" subheading" style="color: #fff; font-size: 30px;">Toyota</span>
                        </div>
                        <img src="assets/images/mobil1.png" class="w-75" alt="..." loading="lazy">
                    </div>
                </div>
            </div>
        </header>
    </body>
</html>
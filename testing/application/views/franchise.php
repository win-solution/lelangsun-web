<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        <?php include('part/nav.php') ?>

        <header class="masthead" >
            <div class="container mw-100 mh-100" style=" background-image: url('assets/images/background-lelang.png'); height: 90vh; background-position: center; background-size: cover;">
                <div class="row justify-content-center h-100">
                    <div class="col-12 col-lg-12 my-auto d-flex justify-content-center text-center">
                        <!-- <div class="row"> -->
                            <div>
                                <h1 class="text-uppercase text-white text-uppercase" style="font-size: 80px;">Lot</h1>
                                <span class="subheading text-white" style="font-size: 30px;">Toyota</span>
                            </div>

                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </header>

        <section class="page-section portfolio" id="about" style="background-image: url('assets/images/backgroundlot1bawah.png'); background-size: cover; position: relative;">
            <div class="container mh-100 mw-90">
              <div class="row">
                <div class="col-12 offset-sm-2 col-md-6 mx-auto">
                    <img src="assets/images/mobil4.png" class="d-block m-auto mw-100" alt="..." loading="lazy">
                </div>
                <div class="col-12 col-md-6 my-auto px-5">
                    <h1 class="text-uppercase pb-1 " style="color: #333;">Ertiga 1.4 GX M/T</h1>
                    <p><span style="font-size: 40px; color: #333">Tahun 2012</span></p>
                    <p><span style="font-size: 40px; color: #333">Plat No DT 48 IB</span></p>
                    <span style="font-size: 30px; color: #333" class="mr-3"><i class="far fa-tachometer-alt-fast" style="color: #ff4500"></i>  10.875</span>
                    <span style="font-size: 30px; color: #333" class="mx-3"><i class="fas fa-map-marker-alt" style="color: #ff4500"></i>  Pool Kendari</span>
                    <br>
                    <span style="font-size: 30px; color: #333" class="mr-3"><i class="fas fa-calendar" style="color: #ff4500"></i>   22 April 2021 / 13.00 WIB</span>
              </div>
            </div>
        </section>

    </body>
</html>
<!-- SweetAlert2 -->
<link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<script src="<?= base_url('assets/'); ?>plugins/sweetalert2/sweetalert2.min.js"></script>
<script>
    var baseURL = "<?= base_url(); ?>";
    var Toast = Swal.mixin({
      position: 'center',
      showConfirmButton: true,
      timer: 5000
    });
</script>
<style>
    .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        font-size:30px;
        box-shadow: 2px 2px 3px #999;
        z-index:100;
    }

    .my-float{
        margin-top:16px;
    }

    .avatar {
      vertical-align: middle;
      width: 50px;
      height: 50px;
      border-radius: 50%;
    }
</style>

<!-- Navigation-->

<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">

    <div class="container mw-90">

        <a class="navbar-brand js-scroll-trigger" href="<?= base_url() ?>">

            <img src="<?= base_url() ?>assets/images/logo.png" alt="" class="ml-5 mw-50" srcset="" style="" loading="lazy">

        </a>

        <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fas fa-bars"></i>

        </button>

        <div class="collapse navbar-collapse overflow-auto" id="navbarResponsive" style="max-height: 70vh;">

            <ul class="navbar-nav ml-auto">

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "index")?"active":""?>" href="<?= base_url(); ?>">Beranda</a></li>

                <!-- <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "berita")?"active":""?>" href="<?= base_url(); ?>berita">Lot</a></li> -->

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "menu")?"active":""?>" href="<?= base_url(); ?>menu">Lot</a></li>

                <!-- <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "booth")?"active":""?>" href="<?= base_url(); ?>booth">Booth</a></li> -->

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "franchise")?"active":""?>" href="<?= base_url(); ?>franchise">Lelang</a></li>

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "outlet")?"active":""?>" href="<?= base_url(); ?>outlet">Event</a></li>

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "kontak")?"active":""?>" href="<?= base_url(); ?>kontak">Kontak</a></li>

                <li class="nav-item mx-0 mx-lg-1 my-float"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "penghargaan")?"active":""?>" href="<?= base_url(); ?>penghargaan">Log In</a></li>

                <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 px-1 rounded js-scroll-trigger <?= ($page == "penghargaan")?"active":""?>" href="<?= base_url(); ?>penghargaan"><img src="assets/images/profile_picture.png" alt="Avatar" class="avatar"></a></li>

            </ul>

        </div>

    </div>

</nav>
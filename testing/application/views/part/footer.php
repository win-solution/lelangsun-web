

<!-- Copyright Section-->

<div class="copyright py-4 text-center text-white fixed-bottom">

    <div class="container mw-90">

        <div class="row">

            <!-- <div class="col-12 mb-3">

                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->ig; ?>"><i class="fab fa-fw fa-instagram"></i></a>

                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->fb; ?>"><i class="fab fa-fw fa-facebook-f"></i></a>

                <a class="btn btn-outline-light btn-social mx-1" href="<?= $setting->tw; ?>"><i class="fab fa-fw fa-twitter"></i></a>

            </div> -->

            <div class="col-12 mt-3">

                <span>2011-2021, Lup Lup Bubble Drink</span>

            </div>

        </div>

    </div>

</div>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->

<div class="scroll-to-top d-lg-none position-fixed">

    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>

</div>

<?php if (!empty($setting->no_wa)) { ?>
<a href="https://api.whatsapp.com/send?phone=<?= $setting->no_wa; ?>&text=Hallo min, saya tertarik dengan produk dan paket kemitraan Lup Lup Bubble Drink." class="btn btn-outline-light btn-social mx-1 float" target="_blank">
    <i class="fab fa-fw fa-whatsapp"></i>
    <!-- <i class="fab fa-fw fa-twitter"></i> -->
</a>
<?php } ?>

<!-- Bootstrap core JS-->

<!-- <script src="assets/vendors/jquery/jquery.min.js"></script> -->

<script src="<?= base_url() ?>assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Third party plugin JS-->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script> -->

<!-- Contact form JS-->

<script src="<?= base_url() ?>assets/images/mail/jqBootstrapValidation.js"></script>

<script src="<?= base_url() ?>assets/images/mail/contact_me.js"></script>


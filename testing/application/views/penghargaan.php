<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
        
        <section class="" style=" background-image: url('assets/images/mobil5.jpg'); height: 90vh; background-position: center; background-repeat: no-repeat;">

            <div style=" background-color: #000000e0; height: 100%">

                <div class="row mx-auto" style="height: 90vh">
                <!-- <div class="container">   -->
                    <div class="mx-auto my-auto col-12 col-lg-4" style="">
                      <h1 class="d-flex justify-content-center fontrajdhani text-white">Login Lelang</h1>
                        <form id="formLogin" name="formLogin" action="" method="post" class="text-white">
                          <div class="form-group has-feedback">
                              <label for="exampleInputEmail1" class="form-label">Email address</label>
                              <input type="text" name="email" id="email" class="form-control input-borderless" placeholder="Email" required="">
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                          </div>
                          <div class="form-group has-feedback">
                              <label for="exampleInputEmail1" class="form-label">Password</label>
                              <input type="password" name="pass" id="pass" class="form-control input-borderless" placeholder="Password" required="">
                              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                          </div>
                          <div class="mb-3 form-check pb-4 text-center">
                            <a href="forgot.html" class="">Lupa Kata Sandi?</a><span> / </span><a href="<?= base_url(); ?>pelajari" class="">Registrasi</a>
                          </div>
                              <!-- <h5 class="pull-right">Lupa Password?</h5> -->
                          <div class="row pt-5">
                              <div class="container">
                                  <div class="col-12 pull-right d-flex justify-content-center">
                                      <span class="btn btn-primary btn-block btn-flat bg-red" onclick="checkAuth();" style="">Masuk</span>
                                  </div>
                              </div>
                          </div>
                        </form>
                      </div>
                    </div>
                <!-- </div> -->
                </div>

            </div>

        </section>

        <?php include('part/footer.php') ?>
    </body>
</html>
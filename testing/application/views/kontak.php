<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">    
        <?php include('part/nav.php') ?>
        <script>
            <?php if (!empty($this->session->flashdata('pesan'))) { ?>
                Toast.fire({
                    icon: "success",
                    title: "<?= $this->session->flashdata('pesan') ?>"
                });
            <?php } ?>
        </script>
        <!-- Masthead-->
        <!-- <header class="masthead bg-primary text-white text-center" style="background-image: url('assets/images/Franchise 1-01-min.png'); background-size: cover;">
            <div class="container d-flex align-items-center flex-column" style="height: 500px;">
                <div class="row">
                    <div class="col-6 offset-6" style="color: black;">
                        <h2>Tentang Kami</h2>
                    </div>
                </div>
            </div>
        </header> -->
        <header class="masthead" style="background-image: url('assets/images/baru 5-01.png'); height: 110vh;">
            <div class="container mw-75">
                <div class="row" style="padding-top: 10vh;">
                    <div class="col-12 col-lg-6 col-sm-6" >
                    <img src="assets/images/Foto Menu 4-01-min.png" class="d-block w-75 m-auto" alt="..." loading="lazy">
                    </div>
                    <div class="col-12 col-lg-6 col-sm-6 my-auto" >
                        <h1 class="text-uppercase " style="color: #ffcc00;">Kontak</h1>
                        <span class="subheading text-white " style="color: #333; font-size: 20px;">Lup Lup hadir dan turut serta dalam membantu peningkatan perekonomian, khususnya pada usaha kecil dan menengah dan itu dibuktikan dengan peningkatan pertumbuhan Kemitraan Terbesar selama 8 tahun terakhir.</span>                            
                    </div>
                </div>
            </div>
        </header>
        <!-- About Section-->
        <section class="page-section portfolio" id="about" style="background-color: #dccab9;">
            <div class="container">
                <div class="row w-100 mx-auto">
                    <div class="col-lg-6 text-center">                        
                        <img src="assets/images/Outlate-01.png" alt="" srcset="" loading="lazy" class="mw-50">
                    </div>
                    <div class="col-lg-6 mt-5" style="font-size: 16px;">       
                        <span><?= $setting->alamat; ?></span>
                        <table class="w-100">
                            <tbody>
                                <tr>
                                    <td>Telpon </td>
                                    <td><b>: +<?= $setting->no_telp; ?></b></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><b>: <?= $setting->email; ?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section portfolio" id="form-kontak" style="background-image: url('assets/images/Kontak 1-01-min.png'); background-size: cover;">
            <div class="container pt-5">
                <div class="row w-100 mx-auto">
                    <div class="col-12">
                        <h4 class="pb-1 text-capitalize text-center pb-1 px-lg-5 pr-5 ">Hubungi Kami Sekarang</h4>
                        <br>
                        <div class="container mw-100">
                            <form action="" method="POST" class="form-group w-100">
                                <div class="row w-100">
                                    <div class="col-12 col-lg-6 m-0 p-0 w-75">
                                        <div class="form-group px-lg-5">
                                            <label for="nama">Nama</label>
                                            <input type="text" class="form-control" id="nama" name="nama" required>
                                        </div>
                                        <div class="form-group px-lg-5">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" required>
                                        </div>
                                        <div class="form-group px-lg-5">
                                            <label for="no_tlp">Telepon</label>
                                            <input type="text" class="form-control" id="no_tlp" name="no_tlp" required>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6 m-0 p-0 w-75">
                                        <div class="form-group h-100 px-lg-5">
                                            <label for="pesan">Pesan</label>
                                            <textarea class="form-control" id="pesan" name="pesan" rows="5" required></textarea>
                                            <br>
                                            <button type="submit" name="submit" id="submit" class="btn btn-danger btn-lg pt-1" style="font-size: 16px;">Kirim Pesan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- <section class="page-section bg-primary text-white mb-0" id="portfolio">
            <div class="container">
                <h2 class="page-section-heading text-center text-uppercase text-white">About</h2>
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <div class="row">
                    <div class="col-lg-4 ml-auto"><p class="lead">Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional SASS stylesheets for easy customization.</p></div>
                    <div class="col-lg-4 mr-auto"><p class="lead">You can create your own custom avatar for the masthead, change the icon in the dividers, and add your email address to the contact form to make it fully functional!</p></div>
                </div>
                <div class="text-center mt-4">
                    <a class="btn btn-xl btn-outline-light" href="https://startbootstrap.com/theme/freelancer/">
                        <i class="fas fa-download mr-2"></i>
                        Free Download!
                    </a>
                </div>
            </div>
        </section> -->
        <!-- <section class="page-section" id="contact">
            <div class="container">
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Me</h2>
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <form id="contactForm" name="sentMessage" novalidate="novalidate">
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Name</label>
                                    <input class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Email Address</label>
                                    <input class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Phone Number</label>
                                    <input class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number." />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Message</label>
                                    <textarea class="form-control" id="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <br />
                            <div id="success"></div>
                            <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Send</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section> -->
        <?php include('part/footer.php') ?>
    </body>
</html>
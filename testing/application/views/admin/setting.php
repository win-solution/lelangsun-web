<!-- summernote -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/summernote/summernote-bs4.min.css">
<script src="<?php echo base_url('assets/'); ?>plugins/summernote/summernote-bs4.min.js"></script>

<!-- Tagsinput -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
<script type="text/javascript" src="<?php echo base_url('assets/'); ?>plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Setting</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Setting</a></li>
                </ol>
            </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Setting <?= APP_NAME ?></h3>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content_tab_1" data-toggle="pill" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true" title="Metadata">Metadata</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="content_tab_2" data-toggle="pill" href="#tab_2" role="tab" aria-controls="tab_2" aria-selected="false" title="Kontak">Kontak</a>
                                </li>
                            </ul>
                            <br>
                            <div class="tab-content" id="tab-content">                                
                                <?= (!empty($_SESSION['msg']))? '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>'.$_SESSION['msg'].'</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>'
                                :''
                                ?>
                                <div class="tab-pane fade active show" id="tab_1" role="tabpanel" aria-labelledby="content_tab_1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title" >Meta Data : </h3>
                                        </div>
                                        <div class="panel-body row">
                                            <!-- <span id="btnBlas" class="btn btn-large btn-primary"><i class="fas fa-paper-plane pr-1"></i> Blas Email Manual</span> -->
                                            <form id="form-filter" class="form-horizontal col-12" action="" method="POST" enctype="multipart/form-data">
                                                <div class="row">
                                                    <div class="form-group col-lg-12">
                                                        <label for="metadata_title" class="col-sm-2 control-label">Title</label>
                                                        <input type="text" id="metadata_title" name="metadata_title" class="form-control" value="<?= (!empty($setting->metadata_title))?$setting->metadata_title:'Lup Lup Bubble Drink' ?>">
                                                    </div> 
                                                    <div class="form-group col-lg-12">
                                                        <label for="metadata_keyword" class="col-sm-2 control-label">Keyword</label>
                                                        <input type="text" id="metadata_keyword" name="metadata_keyword" class="form-control" value="<?= (!empty($setting->metadata_keyword))?$setting->metadata_keyword:'Lup Lup,bubble drink,drink,minuman bubble,minuman kekinian' ?>" data-role="tagsinput">
                                                    </div>
                                                    <div class="form-group col-lg-12">
                                                        <label for="metadata_deskripsi" class="col-sm-2 control-label">Deskripsi</label>
                                                        <textarea name="metadata_deskripsi" id="metadata_deskripsi" cols="30" rows="5" class="form-control summernote" ><?= (!empty($setting->metadata_deskripsi))?$setting->metadata_deskripsi:'' ?></textarea>
                                                    </div> 
                                                    <div class="form-group col-lg-12 text-right">
                                                        <input type="submit" id="submit" name="submit" value="submit" class="btn btn-success btn-large">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="tab-pane fade" id="tab_2" role="tabpanel" aria-labelledby="content_tab_2">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h3 class="panel-title" >Kontak : </h3>                                            
                                        </div>
                                        <div class="panel-body row">
                                            <!-- <span id="btnBlas" class="btn btn-large btn-primary"><i class="fas fa-paper-plane pr-1"></i> Blas Email Manual</span> -->
                                            <form id="form-filter" class="form-horizontal col-12" action="" method="POST">
                                                <div class="row">
                                                    <div class="form-group col-6">
                                                        <label for="no_telp" class="col-sm-2 control-label">Nomer Telpon</label>
                                                        <input type="text" id="no_telp" name="no_telp" class="form-control" value="<?= (!empty($setting->no_telp))?$setting->no_telp:'' ?>">
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <label for="no_wa" class="col-sm-2 control-label">Nomer WA</label>
                                                        <input type="text" id="no_wa" name="no_wa" class="form-control" value="<?= (!empty($setting->no_wa))?$setting->no_wa:'' ?>">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="email" class="col-sm-2 control-label">Email</label>
                                                        <input type="email" id="email" name="email" class="form-control" value="<?= (!empty($setting->email))?$setting->email:'' ?>">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                                                        <textarea name="alamat" id="alamat" cols="30" rows="15" class="form-control" ><?= (!empty($setting->alamat))?$setting->alamat:'' ?></textarea>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group col-12">
                                                        <i class="fas fa-instagram"></i><label for="ig" class="col-sm-2 control-label"> Instagram</label>
                                                        <input type="text" id="ig" name="ig" class="form-control" value="<?= (!empty($setting->ig))?$setting->ig:'' ?>">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <i class="fas fa-facebook"></i><label for="fb" class="col-sm-2 control-label"> Facebook</label>
                                                        <input type="text" id="fb" name="fb" class="form-control" value="<?= (!empty($setting->fb))?$setting->fb:'' ?>">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <i class="fas fa-twitter"></i><label for="tw" class="col-sm-2 control-label"> Twitter</label>
                                                        <input type="text" id="tw" name="tw" class="form-control" value="<?= (!empty($setting->tw))?$setting->tw:'' ?>">
                                                    </div>
                                                    <div class="form-group col-lg-12 text-right">
                                                        <input type="submit" id="submit" name="submit" value="submit" class="btn btn-success btn-large">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                            
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <script>
            $(function () {
                $('.summernote').summernote({height:150,disableResizeEditor:true});
            });
            $("#file_img").change(function (e) { 
                $("#lablefile_img").text($("#file_img").val().replace(/C:\\fakepath\\/i, ''));
            });
            $("#btnBlas").click(function (e) { 
                $.ajax({
                    type: "get",
                    url: "http://localhost:3030/mailer/test_bulk_mail",
                    success: function (res) {
                        // alert("Blas Email berhasil, silahkan tunggu 1 jam lagi untuk melakukan blas manual berikutnya");
                    },
                    complete: function (res) {
                        alert("Blas Email berhasil, silahkan tunggu 1 jam lagi untuk melakukan blas manual berikutnya");
                    }
                });                
            });
            setInterval( function () {
            }, 3 * 60 * 1000 );
        </script>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

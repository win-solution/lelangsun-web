<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url(); ?>" class="brand-link">
        <img src="<?= base_url().'assets/'; ?>images/LUP LUP LOGO.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">CMS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="<?= base_url().'assets/'; ?>dist/img/avatar_icon.png" class="img-circle elevation-2" alt="User Image" style="background: #fff;">
        </div>
        <div class="info">
            <a href="#" class="d-block"><?= $_SESSION['nama'] ?></a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header">Menu</li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/') ?>" class="nav-link <?= ($page == "dashboard")?"active":""; ?>">
                    <i class="nav-icon fas fa-chart-area"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/berita') ?>" class="nav-link <?= ($page == "berita")?"active":""; ?>">
                    <i class="nav-icon fas fa-newspaper"></i>
                    <p>Berita</p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/kontak') ?>" class="nav-link <?= ($page == "kontak")?"active":""; ?>">
                    <i class="nav-icon fas fa-paper-plane"></i>
                    <p>Kontak</p>
                </a>
            </li>
            <?php if ($_SESSION['akses'] == 1) { ?>
            <li class="nav-item ">
                <a href="<?= base_url('admin/mitra') ?>" class="nav-link <?= ($page == "mitra")?"active":""; ?>">
                    <i class="nav-icon fas fa-users"></i>
                    <p>Calon Mitra</p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/outlet') ?>" class="nav-link <?= ($page == "outlet")?"active":""; ?>">
                    <i class="nav-icon fas fa-campground"></i>
                    <p>Outlet</p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/kategori') ?>" class="nav-link <?= ($page == "kategori")?"active":""; ?>">
                    <i class="nav-icon fas fa-clipboard-list"></i>
                    <p>Kategori Menu</p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/menu') ?>" class="nav-link <?= ($page == "menu")?"active":""; ?>">
                    <i class="nav-icon fas fa-beer"></i>
                    <p>Menu Minuman</p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/user') ?>" class="nav-link <?= ($page == "user")?"active":""; ?>">
                    <i class="nav-icon fas fa-user-secret"></i>
                    <p>User</p>
                </a>
            </li>
            <li class="nav-item ">
                <a href="<?= base_url('admin/setting') ?>" class="nav-link <?= ($page == "setting")?"active":""; ?>">
                    <i class="nav-icon fas fa-cogs"></i>
                    <p>Setting</p>
                </a>
            </li>
            <?php } ?>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
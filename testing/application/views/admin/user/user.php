<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>User</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">User</a></li>
                </ol>
            </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
        <script src="<?= base_url().'assets/'; ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <!-- <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.flash.min.js"></script> -->
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
        <!-- Form Validation -->
        <script src="<?php echo base_url('assets/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?php echo base_url('assets/'); ?>plugins/jquery-validation/additional-methods.js"></script>
        <script>
            var kelas = "";
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar User <?= APP_NAME ?></h3>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content_tab_1" data-toggle="pill" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true" title="User Terverifikasi">User Terverifikasi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="content_tab_2" data-toggle="pill" href="#tab_2" role="tab" aria-controls="tab_3" aria-selected="false" title="User Belum Terverifikasi">User Belum Terverifikasi</a>
                                </li>
                            </ul>
                            <br>
                            <div class="tab-content" id="custom-tabs-four-tabContent">
                                <div class="tab-pane fade active show" id="tab_1" role="tabpanel" aria-labelledby="content_tab_1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <!-- <h3 class="panel-title" >Custom Filter : </h3> -->
                                        </div>
                                        <div class="panel-body row">
                                            <form id="form-filter" class="form-horizontal col-12">
                                                <div class="row">
                                                </div>
                                            </form>
                                        </div>
                                    </div>                                    
                                    <button class="btn btn-success" onclick="add();"><i class="fas fa-plus"></i> Tambah User </button>
                                    <table id="tableUser" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="tab_2" role="tabpane2" aria-labelledby="content_tab_2">
                                    <table id="tableDelUser" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>                            
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-admin mw-75 mx-auto">

                <!-- Modal content-->
                <div class="modal-content" style="border-radius: 25px;">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modal_header"></h4>
                        <button type="button" class="close btn btn-danger" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="modal_body">
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    </div>
                </div>

            </div>
        </div>
        <script>
            var table, table_trash, action, action_label;
            $(function () {
                table = $('#tableUser').DataTable({ 
                    "responsive": true,
                    "autoWidth": false,
                    "scrollX": false,
                    "processing"  : true, //Feature control the processing indicator.
                    "serverSide"  : true, //Feature control DataTables' server-side processing mode.
                    "searchDelay" : 1 * 1000,
                    // "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
                    // "pageLength": 25,
                    "dom": 'lBifrtip',
                    "order": [], //Initial no order.
                    "buttons": [
                        {
                            extend: 'excelHtml5',
                            title: 'Data User <?= APP_NAME ?> '+Date.now(),
                            exportOptions: {
                                modifier: {
                                    page: 'all',
                                    search: 'applied',
                                    order:  'applied',
                                }
                            },
                        },
                        'print', 
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('admin/rest_api/dtUser?type=user')?>",
                        "type": "POST",
                        "data": function ( data ) {
                            data.kelas = $('#kelas').val();
                            // data.search = $('#search').val();
                        }
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        { 
                            "targets": [ 0, 4 ], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                });
                table_trash = $('#tableDelUser').DataTable({ 
                    "responsive": true,
                    "autoWidth": false,
                    "scrollX": false,
                    "processing"  : true, //Feature control the processing indicator.
                    "serverSide"  : true, //Feature control DataTables' server-side processing mode.
                    "searchDelay" : 1 * 1000,
                    // "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
                    // "pageLength": 25,
                    "dom": 'lfrtip',
                    "order": [], //Initial no order.                    
                    "dom": 'lBfrtip',
                    "buttons": [
                        {
                            extend: 'excelHtml5',
                            title: 'Data User Tidak Valid <?= APP_NAME ?>',
                            exportOptions: {
                                modifier: {
                                    page: 'all',
                                    search: 'applied',
                                    order:  'applied',
                                }
                            },
                        },
                        'print', 
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('admin/rest_api/dtUser?type=del_user')?>",
                        "type": "POST",
                        "data": function ( data ) {
                            data.kelas = $('#filterKelas').val();
                        }
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        { 
                            "targets": [ 0, 4 ], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                });
            });
            
            function add() {
                action = "<?php echo base_url('admin/user/tambah')?>";
                action_label = "Tambah";
                $.ajax({ url : "<?php echo base_url('admin/user/tambah')?>",
                    type: "GET",
                    dataType: "JSON",
                    async: false,
                    success: function(data){
                        $('#myModal #modal_header').html("");
                        $('#myModal #modal_body').html("");

                        $('#myModal #modal_header').append(data.proses);
                        $('#myModal #modal_body').append(data.content);
                        $('#myModal').modal({backdrop: 'static', keyboard: false}); 
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        newSWAL("Gagal memperoleh data dari ajax!", "error");
                    }        
                });
                
            }

            function edit(id) {
                action = "<?php echo base_url('admin/user/ubah')?>";
                action_label = "Ubah";
                $.ajax({ url : "<?php echo base_url('admin/user/ubah/')?>"+id,
                    type: "GET",
                    dataType: "JSON",
                    async: false,
                    success: function(data){
                        $('#myModal #modal_header').html("");
                        $('#myModal #modal_body').html("");

                        $('#myModal #modal_header').append(data.proses);
                        $('#myModal #modal_body').append(data.content);
                        $('#myModal').modal({backdrop: 'static', keyboard: false}); 
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        newSWAL("Gagal memperoleh data dari ajax!", "error");
                    }        
                });
                
            }

            function save() {
                    var form = $( "#myform" );
                    form.validate();
                    if (form.valid()) {
                        $("#submit_btn").attr('disabled','disabled');
                        $("#submit_btn").val('Sedang menyimpan data!');
                        $("#cancel_btn").attr('style','display:none;');
                        $.ajax({ url : action,
                            type: "POST",
                            data : form.serialize(),
                            dataType: "JSON",
                            async: false,
                            success: function(data){
                                table.ajax.reload(null,false);
                                $('#myModal').modal("hide"); 
                                newSWAL("Data berhasil tersimpan!", "success");
                            },
                            error: function (jqXHR, textStatus, errorThrown){
                                // $("#submit_btn").removeAttr("disabled");
                                // $("#submit_btn").val(action_label);
                                // $("#cancel_btn").attr('style','display:block;');
                                newSWAL("Gagal memperoleh data dari ajax!", "error");
                            }        
                        });
                    } else {

                    }
            }

            setInterval( function () {
                // table.ajax.reload(null,false);
                // table_trash.ajax.reload(null,false);
            }, 3 * 60 * 1000 );
        </script>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php  // var_dump($berita_detail) ?>
<form id="myform" action="<?= $action; ?>" method="post" enctype="multipart/form-data">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-body">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <strong><?= $this->session->flashdata('error') ?></strong>
          </div>
        <?php } ?>
        <input type="hidden" name="id_berita" id="id_berita" value="<?= @$berita_detail[0]->id_berita ?>">
        <input type="hidden" name="submit" id="submit" value="submit">

        <div class="form-group">
          <label for="judul_berita">Judul <span style="color:red;">*</span></label>
          <input class="form-control" id="judul_berita" placeholder="Judul berita" type="text" name="judul_berita" value="<?= @$berita_detail[0]->judul_berita ?>" required>
        </div>

        <div class="form-group">
            <label for="tgl_publish">Tanggal Terbit <span style="color:red;">*</span></label>
            <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                <input type="text" id="tgl_publish" name="tgl_publish" class="form-control datetimepicker-input" data-target="#datetimepicker1" value="<?= @$berita_detail[0]->tgl_publish ?>" readonly required/>
                <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>

        <?php if (strtolower($proses) == "ubah") { ?>
        <div class="form-group">
          <label>Gambar</label><br>
          <img id="prevGambar" class="img-thumbnail" style="max-width: 15vw;" src="<?= base_url('assets/uploads/berita/thumb/').@$berita_detail[0]->gambar ?>" alt="" srcset="">
        </div>
        <?php } else { ?>
          <div class="form-group">
            <label>Gambar</label><br>
            <img class="img-thumbnail" style="max-width: 15vw;" id="prevGambar" src="" alt="" srcset="">
          </div>
        <?php } ?>

        <div class="form-group">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="gambar" name="gambar" accept="image/jpg, image/jpeg, image/png">
            <label id="labelGambar" class="custom-file-label" for="gambar">Pilih Gambar</label>
          </div>
        </div>

        <div class="form-group">
            <label for="isi_berita" class="col-sm-2 control-label">Isi Berita</label>
            <textarea name="isi_berita" id="isi_berita" cols="30" rows="5" class="form-control summernote" required><?= @$berita_detail[0]->isi_berita ?></textarea>
        </div> 

        <?php if (strtolower($proses) !== "ubah") { ?>
        <?php } ?>
        <div class="form-group text-right">
          <span id="text_submit"></span>
          <!-- <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success"> -->
          <input type="submit" id="submit_btn" name="submit" value="<?= $proses; ?>" class="btn btn-success" onclick="save();">          
          <button type="button" id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function () {
    // setTimeout(() => {
      $('.summernote').summernote({
        height:150,
        disableResizeEditor:true
      });
      $('#datetimepicker1').datetimepicker({
        debug: true,
        format: 'YYYY-MM-DD HH:mm',
        setDate: new Date(),
        buttons: {showClose:true},
        ignoreReadonly: true,        
        <?php if (strtolower($proses) == "ubah") { ?>
        <?php } ?>
      });
      $('#datetimepicker2').datetimepicker({
        debug: true,
        format: 'LT'
      });      
    // }, 1 * 1000);
      // akses();
      // getopendata();

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid"
    });
    $( "#myform" ).validate({
      rules: {
        judul_berita: { required: true, },
        isi_berita: { required: true, },
        <?php if (strtolower($proses) == "tambah") { ?>
          gambar: {
            required:true,
            extension: "jpg,jpeg,png",
            maxsize: 200000,
          }
        <?php } else { ?>
          gambar: {
            extension: "jpg,jpeg,png",
            maxsize: 200000,
          }
        <?php } ?>
      }
    });
  });

  $('#submit_btn').on('click',function () {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
  });

  $( "form" ).submit(function(e) {
    // $('#text_submit').text('Sedang Menyimpan Data...'); //change button text
    // $('#submit_btn').attr('disabled',true); //set button disable
    // $('#submit_btn').attr('style','display:none;'); // hide button
    // $('#cancle_btn').attr('style','display:none;'); // hide button
    // if ($('#hak_akses').val() == 1) {
    //   return;
    // } else {
    //   if( ($(".sub_ktda").is(':checked')) || ($(".kecamatan").is(':checked')) ) {
    //     return;
    //   } else {
    //     alert('Hak akses harus dipilih!!!');
    //     e.preventDefault(e);
    //   }
    // }
  });
  $("#gambar").change(function() {
        readURL(this,'#prevGambar','#labelGambar');
    });
</script>

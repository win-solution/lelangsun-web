<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard Admin <?= APP_NAME ?></a></li>
                </ol>
            </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <script type="text/javascript" src="<?= base_url('assets') ?>/plugins/jqvmap/jquery.vmap.js"></script>
        <script type="text/javascript" src="<?= base_url('assets') ?>/plugins/jqvmap/maps/jquery.vmap.indonesia2.js" charset="utf-8"></script>
        <!-- chartjs -->
        <script src="https://www.chartjs.org/dist/2.9.4/Chart.min.js"></script>

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
        <script src="<?= base_url().'assets/'; ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-user-plus"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Calon Mitra Baru Bulan Ini (<b><?= date("M Y"); ?></b>)</span>
                            <span id="dataNewUser" class="info-box-number"><?= $calonMitra; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Total Outlet</span>
                            <span id="dataTotalUser" class="info-box-number"><?= $totalOutlet; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-user-check"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Mitra Terferifikasi</span>
                            <span id="dataNewUser" class="info-box-number"><?= $verifiedMitra; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-user-times"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Mitra Belum Terferifikasi</span>
                            <span id="dataTotalUser" class="info-box-number"><?= $unverifiedMitra; ?></span>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">Partisipan Peserta Kelas Olahraga (Grafik)</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="position-relative mb-4">
                                <canvas id="kelas-chart" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div> -->
                

                <div class="col-12">
                    <div class="card">
                        <div class="card-header border-0">
                            <div class="d-flex justify-content-between">
                                <h3 class="card-title">Sebaran Outlet</h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="vmap" class="mw-100 mh-100" style="width: 100vw; height: 100vh;"></div>
                        </div>
                    </div>
                </div>
                
                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-xl">
                        <!-- konten modal-->
                        <div class="modal-content" style="background-image: url('<?= base_url() ?>assets/images/Bhoot 1-01-min.png'); background-size: cover; position: relative;">                        
                            <div class="modal-header">
                                <h4 class="modal-title">Daftar Outlet</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- body modal -->
                            <div class="modal-body">
                                <div class="container px-0 mw-100">
                                    <div id="modalBodyRow" class="row w-100 mx-auto">
                                    </div>
                                </div>
                            </div>
                            <!-- footer modal -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            var tableKelas;
            async function getData(ajaxurl) { 
                let resValue;
                const result = await $.ajax({
                    url: ajaxurl,
                    type: 'GET',
                    dataType : 'json',
                    async: true,
                }).done(function (res) { 
                    resValue = res;
                });
                return resValue;
            };
            
            const setDataNewUser = async () => {
                try {
                    let dataNewUser = getData("<?= base_url('admin/rest_api/dataNewUser') ?>");
                    dataNewUser.then(function(result) {
                        $('#dataNewUser').html(result)
                    })
                } catch (err) {
                    console.log("Error setDataKelas : ",err);
                }
            }
            
            const setDataTotalUser = async () => {
                try {
                    let dataTotalUser = getData("<?= base_url('admin/rest_api/dataTotalUser') ?>");
                    dataTotalUser.then(function(result) {
                        $('#dataTotalUser').html(result)
                    })
                } catch (err) {
                    console.log("Error setDataKelas : ",err);
                }
            }

            const setDataKelas = async () => {
                try {
                    let dataKelas = getData("<?= base_url('admin/rest_api/dataRegular') ?>");
                    dataKelas.then(function (res) {
                        let i = 0;
                        $.map(res, function (val, key) {
                            i++;
                            // console.log(val);
                            $("#tableKelas tbody").append('<tr><td>'+i+'</td><td>'+val.hari+'</td><td>'+val.tanggal+'</td><td>'+val.kelas+'</td><td>'+val.result+'</td></tr>')
                        });
                        return res;
                    }).then(function (res) { 
                        // console.log("res then 2",res);
                        tableKelas = $('#tableKelas').DataTable({
                            "responsive": true,
                            "autoWidth": false,
                            "lengthMenu": [[10, -1], [10, "All"]],
                            "pageLength": 10,
                        });
                        res.map(val => console.log(val));
                        let ctx = document.getElementById('kelas-chart').getContext('2d');
                        let myBar = new Chart(ctx, {
                            type: 'bar',
                            data: barChartData,
                            options: {
                                responsive: true,
                                legend: {
                                    position: 'top',
                                },
                                title: {
                                    display: true,
                                    text: 'Chart.js Bar Chart'
                                }
                            }
                        });
                    })
                } catch (err) {
                    console.log("Error setDataKelas : ",err);
                }
            }

            $(function () {
                // setDataNewUser();
                // setDataTotalUser();
                // setDataKelas();
            });
            setInterval(() => {
                // setDataNewUser()
                // setDataTotalUser();
            }, 5 * 60 * 1000);
            
            jQuery('#vmap').vectorMap({
                map: 'indonesia_id',
                backgroundColor: '#',
                borderColor: '#333333',
                borderOpacity: 0.5,
                borderWidth: 3,
                color: '#a67c52',
                enableZoom: false,
                hoverColor: '#c9dfaf',
                hoverOpacity: null,
                normalizeFunction: 'linear',
                scaleColors: ['#b6d6ff', '#005ace'],
                selectedColor: '#c9dfaf',
                selectedRegion: null,
                showTooltip: true,
                onRegionClick: function(el, code, region, no_prop) {
                    getDataOutletByProp(no_prop);
                }
            });
            function getDataOutletByProp(no_prop) { 
                $.ajax({ url : "<?= base_url('front/rest_api/outlet')?>",
                    type: "GET",
                    dataType: "JSON",
                    data: {no_prop:no_prop},
                    async: false,        
                    beforeSend: function(){
                        // $("#cover-spin").show();
                    },
                    success: function(data){
                        // swal('Data '+type+' berhasil dihapus','success');
                        // document.location = data.url;
                        // console.log(data);
                        if (data.length == 0) {
                            Swal.fire('Data Kosong!','Data yang diperoleh kosong','info');
                        } else {
                            let valHTML = "";
                            data.map(function (e) {
                                // console.log(e);
                                valHTML += '<div class="col-lg-6 py-3"><div class="row border border-dark rounded mx-1 btn-kategori-menu"><div class="col-lg-12 my-auto text-left py-3"><h2 class="text-uppercase text-black">'+e.nama_outlet+' #'+e.kode_outlet+'</h1><p class="text-black text-capitalize">'+e.alamat_mitra+'</p><hr><a href="'+e.link_maps+'" target="_blank" style="color: #4441ff;"><i class="fas fa-map-marker-alt"></i> Lokasi Outlet</a></div></div></div>';
                            });
                            $('#modalBodyRow').html(valHTML);
                            $("#myModal").modal();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        Swal.fire('Error!','Gagal memproses data dari ajax','error');
                    }
                }).done(function() {
                    setTimeout(function(){
                        // $("#cover-spin").hide();
                        // spin();
                    },500);
                });
            }
        </script>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

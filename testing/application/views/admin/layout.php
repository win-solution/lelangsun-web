
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('part/head.php') ?>
</head>
<body class="hold-transition sidebar-mini">
    <div id="cover-spin" style="display:none;"></div>
    <div class="wrapper">
        <?php 
            include('part/navbar.php');
            include('part/sidemenu.php');
        ?>
        
        <?= $content; ?>
        
        <?php include('part/footer.php') ?>

    </div>
    <!-- ./wrapper -->

    <?php include('part/js_part.php') ?>
</body>
</html>

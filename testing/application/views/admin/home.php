<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Peserta</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Peserta</a></li>
                </ol>
            </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= base_url().'assets/'; ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
        <script src="<?= base_url().'assets/'; ?>plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <!-- <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.flash.min.js"></script> -->
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
        <script src="<?= base_url().'assets/'; ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
        <script>
            var kelas = "";
        </script>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar Peserta <?= APP_NAME ?></h3>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content_tab_1" data-toggle="pill" href="#tab_1" role="tab" aria-controls="tab_1" aria-selected="true" title="Peserta Email Terverifikasi">Peserta Email Terverifikasi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="content_tab_2" data-toggle="pill" href="#tab_2" role="tab" aria-controls="tab_3" aria-selected="false" title="Peserta Email Belum Terverifikasi">Peserta Email Belum Terverifikasi</a>
                                </li>
                            </ul>
                            <br>
                            <div class="tab-content" id="custom-tabs-four-tabContent">
                                <div class="tab-pane fade active show" id="tab_1" role="tabpanel" aria-labelledby="content_tab_1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title" >Custom Filter : </h3>
                                        </div>
                                        <div class="panel-body row">
                                            <form id="form-filter" class="form-horizontal col-12">
                                                <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <label for="kelas" class="col-sm-2 control-label">Kelas</label>
                                                        <?php echo $form_kelas; ?>
                                                    </div>
                                                    <!-- <div class="form-group col-lg-6">
                                                        <label for="search" class="col-sm-2 control-label">Kata Pencarian</label>
                                                        <input type="text" class="form-control filter-input" id="search" placeholder="cari disini" onChange="">
                                                    </div>                                                     -->
                                                </div>
                                                <!-- <div class="form-group">
                                                    <label for="LastName" class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-4">
                                                        <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                                                        <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
                                                    </div>
                                                </div> -->
                                            </form>
                                        </div>
                                    </div>
                                    <table id="tablePeserta" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Kelas</th>
                                                <th>Registration Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="tab_2" role="tabpane2" aria-labelledby="content_tab_2">
                                    <table id="tableDelPeserta" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Kelas</th>
                                                <th>Registration Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>                            
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <script>
            var table, table_trash;
            $(function () {
                table = $('#tablePeserta').DataTable({ 
                    "responsive": true,
                    "autoWidth": false,
                    "scrollX": true,
                    "processing"  : true, //Feature control the processing indicator.
                    "serverSide"  : true, //Feature control DataTables' server-side processing mode.
                    "searchDelay" : 1 * 1000,
                    "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
                    "pageLength": 25,
                    "dom": 'lBifrtip',
                    "order": [], //Initial no order.
                    "buttons": [
                        {
                            extend: 'excelHtml5',
                            // title: 'Data Peserta <?= APP_NAME ?> '+Date.now(),
                            exportOptions: {
                                modifier: {
                                    page: 'all',
                                    search: 'applied',
                                    order:  'applied',
                                }
                            },
                        },
                        'print', 
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('admin/rest_api/dtPeserta?type=peserta')?>",
                        "type": "POST",
                        "data": function ( data ) {
                            data.kelas = $('#kelas').val();
                            // data.search = $('#search').val();
                        }
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        { 
                            "targets": [ 0, 3 ], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                });
                table_trash = $('#tableDelPeserta').DataTable({ 
                    "responsive": true,
                    "autoWidth": false,
                    "processing"  : true, //Feature control the processing indicator.
                    "serverSide"  : true, //Feature control DataTables' server-side processing mode.
                    "searchDelay" : 1 * 1000,
                    "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
                    "pageLength": 25,
                    "dom": 'lfrtip',
                    "order": [], //Initial no order.                    
                    "dom": 'lBfrtip',
                    "buttons": [
                        {
                            extend: 'excelHtml5',
                            title: 'Data Peserta Tidak Valid <?= APP_NAME ?>',
                            exportOptions: {
                                modifier: {
                                    page: 'all',
                                    search: 'applied',
                                    order:  'applied',
                                }
                            },
                        },
                        'print', 
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('admin/rest_api/dtPeserta?type=del_peserta')?>",
                        "type": "POST",
                        "data": function ( data ) {
                            data.kelas = $('#filterKelas').val();
                        }
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        { 
                            "targets": [ 0, 3 ], //first column / numbering column
                            "orderable": false, //set not orderable
                        },
                    ],
                });
            });
            $('#kelas').change(function(){ //class filter-input event change
                document.title = 'Data Peserta <?= APP_NAME ?> '+this.options[this.selectedIndex].text+' '+Date.now();
                setTimeout(() => {
                    table.ajax.reload();  //just reload table
                }, 1 * 1000);
            });
            setInterval( function () {
                table.ajax.reload(null,false);
                table_trash.ajax.reload(null,false);
            }, 3 * 60 * 1000 );
        </script>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!DOCTYPE html>
<html lang="en">
    <?php include('part/head.php') ?>
    <body id="page-top">
    	<?php include('part/nav.php') ?>

    	<header class="masthead" >
    	    <div class="container mw-100 mh-100" style=" background-image: url('assets/images/backgroundlot1.png'); height: 90vh; background-position: center; background-size: cover;">
    	        <div class="row justify-content-center h-100">
    	            <div class="col-12 col-lg-12 my-auto d-flex justify-content-center text-center">
    	                <!-- <div class="row"> -->
    	                    <div>
	                            <h1 class="text-uppercase text-white text-uppercase" style="font-size: 80px;">Lot</h1>
	                            <span class="subheading text-white" style="font-size: 30px;">Toyota</span>
	                        </div>

    	                <!-- </div> -->
    	            </div>
    	        </div>
    	    </div>
    	</header>

    	<section class="page-section portfolio" id="about" style="background-image: url('assets/images/backgroundlot1bawah.png'); background-size: cover; position: relative;">
    	    <div class="container mh-100 mw-90">
    	      <div class="row">
    	        <div class="col-12 offset-sm-2 col-md-3 mx-auto">
    	            <div class="card-body" style="background-color: #31869b;">
    	            	<h4 class="text-uppercase text-white p-5 text-center" style="color: #333;">Filter</h4>
    	            	  <div class="pb-4">
    	            	    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
    	            	      	<a class="nav-link active mb-3 dropdown-toggle" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Merek</a>
    	            	      	<li class="nav-item dropdown mb-3 active">
    	            	          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tahun</a>
    	            	          <div class="dropdown-menu">
    	            	            <a class="dropdown-item" href="#">Action</a>
    	            	            <a class="dropdown-item" href="#">Another action</a>
    	            	            <a class="dropdown-item" href="#">Something else here</a>
    	            	            <div class="dropdown-divider"></div>
    	            	            <a class="dropdown-item" href="#">Separated link</a>
    	            	          </div>
    	            	        </li>
    	            	        <li class="nav-item dropdown mb-3 active">
    	            	            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Kata</a>
    	            	            <div class="dropdown-menu">
    	            	              <a class="dropdown-item" href="#">Action</a>
    	            	              <a class="dropdown-item" href="#">Another action</a>
    	            	              <a class="dropdown-item" href="#">Something else here</a>
    	            	              <div class="dropdown-divider"></div>
    	            	              <a class="dropdown-item" href="#">Separated link</a>
    	            	            </div>
    	            	        </li>
    	            	        <li class="nav-item dropdown mb-3 active">
		            	            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Harga</a>
		            	            <div class="dropdown-menu">
		            	              <a class="dropdown-item" href="#">Action</a>
		            	              <a class="dropdown-item" href="#">Another action</a>
		            	              <a class="dropdown-item" href="#">Something else here</a>
		            	              <div class="dropdown-divider"></div>
		            	              <a class="dropdown-item" href="#">Separated link</a>
		            	            </div>
    	            	        </li>
    	            	    </div>
    	            	  </div>
    	            </div>
    	        </div>
    	        <div class="col-12 col-md-9 px-5">
			        	<div class="input-group mb-3">
		        	      	<div class="input-group-append">
		                        <div class="input-group-text">
		                        <span class="fas fa-search"></span>
		                        </div>
		                    </div>
                        <input type="search" name="search" class="form-control mr-4 form-control-lg" placeholder="Search">
                        <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                    </div>
    	            <div class="row row-cols-1 row-cols-md-3">
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil2.png" class="card-img-top" alt="...">
    	                  </div>
    	                  <div class="card-body">
    	                  	<p class="card-text">Lot 1</p>
    	                    <h5 class="card-title">Vios 1.5 G M/T</h5>
    	                    <p class="card-text">A 1076 TN</p>
    	                    <h5 class="card-title">Rp 36.000.000</h5>
    	                    <p class="card-text"><small class="text-muted"><i class="fas fa-eye"></i>1000  <i class="far fa-user"></i>admin  <i class="fas fa-calendar-alt"></i>Jan 20, 2018</small></p>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil3.jpg" class="card-img-top" alt="...">
    	                  </div>
    	                  <div class="card-body">
    	                  	<p class="card-text">Lot 2</p>
    	                    <h5 class="card-title">APV 1.5 DLX M/T 2012</h5>
    	                    <p class="card-text">B 1613 BKN</p>
    	                    <h5 class="card-title">Rp 60.000.000</h5>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil4.png" class="card-img-top" alt="...">
    	              	  </div>
    	                  <div class="card-body">
    	                  	<p class="card-text">Lot 3</p>
    	                    <h5 class="card-title">Ertiga 1.4 GX M/T 2012</h5>
    	                    <p class="card-text">DT 48 IB</p>
    	                    <h5 class="card-title">Rp 72.000.000</h5>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil6.jpg" class="card-img-top" alt="...">
    	                  </div>
    	                  <div class="card-body">
    	                  	<p class="card-text">Lot 4</p>
    	                    <h5 class="card-title">JUKE 2011</h5>
    	                    <p class="card-text">B 1449 KVL</p>
    	                    <h5 class="card-title">Rp 93.000.000</h5>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil2.png" class="card-img-top" alt="...">
    	              	  </div>
    	                  <div class="card-body">
    	                  	<p class="card-text">Lot 1</p>
    	                    <h5 class="card-title">Vios 1.5 G M/T</h5>
    	                    <p class="card-text">A 1076 TN</p>
    	                    <h5 class="card-title">Rp 36.000.000</h5>
    	                    <p class="card-text"><small class="text-muted"><i class="fas fa-eye"></i>1000  <i class="far fa-user"></i>admin  <i class="fas fa-calendar-alt"></i>Jan 20, 2018</small></p>
    	                  </div>
    	                </div>
    	              </div>
    	              <div class="col mb-4">
    	                <div class="card h-100 btn text-left">
    	                  <div class="card-body mw-90 mx-auto">
    	                  	<img src="assets/images/mobil3.jpg" class="card-img-top" alt="...">
    	                  </div>
    	                  <div class="card-body">
    	                  	<p class="card-text">Lot 2</p>
    	                    <h5 class="card-title">APV 1.5 DLX M/T 2012</h5>
    	                    <p class="card-text">B 1613 BKN</p>
    	                    <h5 class="card-title">Rp 60.000.000</h5>
    	                  </div>
    	                </div>
    	              </div>
    	            </div>
    	        </div>
    	      </div>
    	    </div>
    	</section>

    </body>
</html>
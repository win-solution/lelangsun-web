<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function random_code($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function create_link($string,$kanal="detail_berita") {
	$string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
 
	return $kanal."/".time()."-".preg_replace('/[^A-Za-z0-9\_]/', '', $string); // Removes special chars.
}

function date_indonesia($format = 'd F, Y',$timestamp = NULL) {
    $l = array('', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Minggu');
    $F = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

    $return = '';
    if(is_null($timestamp)) { $timestamp = mktime(); }
    for($i = 0, $len = strlen($format); $i < $len; $i++) {
        switch($format[$i]) {
            case '\\' :
                $i++;
                $return .= isset($format[$i]) ? $format[$i] : '';
                break;
            case 'l' :
                $return .= $l[date('N', $timestamp)];
                break;
            case 'F' :
                $return .= $F[date('n', $timestamp)];
                break;
            default :
                $return .= date($format[$i], $timestamp);
                break;
        }
    }
    return $return;
}
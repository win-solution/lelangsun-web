<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_global extends CI_Model {   

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($type=null,$id=null) {
        switch ($type) {
            case 'user':
            case 'del_user':
                $column_order = array('p.id_user', 'p.nama_user', 'p.email', 'p.role'); //set column field database for datatable orderable
                $column_search = array('p.nama_user', 'p.email','p.role'); //set column field database for datatable searchable
                $order = array('p.id_user' => 'desc'); // default order

                $this->db->select("p.*");
                $this->db->from("m_user as p");
                break;
            case 'berita':
            case 'del_berita':
                $column_order = array('b.id_berita', 'b.judul_berita'); //set column field database for datatable orderable
                $column_search = array('b.judul_berita'); //set column field database for datatable searchable
                $order = array('b.id_berita' => 'desc'); // default order

                $this->db->select("b.*");
                $this->db->from("t_berita as b");
                break;
            case 'kategori':
            case 'del_kategori':
                $column_order = array('k.id_kategori_menu', 'k.nama_kategori_menu'); //set column field database for datatable orderable
                $column_search = array('k.nama_kategori_menu'); //set column field database for datatable searchable
                $order = array('k.id_kategori_menu' => 'desc'); // default order

                $this->db->select("k.*");
                $this->db->from("m_kategori_menu as k");
                break;
            case 'menu':
            case 'del_menu':
                $column_order = array('m.id_menu', 'm.nama_menu'); //set column field database for datatable orderable
                $column_search = array('m.nama_menu'); //set column field database for datatable searchable
                $order = array('m.id_menu' => 'desc'); // default order

                $this->db->select("m.*");
                $this->db->from("m_menu as m");
                break;
            case 'mitra':
            case 'del_mitra':
                $column_order = array('m.id_mitra', 'm.nama_mitra', 'm.email', 'm.no_tlp', 'm.alamat_mitra', 'm.follow_up'); //set column field database for datatable orderable
                $column_search = array( 'm.nama_mitra', 'm.email', 'm.no_tlp', 'm.alamat_mitra'); //set column field database for datatable searchable
                $order = array('m.id_mitra' => 'desc'); // default order

                $this->db->select("m.*, o.id_outlet");
                $this->db->from("t_mitra as m");
                $this->db->join("t_outlet as o", "m.id_mitra = o.id_mitra", "left");
                break;
            case 'outlet':
            case 'del_outlet':
                $column_order = array('o.id_outlet', 'o.nama_outlet', 'mi.alamat_mitra'); //set column field database for datatable orderable
                $column_search = array( 'o.nama_outlet', 'mi.nama_mitra', 'mi.email', 'mi.no_tlp', 'mi.alamat_mitra'); //set column field database for datatable searchable
                $order = array('o.id_outlet' => 'desc'); // default order

                $this->db->select("o.*, mi.id_mitra, mi.nama_mitra, mi.email, mi.no_tlp, mi.alamat_mitra");
                $this->db->from("t_outlet as o");
                $this->db->join("t_mitra as mi", "mi.id_mitra = o.id_mitra", "left");
                break;
            case 'kontak':
            case 'del_kontak':
                $column_order = array('m.id_kontak', 'm.nama', 'm.email', 'm.no_tlp', 'm.pesan'); //set column field database for datatable orderable
                $column_search = array( 'm.nama', 'm.email', 'm.no_tlp', 'm.pesan'); //set column field database for datatable searchable
                $order = array('m.id_kontak' => 'desc'); // default order

                $this->db->select("m.*");
                $this->db->from("t_kontak as m");
                break;

            default:
                # code...
                break;
        }

        $i = 0;
        foreach ($column_search as $item) // loop column 
        {
            if (isset($_POST['search'])) {
                if($_POST['search']['value']) // if datatable send POST for search
                {                     
                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
     
                    if(count($column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
        }
        
        if (!isset($order_req)) {
            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }  else if(isset($order)) {           
                foreach ($order as $key => $value) {
                    $this->db->order_by($key, $value);
                } 
            }
        }  
    }

    public function get_datatables($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user': $this->db->where("p.status = 1"); break;
            case 'del_user': $this->db->where("p.status = 0"); break;

            case 'berita': $this->db->where("b.status = 1"); break;
            case 'del_berita': $this->db->where("b.status = 0"); break;

            case 'kategori': $this->db->where("k.status = 1"); break;
            case 'del_kategori': $this->db->where("k.status = 0"); break;

            case 'menu': $this->db->where("m.status = 1"); break;
            case 'del_menu': $this->db->where("m.status = 0"); break;

            case 'mitra': $this->db->where("m.status = 1"); break;
            case 'del_mitra': $this->db->where("m.status = 0"); break;

            case 'outlet': $this->db->where("o.status = 1"); break;
            case 'del_outlet': $this->db->where("o.status = 0"); break;

            case 'kontak': $this->db->where("m.status = 1"); break;
            case 'del_kontak': $this->db->where("m.status = 0"); break;

            default:
                # code...
                break;
        }

        if($_POST['length'] != -1){
        	$this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function count_filtered($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user': $this->db->where("p.status = 1"); break;
            case 'del_user': $this->db->where("p.status = 0"); break;

            case 'berita': $this->db->where("b.status = 1"); break;
            case 'del_berita': $this->db->where("b.status = 0"); break;

            case 'kategori': $this->db->where("k.status = 1"); break;
            case 'del_kategori': $this->db->where("k.status = 0"); break;

            case 'menu': $this->db->where("m.status = 1"); break;
            case 'del_menu': $this->db->where("m.status = 0"); break;

            case 'mitra': $this->db->where("m.status = 1"); break;
            case 'del_mitra': $this->db->where("m.status = 0"); break;

            case 'outlet': $this->db->where("o.status = 1"); break;
            case 'del_outlet': $this->db->where("o.status = 0"); break;

            case 'kontak': $this->db->where("m.status = 1"); break;
            case 'del_kontak': $this->db->where("m.status = 0"); break;

            default:
                # code...
                break;
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($type=null,$id=null) {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user': $this->db->where("p.status = 1"); break;
            case 'del_user': $this->db->where("p.status = 0"); break;

            case 'berita': $this->db->where("b.status = 1"); break;
            case 'del_berita': $this->db->where("b.status = 0"); break;

            case 'kategori': $this->db->where("k.status = 1"); break;
            case 'del_kategori': $this->db->where("k.status = 0"); break;

            case 'menu': $this->db->where("m.status = 1"); break;
            case 'del_menu': $this->db->where("m.status = 0"); break;

            case 'mitra': $this->db->where("m.status = 1"); break;
            case 'del_mitra': $this->db->where("m.status = 0"); break;

            case 'outlet': $this->db->where("o.status = 1"); break;
            case 'del_outlet': $this->db->where("o.status = 0"); break;

            case 'kontak': $this->db->where("m.status = 1"); break;
            case 'del_kontak': $this->db->where("m.status = 0"); break;

            default:
                # code...
                break;
        }

        return $this->db->count_all_results();
    }

    public function getDataQuery($querySQL) {
        $query = $this->db->query($querySQL);
        return $query->result();
    }

    public function getData($data) {
        $this->db->select($data['select']);

        if (isset($data['table'])) {
            $this->db->from($data['table']);
        } elseif (isset($data['from'])) {
            $this->db->from($data['from']);
        }

        if (isset($data['where'])) {
            $this->db->where($data['where']);
        }

        if (isset($data['join'])) {
            foreach ($data['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }

        if (isset($data['limit'])) {
            if (is_array($data['limit'])) {
                $this->db->limit($data['limit']['0'],$data['limit']['1']);
            } else {
                $this->db->limit($data['limit']);
            }
        }
        
        if (isset($data['group_by'])) {
            $this->db->group_by($data['group_by']);
        }
        
        if (isset($data['order'])) {
            $this->db->order_by($data['order']['0'],$data['order']['1']);
        }

        return $this->db->get()->result();
    }
    
    public function getDataRows($data) {
        $this->db->select($data['select']);

        if (isset($data['table'])) {
            $this->db->from($data['table']);
        } elseif (isset($data['from'])) {
            $this->db->from($data['from']);
        }

        if (isset($data['where'])) {
            $this->db->where($data['where']);
        }

        if (isset($data['join'])) {
            foreach ($data['join'] as $join) {
                $this->db->join($join['0'],$join['1']);
            }
        }

        if (isset($data['limit'])) {
            if (is_array($data['limit'])) {
                $this->db->limit($data['limit']['0'],$data['limit']['1']);
            } else {
                $this->db->limit($data['limit']);
            }
        }
        
        if (isset($data['group_by'])) {
            $this->db->group_by($data['group_by']);
        }
        
        if (isset($data['order'])) {
            $this->db->order_by($data['order']['0'],$data['order']['1']);
        }

        return $this->db->get()->num_rows();
    } 

    public function addData($value=null) {
        $this->db->insert($value['table'],$value['data']);
        $id = $this->db->insert_id();
        return $id;
    }
    
    public function updateData($value) {
        if (isset($value['where'])) {
            foreach ($value['where'] as $where) {
                $this->db->where($where['0'],$where['1']);
            }
        }        
        $this->db->set($value['data']);
        $this->db->update($value['table']);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    public function save($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    } 

    public function edit($table, $data, $where) {
        $this->db->set($data);
        $this->db->where($where['0'],$where['1']);
        $this->db->update($table);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    public function delById($value) {
        $data = array('status' => '0',);

        if (isset($value['field'])) {
            $this->db->where($value['field'], $value['id']);
        } else {
            $this->db->where($value['search'], $value['id']);
        }        
        
        $this->db->update($value['table'], $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    public function restorById($value) {
        $data = array('status' => '1',);        

        if (isset($value['field'])) {
            $this->db->where($value['field'], $value['id']);
        } else {
            $this->db->where($value['search'], $value['id']);
        }

        $this->db->update($value['table'], $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }
}
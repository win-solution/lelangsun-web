<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_setting extends CI_Model {   

    public function __construct() {
        parent::__construct();
    }

    public function getListSetting() {
        $this->db->select('name_setting,value');
        $this->db->from('m_setting');
        return $this->db->get()->result();
    }

    public function save($data) {
        $this->db->insert('m_setting', $data);
        return $this->db->insert_id();
    } 

    public function update($table, $data, $where) {
        $this->db->set($data);
        $this->db->where($where['0'],$where['1']);
        $this->db->update($table);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    } 

}
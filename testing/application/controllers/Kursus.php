<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kursus extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('model_Kategori');
        $this->load->model('model_mentor');
        $this->load->model('model_kursus');
        $this->load->model('model_materi');
        $this->load->model('model_belajar');
        // $this->model_squrity->getsqurity(); 
    }

    public function index()
    {
        $get_mata_pelajaran = $this->model_mata_pelajaran->get_all_aktif();
        $kategori_result = array();
        $data_header['kategori_list'] = $kategori_result;
        $view['header_menu'] = $this->load->view('header_menu',$data_header,true);
        $view['content'] = $this->load->view('page/home','',true);
        $view['footer'] = $this->load->view('footer','',true);
        $this->load->view('base_view', $view);
        // echo json_encode($list_kursus_saya_data);
    }

    public function index_by_id_kursus($id_kursus)
    {
        $list_kursus_saya_data = $this->model_kursus->get_by_id_murid($this->session->userdata('id_murid'));
        $kursus_saya_result = array();
        foreach ($list_kursus_saya_data as $row) {
            $data_kursus_row['id_join_kursus'] = $row->id_join_kursus;
            $data_kursus_row['id_murid'] = $row->id_murid;
            $data_kursus_row['id_mentor'] = $row->id_mentor;
            $data_kursus_row['id_user'] = $row->id_user;
            $data_kursus_row['id_kursus'] = $row->id_kursus;
            $data_kursus_row['nama_user'] = $row->nama_user;
            // $nama_matapelajaran = $row->materi;
            // $matapelajaran = substr($nama_matapelajaran,0,30); // ambil sebanyak 30 karakter
            // $matapelajaran = substr($nama_matapelajaran,0,strrpos($matapelajaran," ")); // potong per spasi kalimat
            // $data_kursus_row['matapelajaran'] = $matapelajaran.'...';
            $data_kursus_row['no_hp'] = $row->no_hp;
            $data_kursus_row['email'] = $row->email;
            $data_kursus_row['alamat'] = $row->alamat;
            $data_kursus_row['tanggal_lahir'] = $row->tanggal_lahir;
            $data_kursus_row['agama'] = $row->agama;
            $data_kursus_row['jenis_kelamin'] = $row->jenis_kelamin;
            $data_kursus_row['path_foto'] = $row->path_foto;
            $data_kursus_row['nama_kursus'] = $row->nama_kursus;
            $data_kursus_row['card_kursus'] = $row->card_kursus;
            $nana_mentor = $this->model_mentor->get_by_id($row->id_mentor);
            $data_kursus_row['nama_mentor'] = $nana_mentor->nama_user;
            $data_kursus_row['nama_kategori'] = $row->nama_kategori;
            $data_kursus_row['background'] = $row->background;
            $kursus_saya_result[] = $data_kursus_row;
        }
        $get_parent_kategori = $this->model_Kategori->get_parent();
        $kategori_result = array();
        foreach ($get_parent_kategori as $row_kategori) {           
            $data_kategori['id_kategori'] = $row_kategori->id_kategori;         
            $data_kategori['nama_kategori'] = $row_kategori->nama_kategori;     
            $data_kategori['icon'] = $row_kategori->icon;           
            $data_kategori['icon_web'] = $row_kategori->icon_web;           
            $data_kategori['background'] = $row_kategori->background;   
            $get_kategori_by_id_parent = $this->model_Kategori->get_by_id_kategori($row_kategori->id_kategori);
            $data_kategori['child'] = $get_kategori_by_id_parent;
            $kategori_result [] = $data_kategori;
        }
        $data_header['list_kursus_saya'] = $kursus_saya_result;
        $data_header['kategori_list'] = $kategori_result;
        $data['id_kursus'] = $id_kursus;
        $view['header_menu'] = $this->load->view('header_menu',$data_header,true);
        $view['content'] = $this->load->view('page/page_kursus',$data,true);
        $view['footer'] = $this->load->view('footer','',true);
        $this->load->view('base_view', $view);
        // echo json_encode($list_kursus_saya_data);
    }

    public function get_top_kursus(){
        $top_kursus = $this->model_kursus->get_top();
        $id_murid = $this->session->userdata('id_murid');

        $resoult_top_kursus = array();
        foreach ($top_kursus as $row_top_kursus) {
            $data_kursus['id_user'] = $row_top_kursus->id_user;
            $data_kursus['id_kursus'] = $row_top_kursus->id_kursus;
            $data_kursus['id_mentor'] = $row_top_kursus->id_mentor;
            $data_kursus['id_sub_kategori'] = $row_top_kursus->id_sub_kategori;
            $data_kursus['id_kategori'] = $row_top_kursus->id_kategori;
            $data_kursus['nama_user'] = $row_top_kursus->nama_user;
            $data_kursus['no_hp'] = $row_top_kursus->no_hp;
            $data_kursus['email'] = $row_top_kursus->email;
            $data_kursus['alamat'] = $row_top_kursus->alamat;
            $data_kursus['tanggal_lahir'] = $row_top_kursus->tanggal_lahir;
            $data_kursus['agama'] = $row_top_kursus->agama;
            $data_kursus['jenis_kelamin'] = $row_top_kursus->jenis_kelamin;
            $data_kursus['path_foto'] = $row_top_kursus->path_foto;
            $data_kursus['nama_kursus'] = $row_top_kursus->nama_kursus;
            $data_kursus['card_kursus'] = $row_top_kursus->card_kursus;
            $data_kursus['nama_kategori'] = $row_top_kursus->nama_kategori;
            $data_kursus['icon'] = $row_top_kursus->icon;
            $data_kursus['icon_web'] = $row_top_kursus->icon_web;
            $data_kursus['background'] = $row_top_kursus->background;
            $data_kursus['nama_sub_kategori'] = $row_top_kursus->nama_sub_kategori;
            $get_kurus_by_id_murid = $this->model_kursus->get_by_id_murid_id_kursus($id_murid, $row_top_kursus->id_kursus);
            if ($get_kurus_by_id_murid) {
                $data_kursus['status_belajar'] = '1';
            }else{
                $data_kursus['status_belajar'] = '0';
            }

            $resoult_top_kursus[] = $data_kursus;
        }

        header('Content-Type: application/json');
        echo json_encode($resoult_top_kursus);
    }

    public function get_kursus_by_id_kategori($id_kategori){
        $kursus_by_id_kategori = $this->model_kursus->get_by_id_kategori($id_kategori);
        $id_murid = $this->session->userdata('id_murid');

        $resoult_top_kursus = array();
        foreach ($kursus_by_id_kategori as $row_top_kursus) {
            $data_kursus['id_user'] = $row_top_kursus->id_user;
            $data_kursus['id_kursus'] = $row_top_kursus->id_kursus;
            $data_kursus['id_mentor'] = $row_top_kursus->id_mentor;
            $data_kursus['id_sub_kategori'] = $row_top_kursus->id_sub_kategori;
            $data_kursus['id_kategori'] = $row_top_kursus->id_kategori;
            $data_kursus['nama_user'] = $row_top_kursus->nama_user;
            $data_kursus['no_hp'] = $row_top_kursus->no_hp;
            $data_kursus['email'] = $row_top_kursus->email;
            $data_kursus['alamat'] = $row_top_kursus->alamat;
            $data_kursus['tanggal_lahir'] = $row_top_kursus->tanggal_lahir;
            $data_kursus['agama'] = $row_top_kursus->agama;
            $data_kursus['jenis_kelamin'] = $row_top_kursus->jenis_kelamin;
            $data_kursus['path_foto'] = $row_top_kursus->path_foto;
            $data_kursus['nama_kursus'] = $row_top_kursus->nama_kursus;
            $data_kursus['card_kursus'] = $row_top_kursus->card_kursus;
            $data_kursus['nama_kategori'] = $row_top_kursus->nama_kategori;
            $data_kursus['icon'] = $row_top_kursus->icon;
            $data_kursus['icon_web'] = $row_top_kursus->icon_web;
            $data_kursus['background'] = $row_top_kursus->background;
            $data_kursus['nama_sub_kategori'] = $row_top_kursus->nama_sub_kategori;
            $get_kurus_by_id_murid = $this->model_kursus->get_by_id_murid_id_kursus($id_murid, $row_top_kursus->id_kursus);
            if ($get_kurus_by_id_murid) {
                $data_kursus['status_belajar'] = '1';
            }else{
                $data_kursus['status_belajar'] = '0';
            }

            $resoult_top_kursus[] = $data_kursus;
        }

        header('Content-Type: application/json');
        echo json_encode($resoult_top_kursus);
    }

    public function get_by_id_kursus($id_kursus){
        $id_murid = $this->session->userdata('id_murid');
        $kursus_by_id_kursus = $this->model_kursus->get_by_id($id_kursus);        
        $materi_by_id_kursus = $this->model_materi->get_by_id_kursus($id_kursus);

        $resoult_materi_belum = array();
        $resoult_materi_sudah = array();
        foreach ($materi_by_id_kursus as $row_materi) {            
            $data_materi['id_materi'] = $row_materi->id_materi;
            $data_materi['id_sesi'] = $row_materi->id_sesi;
            $data_materi['id_kursus'] = $row_materi->id_kursus;
            $data_materi['id_mentor'] = $row_materi->id_mentor;
            $data_materi['id_user'] = $row_materi->id_user;
            $data_materi['id_sub_kategori'] = $row_materi->id_sub_kategori;
            $data_materi['id_kategori'] = $row_materi->id_kategori;
            $data_materi['nama_materi'] = $row_materi->nama_materi;
            $data_materi['embed_video'] = $row_materi->embed_video;
            $tumnile = str_replace('"', "", $row_materi->embed_video);
            $tumnile = explode('/', $tumnile);
            $data_materi['tumnile'] = 'https://img.youtube.com/vi/'.$tumnile[4].'/sddefault.jpg';
            $data_materi['path_pdf'] = $row_materi->path_pdf;
            $data_materi['nama_sesi'] = $row_materi->nama_sesi;
            $data_materi['nama_kursus'] = $row_materi->nama_kursus;
            $data_materi['card_kursus'] = $row_materi->card_kursus;
            $data_materi['nama_user'] = $row_materi->nama_user;
            $data_materi['nama_sub_kategori'] = $row_materi->nama_sub_kategori;
            $data_materi['nama_kategori'] = $row_materi->nama_kategori;
            $data_materi['icon'] = $row_materi->icon;
            $data_materi['icon_web'] = $row_materi->icon_web;
            $data_materi['background'] = $row_materi->background;
            $data_materi['status'] = $row_materi->status;

            $belajar_by_id_materi_id_murid = $this->model_belajar->get_id_murid_id_materi($id_murid, $row_materi->id_materi);
            if ($belajar_by_id_materi_id_murid) {
                $data_materi['status_belajar'] = '1';
                $resoult_materi_sudah[] = $data_materi;
            }else{
                $data_materi['status_belajar'] = '0';
                $resoult_materi_belum[] = $data_materi;
            }
        }

        $data['kursus'] = $kursus_by_id_kursus;
        $data['materi_sudah'] = $resoult_materi_sudah;
        $data['materi_belum'] = $resoult_materi_belum;
        $data['jml_materi_sudah'] = count($resoult_materi_sudah);
        $data['jml_materi_belum'] = count($resoult_materi_belum);

        if (count($resoult_materi_belum) > 0) {
            $data['mater_aktif'] = $resoult_materi_belum[0];
        }else{
            // $jumlah_materi = count($resoult_materi_sudah);
            // $data['mater_aktif'] = $resoult_materi_sudah[$jumlah_materi-1];

            if ($resoult_materi_sudah) {
                $data['mater_aktif'] = $resoult_materi_sudah[0];
            } else {
                $data['mater_aktif'] = '-';                
            }
        }

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function gabung_kursus($id_kursus){
        $id_murid = $this->session->userdata('id_murid');

        if (empty($id_murid)) {
            redirect('login');
        }

        $data_jawaban_kuis = array(
            'id_murid' => $id_murid,
            'id_kursus' => $id_kursus,
            'poin' => '5'
        );

        $simpan_jawaban_kuis = $this->model_kursus->save('t_join_kursus',$data_jawaban_kuis);

        if ($simpan_jawaban_kuis) {
             redirect('Kursus/index_by_id_kursus/'.$id_kursus);
        }
    }

}


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct() {
        parent::__construct();
		$this->load->model('model_setting');
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		if (empty($_SESSION['login_admin'])) {
			if ($_SESSION['login_admin'] !== TRUE) {
				redirect('auth');
			}			
		}
		// var_dump($_SESSION);
	}
	
	public function index() {
		$data = array();
		$mitra['select'] = "m.id_mitra";
		$mitra['from'] = "t_mitra as m";

		$mitra['where'] = "m.status = 1 and MONTH(m.created_at)='".date("m")."' and YEAR(m.created_at)='".date("Y")."' ";
		$data['calonMitra'] = $this->model_global->getDataRows($mitra);

		$mitra['where'] = "m.status = 1 and m.follow_up = 0";
		$data['unverifiedMitra'] = $this->model_global->getDataRows($mitra);

		// $mitra['join'][0] = array('t_outlet as o','o.id_mitra = m.id_mitra');
		$mitra['where'] = "m.status = 1 and m.follow_up = 1";
		$data['verifiedMitra'] = $this->model_global->getDataRows($mitra);

		$mitra['join'][0] = array('t_outlet as o','o.id_mitra = m.id_mitra');
		// $mitra['where'] = "m.status = 1 and m.follow_up = 1 and o.status = 1 and o.id_tipe_kemitraan IS NOT NULL";
		$mitra['where'] = "m.status = 1 and m.follow_up = 1 and o.status = 1";
		$data['totalOutlet'] = $this->model_global->getDataRows($mitra);

		$data['page'] = "dashboard";
		$view['content'] = $this->load->view('admin/dashboard',$data,true);
		$this->load->view('admin/layout',$view,false);
	}
	
	public function berita($action=null,$id=null) {
		if ($_SESSION['akses'] == 1) {
			$data = array();
			$data['page'] = "berita";
			// $data['page'] 	= array('p' => 'berita', 'c' => '' );
	
			$data['back']	= base_url('admin/berita');
			$data['proses'] = ucwords($action);

			switch ($action) {			
				case 'tambah':
					$data['action'] = base_url('admin/berita/tambah');
					if (isset($_POST['submit'])) {
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						$berita['data']['judul_berita'] = $_POST['judul_berita'];
						$berita['data']['tgl_publish'] = date_format(date_create($_POST['tgl_publish']),'Y-m-d H:i:s');
						if (strlen($_FILES['gambar']['name'])) {
							$berita['data']['gambar']	= unggah_gambar('./assets/uploads/berita', 'gambar', 'png|jpg|jpeg');
						};
						// var_dump($berita['data'],"<hr>");						
						// die();
						$berita['data']['isi_berita'] = $_POST['isi_berita'];
						$berita['data']['link_berita'] = create_link($_POST['judul_berita']);
						$berita['data']['created_at'] = date('Y-m-d H:i:s');
						$berita['data']['created_by'] = $_SESSION['id_user_admin'];
						$berita['data']['status'] = 1;
						// var_dump($berita);
						// die();
						$berita['table']		= "t_berita";

						$berita_link['data']['id_berita'] 	= $this->model_global->addData($berita);
						$berita_link['data']['link_berita'] = $berita['data']['link_berita'];
						$berita_link['data']['created_at'] = date('Y-m-d H:i:s');
						$berita_link['data']['created_by'] = $_SESSION['id_user_admin'];
						$berita_link['data']['status'] = 1;

						$berita_link['table']		= "t_link_berita";
						// redirect('admin/berita');
						if ($this->model_global->addData($berita_link)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil tambah";
							echo json_encode($json);
						} else {						
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						// var_dump($_SESSION);
						$data['page'] 	= array('p' => 'berita', 'c' => 'tambah berita menu' );
						$data['content'] 	= $this->load->view('admin/berita/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'ubah':
					$data['action'] = base_url('admin/berita/ubah');
					$berita_detail['select']	= "k.*";
					$berita_detail['table']		= "t_berita as k";
	
					if (isset($id)) {
						$berita_detail['where']	= "k.id_berita = '".$id."' and k.status = 1";
					} else {
						$berita_detail['where']	= "k.id_berita = '".$_POST['id_berita']."' and k.status = 1";
					}
	
					$data['berita_detail'] 	= $this->model_global->getData($berita_detail);
	
					if (isset($_POST['submit'])) {
						// var_dump($data['berita_detail'],"<hr>");
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();
						
						$berita['data']['judul_berita'] = $_POST['judul_berita'];
						$berita['data']['tgl_publish'] = date_format(date_create($_POST['tgl_publish']),'Y-m-d H:i:s');
						if (strlen($_FILES['gambar']['name'])) {
							$berita['data']['gambar']	= unggah_gambar('./assets/uploads/berita', 'gambar', 'png|jpg|jpeg');
							if ($berita['data']['gambar'] == "Gagal Upload") {
								$json['status'] = FALSE;
								// $json['msg'] = ;

								echo json_encode($json);
								die;
							}
						};
						// var_dump($berita['data'],"<hr>");						
						// die();
						$berita['data']['isi_berita'] = $_POST['isi_berita'];
						$berita['data']['updated_at'] = date('Y-m-d H:i:s');
						$berita['data']['updated_by'] = $_SESSION['id_user_admin'];
						$berita['table']		= "t_berita";
						$berita['where'][0] 	= array('id_berita', $_POST['id_berita']);
						// $this->model_global->updateData($berita);
						if($_POST['judul_berita'] !== $data['berita_detail'][0]->judul_berita){
							$berita['data']['link_berita'] = create_link($_POST['judul_berita']);

							$berita_link['data']['id_berita'] 	= $_POST['id_berita'];
							$berita_link['data']['link_berita'] = $berita['data']['link_berita'];
							$berita_link['data']['created_at'] = date('Y-m-d H:i:s');
							$berita_link['data']['created_by'] = $_SESSION['id_user_admin'];
							
							$berita_link['table']		= "t_link_berita";
							$this->model_global->addData($berita_link);
						}
						  // redirect('admin/berita');
						if ($this->model_global->updateData($berita)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil ubah";
							echo json_encode($json);
						} else {
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						//var_dump($data['berita_detail']);
						$data['page'] 	= array('p' => 'berita', 'c' => 'ubah kata kunci' );
						$data['content'] 	= $this->load->view('admin/berita/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'hapus':
					$data['id']		= $_POST['id'];
					$data['search']	= "id_berita";
					$data['table']	= "t_berita";
	
					$this->model_global->delById($data);
					echo json_encode(array('url' => base_url('admin/berita')));
				break;
	
				case 'restore':
					$data['id']		= $id;
					$data['search']	= "id_berita";
					$data['table']	= "t_berita";
	
					$this->model_global->restorById($data);
					redirect('admin/berita');
				break;
	
				default:
					// $data['page'] 		= array('p' => 'berita', 'c' => '' );
			
					$view['content'] = $this->load->view('admin/berita/berita',$data,true);
					$this->load->view('admin/layout',$view,false);
				break;
			}
		} else {
			redirect('admin');
		}
	}
	
	public function kontak($action=null,$id=null) {
		if ($_SESSION['akses'] == 1) {
			$data = array();
			$data['page'] = "kontak";
			// $data['page'] 	= array('p' => 'kontak', 'c' => '' );
	
			$data['back']	= base_url('admin/kontak');
			$data['proses'] = ucwords($action);

			switch ($action) {	
				case 'ubah':
					$data['proses'] = "Respon Kontak";
					$data['action'] = base_url('admin/kontak/ubah');
					$kontak_detail['select']	= "k.*";
					$kontak_detail['table']		= "t_kontak as k";
	
					if (isset($id)) {
						$kontak_detail['where']	= "k.id_kontak = '".$id."' and k.status = 1";
					} else {
						$kontak_detail['where']	= "k.id_kontak = '".$_POST['id_kontak']."' and k.status = 1";
					}
	
					$data['kontak_detail'] 	= $this->model_global->getData($kontak_detail);
	
					if (isset($_POST['submit'])) {
						// var_dump($data['kontak_detail'],"<hr>");
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();						
						
						$kontak['data']['nama'] = $_POST['nama'];						
						$kontak['data']['email'] = $_POST['email'];
						$kontak['data']['no_tlp'] = $_POST['no_tlp'];
						$kontak['data']['pesan'] = $_POST['pesan'];
						$kontak['data']['updated_at'] = date('Y-m-d H:i:s');
						$kontak['data']['updated_by'] = $_SESSION['id_user_admin'];
						$kontak['table']		= "t_kontak";
						$kontak['where'][0] 	= array('id_kontak', $_POST['id_kontak']);
						  // $this->model_global->updateData($data);
						  // redirect('admin/kontak');
						if ($this->model_global->updateData($kontak)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil ubah";
							echo json_encode($json);
						} else {
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						//var_dump($data['berita_detail']);
						$data['page'] 	= array('p' => 'kontak', 'c' => 'ubah kontak' );
						$data['content'] 	= $this->load->view('admin/kontak/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'hapus':
					$data['id']		= $_POST['id'];
					$data['search']	= "id_kontak";
					$data['table']	= "t_kontak";
	
					$this->model_global->delById($data);
					echo json_encode(array('url' => base_url('admin/kontak')));
				break;
	
				case 'restore':
					$data['id']		= $id;
					$data['search']	= "id_kontak";
					$data['table']	= "t_kontak";
	
					$this->model_global->restorById($data);
					redirect('admin/kontak');
				break;
	
				default:
					// $data['page'] 		= array('p' => 'kontak', 'c' => '' );
			
					$view['content'] = $this->load->view('admin/kontak/kontak',$data,true);
					$this->load->view('admin/layout',$view,false);
				break;
			}
		} else {
			redirect('admin');
		}
	}
	
	public function mitra($action=null,$id=null) {
		if ($_SESSION['akses'] == 1) {
			$data = array();
			$data['page'] = "mitra";
			// $data['page'] 	= array('p' => 'mitra', 'c' => '' );
	
			$data['back']	= base_url('admin/mitra');
			$data['proses'] = ucwords($action);

			switch ($action) {	
				case 'tambah':
					$data['action'] = base_url('admin/mitra/tambah');
					if (isset($_POST['submit'])) {
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();
						$mitra['data']['nama_mitra'] = $_POST['nama_mitra'];						
						$mitra['data']['email'] = $_POST['email'];						
						$mitra['data']['no_tlp'] = $_POST['no_tlp'];						
						$mitra['data']['alamat_mitra'] = $_POST['alamat_mitra'];
						$mitra['data']['created_at'] = date('Y-m-d H:i:s');
						$mitra['data']['created_by'] = $_SESSION['id_user_admin'];
						$mitra['data']['status'] = 1;
						// var_dump($mitra);
						// die();
						$mitra['table']		= "t_mitra";
						// $json['id_mitra_menu'] 	= $this->model_global->addData($data);
						// redirect('admin/mitra');
						if ($this->model_global->addData($mitra)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil tambah";
							echo json_encode($json);
						} else {						
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						// var_dump($_SESSION);
						$data['page'] 	= array('p' => 'mitra', 'c' => 'tambah mitra menu' );
						$data['content'] 	= $this->load->view('admin/mitra/form',$data,TRUE);
						echo json_encode($data);
					}
					break;

				case 'ubah':
					$data['action'] = base_url('admin/mitra/ubah');
					$mitra_detail['select']	= "k.*";
					$mitra_detail['table']		= "t_mitra as k";
	
					if (isset($id)) {
						$mitra_detail['where']	= "k.id_mitra = '".$id."' and k.status = 1";
					} else {
						$mitra_detail['where']	= "k.id_mitra = '".$_POST['id_mitra']."' and k.status = 1";
					}
	
					$data['mitra_detail'] 	= $this->model_global->getData($mitra_detail);
	
					if (isset($_POST['submit'])) {
						// var_dump($data['mitra_detail'],"<hr>");
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();						
						
						$mitra['data']['nama_mitra'] = $_POST['nama_mitra'];
						$mitra['data']['email'] = $_POST['email'];
						$mitra['data']['no_tlp'] = $_POST['no_tlp'];
						$mitra['data']['alamat_mitra'] = $_POST['alamat_mitra'];
						$mitra['data']['follow_up'] = isset($_POST['follow_up'])? $_POST['follow_up']:0;					
						$mitra['data']['pesan'] = $_POST['pesan'];					
						$mitra['data']['updated_at'] = date('Y-m-d H:i:s');
						$mitra['data']['updated_by'] = $_SESSION['id_user_admin'];
						$mitra['table']		= "t_mitra";
						$mitra['where'][0] 	= array('id_mitra', $_POST['id_mitra']);
						  // $this->model_global->updateData($data);
						  // redirect('admin/mitra');
						if ($this->model_global->updateData($mitra)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil ubah";
							echo json_encode($json);
						} else {
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						//var_dump($data['berita_detail']);
						$data['page'] 	= array('p' => 'mitra', 'c' => 'ubah mitra' );
						$data['content'] 	= $this->load->view('admin/mitra/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'hapus':
					$data['id']		= $_POST['id'];
					$data['search']	= "id_mitra";
					$data['table']	= "t_mitra";
	
					$this->model_global->delById($data);
					echo json_encode(array('url' => base_url('admin/mitra')));
				break;
	
				case 'restore':
					$data['id']		= $id;
					$data['search']	= "id_mitra";
					$data['table']	= "t_mitra";
	
					$this->model_global->restorById($data);
					redirect('admin/mitra');
				break;

				case 'active':
					$outlet['data']['id_mitra'] = $id;
					$outlet['data']['created_at'] = date('Y-m-d H:i:s');
					$outlet['data']['created_by'] = $_SESSION['id_user_admin'];
					$outlet['data']['status'] = 1;
					// var_dump($kategori);
					// die();
					$outlet['table']		= "t_outlet";
					$this->model_global->addData($outlet);
					redirect('admin/mitra');
				break;
	
				default:
					// $data['page'] 		= array('p' => 'mitra', 'c' => '' );
			
					$view['content'] = $this->load->view('admin/mitra/mitra',$data,true);
					$this->load->view('admin/layout',$view,false);
				break;
			}
		} else {
			redirect('admin');
		}
	}
	
	public function kategori($action=null,$id=null) {
		if ($_SESSION['akses'] == 1) {
			$data = array();
			$data['page'] = "kategori";
			// $data['page'] 	= array('p' => 'kategori', 'c' => '' );
	
			$data['back']	= base_url('admin/kategori');
			$data['proses'] = ucwords($action);

			switch ($action) {			
				case 'tambah':
					$data['action'] = base_url('admin/kategori/tambah');
					if (isset($_POST['submit'])) {
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();
						$kategori['data']['nama_kategori_menu'] = $_POST['nama_kategori_menu'];
						if (strlen($_FILES['gambar']['name'])) {
							$kategori['data']['gambar']	= unggah_gambar('./assets/uploads/kategori_menu', 'gambar', 'png|jpg|jpeg');
						};						
						$kategori['data']['deskripsi'] = $_POST['deskripsi'];
						$kategori['data']['created_at'] = date('Y-m-d H:i:s');
						$kategori['data']['created_by'] = $_SESSION['id_user_admin'];
						$kategori['data']['status'] = 1;
						// var_dump($kategori);
						// die();
						$kategori['table']		= "m_kategori_menu";
						// $json['id_kategori_menu'] 	= $this->model_global->addData($data);
						// redirect('admin/kategori');
						if ($this->model_global->addData($kategori)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil tambah";
							echo json_encode($json);
						} else {						
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						// var_dump($_SESSION);
						$data['page'] 	= array('p' => 'kategori', 'c' => 'tambah kategori menu' );
						$data['content'] 	= $this->load->view('admin/kategori/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'ubah':
					$data['action'] = base_url('admin/kategori/ubah');
					$kategori_detail['select']	= "k.*";
					$kategori_detail['table']		= "m_kategori_menu as k";
	
					if (isset($id)) {
						$kategori_detail['where']	= "k.id_kategori_menu = '".$id."' and k.status = 1";
					} else {
						$kategori_detail['where']	= "k.id_kategori_menu = '".$_POST['id_kategori_menu']."' and k.status = 1";
					}
	
					$data['kategori_detail'] 	= $this->model_global->getData($kategori_detail);
	
					if (isset($_POST['submit'])) {
						// var_dump($data['kategori_detail'],"<hr>");
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();						
						
						$kategori['data']['nama_kategori_menu'] = $_POST['nama_kategori_menu'];
						if (strlen($_FILES['gambar']['name'])) {
							$kategori['data']['gambar']	= unggah_gambar('./assets/uploads/kategori_menu', 'gambar', 'png|jpg|jpeg');
						};						
						$kategori['data']['deskripsi'] = $_POST['deskripsi'];
						$kategori['data']['deskripsi'] = $_POST['deskripsi'];
						$kategori['data']['updated_at'] = date('Y-m-d H:i:s');
						$kategori['data']['updated_by'] = $_SESSION['id_user_admin'];
						$kategori['table']		= "m_kategori_menu";
						$kategori['where'][0] 	= array('id_kategori_menu', $_POST['id_kategori_menu']);
						  // $this->model_global->updateData($data);
						  // redirect('admin/kategori');
						if ($this->model_global->updateData($kategori)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil ubah";
							echo json_encode($json);
						} else {
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						//var_dump($data['berita_detail']);
						$data['page'] 	= array('p' => 'kategori', 'c' => 'ubah kata kunci' );
						$data['content'] 	= $this->load->view('admin/kategori/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'hapus':
					$data['id']		= $_POST['id'];
					$data['search']	= "id_kategori_menu";
					$data['table']	= "m_kategori_menu";
	
					$this->model_global->delById($data);
					echo json_encode(array('url' => base_url('admin/kategori')));
				break;
	
				case 'restore':
					$data['id']		= $id;
					$data['search']	= "id_kategori_menu";
					$data['table']	= "m_kategori_menu";
	
					$this->model_global->restorById($data);
					redirect('admin/kategori');
				break;
	
				default:
					// $data['page'] 		= array('p' => 'kategori', 'c' => '' );
			
					$view['content'] = $this->load->view('admin/kategori/kategori',$data,true);
					$this->load->view('admin/layout',$view,false);
				break;
			}
		} else {
			redirect('admin');
		}
	}
	
	public function menu($action=null,$id=null) {
		if ($_SESSION['akses'] == 1) {
			$data = array();
			$data['page'] = "menu";
			// $data['page'] 	= array('p' => 'menu', 'c' => '' );
	
			$data['back']	= base_url('admin/menu');
			$data['proses'] = ucwords($action);
			$kategori_detail['select']	= "id_kategori_menu, nama_kategori_menu";
			$kategori_detail['table']	= "m_kategori_menu as k";
			$kategori_detail['where']	= "k.status = 1";

			$data['kategori_detail'] 	= $this->model_global->getData($kategori_detail);
			switch ($action) {			
				case 'tambah':
					$data['action'] = base_url('admin/menu/tambah');
					if (isset($_POST['submit'])) {
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();						
						$menu['data']['id_kategori_menu'] = implode(",",$_POST['id_kategori_menu']);;
						$menu['data']['nama_menu'] = $_POST['nama_menu'];
						if (strlen($_FILES['gambar']['name'])) {
							$menu['data']['gambar']	= unggah_gambar('./assets/uploads/menu', 'gambar', 'png|jpg|jpeg');
						};						
						$menu['data']['deskripsi'] = $_POST['deskripsi'];
						$menu['data']['isbest'] = (empty($_POST['isbest']))?0:1;
						$menu['data']['created_at'] = date('Y-m-d H:i:s');
						$menu['data']['created_by'] = $_SESSION['id_user_admin'];
						$menu['data']['status'] = 1;
						// var_dump($menu);
						// die();
						$menu['table']		= "m_menu";
						// $json['id_menu'] 	= $this->model_global->addData($data);
						// redirect('admin/menu');
						if ($this->model_global->addData($menu)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil tambah";
							echo json_encode($json);
						} else {						
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						// var_dump($_SESSION);
						$data['page'] 	= array('p' => 'menu', 'c' => 'tambah menu' );
						$data['content'] 	= $this->load->view('admin/menu/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'ubah':
					$data['action'] = base_url('admin/menu/ubah');
					$menu_detail['select']	= "k.*";
					$menu_detail['table']		= "m_menu as k";
	
					if (isset($id)) {
						$menu_detail['where']	= "k.id_menu = '".$id."' and k.status = 1";
					} else {
						$menu_detail['where']	= "k.id_menu = '".$_POST['id_menu']."' and k.status = 1";
					}
	
					$data['menu_detail'] 	= $this->model_global->getData($menu_detail);
	
					if (isset($_POST['submit'])) {
						// var_dump($data['menu_detail'],"<hr>");
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();						
						$menu['data']['id_kategori_menu'] = implode(",",$_POST['id_kategori_menu']);
						$menu['data']['nama_menu'] = $_POST['nama_menu'];
						if (strlen($_FILES['gambar']['name'])) {
							$menu['data']['gambar']	= unggah_gambar('./assets/uploads/menu', 'gambar', 'png|jpg|jpeg');
						};						
						$menu['data']['deskripsi'] = $_POST['deskripsi'];
						$menu['data']['deskripsi'] = $_POST['deskripsi'];
						$menu['data']['isbest'] = (empty($_POST['isbest']))?0:1;
						$menu['data']['updated_at'] = date('Y-m-d H:i:s');
						$menu['data']['updated_by'] = $_SESSION['id_user_admin'];
						$menu['table']		= "m_menu";
						$menu['where'][0] 	= array('id_menu', $_POST['id_menu']);
						  // $this->model_global->updateData($data);
						  // redirect('admin/menu');
						if ($this->model_global->updateData($menu)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil ubah";
							echo json_encode($json);
						} else {
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						//var_dump($data['berita_detail']);
						$data['page'] 	= array('p' => 'menu', 'c' => 'ubah kata kunci' );
						$data['content'] 	= $this->load->view('admin/menu/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'hapus':
					$data['id']		= $_POST['id'];
					$data['search']	= "id_menu";
					$data['table']	= "m_menu";
	
					$this->model_global->delById($data);
					echo json_encode(array('url' => base_url('admin/menu')));
				break;
	
				case 'restore':
					$data['id']		= $id;
					$data['search']	= "id_menu";
					$data['table']	= "m_menu";
	
					$this->model_global->restorById($data);
					redirect('admin/menu');
				break;
	
				default:
					// $data['page'] 		= array('p' => 'menu', 'c' => '' );
			
					$view['content'] = $this->load->view('admin/menu/menu',$data,true);
					$this->load->view('admin/layout',$view,false);
				break;
			}
		} else {
			redirect('admin');
		}
	}
	
	public function outlet($action=null,$id=null) {
		if ($_SESSION['akses'] == 1) {
			$data = array();
			$data['page'] = "outlet";
			// $data['page'] 	= array('p' => 'menu', 'c' => '' );
	
			$data['back']	= base_url('admin/outlet');
			$data['proses'] = ucwords($action);
			switch ($action) {	
				case 'ubah':

					$data['action'] = base_url('admin/outlet/ubah');
					$outlet_detail['select']	= "k.*, m.alamat_mitra";
					$outlet_detail['table']		= "t_outlet as k";
					$outlet_detail['join'][0]		= array('t_mitra as m', 'm.id_mitra = k.id_mitra' );
	
					if (isset($id)) {
						$outlet_detail['where']	= "k.id_outlet = '".$id."' and k.status = 1";
					} else {
						$outlet_detail['where']	= "k.id_outlet = '".$_POST['id_outlet']."' and k.status = 1";
					}
	
					$data['outlet_detail'] 	= $this->model_global->getData($outlet_detail);
	
					if (isset($_POST['submit'])) {
						// var_dump($data['menu_detail'],"<hr>");
						// var_dump($_POST,"<hr>",$_FILES,"<hr>");
						// die();
						$outlet['data']['id_tipe_kemitraan'] = $_POST['id_tipe_kemitraan'];
						$outlet['data']['kode_outlet'] = $_POST['kode_prop'].substr("00000".$_POST['nomor_outlet'],-5);
						$outlet['data']['nomor_outlet'] = $_POST['nomor_outlet'];
						$outlet['data']['nama_outlet'] = $_POST['nama_outlet'];
						$outlet['data']['kode_prop'] = $_POST['kode_prop'];
						// $outlet['data']['kode_kab'] = $_POST['kode_kab'];
						// $outlet['data']['kode_kec'] = $_POST['kode_kec'];
						// $outlet['data']['kode_kel'] = $_POST['kode_kel'];
						// $outlet['data']['latitude'] = $_POST['latitude'];
						// $outlet['data']['longitude'] = $_POST['longitude'];
						$outlet['data']['join_at'] = $_POST['join_at'];
						// $outlet['data']['nomor_outlet'] = $_POST['nomor_outlet']
						// if (strlen($_FILES['gambar']['name'])) {
						// 	$outlet['data']['gambar']	= unggah_gambar('./assets/uploads/outlet', 'gambar', 'png|jpg|jpeg');
						// };						
						$outlet['data']['link_maps'] = $_POST['link_maps'];
						$outlet['data']['updated_at'] = date('Y-m-d H:i:s');
						$outlet['data']['updated_by'] = $_SESSION['id_user_admin'];
						$outlet['table']		= "t_outlet";
						$outlet['where'][0] 	= array('id_outlet', $_POST['id_outlet']);

						$mitra['data']['alamat_mitra'] = $_POST['alamat_mitra'];
						$mitra['table']		= "t_mitra";
						$mitra['where'][0] 	= array('id_mitra', $_POST['id_mitra']);
						$this->model_global->updateData($mitra);
						  // $this->model_global->updateData($data);
						  // redirect('admin/menu');
						if ($this->model_global->updateData($outlet)) {
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil ubah";
							echo json_encode($json);
						} else {
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {						
						$tipe_kemitraan['select']	= "*";
						$tipe_kemitraan['table']	= "m_tipe_kemitraan";
						$tipe_kemitraan['where']	= "status = 1";
			
						$data['tipe_kemitraan'] 	= $this->model_global->getData($tipe_kemitraan);

						//var_dump($data['berita_detail']);
						$data['propinsi'] = $this->rest_api('getProp');
						$data['page'] 	= array('p' => 'outlet', 'c' => 'ubah outlet' );
						$data['content'] 	= $this->load->view('admin/outlet/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'hapus':
					$data['id']		= $_POST['id'];
					$data['search']	= "id_outlet";
					$data['table']	= "t_outlet";
	
					$this->model_global->delById($data);
					echo json_encode(array('url' => base_url('admin/outlet')));
				break;
	
				case 'restore':
					$data['id']		= $id;
					$data['search']	= "id_outlet";
					$data['table']	= "t_outlet";
	
					$this->model_global->restorById($data);
					redirect('admin/outlet');
				break;
	
				default:
					// $data['page'] 		= array('p' => 'menu', 'c' => '' );
			
					$view['content'] = $this->load->view('admin/outlet/outlet',$data,true);
					$this->load->view('admin/layout',$view,false);
				break;
			}
		} else {
			redirect('admin');
		}
	}

	public function user($action=null,$id=null) {
		if ($_SESSION['akses'] == 1) {
			$data = array();
			$data['page'] = "user";
			// $data['page'] 	= array('p' => 'user', 'c' => '' );
	
			$data['back']	= base_url('admin/user');
			$data['proses'] = ucwords($action);

			switch ($action) {			
				case 'tambah':
					$data['action'] = base_url('admin/user/tambah');
					if (isset($_POST['submit'])) {
						// var_dump($_POST,"<hr>");
						// die();
						// qecLAJgqcT
						$user['data']['role'] = (empty($_POST['role']))?0:1;
						$user['data']['nama_user'] = $_POST['nama_user'];
						$user['data']['email'] = $_POST['email'];
						$user['data']['password'] = sha1($_POST['password']);
						$user['data']['created_at'] = date('Y-m-d H:i:s');
						$user['data']['status'] = 1;
						// var_dump($user);
						// die();
						$user['table']		= "m_user";
						// $json['id_user'] 	= $this->model_global->addData($data);
						// redirect('admin/user');
						if ($this->model_global->addData($user)) {
							// $email_setting = (object) array();
							// $email_setting->email_from = $this->config->item('smtp_user');
							// $email_setting->email_to = $_POST['email'];
							// $email_setting->subject = "Access to news aggregator JN1";
							// $email_setting->message = "Berikut adalah password untuk login ke situs ".base_url()."<hr>";
							// $email_setting->message .= "Password : <b>".$_POST['password']."</b><hr>";
							// $email_setting->message .= "<b>*NB</b> : Diharap untuk menjaga kerahasiaan akses ini.";
	
							// send_email($email_setting);
							// $json['status'] = send_email($email_setting);
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil tambah";
							echo json_encode($json);
						} else {						
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						//var_dump($_SESSION);
						$data['page'] 	= array('p' => 'user', 'c' => 'tambah user' );
						$data['content'] 	= $this->load->view('admin/user/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'ubah':
					$data['action'] = base_url('admin/user/ubah');
					$user_detail['select']	= "k.*";
					$user_detail['table']		= "m_user as k";
	
					if (isset($id)) {
						$user_detail['where']	= "k.id_user = '".$id."' and k.status = 1";
					} else {
						$user_detail['where']	= "k.id_user = '".$_POST['id_user']."' and k.status = 1";
					}
	
					$data['user_detail'] 	= $this->model_global->getData($user_detail);
	
					if (isset($_POST['submit'])) {
						// var_dump($data['user_detail'],"<hr>");
						// var_dump($_POST,"<hr>");
						// die();						
						
						$user['data']['role'] 		= (empty($_POST['role']))?0:1;
						$user['data']['nama_user'] 	= $_POST['nama_user'];
						$user['data']['email'] 		= $_POST['email'];
						if (!empty($_POST['password'])) {
							$user['data']['password'] 	= sha1($_POST['password']);
						}
						$user['data']['updated_at'] = date('Y-m-d H:i:s');
						$user['data']['updated_by'] = $_SESSION['id_user_admin'];
						$user['table']		= "m_user";
						$user['where'][0] 	= array('id_user', $_POST['id_user']);
						  // $this->model_global->updateData($data);
						  // redirect('admin/user');
						if ($this->model_global->updateData($user)) {						
							// $email_setting = (object) array();
							// $email_setting->email_from = $this->config->item('smtp_user');
							// $email_setting->email_to = $_POST['email'];
							// $email_setting->subject = "Update access to news aggregator JN1";
							// $email_setting->message = "Berikut adalah password untuk login ke situs ".base_url()."<hr>";
							// $email_setting->message .= "Password : <b>".$_POST['password']."</b><hr>";
							// $email_setting->message .= "<b>*NB</b> : Diharap untuk menjaga kerahasiaan akses ini.";
							// send_email($email_setting);
							// $json['status'] = send_email($email_setting);
							$json['status'] = TRUE;
							$json['status_add'] = "berhasil ubah";
							echo json_encode($json);
						} else {
							$json['status'] = FALSE;
							echo json_encode($json);
						}
					} else {
						//var_dump($data['berita_detail']);
						$data['page'] 	= array('p' => 'user', 'c' => 'ubah kata kunci' );
						$data['content'] 	= $this->load->view('admin/user/form',$data,TRUE);
						echo json_encode($data);
					}
				break;
	
				case 'hapus':
					$data['id']		= $_POST['id'];
					$data['search']	= "id_user";
					$data['table']	= "m_user";
	
					$this->model_global->delById($data);
					echo json_encode(array('url' => base_url('admin/user')));
				break;
	
				case 'restore':
					$data['id']		= $id;
					$data['search']	= "id_user";
					$data['table']	= "m_user";
	
					$this->model_global->restorById($data);
					redirect('admin/user');
				break;
	
				default:
					// $data['page'] 		= array('p' => 'user', 'c' => '' );
			
					$view['content'] = $this->load->view('admin/user/user',$data,true);
					$this->load->view('admin/layout',$view,false);
				break;
			}
		} else {
			redirect('admin');
		}
	}
	
	public function setting() {
		if ($_SESSION['akses'] == 1) {
			$data = array();			
			$setting = $this->model_setting->getListSetting();
			$arrSetting = array();
			foreach ($setting as $key => $val) {
				$arrSetting[$val->name_setting] = $val->value;
			}
			$data['setting'] = (object) $arrSetting;
			if (!empty($_POST['submit'])) {
				$value = array();
				// var_dump($_POST,"<hr>");
				// var_dump(array_keys($arrSetting),"<hr>");
				// die;
				foreach ($_POST as $key => $val) {
					// var_dump(array_keys($arrSetting),"<hr>",$key,"<hr>",in_array($key, array_keys($arrSetting)));die;
					switch ($key) {
						case 'metadata_title':
							$value['name_setting'] = 'metadata_title';
							$value['value'] = $_POST['metadata_title'];
							break;
						case 'metadata_keyword':
							$value['name_setting'] = 'metadata_keyword';
							$value['value'] = $_POST['metadata_keyword'];
							break;
						case 'metadata_deskripsi':
							$value['name_setting'] = 'metadata_deskripsi';
							$value['value'] = $_POST['metadata_deskripsi'];
							break;
						case 'no_telp':
							$value['name_setting'] = 'no_telp';
							$value['value'] = $_POST['no_telp'];
							break;
						case 'no_wa':
							$value['name_setting'] = 'no_wa';
							$value['value'] = $_POST['no_wa'];
							break;
						case 'email':
							$value['name_setting'] = 'email';
							$value['value'] = $_POST['email'];
							break;
						case 'alamat':
							$value['name_setting'] = 'alamat';
							$value['value'] = $_POST['alamat'];
							break;
						case 'ig':
							$value['name_setting'] = 'ig';
							$value['value'] = $_POST['ig'];
							break;
						case 'fb':
							$value['name_setting'] = 'fb';
							$value['value'] = $_POST['fb'];
							break;
						case 'tw':
							$value['name_setting'] = 'tw';
							$value['value'] = $_POST['tw'];
							break;
						
						default:
							break;
					}
					// var_dump($value);
					// die;	
					if ($key !== 'submit') {
						if (in_array($key, array_keys($arrSetting))) {
							var_dump("masuk update");
							$this->model_setting->update('m_setting',$value,array('name_setting',$key));
						} else {
							var_dump("masuk save");
							$this->model_setting->save($value);
						}						
					}
				}			

				$this->session->set_flashdata('msg', 'Data Setting Berhasil Tersimpan.');
				redirect('admin/setting');
			} else {
				// var_dump(json_encode(array('fileIMG' => '', 'bulk' => '1',)));
				$data['page'] = "setting";
				// var_dump($data['setting']);die;
				$view['content'] = $this->load->view('admin/setting',$data,true);
				$this->load->view('admin/layout',$view,false);
			}
		} else {
			redirect('admin');
		}
	}

	public function rest_api($type) {
		switch ($type) {
			case 'dataNewUser':
				$value = $this->db->query('SELECT COUNT(id_user) as result FROM m_user WHERE DATE(created_at) = CURRENT_DATE() AND status = 1')->row();
				echo json_encode($value->result);
			break;

			case 'getProp':				
				$prop['select']	= "*";
				$prop['table']	= "m_provinsi";
				$prop['order']	= array('NAMA_PROP','ASC');
	
				$value 	= $this->model_global->getData($prop);
				foreach($value as $val){
					$response[] = array(
					   "id" => $val->NO_PROP,
					   "text" => $val->NAMA_PROP
					);
				}
				
				return ($response);
				// exit();
			break;

			case 'getKab':				
				$prop['select']	= "*";
				$prop['table']	= "m_kabupaten_kota";
				$prop['where']	= "NO_PROP = '".$_POST['kode_prop']."'";
				$prop['order']	= array('NAMA_KAB','ASC');
	
				$value 	= $this->model_global->getData($prop);
				foreach($value as $val){
					$response[] = array(
					   "id" => $val->NO_KAB,
					   "text" => $val->NAMA_KAB
					);
				}
				
				echo json_encode($response);
				// exit();
			break;
			
			case 'dtUser':
				// var_dump($_POST);die;
				$list = $this->model_global->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_user'];
					$row[] = $l['email'];
					$row[] = ($l['role'] == 1)?"Admin":"Operator";

					if ($_GET['type'] == 'user') {
						$row[] = '<button class="btn btn-warning" type="button" title="Ubah Data User" onclick="edit('.$l['id_user'].');"><i class="fas fa-pen"></i></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`user`,'.$l['id_user'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data User"><i class="fas fa-trash"></i></button></a>';
					} else {
							$row[] = '<a href="'.base_url('admin/user/restore').'/'.$l['id_user'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data User"><i class="fas fa-recycle"></i></button></a>';
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_global->count_all($_GET['type']),
					"recordsFiltered" => $this->model_global->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;
		
			case 'dtBerita':
					// var_dump($_POST);die;
					$list = $this->model_global->get_datatables($_GET['type']);
					// var_dump($list);
					// die();
					$no = $_POST['start'];
					$data = array();
					foreach ($list as $l) {
						$no++;
						$row = array();
						$row[] = $no;
						$row[] = $l['judul_berita'];
						$row[] = $l['tgl_publish'];
						$row[] = '<div class="w-100"><img class="img-thumbnail" src="'.base_url('assets/uploads/berita/thumb/'.@$l['gambar']).'" title="'.$l['judul_berita'].'" alt="'.$l['judul_berita'].'" srcset="" style="max-width: 15vw;"></div>';
	
						if ($_GET['type'] == 'berita') {
							$row[] = '<button class="btn btn-warning" type="button" title="Ubah Data Berita Menu" onclick="edit('.$l['id_berita'].');"><i class="fas fa-pen"></i></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`berita`,'.$l['id_berita'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Berita Menu"><i class="fas fa-trash"></i></button></a>';
						} else {
								$row[] = '<a href="'.base_url('admin/berita/restore').'/'.$l['id_berita'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Berita Menu"><i class="fas fa-recycle"></i></button></a>';
						}
	
						// $date = new DateTime($l['created_at']);
						// $date->modify("+7 hours");
						// $row[] = $date->format("d M Y H:i:s");
	
						$data[] = $row;
					}
	
					$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_global->count_all($_GET['type']),
						"recordsFiltered" => $this->model_global->count_filtered($_GET['type']),
						"data" => $data,
					);
					echo json_encode($output);
					break;
	
			case 'dtKategori':
				// var_dump($_POST);die;
				$list = $this->model_global->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_kategori_menu'];

					if ($_GET['type'] == 'kategori') {
						$row[] = '<button class="btn btn-warning" type="button" title="Ubah Data Kategori Menu" onclick="edit('.$l['id_kategori_menu'].');"><i class="fas fa-pen"></i></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`kategori`,'.$l['id_kategori_menu'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Kategori Menu"><i class="fas fa-trash"></i></button></a>';
					} else {
							$row[] = '<a href="'.base_url('admin/kategori/restore').'/'.$l['id_kategori_menu'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Kategori Menu"><i class="fas fa-recycle"></i></button></a>';
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_global->count_all($_GET['type']),
					"recordsFiltered" => $this->model_global->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtMenu':
				// var_dump($_POST);die;
				$kategori_detail['select']	= "id_kategori_menu, nama_kategori_menu";
				$kategori_detail['table']	= "m_kategori_menu as k";
				$kategori_detail['where']	= "k.status = 1";

				$data['kategori_detail'] 	= $this->model_global->getData($kategori_detail);
				// var_dump($data);
				// die;
				$kategori = array();
				foreach ($data['kategori_detail'] as $val) {
					$kategori[$val->id_kategori_menu] = $val->nama_kategori_menu;
				}
				$list = $this->model_global->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$arrKategori = explode(",",$l['id_kategori_menu']);
					$strKategori = "";
					foreach ($arrKategori as $val) {
						if (isset($kategori[$val])) {
							$strKategori .= '<span class="badge badge-pill bg-primary" title="'.$kategori[$val].'">'.$kategori[$val].'</span> ';
						}
					}
					$no++;
					if ($strKategori !== "") {
						$row = array();
						$row[] = $no;
						$row[] = $l['nama_menu'];
						$row[] = $strKategori;
						$row[] = '<div class="w-100"><img class="img-thumbnail" src="'.base_url('assets/uploads/menu/thumb/'.@$l['gambar']).'" title="'.$l['nama_menu'].'" alt="'.$l['nama_menu'].'" srcset="" style="max-width: 15vw;"></div>';
						
						if ($_GET['type'] == 'menu') {
							$row[] = '<button class="btn btn-warning" type="button" title="Ubah Data Menu Minuman" onclick="edit('.$l['id_menu'].');"><i class="fas fa-pen"></i></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`menu`,'.$l['id_menu'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Menu Minuman"><i class="fas fa-trash"></i></button></a>';
						} else {
								$row[] = '<a href="'.base_url('admin/menu/restore').'/'.$l['id_menu'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Menu Minuman"><i class="fas fa-recycle"></i></button></a>';
						}
						$data[] = $row;
					}
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_global->count_all($_GET['type']),
					"recordsFiltered" => $this->model_global->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;

			case 'dtMitra':
				$list = $this->model_global->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_mitra'];
					$row[] = '<a target="_blank" href="mailto:'.$l['email'].'">'.$l['email'].'</a>';
					$row[] = '<a target="_blank" href="tel:'.$l['no_tlp'].'">'.$l['no_tlp'].'</a>';
					$row[] = $l['alamat_mitra'];
					$row[] = ($l['follow_up'] == 1)?'<span class="badge badge-success">Sudah</span>':'<span class="badge badge-danger">Belum</span>';

					$verifikasi = (is_null($l['id_outlet']))?'<a href="'.base_url('admin/mitra/active').'/'.$l['id_mitra'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Data Mitra Ini???`)" ><button class="btn btn-success" type="button" title="Verifikasi Data Calon Mitra"><i class="fas fa-check"></i></button></a>':'';

					
					if ($_GET['type'] == 'mitra') {
						$row[] = $verifikasi.' '.'<button class="btn btn-warning" type="button" title="Detail Data Calon Mitra" onclick="edit('.$l['id_mitra'].');"><i class="fas fa-eye"></i></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`mitra`,'.$l['id_mitra'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Calon Mitra"><i class="fas fa-trash"></i></button></a>';
					} else {
						$row[] = '<a href="'.base_url('admin/mitra/restore').'/'.$l['id_mitra'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Calon Mitra"><i class="fas fa-recycle"></i></button></a>';
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_global->count_all($_GET['type']),
					"recordsFiltered" => $this->model_global->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;
			
			case 'dtOutlet':
				$list = $this->model_global->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama_outlet'];
					$row[] = (empty($l['kode_outlet']))?((!is_null($l['kode_prop']))?$l['kode_prop'].substr("00000".$l['nomor_outlet'],-5):"-"):$l['kode_outlet'];
					// $row[] = '<div class="w-100"><img class="img-thumbnail" src="'.base_url('assets/uploads/outlet/thumb/'.@$l['gambar']).'" title="'.$l['nama_outlet'].'" alt="'.$l['nama_outlet'].'" srcset="" style="max-width: 15vw;"></div>';

					$row[] = $l['alamat_mitra'];
					
					if ($_GET['type'] == 'outlet') {
						$row[] = '<button class="btn btn-warning" type="button" title="Ubah Data Mitra" onclick="edit('.$l['id_outlet'].');"><i class="fas fa-pen"></i></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`outlet`,'.$l['id_outlet'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Mitra"><i class="fas fa-trash"></i></button></a>';
					} else {
						$row[] = '<a href="'.base_url('admin/outlet/restore').'/'.$l['id_outlet'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Mitra"><i class="fas fa-recycle"></i></button></a>';
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_global->count_all($_GET['type']),
					"recordsFiltered" => $this->model_global->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;
			
			case 'dtKontak':
				$list = $this->model_global->get_datatables($_GET['type']);
				// var_dump($list);
				// die();
				$no = $_POST['start'];
				$data = array();
				foreach ($list as $l) {
					$no++;
					$row = array();
					$row[] = $no;
					$row[] = $l['nama'];
					$row[] = '<a target="_blank" href="mailto:'.$l['email'].'">'.$l['email'].'</a>';
					$row[] = '<a target="_blank" href="tel:'.$l['no_tlp'].'">'.$l['no_tlp'].'</a>';
					$row[] = $l['pesan'];
					
					if ($_GET['type'] == 'kontak') {
						$row[] = '<button class="btn btn-primary" type="button" title="Respon Data Kontak" onclick="edit('.$l['id_kontak'].');"><i class="fas fa-reply"></i></button>'.''.'<a href="javascript:void(0)" onclick="if(confirm(`Apa Anda Yakin Untuk Menghapus Data Ini???`)) delData(`kontak`,'.$l['id_kontak'].')" > <button class="btn btn-danger" type="button" title="Nonaktifkan Data Kontak"><i class="fas fa-trash"></i></button></a>';
					} else {
							$row[] = '<a href="'.base_url('admin/kontak/restore').'/'.$l['id_kontak'].'" onclick="return confirm(`Apa Anda Yakin Untuk Mengaktifkan Kembali Data Ini???`)" ><button class="btn btn-success" type="button" title="Aktifkan Data Kontak"><i class="fas fa-recycle"></i></button></a>';
					}

					// $date = new DateTime($l['created_at']);
					// $date->modify("+7 hours");
					// $row[] = $date->format("d M Y H:i:s");

					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->model_global->count_all($_GET['type']),
					"recordsFiltered" => $this->model_global->count_filtered($_GET['type']),
					"data" => $data,
				);
				echo json_encode($output);
				break;
			
			default:
				# code...
				break;
		}
	}

	public function inject() {
		echo "<span>start time : ".microtime(true)."</span><br>";
		$data = array();
		$data['limit'] = 1000;
		$query = $this->db->query('SELECT id_user, nama_user, email, kode_uniqlo, kelas FROM m_user AS p WHERE p.kelas LIKE "%26-07:15-yoga%" OR p.kelas LIKE "%20-08:30-yoga%" OR p.kelas LIKE "%20-16:00-strong%" LIMIT '.$data['limit']);
		$data['num_rows'] = $query->num_rows();
		$data['query'] = $query->result();
		foreach ($data['query'] as $val) {
			// var_dump($val);
			$val->kelas = str_replace('26-07:15-yoga','26-07:15-strong',$val->kelas);
			$val->kelas = str_replace('20-08:30-yoga','21-08:30-yoga',$val->kelas);
			$val->kelas = str_replace('20-16:00-strong','21-16:00-strong',$val->kelas);

			$this->db->set('kelas', $val->kelas);
			$this->db->where('id_user', $val->id_user);
			$this->db->update('m_user'); 
			
			// break;
		}
		// var_dump($data);
		echo "<span>end time : ".microtime(true)."</span><br>";
		$this->load->view('admin/inject',$data,false);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $setting;
    public function __construct() {
        parent::__construct();
        $this->load->model('model_setting');

		$setting = $this->model_setting->getListSetting();
		$arrSetting = array();
		foreach ($setting as $key => $val) {
			$arrSetting[$val->name_setting] = $val->value;
		}
		$this->setting = (object) $arrSetting;
	}
	
	public function index() {
		$data = array();
		$data['page'] = "index";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('index',$data,false);
	}

	public function pelajari() {
		$data = array();
		$data['page'] = "pelajari";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('pelajari',$data,false);
	}

	public function pelajari2() {
		$data = array();
		$data['page'] = "pelajari2";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('pelajari2',$data,false);
	}

	public function syarat() {
		$data = array();
		$data['page'] = "syarat";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('syarat',$data,false);
	}

	public function biaya() {
		$data = array();
		$data['page'] = "biaya";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('biaya',$data,false);
	}

	public function bergabung() {
		$data = array();
		$data['page'] = "bergabung";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('bergabung',$data,false);
	}
	
	public function berita($page=1) {
		$data = array();
		$data['page'] = "berita";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;

		$berita['select'] = "*";
		$berita['from'] = "t_berita";
		$berita['where'] = "status = 1 and tgl_publish < '".date('Y-m-d H:i:s')."'";
		$berita['order'] = array('tgl_publish','desc');

		$jumlah_data = $this->model_global->getDataRows($berita);

        $this->load->helper('pagging_helper');
		$configPaging['base_url'] = base_url().'berita/';
		$configPaging['total_rows'] = $jumlah_data;
		$configPaging['per_page'] = 5;
		$configPaging['num_links'] = 2;
		$from = ($page * $configPaging['per_page']) - $configPaging['per_page'];
		$data['pagingView'] = create_pagging($configPaging);
		// $from = $this->uri->segment(3);
		$berita['limit'] = array($configPaging['per_page'],$from);
		$data['berita'] = $this->model_global->getData($berita);

		$this->load->view('berita',$data,false);
	}

	public function detailberita($link='') {				
		$data = array();
		if ($link !== '') {
			// var_dump($link,"<hr>");
			// die;
			// get id_berita from link_berita
			$link_berita['select'] = "id_link_berita, id_berita, hit";
			$link_berita['from'] = "t_link_berita";
			$link_berita['where'] = "status = 1 and link_berita like '%".$link."%'";
			$link_berita['limit'] = "1";

			$data['link_berita'] = $this->model_global->getData($link_berita);
			if (!empty($data)) {
				$data['page'] = "berita";
				$data['setting'] = $this->setting;
				// var_dump($data,"<hr>");
				// get detail berita
				$detail_berita['select'] = "*";
				$detail_berita['from'] = "t_berita";
				$detail_berita['where'] = "status = 1 and id_berita = ".$data['link_berita'][0]->id_berita;
				$detail_berita['limit'] = "1";

				$data['detail_berita'] = $this->model_global->getData($detail_berita);

				// update hit link_berita
				$data_link_berita['data']['hit'] = $data['link_berita'][0]->hit+1;
				$data_link_berita['table']		= "t_link_berita";
				$data_link_berita['where'][0] 	= array('id_link_berita', $data['link_berita'][0]->id_link_berita);
				
				$this->model_global->updateData($data_link_berita);
				// die;
				$this->load->view('detailberita',$data,false);
			} else {
				redirect('berita');
			}			
		} else {
			redirect('berita');
		}		
	}
	
	public function menu() {
		$data = array();
		$data['page'] = "menu";
		$data['setting'] = $this->setting;

		$kategori['select'] = "id_kategori_menu as id, nama_kategori_menu as nama, gambar, deskripsi";
		$kategori['from'] = "m_kategori_menu";
		$kategori['where'] = "status = 1";
		$data['kategori'] = $this->model_global->getData($kategori);
		// var_dump($data);
		// die;
		$this->load->view('menu',$data,false);
	}
	
	public function booth() {
		$data = array();
		$data['page'] = "booth";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('booth',$data,false);
	}
	
	public function outlet() {
		$data = array();
		$data['page'] = "outlet";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('outlet',$data,false);
	}
	
	public function franchise() {
		if (isset($_POST['submit'])) {
			// var_dump($_POST);
			// die;
			$mitra['data']['nama_mitra'] = $_POST['nama_mitra'];
			$mitra['data']['email'] = $_POST['email'];
			$mitra['data']['no_tlp'] = $_POST['no_tlp'];
			$mitra['data']['alamat_mitra'] = $_POST['alamat_mitra'];
			$mitra['data']['created_at'] = date('Y-m-d H:i:s');
			$mitra['data']['status'] = 1;
			// var_dump($berita);
			// die();
			$mitra['table']		= "t_mitra";
			// $json['id_berita'] 	= $this->model_global->addData($data);
			// redirect('admin/berita');
			if ($this->model_global->addData($mitra)) {
				$this->session->set_flashdata('pesan', 'Anda berhasil mendaftar, admin kami akan segera menghubungi anda untuk proses berikutnya.');
			}
			redirect('franchise');
		} else {
			$data = array();
			$data['page'] = "franchise";
			$data['setting'] = $this->setting;
			// var_dump($data);
			// die;
			$this->load->view('franchise',$data,false);
		}		
	}
	
	public function penghargaan() {
		$data = array();
		$data['page'] = "penghargaan";
		$data['setting'] = $this->setting;
		// var_dump($data);
		// die;
		$this->load->view('penghargaan',$data,false);
	}
	
	public function kontak() {
		if (isset($_POST['submit'])) {
			// var_dump($_POST);
			// die;
			$mitra['data']['nama'] = $_POST['nama'];
			$mitra['data']['email'] = $_POST['email'];
			$mitra['data']['no_tlp'] = $_POST['no_tlp'];
			$mitra['data']['pesan'] = $_POST['pesan'];
			$mitra['data']['created_at'] = date('Y-m-d H:i:s');
			$mitra['data']['status'] = 1;
			// var_dump($berita);
			// die();
			$mitra['table']		= "t_kontak";
			// $json['id_berita'] 	= $this->model_global->addData($data);
			// redirect('admin/berita');
			if ($this->model_global->addData($mitra)) {
				$this->session->set_flashdata('pesan', 'Pesan anda telah terkirim, admin kami akan segera menghubungi anda untuk proses berikutnya.');
			}
			redirect('kontak');
		} else {
			$data = array();
			$data['page'] = "kontak";
			$data['setting'] = $this->setting;
			// var_dump($data);
			// die;
			$this->load->view('kontak',$data,false);
		}		
	}

	public function submitForm() {
		// var_dump($_POST);
		// die;
		$data_peserta = array(
            'nama_peserta' => $_POST['nama'],
            'email' => $_POST['email'],
            'kode_uniqlo' => $_POST['kode_uniqlo'],
            'kelas' => implode(", ",$_POST['kelas']).',',
            'created_at' => date('Y-m-d H:i:s'),
            'status' => '1',
        );
		// var_dump($data_peserta);
		// die;
        $simpan_jawaban_kuis = $this->model_peserta->save('m_peserta',$data_peserta);

        if ($simpan_jawaban_kuis) {
			$this->session->set_flashdata('msg_flash', 'Anda berhasil terdaftar untuk mengikuti kegiatan Uniqlo Fit Fest.');
            redirect('/');
        }
	}

	public function rest_api($type,$id=null) {
		$result = "";
		switch ($type) {
			case 'menu':
				$menu['select'] = "m.id_menu as id, m.nama_menu as nama, m.gambar, m.deskripsi, m.isbest, k.id_kategori_menu, k.nama_kategori_menu";
				$menu['from'] = "m_menu as m";
				$menu['join'][0] = array('m_kategori_menu as k',"m.id_kategori_menu LIKE CONCAT('%', k.id_kategori_menu, '%')");
				$menu['where'] = "m.status = 1 AND k.status = 1 AND m.id_kategori_menu like '%".$_GET['id_kategori']."%' AND k.id_kategori_menu = ".$_GET['id_kategori'];
				$result = $this->model_global->getData($menu);
				break;
			case 'outlet':
				// $outlet['select'] = "id_mitra as id, nama_mitra as nama, email, no_tlp, alamat_mitra, gambar";
				// $outlet['from'] = "t_mitra";
				// $outlet['where'] = "status = 1 AND kode_prop = ".$_POST['no_prop'];
				$outlet['select'] = "o.kode_outlet, o.nama_outlet, o.kode_prop, o.nomor_outlet, o.gambar, o.link_maps, m.alamat_mitra, k.kode, k.nama_prop";
				$outlet['from'] = "t_outlet as o";
				$outlet['join'][0] = array('t_mitra as m','m.id_mitra = o.id_mitra');
				$outlet['join'][1] = array('m_provinsi as k','k.kode = o.kode_prop');
				$outlet['where'] = "o.status = 1 AND o.kode_prop like '%".$_GET['no_prop']."%'";
				// $outlet['join'] = "t_mitra "
				$result = $this->model_global->getData($outlet);
				break;
			default:
				# code...
				break;
		}
		echo json_encode($result);
	}
}
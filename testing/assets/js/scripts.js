/*!
    * Start Bootstrap - Freelancer v6.0.5 (https://startbootstrap.com/theme/freelancer)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-freelancer/blob/master/LICENSE)
    */
   
(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 71)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 80
  });

  // Collapse Navbar
  // var navbarCollapse = function() {
  //   if ($("#mainNav").offset().top > 100) {
  //     $("#mainNav").addClass("navbar-shrink");
  //   } else {
  //     $("#mainNav").removeClass("navbar-shrink");
  //   }
  // };
  // Collapse now if page is not at top
  // navbarCollapse();
  // Collapse the navbar when page is scrolled
  // $(window).scroll(navbarCollapse);

  // Floating label headings for the contact form
  $(function() {
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
      $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
      $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
      $(this).removeClass("floating-label-form-group-with-focus");
    });
  });
  spin();
  $(document).ajaxSend(function() {
      // setTimeout(() => {
      //     console.log("spin show");
          // $("#cover-spin").show();
      // }, 0.5 * 1000);
      spin();　
  });

  $(document).ajaxStop(function() {
      // setTimeout(() => {
          // console.log("spin hide");
          // $("#cover-spin").hide();
      // }, 2 * 1000);
      spin();　
  });
  setInterval(() => {
  // setTimeout(() => {
      $("#cover-spin").hide();
      // spin();
  }, 3 * 1000);
})(jQuery); // End of use strict
function spin() {
    let x = document.getElementById("cover-spin");
    if (x.style.display === "none") {
        console.log("masuk spin");
        setTimeout(() => {
            x.style.display = "block";
        }, 0.5 * 1000);
    } else {
        console.log("masuk spin hide");
        setTimeout(() => {
            x.style.display = "none";
        }, 1 * 1000);
    }
}
function genPass(source,target) {
    let length = 10;
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()[]{}<>-+?|\/=_~:;';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    // return result;
    $("#password").val(result);
    $("#repassword").val(result);
    alert("Copied this password in a safe place! : "+result);
}
function delData(type,id) {
    // console.log(type,id)
    $.ajax({ url : type+"/hapus",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        async: false,        
        beforeSend: function(){
            // $("#cover-spin").show();
        },
        success: function(data){
            // swal('Data '+type+' berhasil dihapus','success');
            document.location = data.url;
        },
        error: function (jqXHR, textStatus, errorThrown){
            swal('Gagal memproses data dari ajax','error');
        }
    }).done(function() {
        setTimeout(function(){
            // $("#cover-spin").hide();
            // spin();
        },500);
    });
}
function readURL(input,target="#prevImg",label="#labelImg") {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
          $(target).attr('src', e.target.result);
      }
      // console.log(input.files[0].name);
      $(label).html(input.files[0].name)
      reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function newSWAL(msg,type) { 
  Toast.fire({
    icon: type,
    title: msg
  })
}
  